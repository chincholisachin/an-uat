public class ProcessPath{
public List<String> options{get; set;}
public string opts {get; set;}
public string processpathRecordtype;
public string currentStatus;
public boolean displayGuidancetext{get;set;}
public string guidancetext{get;set;}
public id agrId; 

public Apttus__APTS_Agreement__c agreement {get; set;}

    public ProcessPath(ApexPages.StandardController controller) {
        agrId = System.currentPageReference().getParameters().get('Id');
                       //agrId='a5F160000000N0BEAU'; //for testing
        try {
                agreement = [Select Apttus__Status__c, Apttus__Status_Category__c, RecordTypeId from Apttus__APTS_Agreement__c
                                 where id = :agrId];
                
                string recordtypename = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosById().get(agreement.recordtypeid).getname();
                System.debug(recordtypename);
                
                If ( recordtypename == 'MSA' || recordtypename == 'NDA' || recordtypename == 'MHA' || recordtypename == 'AppNexus Exchange Terms of Service') {
                    processpathRecordtype = recordtypename;
                } else {
                    processpathRecordtype = 'Others';
                }
                
                options = new List<String>();       
                //List To populate the process path tab 
                List<ProcessPath__c> processPath = [Select Step_Name__c from processPath__c where IsActive__c =true and ObjectRecordtype__c =: processpathRecordtype order by Order_Number__c];
                    
                System.debug('Process Path *****' + processPath);
                 if (processPath.size() > 0) {     
                 //Adding the List Values into An Array           
                for(ProcessPath__c p: processPath){
                 System.debug('Process Path StepName::' + p.Step_Name__c); 
                   options.add(p.Step_Name__c);
                 }     
                //Stringifying The Array and Assigning It To A String 
                opts = JSON.serialize(options);    
                getCurrentStatus();
                getGuidanceText();
                } 
            } catch (Exception e) { 
               System.debug(e.getMessage());
               ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Agreement Process flow Path could not be displayed. Please Contact System Administrator.'));
            }
    
    }
    public String getCurrentStatus() { 
                
                String cs = null;
                List<ProcessPath__c> currentProcess =  [Select Step_Name__c from processPath__c where IsActive__c =true and ObjectRecordtype__c =: processpathRecordtype and Status__c =: agreement.Apttus__Status__c and Status_Category__c=: agreement.Apttus__Status_Category__c and ObjectName__c= 'Agreement' order by Order_Number__c];  
                if (currentProcess.size() > 0) {
                cs = currentProcess[0].Step_Name__c;
                System.debug('Step Name in getter*****' + cs);
                
                }
                else {
                opts = null;
                }
                return cs;
    }
    
    public pageReference getGuidanceText() { 
                
                //String gt = null;
                List<ProcessPath__c> currentProcess =  [Select Guidance_Text__c from processPath__c where IsActive__c =true and ObjectRecordtype__c =: processpathRecordtype and Status__c =: agreement.Apttus__Status__c and Status_Category__c=: agreement.Apttus__Status_Category__c and ObjectName__c= 'Agreement' order by Order_Number__c];  
                if (currentProcess.size() > 0) {
                guidancetext = currentProcess[0].Guidance_Text__c;
                displayGuidancetext = true;
                System.debug('Guidance text*****' + guidancetext);
                } else {
                displayGuidancetext = false;
                }
        return null;
    }
    
     
    
    
}