@isTest(seeAllData=false)
private class AgreementServiceTest {
    private static Apttus__APTS_Agreement__c creatAgreement;
    private static Account creatAccount;
    
    public static void loadData(){
        Account_Record_Type__c Ar = InitializeTestData.CreateAccountCustomSetting('Customer Account',false,false,false);
        insert ar;
        
        creatAccount = InitializeTestData.createAccount();
        creatAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        
        insert creatAccount;
        
        creatAgreement = InitializeTestData.createParentAgreement(creatAccount.Id);
        insert creatAgreement;
    }
    
    private static testMethod void agreemethod() {
        loadData();
        Test.StartTest();
            Set<Id> agreementsSet = new Set<Id>();
            List<Apttus__APTS_Agreement__c> aggrementsList = new List<Apttus__APTS_Agreement__c>();
            aggrementsList.add(creatAgreement);
            agreementsSet.add(creatAgreement.Id);
            try {
            // AgreementService agg = new AgreementService();
            AgreementService.getAgreementsByFilteredIDs(agreementsSet);
            AgreementService.updateAgreements(aggrementsList);
            AgreementService.getAgreementsByAccount(aggrementsList[0].Id,'Request');
           
        } catch(DMLException  e) {
        }
        Test.StopTest();
        
    }
     private static testMethod void agreemethod1() {
        loadData();
        Test.StartTest();
            Set<Id> agreementsSet = new Set<Id>();
            List<Apttus__APTS_Agreement__c> aggrementsList = new List<Apttus__APTS_Agreement__c>();
            aggrementsList.add(creatAgreement);
            agreementsSet.add(creatAgreement.Id);
            try {
            // AgreementService agg = new AgreementService();
            AgreementService.getAgreementsByFilteredIDs(null);
            AgreementService.updateAgreements(null);
          
           
        } catch(DMLException  e) {
        }
        Test.StopTest();
        
    }
}