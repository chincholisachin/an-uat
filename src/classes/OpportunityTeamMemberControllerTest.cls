@isTest
private class OpportunityTeamMemberControllerTest {

    private static OpportunityTeamMemberController controller;
    private static Opportunity oppy;
    
    private static testMethod void deletionOfOwner(){
        
        User commercial = TestingUtils.getUser('* Commercial');
 
        Opportunity[] opps = OpportunityTriggerTest.prepareOppData();
        for (Opportunity o: opps){
            o.ownerid = commercial.id;
        }
        insert opps;            

        System.runAs ( commercial ){
            
            Opportunity op = [select id, name from opportunity where ownerid =: commercial.id limit 1];
            Test.startTest();
            PageReference pageRef = new PageReference('/apex/OpportunityTeamMember');
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(op);
            controller = new OpportunityTeamMemberController(sc);
            List<SelectOption> opportunityTeamRoles = controller.opportunityTeamRoles;
            System.currentPageReference().getParameters().put('rowIndex', '0');
            controller.deleteRow();
            controller.ave();
            Test.stopTest();
            //deletion of only opp owner should replace it with the owner. 
            System.assertEquals(1,[select id from opportunityteammember where opportunityid =: op.id].size());
            
        }   
    }

    private static testMethod void addANewOwner(){
    
        User commercial = TestingUtils.getUser('* Commercial');
        User implementationC = TestingUtils.getUser('* Implementation Consultant');
 
        Opportunity[] opps = OpportunityTriggerTest.prepareOppData();
        for (Opportunity o: opps){
            o.ownerid = commercial.id;
        }
        insert opps;            

        System.runAs ( commercial ){
            
            Opportunity op = [select id, name from opportunity where ownerid =: commercial.id limit 1];
            Test.startTest();
            PageReference pageRef = new PageReference('/apex/OpportunityTeamMember');
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(op);
            controller = new OpportunityTeamMemberController(sc);
            List<SelectOption> opportunityTeamRoles = controller.opportunityTeamRoles;
            System.currentPageReference().getParameters().put('rowIndex', '0');
            controller.deleteRow();
            controller.addRow();        
            for (OpportunityTeamMember otm: controller.OTMList){
                if (otm.userid == null) {
                    otm.userid = implementationC.id;
                    otm.teammemberrole = 'Opportunity Owner';
                }
            }

            pageRef = controller.ave();
            Test.stopTest();
        }

        System.assertEquals(1,[select id from opportunityteammember where userid = :implementationC.id and teammemberrole = 'Opportunity Owner'].size());
        System.assertEquals(199,[select id from opportunityteammember where userid = :commercial.id and teammemberrole = 'Opportunity Owner'].size());
        System.assertEquals(1,[select ownerid from opportunity where ownerid = :implementationC.id].size());   
    
    }
    
    private static testMethod void addAnotherRole(){
    
        User commercial = TestingUtils.getUser('* Commercial');
        User implementationC = TestingUtils.getUser('* Implementation Consultant');
 
        Opportunity[] opps = OpportunityTriggerTest.prepareOppData();
        for (Opportunity o: opps){
            o.ownerid = commercial.id;
        }
        insert opps;            

        System.runAs ( commercial ){
            
            Opportunity op = [select id, name from opportunity where ownerid =: commercial.id limit 1];
            Test.startTest();
            PageReference pageRef = new PageReference('/apex/OpportunityTeamMember');
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(op);
            controller = new OpportunityTeamMemberController(sc);
            List<SelectOption> opportunityTeamRoles = controller.opportunityTeamRoles;
            controller.addRow();        
            for (OpportunityTeamMember otm: controller.OTMList){
                if (otm.userid == null) {
                    otm.userid = implementationC.id;
                    otm.teammemberrole = 'Senior Opportunity Owner';
                }
            }
            controller.ave();
            Test.stopTest();       
            System.assertEquals(2,[select id from opportunityteammember where opportunityid =: op.id].size());
            
        }   
    
    }
    
    private static testMethod void addADuplicateRole(){
    
        User commercial = TestingUtils.getUser('* Commercial');
        User implementationC = TestingUtils.getUser('* Implementation Consultant');
 
        Opportunity[] opps = OpportunityTriggerTest.prepareOppData();
        for (Opportunity o: opps){
            o.ownerid = commercial.id;
        }
        insert opps;            

        System.runAs ( commercial ){
            
            Opportunity op = [select id, name from opportunity where ownerid =: commercial.id limit 1];
            Test.startTest();
            PageReference pageRef = new PageReference('/apex/OpportunityTeamMember');
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(op);
            controller = new OpportunityTeamMemberController(sc);
            List<SelectOption> opportunityTeamRoles = controller.opportunityTeamRoles;
            controller.addRow();        
            for (OpportunityTeamMember otm: controller.OTMList){
                if (otm.userid == null) {
                    otm.userid = implementationC.id;
                    otm.teammemberrole = 'Opportunity Owner';
                }
            }
            controller.ave();
            Test.stopTest();
            System.assertEquals(1,[select id from opportunityteammember where opportunityid =: op.id].size());
            System.assertEquals(0,[select id from opportunityteammember where userid =: implementationC.id].size());
            
        }   
    
    }
    
}