public  class CheckRecursion {

    //-- Case --//
    private static boolean Run_CaseBeforeInsert = true;
    private static boolean Run_CaseAfterInsert = true;
    private static boolean Run_CaseBeforeUpdate = true;
    private static boolean Run_CaseAfterUpdate = true;
    
    

    public static boolean checkCaseRecursion(String triggerName){
        if(triggerName == CONSTANTS.BEFORE_INSERT){
            if(Run_CaseBeforeInsert){
                Run_CaseBeforeInsert = false;
                return true;
            }else{
                return Run_CaseBeforeInsert;
            }
        }else if(triggerName == CONSTANTS.AFTER_INSERT){
            if(Run_CaseAfterInsert){
                Run_CaseAfterInsert = false;
                return true;
            }else{
                return Run_CaseAfterInsert;
            }
        }else if(triggerName == CONSTANTS.BEFORE_UPDATE){
            if(Run_CaseBeforeUpdate){
                Run_CaseBeforeUpdate = false;
                return true;
            }else{
                return Run_CaseBeforeUpdate;
            }
        }else if(triggerName == CONSTANTS.AFTER_UPDATE){
            if(Run_CaseAfterUpdate){
                Run_CaseAfterUpdate = false;
                return true;
            }else{
                return Run_CaseAfterUpdate;
            }
        }
        system.debug('--Not letting the Case Triggers to Run--');
        return false;
    }

    //-- Case Comment --//
    private static boolean Run_CaseCommentBeforeInsert = true;
    private static boolean Run_CaseCommentAfterInsert = true;
    private static boolean Run_CaseCommentBeforeUpdate = true;
    private static boolean Run_CaseCommentAfterUpdate = true;

    public static boolean checkCaseCommentRecursion(String triggerName){
        if(triggerName == CONSTANTS.BEFORE_INSERT){
            if(Run_CaseCommentBeforeInsert){
                Run_CaseCommentBeforeInsert = false;
                return true;
            }else{
                return Run_CaseCommentBeforeInsert;
            }
        }else if(triggerName == CONSTANTS.AFTER_INSERT){
            if(Run_CaseCommentAfterInsert){
                Run_CaseCommentAfterInsert = false;
                return true;
            }else{
                return Run_CaseCommentAfterInsert;
            }
        }else if(triggerName == CONSTANTS.BEFORE_UPDATE){
            if(Run_CaseCommentBeforeUpdate){
                Run_CaseCommentBeforeUpdate = false;
                return true;
            }else{
                return Run_CaseCommentBeforeUpdate;
            }
        }else if(triggerName == CONSTANTS.AFTER_UPDATE){
            if(Run_CaseCommentAfterUpdate){
                Run_CaseCommentAfterUpdate = false;
                return true;
            }else{
                return Run_CaseCommentAfterUpdate;
            }
        }
        system.debug('--Not letting the Case Comment Triggers to Run--');
        return false;
    }
    
    //-- LegalRequest --//
    private static boolean Run_LRBeforeInsert = true;
    private static boolean Run_LRAfterInsert = true;
    private static boolean Run_LRBeforeUpdate = true;
    private static boolean Run_LRAfterUpdate = true;

    public static boolean checkLRRecursion(String triggerName){
        if(triggerName == CONSTANTS.BEFORE_INSERT){
            if(Run_LRBeforeInsert){
                Run_LRBeforeInsert = false;
                return true;
            }else{
                return Run_LRBeforeInsert;
            }
        }else if(triggerName == CONSTANTS.AFTER_INSERT){
            if(Run_LRAfterInsert){
                Run_LRAfterInsert = false;
                return true;
            }else{
                return Run_LRAfterInsert;
            }
        }else if(triggerName == CONSTANTS.BEFORE_UPDATE){
            if(Run_LRBeforeUpdate){
                Run_LRBeforeUpdate = false;
                return true;
            }else{
                return Run_LRBeforeUpdate;
            }
        }else if(triggerName == CONSTANTS.AFTER_UPDATE){
            if(Run_LRAfterUpdate){
                Run_LRAfterUpdate = false;
                return true;
            }else{
                return Run_LRAfterUpdate;
            }
        }
        system.debug('--Not letting the Legal Request Triggers to Run--');
        return false;
    }

    //-- Email Message --//
    private static boolean Run_EmailMessageBeforeInsert = true;
    private static boolean Run_EmailMessageAfterInsert = true;
    private static boolean Run_EmailMessageBeforeUpdate = true;
    private static boolean Run_EmailMessageAfterUpdate = true;

    public static boolean checkEmailMessageRecursion(String triggerName){
        if(triggerName == CONSTANTS.BEFORE_INSERT){
            if(Run_EmailMessageBeforeInsert){
                Run_EmailMessageBeforeInsert = false;
                return true;
            }else{
                return Run_EmailMessageBeforeInsert;
            }
        }else if(triggerName == CONSTANTS.AFTER_INSERT){
            if(Run_EmailMessageAfterInsert){
                Run_EmailMessageAfterInsert = false;
                return true;
            }else{
                return Run_EmailMessageAfterInsert;
            }
        }else if(triggerName == CONSTANTS.BEFORE_UPDATE){
            if(Run_EmailMessageBeforeUpdate){
                Run_EmailMessageBeforeUpdate = false;
                return true;
            }else{
                return Run_EmailMessageBeforeUpdate;
            }
        }else if(triggerName == CONSTANTS.AFTER_UPDATE){
            if(Run_EmailMessageAfterUpdate){
                Run_EmailMessageAfterUpdate = false;
                return true;
            }else{
                return Run_EmailMessageAfterUpdate;
            }
        }
        system.debug('--Not letting the Case Comment Triggers to Run--');
        return false;
    }

    //-- CaseStatusTracker --//
    private static boolean Run_CaseStatusTrackerBeforeInsert = true;
    private static boolean Run_CaseStatusTrackerAfterInsert = true;
    public static boolean Run_CaseStatusTrackerBeforeUpdate = true;
    private static boolean Run_CaseStatusTrackerAfterUpdate = true;

    public static boolean checkCaseStatusTrackerRecursion(String triggerName){
        if(triggerName == CONSTANTS.BEFORE_INSERT){
            if(Run_CaseStatusTrackerBeforeInsert){
                Run_CaseStatusTrackerBeforeInsert = false;
                return true;
            }else{
                return Run_CaseStatusTrackerBeforeInsert;
            }
        }else if(triggerName == CONSTANTS.AFTER_INSERT){
            if(Run_CaseStatusTrackerAfterInsert){
                Run_CaseStatusTrackerAfterInsert = false;
                return true;
            }else{
                return Run_CaseStatusTrackerAfterInsert;
            }
        }else if(triggerName == CONSTANTS.BEFORE_UPDATE){
            if(Run_CaseStatusTrackerBeforeUpdate){
                Run_CaseStatusTrackerBeforeUpdate = false;
                return true;
            }else{
                return Run_CaseStatusTrackerBeforeUpdate;
            }
        }else if(triggerName == CONSTANTS.AFTER_UPDATE){
            if(Run_CaseStatusTrackerAfterUpdate){
                Run_CaseStatusTrackerAfterUpdate = false;
                return true;
            }else{
                return Run_CaseStatusTrackerAfterUpdate;
            }
        }
        system.debug('--Not letting the Case Comment Triggers to Run--');
        return false;
    }
    
    // -- Agreement --//
    private static boolean Run_AgreementBeforeInsert = true;
    private static boolean Run_AgreementAfterInsert = true;
    private static boolean Run_AgreementBeforeUpdate = true;
    private static boolean Run_AgreementAfterUpdate = true; 
    public static boolean checkAgreementRecursion(String triggerName){
        if(triggerName == CONSTANTS.BEFORE_INSERT){
            if(Run_AgreementBeforeInsert){
                Run_AgreementBeforeInsert = false;
                return true;
            }else{
                return Run_AgreementBeforeInsert;
            }
        }else if(triggerName == CONSTANTS.AFTER_INSERT){
            if(Run_AgreementAfterInsert){
                Run_AgreementAfterInsert = false;
                return true;
            }else{
                return Run_AgreementAfterInsert;
            }
        }else if(triggerName == CONSTANTS.BEFORE_UPDATE){
            if(Run_AgreementBeforeUpdate){
                Run_AgreementBeforeUpdate = false;
                return true;
            }else{
                return Run_AgreementBeforeUpdate;
            }
        }else if(triggerName == CONSTANTS.AFTER_UPDATE){
            if(Run_AgreementAfterUpdate){
                Run_AgreementAfterUpdate = false;
                return true;
            }else{
                return Run_AgreementAfterUpdate;
            }
        }
        
        return false;
    }
    
    
    // Legal Agreement
    //public static boolean Run_LegalAgreementAfterUpdate = true;
    
    private static boolean Run_LegalAgreementBeforeInsert = true;
    private static boolean Run_LegalAgreementAfterInsert = true;
    private static boolean Run_LegalAgreementBeforeUpdate = true;
    private static boolean Run_LegalAgreementAfterUpdate = true; 
    
    public static boolean checkLegalAgreementRecursion(String triggerName){
        if(triggerName == CONSTANTS.BEFORE_INSERT){
            if(Run_LegalAgreementBeforeInsert){
                Run_LegalAgreementBeforeInsert = false;
                return true;
            }else{
                return Run_LegalAgreementBeforeInsert;
            }
        }else if(triggerName == CONSTANTS.AFTER_INSERT){
            if(Run_LegalAgreementAfterInsert){
                Run_LegalAgreementAfterInsert = false;
                return true;
            }else{
                return Run_LegalAgreementAfterInsert;
            }
        }else if(triggerName == CONSTANTS.BEFORE_UPDATE){
            if(Run_LegalAgreementBeforeUpdate){
                Run_LegalAgreementBeforeUpdate = false;
                return true;
            }else{
                return Run_LegalAgreementBeforeUpdate;
            }
        }else if(triggerName == CONSTANTS.AFTER_UPDATE){
            if(Run_LegalAgreementAfterUpdate){
                Run_LegalAgreementAfterUpdate = false;
                return true;
            }else{
                return Run_LegalAgreementAfterUpdate;
            }
        }
        
        return false;
    }
    
    
    private static boolean Run_AttachmentBeforeInsert = true;
    private static boolean Run_AttachmentAfterInsert = true;
    private static boolean Run_AttachmentBeforeUpdate = true;
    private static boolean Run_AttachmentAfterUpdate = true; 
    
    public static boolean checkAttachmentRecursion(String triggerName){
        if(triggerName == CONSTANTS.BEFORE_INSERT){
            if(Run_AttachmentBeforeInsert){
                Run_AttachmentBeforeInsert = false;
                return true;
            }else{
                return Run_AttachmentBeforeInsert;
            }
        }else if(triggerName == CONSTANTS.AFTER_INSERT){
            if(Run_AttachmentAfterInsert){
                Run_LegalAgreementAfterInsert = false;
                return true;
            }else{
                return Run_LegalAgreementAfterInsert;
            }
        }else if(triggerName == CONSTANTS.BEFORE_UPDATE){
            if(Run_AttachmentBeforeUpdate){
                Run_AttachmentBeforeUpdate = false;
                return true;
            }else{
                return Run_AttachmentBeforeUpdate;
            }
        }else if(triggerName == CONSTANTS.AFTER_UPDATE){
            if(Run_AttachmentAfterUpdate){
                Run_AttachmentAfterUpdate = false;
                return true;
            }else{
                return Run_AttachmentAfterUpdate;
            }
        }
        
        return false;
    }
    
    
}