public without sharing class AcntOwner {

    public Account acn {get;set;}
    public List<String> ownerList {get;set;}
    public Boolean independent {get;set;}
    public Boolean initial {get;set;}
    public List<AccountTeamMember> atmList {get;set;}
    public String CommercialLeader {get;set;}
    public Boolean primaryList {get;set;}
    public List<Account> changingMembers {get;set;}
    public List<Account> notchangingMembers {get;set;}
    public List<Account> anMember {get;set;}
    public Integer listSize {get;set;}
    public Boolean secondaryList {get;set;}
    public Map<String,Id> accountIdandRoletoOwnerId{get;set;}
    public Boolean confirmationScreen {get;set;}
    public List<AccountAndOwners> changingMem {get;set;}
    public List<AccountAndOwners> nonChangingMem {get;set;}
    public List<AccountAndOwners> lam {get;set;}
    public List<AccountAndOwners> lamFinal {get;set;}
    public List<AccountTeamMember> acnFinal {get;set;}
    public Boolean memberChanged {get;set;}
    public Boolean accountChanged {get;set;}
    public Boolean renderTeamMembers {get;set;}
    public List<AccountTeamMember> otherMembers {get;set;}
    private List<String> otherMemberRoles;
    public AccountTeamMember del;
    public List< AccountTeamMember > delOtherMembers {get;set;}
    public Integer rowIndex {get;set;}
    public Boolean isMember {get;set;}
    
    public List<SelectOption> AccountTeamRoles{
        get{
            List<SelectOption> options = new List<SelectOption>();
            for (String tmp: otherMemberRoles){
                options.add(new SelectOption(tmp,tmp));
            }
            return options;
        }
    }
    

    public List<SelectOption> commercialLeaders{
        get{
            List<PermissionSetAssignment> per = new List<PermissionSetAssignment>([select assignee.name, assigneeid from permissionsetassignment where permissionsetid = :[select id from permissionset where name = 'Account_Ownership_Change_Request_Approval'] and assignee.profile.name != 'System Administrator']);
            List<SelectOption> options = new List<SelectOption>();
            for (PermissionSetAssignment tmp: per){
                options.add(new SelectOption(tmp.assigneeid,tmp.assignee.name));
            }
            return options;
            
        }
    }
    

    public AcntOwner(ApexPages.StandardController controller) {
    
    
        ownerList = Label.AccountTeamRoleOwner.split('\r\n');
        
        this.acn = [select id, Name, OwnerId, RecordTypeId, parentid, (select id, userid, teammemberrole from accountteammembers where teammemberrole in :ownerList) from Account where id = :controller.getId()];
        isMember = AccountUtils.isMember().get(acn.recordtypeId) ? true : false;
        accountIdandRoletoOwnerId = new Map<String,ID>();
        
        atmList = new List<AccountTeamMember>(); 
        for (AccountTeamMember atm : acn.accountteammembers){
            atmList.add(AccountUtils.generateAccountTeamMember(acn.id, atm.userid, atm.teammemberrole));
            accountIdandRoletoOwnerId.put(acn.id+atm.teammemberrole,atm.userid);
        }
        
        for (String s: ownerList){
            if (!accountIdandRoletoOwnerId.containsKey(acn.id+s)){
                atmList.add(AccountUtils.generateAccountTeamMember(acn.id, null, s));
            }
        }
        

        rowIndex = 0;
        initial = true;
        independent = false;
        confirmationScreen = false;
        primaryList = false;
        secondaryList = false;
        CommercialLeader = AccountUtils.COMMERCIAL_LEADER;
        accountChanged = false;
        memberChanged = false;
        renderTeamMembers = false;

        changingMem = new List<AccountAndOwners>();
        nonChangingMem = new List<AccountAndOwners>();
        lam = new List<AccountAndOwners>();
        
        changingMembers = new List<Account>();
        notchangingMembers = new List<Account>();
        delOtherMembers = new List< AccountTeamMember >();
        
        anMember = new List<Account>([select id, appnexus_member_id__c, ownerid, parentid, recordtypeid, name, sudoku_row__c, sudoku_column__c, (select id, userid, user.name, teammemberrole from accountteammembers where teammemberrole in :ownerList ) from Account where parentid=:acn.id]);
        listSize = anMember.size();
        if (anMember.size()>0){
            for (Account amTemp : anMember){
                if (AccountUtils.isMember().get(amTemp.recordTypeId)){
                    if (amTemp.ownerid == acn.ownerId) {

                        primaryList = true;
                        AccountAndOwners ao = new AccountAndOwners();
                        ao.acn = amTemp;
                        for (AccountTeamMember atm : amTemp.accountteammembers){
                            accountIdandRoletoOwnerId.put(amTemp.id+atm.teammemberrole,atm.userid);
                            if (atm.teammemberrole == AccountUtils.ACCOUNT_OWNER) ao.owner = AccountUtils.generateAccountTeamMember(amTemp.id, atm.userid, atm.teammemberrole);
                            if (atm.teammemberrole == AccountUtils.SR_ACCOUNT_OWNER) ao.srOwner = AccountUtils.generateAccountTeamMember(amTemp.id, atm.userid, atm.teammemberrole);
                            if (atm.teammemberrole == AccountUtils.COMMERCIAL_LEADER) ao.comLeader = AccountUtils.generateAccountTeamMember(amTemp.id, atm.userid, atm.teammemberrole);
                        }
                        changingMem.add(ao);
                    } else {
                      
                        secondaryList = true;
                        AccountAndOwners ao = new AccountAndOwners();
                        ao.acn = amTemp;
                        for (AccountTeamMember atm : amTemp.accountteammembers){
                            accountIdandRoletoOwnerId.put(amTemp.id+atm.teammemberrole,atm.userid);
                            if (atm.teammemberrole == AccountUtils.ACCOUNT_OWNER) ao.owner = AccountUtils.generateAccountTeamMember(amTemp.id, atm.userid, atm.teammemberrole);
                            if (atm.teammemberrole == AccountUtils.SR_ACCOUNT_OWNER) ao.srOwner = AccountUtils.generateAccountTeamMember(amTemp.id, atm.userid, atm.teammemberrole);
                            if (atm.teammemberrole == AccountUtils.COMMERCIAL_LEADER) ao.comLeader = AccountUtils.generateAccountTeamMember(amTemp.id, atm.userid, atm.teammemberrole);
                        }
                        nonChangingMem.add(ao);
                    }
                }
            }
        }
        
        lam.addAll(changingMem);
        lam.addAll(nonChangingMem);
        
    }
    
  
    public void ave(){
 
         if ( delOtherMembers.size() > 0) Database.delete(delOtherMembers,false);
         if ( otherMembers.size() > 0 ) upsert otherMembers;
         renderTeamMembers = false;
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Account has been updated. This window will close momentarily.'));
    } 
    
    
    public void confirmScreen(){
        lamFinal = new List<AccountAndOwners>();
        acnFinal = new List<AccountTeamMember>();

        for (AccountAndOwners lamTemp : lam){
            if(lamTemp.owner.userId != accountIdandRoletoOwnerId.get(lamTemp.acn.id+AccountUtils.ACCOUNT_OWNER) || lamTemp.srOwner.userId != accountIdandRoletoOwnerId.get(lamTemp.acn.id+AccountUtils.SR_ACCOUNT_OWNER)) {
                lamFinal.add(lamTemp);
                memberChanged = true;
            }
        }
        
        for(AccountTeamMember a: atmList){
            if(accountIdandRoletoOwnerId.get(acn.id+a.teammemberrole) != a.userId) {
                acnFinal = atmList;
                accountChanged = true;
            } 
        }
        
          
        confirmationScreen = true;
        independent = false;
        initial = false;
    }
    
    
    public void deleteRow(){
 
         rowIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('rowIndex'));
         del = otherMembers.remove(rowIndex);
         if (del.id != null) delOtherMembers.add(del);
 
    } 
    
    public void addRow(){
        otherMembers.add(new AccountTeamMember(accountid = acn.id, opportunityaccesslevel = 'Read', contactaccesslevel = 'Edit', caseaccesslevel = 'Read', accountaccesslevel = 'Read'));
    }
    
    public void backFromSecond(){
        confirmationScreen = false;
        independent = false;
        initial = true;
        renderTeamMembers = false;
    }
    
    
    public void backFromThird(){
        confirmationScreen = false;
        independent = true;
        initial = false;
    } 
    
    public void renderIndependent(){
        independent = true; 
    }
    
    
    
    public void renderOtherMembers(){
        otherMembers = new List<AccountTeamMember>();
        otherMemberRoles = new List<String>();
        otherMemberRoles.addAll(Label.AccountTeamRoleOther.split('\r\n'));
        otherMemberRoles.addAll(Label.AccountTeamRoleFinance.split('\r\n'));
        otherMemberRoles.addAll(Label.AccountTeamRoleSupport.split('\r\n'));
        otherMembers = [select id, userid, teammemberrole from accountteammember where accountid = :acn.id and teammemberrole in: otherMemberRoles ];
        independent = false;
        initial = false;
        renderTeamMembers = true;

    }
   
    public void saveRequest(){
      
        Map<String, Id> roleToUser = new Map<String,Id>();
        for (AccountTeamMember a: atmList){
            roleToUser.put(a.teammemberrole,a.userid);
        }
        
        Account_Owner_Change_Request__c changeReq;
        User u = [select username from user where id =:acn.OwnerId limit 1];
        
        String relatedAccount = acn.id;
        String relatedMember = null;
        Boolean changeAccountOnly = false;
        
        if (AccountUtils.isMember().get(acn.recordTypeId)){
            relatedAccount = acn.parentid;
            relatedMember = acn.id;
            changeAccountOnly = true;
        }

        changeReq = new Account_Owner_Change_Request__c(Related_Account__c = relatedAccount,
                                                        AppNexus_Member2__c = relatedMember,
                                                        Change_Account_Only__c = changeAccountOnly,
                                                        Market_Owner__c = roleToUser.get('Commercial Leader'), 
                                                        Secondary_Approver__c = u.username.contains('businesssupport@appnexus.com') ? Label.Business_Support_Approver : null,
                                                        New_Account_Owner__c = roleToUser.get('Account Owner'),
                                                        New_Sr_Account_Owner__c = roleToUser.get('Senior Account Owner'),
                                                        Previous_Account_Owner__c = accountIdandRoletoOwnerId.get(acn.id+'Account Owner'),
                                                        Previous_Sr_Account_Owner__c = accountIdandRoletoOwnerId.get(acn.id+'Senior Account Owner'),
                                                        Previous_Commercial_Leader__c = accountIdandRoletoOwnerId.get(acn.id+'Commercial Leader'),
                                                        Status__c = 'Pending');
                                                                                         
        insert changeReq;
        
        confirmationScreen = false;
        independent = false;
        initial = false;
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Your request is recorded. Account will be updated upon approval from the commercial leader. This window will close momentarily.'));
                                                                                            
    }
    
    public class AccountAndOwners{ 
        public Account acn {get; set;}
        public AccountTeamMember owner {get;set;}
        public AccountTeamMember srOwner {get;set;}
        public AccountTeamMember comLeader {get;set;}
        
        public AccountAndOwners(){
            if(acn == null){acn = new Account();}
            if(owner == null){owner = new AccountTeamMember();}
            if(srOwner == null){srOwner = new AccountTeamMember();}
            if(comLeader == null){comLeader = new AccountTeamMember();}
        }
    }    
 
    public void saveIndependentRequest(){
    
        List<Account_Owner_Change_Request__c> changeReqList = new List<Account_Owner_Change_Request__c>();
        User businessSupport = [select id from user where username like 'businesssupport@appnexus.com%' limit 1];
        if (lamFinal.size()>0){
            for (AccountAndOwners lm : lamFinal){
                Account_Owner_Change_Request__c aocr = new Account_Owner_Change_Request__c();
                aocr.Market_Owner__c = lm.comLeader.userId;
                aocr.Secondary_Approver__c = (lm.owner.userid == businessSupport.id) ? Label.Business_Support_Approver : null;
                aocr.appnexus_member2__c = lm.acn.id;
                aocr.status__c = 'Pending';
                aocr.Change_Account_Only__c = true;
                aocr.Related_Account__c = lm.acn.parentid;
                aocr.New_Account_Owner__c = lm.owner.userid;
                aocr.New_Sr_Account_Owner__c = lm.srOwner.userid;
                aocr.Previous_Account_Owner__c = accountIdandRoletoOwnerId.get(lm.acn.id+AccountUtils.ACCOUNT_OWNER);
                aocr.Previous_Sr_Account_Owner__c = accountIdandRoletoOwnerId.get(lm.acn.id+AccountUtils.SR_ACCOUNT_OWNER);
                changeReqList.add(aocr);
                
            }
        }
        if (acnFinal.size()>0){

            Account_Owner_Change_Request__c aocr = new Account_Owner_Change_Request__c();
            aocr.status__c = 'Pending';
            aocr.Previous_Commercial_Leader__c = accountIdandRoletoOwnerId.get(acn.id+AccountUtils.COMMERCIAL_LEADER);
            aocr.Previous_Account_Owner__c = accountIdandRoletoOwnerId.get(acn.id+AccountUtils.ACCOUNT_OWNER);
            aocr.Previous_Sr_Account_Owner__c = accountIdandRoletoOwnerId.get(acn.id+AccountUtils.SR_ACCOUNT_OWNER);
            aocr.Change_Account_Only__c = true;
            
            for (AccountTeamMember at: acnFinal){
                if(at.TeamMemberRole == AccountUtils.ACCOUNT_OWNER) {
                    aocr.New_Account_Owner__c = at.userid;
                    aocr.Secondary_Approver__c = (at.userid == businessSupport.id) ? Label.Business_Support_Approver : null;
                }
                if(at.TeamMemberRole == AccountUtils.SR_ACCOUNT_OWNER) aocr.New_Sr_Account_Owner__c = at.userid;
                if(at.TeamMemberRole == AccountUtils.COMMERCIAL_LEADER) aocr.Market_Owner__c = at.userid;
            }
            
            aocr.Related_Account__c = acn.id;
            changeReqList.add(aocr);
        }
        
        if (changeReqList.size() > 0) insert changeReqList;
        
        confirmationScreen = false;
        independent = false;
        initial = false;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Your request is recorded. If approval is required, account and/or members will be updated upon approval from the commercial leader. This window will close momentarily.'));
        }    



}