public  class ServiceFacade {
    

    public static list<Outage_Notifications__c> getActiveOutageNotifications(){
        return OutageNotificationService.getActiveOutageNotifications();
    }

    public static BusinessHours getDefaultBusinessHours(){
        return BusinessHoursService.getDefaultBusinessHours();
    }

    public static BusinessHours getBusinessHoursByName(String businessHourName){
        return BusinessHoursService.getBusinessHoursByName(businessHourName);
    }

    public static Decimal getDifferenceInBusinessHours(String BusinessHourID, Datetime startDate, Datetime endDate){

        return BusinessHoursService.getDifferenceInBusinessHours(BusinessHourID,startDate,endDate);
    }

    public static Datetime addBusinessHours(String BusinessHourID, Datetime startDate, Long intervalMilliseconds){

        return BusinessHoursService.addBusinessHours(BusinessHourID,startDate,intervalMilliseconds);
    }

    public static void completeMilestone(List<Id> caseIds, String milestoneName, DateTime complDate){
        CaseMilestoneService.completeMilestone(caseIds,milestoneName,complDate);
    }

    public static map<ID,Apttus__APTS_Agreement__c> getAgreementsByFilteredIDs(set<ID> agreementIDSet){
        return AgreementService.getAgreementsByFilteredIDs(agreementIDSet);
    }

    public static void updateAgreements(list<Apttus__APTS_Agreement__c> agreementsToUpdateList){
        AgreementService.updateAgreements(agreementsToUpdateList);
    }

    public static Entitlement fetchEntitlementByName(String EntitleMentName){
        return EntitlementService.fetchEntitlementByName(EntitleMentName);
    }

    public static map<ID,Case> getCasesbyIDList(set<ID> parentCaseIDList){
        return CaseService.getCasesbyIDList(parentCaseIDList);
    }
    
    public static List<Apttus__APTS_Agreement__c> getAgreementsByAccount(String aptusAccountId ,String status) {
    
        return AgreementService.getAgreementsByAccount(aptusAccountId,status);
    }
    
    public static Id getDummyContact() {
         
        return  ContactServices.getDummyContact();
    }
    
    public static List<contact> getContactByIds(set<Id> contId ) {
        
        return ContactServices.getContactByIds(contId);
    }
    
    public static List<Account> getAccountsByIds(Set<Id> accIds) {
    
        return AccountServices.getAccountsByIds(accIds);
    }
    
    public static Map<Id,User> getUserByContactId(Set<Id> contIds) {
    
        return UserServices.getUserByContactId(contIds);
    }
    
    public static List<Contact> getCustomerPortalContacts(Set<Id> accIds,Set<Id> conIds) {
        
        return ContactServices.getCustomerPortalContacts(accIds,conIds);
    }
    
    public static List<case>  NotClosedCasesByOwner(String OwnerId) {
        
        return  CaseService.NotClosedCasesByOwner(OwnerId);
    }
 }