@isTest
private class SalesOpsCaseExtV2Test
{
    static Map<String, List<String>> testTypeToReasonMap;
    static final String TEST_DESCRIPTION_TYPE = 'THIS IS A TYPE DESCRIPTION';
    static final String TEST_DESCRIPTION_REASON = 'THIS IS A REASON DESCRIPTION';
    static final String TEST_REMINDER = 'THIS IS A REMINDER';
    static final String TEST_SEGMENT = 'SEGMENT';
    static final String TEST_NAME = 'NAME';
    
    
    static
    {
        Map<String, Schema.FieldSet> fsetMap = Case.sObjectType.getDescribe().Fieldsets.getMap();
        testTypeToReasonMap = new Map<String, List<String>>();
        for(Schema.FieldSet thisFieldSet : fsetMap.Values())
        {
            if(thisFieldSet.getName().contains('0'))
            {
                List<String> testTypeReason = thisFieldSet.getName().split('0');
                String testType = testTypeReason[0];
                String testReason = '';
                if( testTypeReason.size() > 1 ) 
                    testReason = testTypeReason[1];  
                if(testTypeToReasonMap.keyset().contains(testType))
                {
                    testTypeToReasonMap.get(testType).add(testReason);
                }
                else
                {
                    testTypeToReasonMap.put(testType, new List<String>{testReason});
                }
            }
        }
        if(testTypeToReasonMap.size() == 0)
        {
            System.assert(false, 'There are no fieldsets defined for the Case Object');
        }
    }
    
    
    static List<SalesOps_Descrip__c> testDescripts;
    static Account[] testAccount;
    static Case testCase;
    
    static void setupCustomSettings()
    {
        testDescripts = new List<SalesOps_Descrip__c>();
        Integer i=0;
        for(String aType : testTypeToReasonMap.keySet())
        {
            SalesOps_Descrip__c testDescriptType = new SalesOps_Descrip__c();
            testDescriptType.CaseType__c = aType;
            testDescriptType.Name = 'obj' + i++;
            testDescriptType.Description__c = TEST_DESCRIPTION_TYPE + aType;
            testDescriptType.Case_Type_Description__c = true;
            testDescriptType.Order__c = i++;
            testDescripts.add(testDescriptType);
            for(String aReason : testTypeToReasonMap.get(aType))
            {
                SalesOps_Descrip__c testDescriptReason = new SalesOps_Descrip__c();
                testDescriptReason.Case_Type_Description__c = false;
                String descripName = (aType + ' ' + aReason);
                if(descripName.length() > 38) descripName = descripName.left(38);
                testDescriptReason.Name = descripName;
                testDescriptReason.CaseReason__c = aReason;
                testDescriptReason.CaseType__c = aType;
                testDescriptReason.Description__c = TEST_DESCRIPTION_REASON +aType+ aReason;
                testDescriptReason.Reminder__c = TEST_REMINDER + aType + aReason;
                testDescriptReason.Order__c = i++;
                testDescripts.add(testDescriptReason);
            }
        }
        insert testDescripts;
    }
    
    static void setup()
    { 
        
        //Account_Record_Type__c ar=InitializeTestData.CreateAccountCustomSetting(false,false,true);  
        // insert ar;
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Internal Entity').getRecordTypeId();
        system.debug('*********RecordTypeIdAccount'+RecordTypeIdAccount);
        //    testAccount = new Account();
        //testAccount.Active_Client_Sub_Segment__c = TEST_SEGMENT;
        // testAccount.Name = TEST_NAME;
        //testAccount.RecordTypeId= RecordTypeIdAccount;
        // insert testAccount;
        
        testAccount = AccountTriggerTest.createAccounts('Customer Account',1);
        testAccount[0].RecordTypeId= RecordTypeIdAccount;
        insert testAccount;
        TestingUtils.createCustomSettings();
        testCase = new Case();
        testCase.AccountId = testAccount[0].Id;
    }
    
    static testMethod void testReminder()
    {
        setup();
        setupCustomSettings();
        Apexpages.Standardcontroller std = new Apexpages.Standardcontroller(testCase);
        SalesOpsCaseExtV2 cont = new SalesOpsCaseExtV2(std);
        SalesOps_Descrip__c targetTestDescrip;
        for(SalesOps_Descrip__c aDescrip : testDescripts)
        {
            if(!(String.isBlank(aDescrip.CaseReason__c) || String.isBlank(aDescrip.CaseType__c)))
            {
                targetTestDescrip = aDescrip;
                break;
                
            }
        }
        cont.newCase.Sales_Ops_Case_Reason__c = targetTestDescrip.CaseReason__c;
        cont.newCase.Sales_Ops_Case_Type__c = targetTestDescrip.CaseType__c;
        Test.startTest();
        String reminder = cont.reminder;
        System.assertEquals(targetTestDescrip.Reminder__c, reminder, 'The reminder specified in the custom setting should be returned');
        Test.stopTest();
    }
    
    /*  static testMethod void testConstructor()
{
setup();
insert testCase;
Apexpages.Pagereference ref = Page.SalesOpsFileAttacher;
//ref.getParameters().put(SalesOpsCaseExtV2.CASE_ID_PARAM, testCase.Id);
// ref.getParameters().put(SalesOpsCaseExtV2.REASON_PARAM, TEST_SEGMENT);
// ref.getParameters().put(SalesOpsCaseExtV2.TYPE_PARAM, TEST_NAME);
// ref.getParameters().put(SalesOpsCaseExtV2.RELATED_ACCOUNT_ID, testAccount.Id);
Test.setCurrentPage(ref);
Apexpages.Standardcontroller std = new Apexpages.Standardcontroller(testCase);
Test.startTest();
SalesOpsCaseExtV2 cont = new SalesOpsCaseExtV2(std);
Test.stopTest();
System.assertEquals(TEST_SEGMENT, cont.newCase.Sales_Ops_Case_Reason__c, 'The Sales Ops Reason should have carried over from the params');
System.assertEquals(TEST_NAME, cont.newCase.Sales_Ops_Case_Type__c, 'The Sales ops Case Type should carried over from the params');
System.assertEquals(testCase.Id, cont.newCase.Id, 'The Case Id should have carried over from the Params');
// System.assertEquals(testAccount.Id, cont.newCase.Account_Related_To_Sales_Ops_Request__c, 'The Account related to sales ops Request should have carried over from the params');
}*/
    
    /* static testMethod void testSTDConstructor_WithParam()
{
setup();
insert testCase;
Apexpages.Pagereference ref = Page.SalesOpsFileAttacher;
//   ref.getParameters().put(SalesOpsCaseExtV2.CASE_ID_PARAM, testCase.Id);
//  ref.getParameters().put(SalesOpsCaseExtV2.REASON_PARAM, TEST_SEGMENT);
// ref.getParameters().put(SalesOpsCaseExtV2.TYPE_PARAM, TEST_NAME);
// ref.getParameters().put(SalesOpsCaseExtV2.RELATED_ACCOUNT_ID, testAccount.Id);
Test.setCurrentPage(ref);
Test.startTest();
SalesOpsCaseExtV2 cont = new SalesOpsCaseExtV2();
Test.stopTest();
System.assertEquals(TEST_SEGMENT, cont.newCase.Sales_Ops_Case_Reason__c, 'The Sales Ops Reason should have carried over from the params');
System.assertEquals(TEST_NAME, cont.newCase.Sales_Ops_Case_Type__c, 'The Sales ops Case Type should carried over from the params');
System.assertEquals(testCase.Id, cont.newCase.Id, 'The Case Id should have carried over from the Params');
//System.assertEquals(testAccount.Id, cont.newCase.Account_Related_To_Sales_Ops_Request__c, 'The Account related to sales ops Request should have carried over from the params');
}*/
    
    
    
    
    static testMethod void testSTDConstructor()
    {
        setup();
        setupCustomSettings();
        Apexpages.Standardcontroller std = new Apexpages.Standardcontroller(testCase);
        Test.startTest();
        SalesOpsCaseExtV2 cont = new SalesOpsCaseExtV2(std);
        Test.stopTest();
        System.assertEquals(testTypeToReasonMap.size(), cont.typeDescriptions.size());
    }
    
    static testMethod void testNextPage_Success()
    {
        setup();
        setupCustomSettings();
        Apexpages.Standardcontroller std = new Apexpages.Standardcontroller(testCase);
        SalesOpsCaseExtV2 cont = new SalesOpsCaseExtV2(std);
        SalesOps_Descrip__c targetTestDescrip;
        for(SalesOps_Descrip__c aDescrip : testDescripts)
        {
            if(!(String.isBlank(aDescrip.CaseReason__c) || String.isBlank(aDescrip.CaseType__c)))
            {
                targetTestDescrip = aDescrip;
                break;
                
            }
        }
        cont.newCase.Sales_Ops_Case_Reason__c = targetTestDescrip.CaseReason__c;
        cont.newCase.Sales_Ops_Case_Type__c = targetTestDescrip.CaseType__c;
        Test.startTest();
        cont.nextPage();
        cont.saveAndGo();
        cont.goBack();
        Test.stopTest();
        //System.assert(!Apexpages.hasMessages(), 'No error should have been thrown');
        //System.assert(!cont.displayingCaseTypeAndReasonPage, 'This boolean should have been set to false');
        
    }
    static testMethod void testNextPage_Fail()
    {
        setup();
        setupCustomSettings();
        Apexpages.Standardcontroller std = new Apexpages.Standardcontroller(testCase);
        SalesOpsCaseExtV2 cont = new SalesOpsCaseExtV2(std);
        Test.startTest();
        cont.nextPage();
        Test.stopTest();
        System.assert(Apexpages.hasMessages(), 'An error should have been thrown');
        System.assert(cont.displayingCaseTypeAndReasonPage, 'This boolean should not have been set');
    }
    
    static testMethod void testGoHome()
    {
        SalesOpsCaseExtV2 cont = new SalesOpsCaseExtV2();
        Test.startTest();
        Pagereference home = cont.goHome();
        Test.stopTest();
        System.assertEquals('/', home.getUrl(), 'THe Url should match up');
    }
    
    
    
    static testMethod void testPopulateFieldSet_Fail()
    {
        setup();
        InitializeTestData.createSystemConfigSettings();
        insert testCase;
        Apexpages.Standardcontroller std = new Apexpages.Standardcontroller(testCase);
        SalesOpsCaseExtV2 cont = new SalesOpsCaseExtV2(std);
        Test.startTest();
        cont.populateFieldSet();
        Test.stopTest();
        System.assert(Apexpages.hasMessages(), 'An error should have been thrown');
        System.assert(cont.displayingCaseTypeAndReasonPage, 'This boolean should not have been set');
    }
    
    static testMethod void testChangeType()
    {
        setup();
        setupCustomSettings();
        Apexpages.Standardcontroller std = new Apexpages.Standardcontroller(testCase);
        SalesOpsCaseExtV2 cont = new SalesOpsCaseExtV2(std);
        String caseType = new List<String>(testTypeToReasonMap.keySet())[0];
        cont.newCase.Sales_Ops_Case_Type__c = caseType;
        Test.startTest();
        cont.changeType();
        Test.stopTest();
        System.assertEquals(testTypeToReasonMap.get(caseType).size(), cont.reasonDescriptions.size(), 'There should be one description for every one case Reason under the specified type');
    }
    
    static testMethod void testSave_ExpediteFail()
    {
        setup();
        setupCustomSettings();
        Apexpages.Standardcontroller std = new Apexpages.Standardcontroller(testCase);
        SalesOpsCaseExtV2 cont = new SalesOpsCaseExtV2(std);
        cont.newCase.Expedite__c = true;
        Test.startTest();
        cont.save();
        Test.stopTest();
        //System.assert(Apexpages.hasMessages(), 'An error should have been thrown for expedite');
    }
    static testMethod void testSave_NoExpedite()
    {
        setup();
        setupCustomSettings();
        Apexpages.Standardcontroller std = new Apexpages.Standardcontroller(testCase);
        SalesOpsCaseExtV2 cont = new SalesOpsCaseExtV2(std);
        cont.newCase = testCase;
        Test.startTest();
        cont.save();
        Test.stopTest();
        //System.assert(!Apexpages.hasMessages(), 'No error should have been thrown for expedite');
    }
    
    static testMethod void testAddAttachment()
    {
        setup();
        InitializeTestData.createSystemConfigSettings();
        Apexpages.Standardcontroller std = new Apexpages.Standardcontroller(testCase);
        SalesOpsCaseExtV2 cont = new SalesOpsCaseExtV2(std);
        cont.newCase = testCase;
        cont.save();
        cont.theFileName = TEST_NAME;
        cont.theFile = Blob.valueOf(TEST_NAME);
        cont.save();
        Test.startTest();
        ApexPages.PageReference ref= cont.addAttachment();
        Test.stopTest();
        System.assertEquals('/' + cont.newCase.Id, ref.getUrl(), 'The user should be redirected back to the new case');
        List<Attachment> newAttach = [SELECT Id, Name FROM Attachment WHERE ParentId = :cont.newCase.Id];
        System.assertEquals(1, newAttach.size(), 'THere should only be 1 attachment created');
        System.assertEquals(TEST_NAME, newAttach[0].Name, 'The file name should have been pulled from the controller');
    }
    
    static testMethod void testSaveCaseAndAttachment()
    {
        setup();
        Apexpages.Standardcontroller std = new Apexpages.Standardcontroller(testCase);
        SalesOpsCaseExtV2 cont = new SalesOpsCaseExtV2(std);
        String caseType = new List<String>(testTypeToReasonMap.keySet())[0];
        cont.newCase.Sales_Ops_Case_Type__c = caseType;
        Test.startTest();
        ApexPages.PageReference ref = cont.saveCaseAndAttachment();
        Test.stopTest();
        // System.assertEquals(caseType, ref.getParameters().get(SalesOpsCaseExtV2.TYPE_PARAM), 'The case type should carry over in the page reference');
        //System.assertEquals(cont.newCase.Id, ref.getParameters().get(SalesOpsCaseExtV2.CASE_ID_PARAM), 'The case Id should carry over in the page reference');
    }
    
    
}