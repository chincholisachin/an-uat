public  class EmailMessageTriggerHandler {
	private static EmailMessageTriggerHandler instance;
	//*** Singleton Pattern to obtain instance of the Handler***
    public static EmailMessageTriggerHandler getInstance(){
        if(instance==null){
            instance = new EmailMessageTriggerHandler();
        }
        return instance;
    }

    public void beforeInsertMethod(List<EmailMessage> newList,map<ID,EmailMessage> newMap){
        EmailMessageService.handleIncomingEmailMessage(newList);
        EmailMessageService.completeCaseMilestone(newList);
    }

    public void afterInsertMethod(List<EmailMessage> newList,map<ID,EmailMessage> newMap){
        EmailMessageService.sendEmailMessage();
    }
}