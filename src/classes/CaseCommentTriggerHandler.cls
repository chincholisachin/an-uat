public  class CaseCommentTriggerHandler {
	
	private static CaseCommentTriggerHandler instance;

	//*** Singleton Pattern to obtain instance of the Handler***
    public static CaseCommentTriggerHandler getInstance(){
        if(instance==null){
            instance = new CaseCommentTriggerHandler();
        }
        return instance;
    }	

    // --All before insert operations will be called from here --//
    public void beforeInsertMethod(list<CaseComment> newList,map<ID,CaseComment> newMap){
            CaseCommentService.updateCaseMilestone(newList);
        
    }

}