public class AccountServices {

    public static List<Account> getAccountsByIds(Set<Id> accIds)
    {
    
        return [SELECT Id,IsCustomerPortal From Account Where Id IN: accIds];
    }
    
}