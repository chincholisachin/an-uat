public class ContactTriggerHandler{
    
    private static Contact cspContact = new Contact();
    private static Boolean isContactCached = false;
    
    
    private static Contact getCspContact(){
        if(isContactCached) return cspContact;
        cspContact = [select id, accountid, recordtypeId from contact where firstname = 'CSP' and lastname = 'Dummy' limit 1];
        isContactCached = true;
        return cspContact;
    }
    
    public static void createUserRelationship(List<Contact> con){
    
        Map<String,String> personToUser = new Map<String,String>();
        for (User u: [select id, DRT_Person_Id__c from user where drt_person_id__c != null]){
            personToUser.put(u.DRT_Person_Id__c, u.id);
        }
        
        for(Contact c: con){
            if (c.DRT_Person_Id__c != null) {
                c.user__c = personToUser.get(c.DRT_Person_Id__c);
                c.ReportsTo = getCspContact();
            }
        }  
    }   
}