public class AccAgrAttachViewer{

public String getagreementLink(){
    Account acn = [Select name from account where id =:Apexpages.currentPage().getParameters().get('id')]; 
    String a = URL.getSalesforceBaseUrl().toExternalForm();

    a = a + '/apex/APTS_AgreementRecordtype?ent=01IR0000000AUgd&retURL=%2Fapex%2FacctOverride%3Fid%3D001R0000010zixd%26srPos%3D0%26srKp%3D001%26sfdc.override%3D1&save_new_url=%2Fa55%2Fe%3FCF00Nf40000077CVr=';
    a = a + EncodingUtil.urlEncode(acn.Name, 'UTF-8') + '%26CF00Nf40000077CVr_lkid='+Apexpages.currentPage().getParameters().get('id')+'%26scontrolCaching%3D1%26retURL%3D%252Fapex%252FacctOverride%253Fid%253D001R0000010zixd%2526srPos%253D0%2526srKp%253D001%2526sfdc.override%253D1'; 
    
    return a;
}

public List<Attachment> att{get;set;}

    /* Wrapper class to contain the nodes and their children */
    public class cNodes
    {
        public List<Apttus__APTS_Agreement__c> parent {get; set;}
        Public Apttus__APTS_Agreement__c gparent {get;set;}
        public cNodes(Apttus__APTS_Agreement__c  gp, List<Apttus__APTS_Agreement__c> p)
        {       
            parent = p;       
            gparent = gp;   
        }  
    }
    /* end of Wrapper class */
          
    Public List<cNodes> hierarchy;
    public Id currentAccId {get;set;}
    Public List<cNodes> getmainnodes()
    {
        currentAccId = Apexpages.currentPage().getParameters().get('id');
        hierarchy = new List<cNodes>();
        List<Apttus__APTS_Agreement__c> tempparent = [select id, name, Apttus__Status__c, Apttus__Status_Category__c, (select ContentDocumentId, Title from Content_Versions__r) from Apttus__APTS_Agreement__c where Apttus__Parent_Agreement__c = null and Apttus__Account__c =:currentAccId];
        for (Integer i =0; i< tempparent.size() ; i++)
        {
            List<Apttus__APTS_Agreement__c> tempchildren = [Select Id, name, Apttus__Status__c, Apttus__Status_Category__c, (select ContentDocumentId, Title from Content_Versions__r) from Apttus__APTS_Agreement__c where Apttus__Parent_Agreement__c = :tempparent[i].Id];
            
            hierarchy.add(new cNodes(tempparent[i],tempchildren));
        }
        return hierarchy;
    }

}