public without Sharing class CSP_GetCaseDetails{

    
    @AuraEnabled
    public static userWrapper isActiveUser() {
        Id userId = UserInfo.getUserId();
        user u = [select id,ProfileId,ContactId,AccountId From User WHERE id=: userId];
        Profile p = [select Id,Name From Profile WHERE id=:u.ProfileId];
        userWrapper uw = new userWrapper();
        uw.userId = u.Id;
        uw.userProfileName = p.Name;
        uw.userProfileId = u.profileId;
        uw.AccountId = u.AccountId;
        uw.contactId = u.contactId;
        return uw;
    }
    
    @AuraEnabled
    public static List<String> getCaseCategory(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Case.Category__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(f.getLabel());
        }
        return options;
    }
    
   /* @AuraEnabled
    public static Case createNewCase(Case newCase,list<String> emailList ){
        system.debug('--newCase--'+newCase);
        system.debug('--emailList--'+emailList);
        return null;
    }*/    
    
    // Getting the dependent picklist values based on provided object Name and controlling fieldName and Dependent Filed Name.
    @AuraEnabled
    public static Map<String,List<String>>  getDependentOptions(String pObjName, String pControllingFieldName, String pDependentFieldName){
        Map<String,List<String>> objResults = new Map<String,List<String>>();
        //get the string to sobject global map
        Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
        if (!objGlobalMap.containsKey(pObjName))
            return objResults ;
        //get the type being dealt with
        Schema.SObjectType pType = objGlobalMap.get(pObjName);
        Map<String, Schema.SObjectField> objFieldMap = pType.getDescribe().fields.getMap();
        //verify field names
        if (!objFieldMap.containsKey(pControllingFieldName) || !objFieldMap.containsKey(pDependentFieldName))
            return objResults ;   
        //get the control values   
        List<Schema.PicklistEntry> ctrl_ple = objFieldMap.get(pControllingFieldName).getDescribe().getPicklistValues();
        //get the dependent values
        List<Schema.PicklistEntry> dep_ple = objFieldMap.get(pDependentFieldName).getDescribe().getPicklistValues();
        //iterate through the values and get the ones valid for the controlling field name
        Bitset objBitSet = new Bitset();
        //set up the results
        for(Integer pControllingIndex=0; pControllingIndex<ctrl_ple.size(); pControllingIndex++){           
            //get the pointer to the entry
            Schema.PicklistEntry ctrl_entry = ctrl_ple[pControllingIndex];
            //get the label
            String pControllingLabel = ctrl_entry.getLabel();
            //create the entry with the label
            objResults.put(pControllingLabel,new List<String>());
        }
        for(Integer pDependentIndex=0; pDependentIndex<dep_ple.size(); pDependentIndex++){          
            //get the pointer to the dependent index
            Schema.PicklistEntry dep_entry = dep_ple[pDependentIndex];
            //get the valid for
            String pEntryStructure = JSON.serialize(dep_entry);                
            TPicklistEntry objDepPLE = (TPicklistEntry)JSON.deserialize(pEntryStructure, TPicklistEntry.class);
            //if valid for is empty, skip
            if (objDepPLE.validFor==null || objDepPLE.validFor==''){
                continue;
            }
            //iterate through the controlling values
            for(Integer pControllingIndex=0; pControllingIndex<ctrl_ple.size(); pControllingIndex++){    
                if (objBitSet.testBit(objDepPLE.validFor,pControllingIndex)){                   
                    //get the label
                    String pControllingLabel = ctrl_ple[pControllingIndex].getLabel();
                    objResults.get(pControllingLabel).add(objDepPLE.label);
                }
            }
        } 
        return objResults;
    }
    
    // This Method is used to get all the picklist values for the 
    @AuraEnabled
    public static List<String> getPickValues(String field_name,String first_val) {
        List<String> options = new List<String>(); //new list for holding all of the picklist options
        if (first_val != null) { //if there is a first value being provided
            options.add(first_val); //add the first option
        }
        Schema.sObjectType sobject_type = case.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
            
            options.add(a.getLabel()); //add the value and label to our final list
        }
        System.debug(options+'*&&*&*&*&*');
        return options; //return the List
    }
    
    @AuraEnabled
    public static String createCaseRecord(String caseInfo,List<String> membersList) {
        
        Map<String,String> cas =  (Map<String,String>)System.JSON.deserialize(caseInfo,Map<String,String>.class);
        
        case cs = new case();   
        List<String> emailsList;
        cs.Category__c= cas.get('category');  
        cs.Severity__c = cas.get('severity');
        cs.What_can_we_help_with__c = cas.get('help');
        cs.Description = cas.get('description');
        cs.Subject =cas.get('subject');
        cs.Invoice_Number__c = cas.get('invoiceNumber');
        
        if(cas.get('ccEmail')!=null){
            emailsList = new list<String>();
            emailsList = cas.get('ccEmail').split(',');
            system.debug('---emailsList---'+emailsList);
            // -- This might change later, when all CC Emails are consolidated as part of Case Management Capability.
            list<String> ccEmailFields = new list<String>{'CC_Email_1__c','CC_Email_2__c','CC_Email_3__c','CC_Email_4__c','CC_Email_5__c','CC_Email_6__c','CC_Email_7__c','CC_Email_8__c','CC_Email_9__c','CC_Email_10__c'};
                
                for(integer i =0;i<emailsList.size();i++){
                    cs.put(ccEmailFields.get(i),emailsList.get(i));
                }
        }
        system.debug('---cs---'+cs);
        //cs.CC_Email_1__c = cas.get('ccEmail');
        cs.Member_Number__c = cas.get('memberId');
        cs.SuppliedEmail = cas.get('email') ;
        cs.SuppliedName = cas.get('name');
        cs.SuppliedCompany = cas.get('company');
        list<Case_Member_Account_Relationship__c> caseMemAccList;
        String memberId = cas.get('memberId');
        try{
            insert cs;
            if(cs.Id != null) {
                caseMemAccList = new list<Case_Member_Account_Relationship__c>();
                if(membersList.size()>0) {
                 
                    for(String s : membersList) {
                        if(s != '--None--') {
                            Case_Member_Account_Relationship__c cm = new Case_Member_Account_Relationship__c();
                            cm.Case__c = cs.Id;
                            cm.Member_Account__c = s;
                            caseMemAccList.add(cm);
                        } 
                    }
                    try{
                        if(!caseMemAccList.isEmpty()){
                            insert caseMemAccList;
                          }
                       }catch(Exception e) {
                             System.debug(e.getMessage()+'=====');
                       }
                } else if(memberId!=null && memberId!=''){
                    Exception_Logs__c el = new Exception_Logs__c();
                    el.Exception_Message__c = 'Entered';
                    insert el;
                    list<String> memIDs = new list<String>();
                    if(memberId !=null && memberId !=''){
                        if(memberID.contains(',')){
                            memIDs = memberID.split(',');
                        }else{
                            memIDs.add(memberId);
                        }
                    }
                    
                    system.debug('===memIDs==='+memIDs);
                    if(!memIDs.isEmpty()) {
                        Case_Member_Account_Relationship__c cm ;
                        Contact contactByEmailFound = new Contact();
                        String currentEmailAddress = cas.get('email') ; // -- Current email entered by the end user.

                        //-- Find the contacts related to the current user entered email address.
                        if(currentEmailAddress!=null && currentEmailAddress!=''){
                            try{
                                contactByEmailFound = [SELECT id,AccountID from Contact where Email=:currentEmailAddress];
                                }
                                catch(Exception e){
                                    system.debug('==Exception=='+e.getMessage());
                                }
                        }
                        
                        if(contactByEmailFound!=null){
                            list<Account> memAccByConEmailList = new list<Account>([SELECT id,AppNexus_Member_ID__c from Account where ParentID=:contactByEmailFound.AccountID]);

                            if(!memAccByConEmailList.isEmpty()){
                                for(String eachMemberID :memIDs){
                                    for(Account eachAccInstance:memAccByConEmailList){
                                        if(eachMemberID==eachAccInstance.AppNexus_Member_ID__c){
                                            cm = new Case_Member_Account_Relationship__c();
                                            cm.Case__c = cs.Id;
                                            cm.Member_Account__c = eachAccInstance.ID;
                                            caseMemAccList.add(cm); 
                                        } 
                                    }
                                }

                                if(!caseMemAccList.isEmpty()){
                                    try{
                                        insert caseMemAccList;
                                    }catch(Exception e){
                                        system.debug('==Exception=='+e.getMessage());
                                    }

                                }
                            }
                        }  
                    }
                }
                
            }
            return cs.Id;
        }
        catch(Exception ex){
            System.debug(ex.getMessage()+'********%%%%%');
            Exception_Logs__c el = new Exception_Logs__c();
            el.Exception_Message__c = ex.getMessage()+'--'+ex.getCause()+'--'+ex.getLineNumber()+'--'+ex.getStackTraceString();
            insert el;
            return ex.getMessage(); 
        }  
    }
    
    @AuraEnabled
    public static Id uploadFile(String fileName, String base64Data, String contentType,String ParentId) { 
        // base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        Attachment a = new Attachment();
        a.parentId = ParentId;
        a.Body = EncodingUtil.base64Decode(base64Data);
        a.Name = fileName;
        a.ContentType = contentType; 
        insert a;
        System.debug(a.Id);
        return a.Id;
    }
    
    public class TPicklistEntry{
        public string active {get;set;}
        public string defaultValue {get;set;}
        public string label {get;set;}
        public string value {get;set;}
        public string validFor {get;set;}
        public TPicklistEntry(){
            
        }
    }
    
    /*public class caseWrapper {
        public string category;   
        public string severity;
        public String member;
        public Date help;
        public string description;
        public string subject;
        public String invoice;
        public String ccEmail;
        
    }*/
    
    public class userWrapper {
        @AuraEnabled public String userId{get;set;}
        @AuraEnabled public String userProfileName{get;set;}
        @AuraEnabled public String userProfileId {get;set;}
        @AuraEnabled public String AccountId{get;set;}
        @AuraEnabled public String contactId{get;set;}
        
    }
    
    
    public class commentsWrapper {
        @AuraEnabled public List<caseComment> caseCommentsList{get;set;}
        @AuraEnabled public List<EmailMessage> EmailMessagesList{get;set;}
        @AuraEnabled public List<Case> caseList {get;set;}
        public commentsWrapper() {
            this.caseCommentsList = new List<caseComment>();
            this.EmailMessagesList = new List<EmailMessage>();
            this.caseList = new List<Case>();
        }
    }
  
    
    @AuraEnabled
    public static Map<String,String> getMemberAccounts (Id recordId) {        
        Map<String,String> AccMap = new Map<String,String>();
        try{
            List<AccountContactRelation> accountContacts = [SELECT Id, AccountId,Account.AppNexus_Member_ID__c,
                                                            Account.Name,ContactId,contact.Name FROM 
                                                            AccountContactRelation WHERE contactId=: recordId 
                                                            AND IsDirect = false];
            if(!accountContacts.isEmpty()){
                for(AccountContactRelation accConRec : accountContacts){
                    AccMap.put(accConRec.Account.Name+'-'+accConRec.Account.AppNexus_Member_ID__c,accConRec.AccountId);
                }
                return AccMap;
            }else{
                return null;
            }
            
        }catch(Exception e) {
            return null;
            
        }
    } 
    public class chatWrapper{
        @AuraEnabled public string CommentBody{get;set;}
        @AuraEnabled public string CreatedById{get;set;}
        @AuraEnabled public string chatCreatedByProfileName{get;set;}
        @AuraEnabled public string commentCreatedDate{get;set;}
        @AuraEnabled public Id commentId{get;set;}
        @AuraEnabled public string TextBody{get;set;}
        @AuraEnabled public string emailCreatedByProfileName{get;set;}
        @AuraEnabled public string emailcreatedDate{get;set;}
        @AuraEnabled public String emailHtmlBody {get;set;}
        @AuraEnabled public Boolean emailByccMailUser {get;set;}
        @AuraEnabled public String CreatedUserPhotoUrl {get;set;}
        @AuraEnabled public String createdByAlias {get;set;} 
       
    }
    @AuraEnabled
    public static LIST<chatWrapper> getComments(String recordId) {
        Map<Id,user> userMap = new Map<Id,user>([Select id,smallPhotourl From user]);
        commentsWrapper  cw = new commentsWrapper();
        LIST<chatWrapper> chtwrpList = new LIST<chatWrapper>();
        List<DateTime> dateTimesList = new List<Datetime>();
        Map<DateTime,chatWrapper> commentsMap = new  Map<DateTime,chatWrapper>();
        Set<String> ccEmailFields = new Set<String>{'CC_Email_1__c',
                                                    'CC_Email_2__c',
                                                    'CC_Email_3__c',
                                                    'CC_Email_4__c',
                                                    'CC_Email_5__c',
                                                    'CC_Email_6__c',
                                                    'CC_Email_7__c',
                                                    'CC_Email_8__c',
                                                    'CC_Email_9__c',
                                                    'CC_Email_10__c'};
        Set<String> ccEmailsSet = new Set<String>();
        for(Case c : [SELECT Id,CC_Email_1__c,CC_Email_2__c,
                      CC_Email_3__c,CC_Email_4__c,CC_Email_5__c,
                      CC_Email_6__c,CC_Email_7__c,CC_Email_8__c,
                      CC_Email_9__c,CC_Email_10__c FROM Case WHERE id=: recordId]) {
                          for(String eachCCField : ccEmailFields){
                            if(c.get(eachCCField)!=null && c.get(eachCCField)!=''){
                                if(!String.valueOf(c.get(eachCCField)).contains('@appnexus.com')){
                                    ccEmailsSet.add(String.valueOf(c.get(eachCCField)));
                                }
                            }
                          }
                          /*ccEmailsSet.add(c.CC_Email_1__c);
                          ccEmailsSet.add(c.CC_Email_1__c);    
                          ccEmailsSet.add(c.CC_Email_2__c);    
                          ccEmailsSet.add(c.CC_Email_3__c);    
                          ccEmailsSet.add(c.CC_Email_4__c);    
                          ccEmailsSet.add(c.CC_Email_5__c);    
                          ccEmailsSet.add(c.CC_Email_6__c);    
                          ccEmailsSet.add(c.CC_Email_7__c);    
                          ccEmailsSet.add(c.CC_Email_8__c);    
                          ccEmailsSet.add(c.CC_Email_9__c);    
                          ccEmailsSet.add(c.CC_Email_10__c);*/    
        }
        //cw.caseList = '';
        cw.caseCommentsList = [SELECT CommentBody,createdBy.FirstName,CreatedById,createdBy.alias,CreatedBy.Profile.Name,CreatedDate,Id,IsDeleted,IsPublished,LastModifiedById,
                               LastModifiedDate,SystemModstamp FROM CaseComment WHERE ParentId=: recordId AND IsPublished=true ORDER BY CreatedDate DESC];
        cw.EmailMessagesList = [SELECT TextBody,FromAddress,createdBy.FirstName,createdBy.alias,createdBy.smallPhotoUrl,CreatedById,CreatedBy.Profile.Name,createdDate,HtmlBody FROM EmailMessage WHERE ParentId =: recordId ORDER BY CreatedDate DESC ];
        
        for(CaseComment cc :  cw.caseCommentsList) {
            chatWrapper chtwrp = new chatWrapper();
            chtwrp.CommentBody = cc.CommentBody;
            chtwrp.CreatedById = cc.CreatedById;
            chtwrp.chatCreatedByProfileName = cc.CreatedBy.Profile.Name;
            chtwrp.commentId = cc.Id;
            chtwrp.commentCreatedDate = String.valueOf(cc.createdDate);
            chtwrp.createdByAlias = cc.createdby.alias;
            chtwrp.CreatedUserPhotoUrl = userMap.containsKey(cc.createdById)?userMap.get(cc.createdById).smallPhotourl:'';
            commentsMap.put(cc.CreatedDate,chtwrp);
            System.debug(commentsMap.size()+'commentsMap::::::');
        }
        
        for(EmailMessage em : cw.EmailMessagesList) {
            chatWrapper chtwrp = new chatWrapper();
             if(em.HTMlBody==null){
                if(em.TextBody.contains('--------------- Original Message ---------------')){
                    list<String> tempList = new list<String>();
                    tempList = em.TextBody.split('--------------- Original Message ---------------');
                    if(!tempList.isEmpty()){
                        chtwrp.TextBody = tempList[0];
                    }
                }
                else{
                    chtwrp.TextBody = em.TextBody;
                }
             }
            //chtwrp.TextBody = em.TextBody;
            chtwrp.emailHtmlBody = em.HTMlBody;
            chtwrp.emailCreatedByProfileName = em.CreatedBy.Profile.Name;
            chtwrp.CreatedUserPhotoUrl = em.createdBy.smallPhotoUrl;
            chtwrp.createdByAlias = em.createdBy.alias;
            if(ccEmailsSet.contains(em.FromAddress)) {
               chtwrp.emailByccMailUser = true;
            }
            commentsMap.put(em.CreatedDate,chtwrp);
            System.debug(commentsMap.size()+'emailMessagesMap::::::');
        }
        
        for(DateTime dt : commentsMap.keySet()) {
            dateTimesList.add(dt);
        }
        dateTimesList.sort();
        
        List<DateTime> finaldateTimesList = new List<Datetime>();
        
        for(Integer i = dateTimesList.size()-1; i>=0;i--)
        {
            finaldateTimesList.add(dateTimesList.get(i));
        }
        
        
        
        for(DateTime dt : finaldateTimesList) {
            chtwrpList.add(commentsMap.get(dt));
        }
        System.debug(chtwrpList+'******'+chtwrpList.size());
        return chtwrpList; 
    }
    
    @AuraEnabled
    public static void creatComment(String CaseParentId,String caseComment) {
        CaseComment cc = new CaseComment();
        cc.CommentBody = caseComment;
        cc.ParentId = CaseParentId;
        cc.IsPublished = true;
        insert cc;
        
    }
}