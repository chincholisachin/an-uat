/*
Testing Utilities to provide a common area for similar testing functions that span several test methods
Authored by: Alper Tovi
*/

@isTest
public class TestingUtils {

    private static Boolean adminUserLoaded = false;
    private static User adminUser;
    public static Boolean profileMapLoaded = false;
    private static Map<String,String> profilemap;
    
    
      public static User ADMIN_USER
    {
        get
        {
            if( null == ADMIN_USER )
            {
                ADMIN_USER = [SELECT Id, ProfileId FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true LIMIT 1 ];
            }
            return ADMIN_USER;
        }
    }
    
    public static List<Case> createCases(Integer numCases, String status, User Owner, Boolean doInsert)
    {

        //It is necessary to use See All Data for any test that requires a case insertion
        List<Case> cases = new List<Case>();
        {
            Account acc = new Account(Name= 'test account'/*, Active_Client_Segment__c = 'test', Active_Client_Sub_Segment__c = 'MM | Strategic supply'*/);
            for(Integer i=0;i<numCases; i++)
            {

                cases.add(new Case(AccountId = acc.Id, Status = status, OwnerId = Owner.Id,Priority='Emergency',Subject='Test1',emailFlag__c=False));
            }
        }
        if(doInsert) insert cases;
        return cases;
    }


    public static void createEssentialCustomSettings(){
    
        Account_Record_Type__c[] artList = new Account_Record_Type__c[]{};
        artList.add(new Account_Record_Type__c(Name = 'Customer Account', isCustomer__c = true, isMember__c = false, isInternal__c = false));
        artList.add(new Account_Record_Type__c(Name = 'Console Member', isMember__c = true, isCustomer__c = false, isInternal__c = false));
        artList.add(new Account_Record_Type__c(Name = 'Internal Entity', isInternal__c = true, isCustomer__c = false, isMember__c = false));
        insert artList;    
    
    }
    
    public static void createCustomSetting(String setting){
        if (setting == 'accountTeamModify'){
            Account_Team_Modify__c atm = new Account_Team_Modify__c(financeroles__c = true, otherroles__c = true, ownerroles__c = true, supportroles__c = true, setupownerid = UserInfo.getUserId()); 
            insert atm;
        } else if (setting == 'countryStateCodes'){
            CountryISOCodes__c cnt = new CountryISOCodes__c(Name = 'USA', ISO3__c = 'US');
            insert cnt;
            StateCodes__c stt = new StateCodes__c(Name = 'CALIFORNIA', StateCd__c = 'CA');
            insert stt;
        }
    }

    //Method returns an admin User
    public static User getAdminUser(){
        if (adminUserLoaded) return adminUser;
        User u = [SELECT Id, ProfileId FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true LIMIT 1];
        adminUser = u;
        adminUserLoaded = true;
        return adminUser;
    }
    
    //Method returns a user from given ProfileName
    public static User getUser(String profileName){
        User u = [SELECT Id, ProfileId FROM User WHERE Profile.Name =:profileName AND isActive = true LIMIT 1];
        return u;
    }
    
    //Method returns a profile map (name as key, id as value), accepts license type as an argument. Use 'Salesforce' for standard and custom Salesforce profiles.
    public static Map<String,String> getProfile(String licenseType){
        if (profileMapLoaded) return profilemap;
        Map<String, String> profileList = new Map<String,String>();
        for (Profile p : [SELECT Id, Name FROM Profile WHERE UserLicense.Name =:licenseType]){
            profileList.put(p.name,p.id);
        }
        profileMap = profileList;
        profileMapLoaded = true;
        return profileMap;
    }
    
    //Method returns a list of users from given profile
    public static List<User> createUsers(Integer numUsers, String UserName, String lastName, String profileName, Boolean doInsert){
       
        RecordType internal = [select id, name from recordtype where sobjecttype = 'Account' and name = 'Internal Entity' limit 1]; 
        Account[] employeeAccount = TestingUtils.createAccounts(1, 'testAccount', false);
        employeeAccount[0].name = 'AppNexusEmployees';
        employeeAccount[0].recordtypeid = internal.id;
        //insert employeeAccount;

        //Contact con = new Contact(Firstname = 'CSP', LastName = 'Dummy', accountid = employeeAccount[0].id);
        Contact con = new Contact(Firstname = 'CSP', LastName = 'Dummy');
        insert con;
        
        List<User> newUsers = new List<User>();
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey).substring(0,6);
        for(integer i=0;i<numUsers;i++){
            
            newUsers.add( new User( Username = UserName + key + i + '@email.com',
                                    Email = 'usernameSE@email.com',
                                    LastName = lastName,
                                    ProfileID = getProfile('Salesforce').get(profileName),
                                    Alias = 'alias',
                                    LocaleSidKey = 'en_US',
                                    LanguageLocaleKey = 'en_US',
                                    TimeZoneSidKey = 'America/Los_Angeles',
                                    EmailEncodingKey='UTF-8',
                                    isActive = true
                                    ));
        }
        if(doInsert) {
            //insert newUsers;
            //System.assertEquals(numUsers, newUsers.size());
        }
        return newUsers;
    }
    
    //Method returns a list of Accounts
    public static List<Account> createAccounts(Integer numAccts, String acctNamePreFix, Boolean doInsert){
        List<Account> accounts = new List<Account>();
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey).substring(0,6);
        for(Integer i=0;i<numAccts;i++){
            accounts.add(new Account(Name= acctNamePrefix +key+ i));
        }
        if(doInsert) {
            insert accounts;
            System.assertEquals(numAccts, accounts.size());
        }
        return accounts;
    }

    //Method returns a list of Opportunities
    public static List<Opportunity> createOpportunities(Integer numOpps, String oppName, String salesStage, Boolean doInsert){
        List<Opportunity> opps = new List<Opportunity>();
        for(Integer i=0;i<numOpps;i++){
            opps.add(new Opportunity(Name= oppName + i, stageName = salesStage, closeDate = System.date.today(), Monthly_Revenue_Opportunity__c = 1000 ));
        }
        if(doInsert) {
            insert opps;
            System.assertEquals(numOpps, opps.size());
        }
        return opps;    
    }
    
    public static List<Contact> createContacts(Integer numCon, String lastName, Boolean doInsert){
        List<Contact> con = new List<Contact>();
        
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey).substring(0,6);
        
        for(Integer i=0;i< numCon;i++){
            con.add(new Contact(LastName= lastName +key+i ));
        }
        if(doInsert) {
            insert con;
            System.assertEquals(numCon, con.size());
        }
        return con;    
    }
    
    //Method returns a list of Leads
    public static List<Lead> createLeads(Integer numLeads, String lastNamePrefix, String companyNamePrefix, String leadStatus, Boolean doInsert){
        List<Lead> leads = new List<Lead>();
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey).substring(0,6);
        for(Integer i=0;i< numLeads;i++){
            leads.add(new Lead(LastName= lastNamePrefix + key + i, Company = companyNamePrefix + key + i, Status = leadStatus ));
        }
        if(doInsert) {
            insert leads;
            System.assertEquals(numLeads, leads.size());
        }
        return leads;    
    }
    
    //creates test Users, accounts, opportunities and leads.
    /*
    public static testMethod void createRecords(){
    
           createEssentialCustomSettings();
           User[] users = TestingUtils.createUsers(5, 'testUser', 'testUser', '* Commercial', true);
           //Account[] accounts = TestingUtils.createAccounts(100, 'testAccount', true);
           //Opportunity[] opps = TestingUtils.createOpportunities(100, 'testOpportunity', 'Discovery', false);
           //for (Integer i=0;i< 100;i++){
           //    opps[i].accountid = accounts[i].id;
           //}
           //insert opps;
           //System.assertEquals(100,opps.size());
           //Lead[] leads = TestingUtils.createLeads(100, 'TestLead', 'TestCompany', 'Open', true);
    }
    */
     
     
     
    public static void createCustomSettings()
    {
        emailSettings__c settings = emailSettings__c.getOrgDefaults();
        if(settings.Org_Wide_Email__c == null)
        {
            //Account testAccount = createAccounts(1, 'TUser', true)[0];
            System.runAs( TestingUtils.ADMIN_USER )
            {
                settings = new emailSettings__c();
                //settings.AppNexus_Account__c = testAccount.Id;
                settings.Automate_Email_Handling__c = true;
                settings.Case_Close_Template__c = 'Case Closure - Surv';
                settings.Case_Email_Priority__c = 'Emergency';
                settings.Case_Insert_Template__c = 'Case Creation';
                settings.Case_Priority__c = 'Emergency';
                settings.Email_Domain_Exclusion__c = '';
                settings.Emergency_Email__c = 'chrisshiftcrm@gmail.com';
                settings.Job_Id__c = '08eZ0000000kgPQ';
                settings.Org_Wide_Email__c = 'customer-support@appnexus.com';
                settings.Record_Type_Exclusion__c = 'Known Issue,SWAT Ops,Sales Ops';
                settings.Routing_Email__c = 'customer-support@appnexus.com';
                settings.Time_Interval__c = 5;
                insert settings;
            }
        }
        
        List<HardCodeIds__c>  hd1 = new List<HardCodeIds__c>();
        hd1 = [select id from HardCodeIds__c  where name = 'BusHrsID'];
        if( hd1.size() == 0 ){
            System.runAs( TestingUtils.ADMIN_USER )
            {
                 HardCodeIds__c setting1 = new HardCodeIds__c();
                 setting1.Name = 'BusHrsID';
                 setting1.value__c = '01mA0000000QEmb';
                 insert setting1 ;
            }
           }
        List<HardCodeIds__c>  hd2 = new List<HardCodeIds__c>();
        hd2 = [select id from HardCodeIds__c  where name = 'CommSeverityReq'];
        if( hd2.size() == 0 ){
            System.runAs( TestingUtils.ADMIN_USER )
            {
                 HardCodeIds__c setting2 = new HardCodeIds__c();
                 setting2.Name = 'CommSeverityReq';
                 setting2.value__c = 'Access; API; Campaign; Pixels; Placement; Something_Else; Creative:Technical_Support Creative:Something_else Domain:Something_else Reporting:Discrepancies Reporting:Something_else';
                 insert setting2 ;
            }
           }
        
    }

}