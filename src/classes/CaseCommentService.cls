public  class CaseCommentService {
	public static boolean isRunning;
	private static Datetime completionDate = Datetime.Now();
	public static list<Id> caseIDList = new list<ID>();
	
	public static void updateCaseMilestone(list<CaseComment> newList){

		for(CaseComment eachCaseComment :newList){
			caseIDList.add(eachCaseComment.ParentID);
		}

		if(!caseIDList.isEmpty()){
			ServiceFacade.completeMilestone(caseIDList,CONSTANTS.FIRST_RESPONSE,completionDate);
		}
		
	}
}