public with sharing class FixOpportunities {

    private Map<Id,Opportunity> dupeCheck {get;set;}
    private Map<String, String> exceptionList {get;set;}
    public List<OppIssue> opIssue {get;set;}
    Integer timeDuration = -120;

    public FixOpportunities(ApexPages.StandardController controller) {   
        exceptionList = new Map<String, String>();
        dupeCheck = new Map<Id, Opportunity>();
        prePopulateOppIssueList();
    }
    
    public class OppIssue{       
        public String issue {get; set;}      
        public Opportunity opp {get; set;}
        public OppIssue(){
            if(opp ==null) opp = new Opportunity();
        }  
    }
    
    public void prePopulateOppIssueList(){
        if (opIssue == null) {
            opIssue = new List<OppIssue>();        
            Date dt = Date.today().addDays(timeDuration);
            List<Opportunity> opps = new List<Opportunity>([select id, 
                              accountid, 
                              name, 
                              Reason_for_New_Opp__c, 
                              stagename, 
                              closedate, 
                              amount,
                              Opportunity_Overview__c, 
                              Loss_Reason_rdp__c,
                              Stage_change_date__c from
                              opportunity where 
                              (Stage_change_date__c <= :dt or CloseDate <: Date.today()) and 
                              stagename != 'Identification' and 
                              stagename != 'Closed Won' and 
                              stagename != 'Closed Lost' and 
                              ownerid =: UserInfo.getUserId() order by account.Name]);
            for (Opportunity o: opps){
                OppIssue oi = new OppIssue();
                oi.opp = o;
                oi.issue = populateIssue(o);
                dupeCheck.put(o.id,o);
                opIssue.add(oi);             
            }          
            if (opps.size() == 0) ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Congratulations! You don\'t have any opportunities to fix.'));                 
        }else{
            List<OppIssue> refreshList = new List<OppIssue>();
            for (OppIssue oi: opIssue){
                if (exceptionList.containsKey(oi.opp.id)) {
                    oi.issue = populateIssue(oi.opp);
                    refreshList.add(oi);
                } 
            }
            if (refreshList.isEmpty()) {
                opIssue.clear();
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Congratulations! You don\'t have any opportunities to fix.'));
            } else {
                opIssue = refreshList;
            }
        }
    }
       
    public String populateIssue(Opportunity o){
        String issue;
        if (o.closeDate < Date.today()) issue = 'Past Close Date';
        Date dt = Date.today().addDays(timeDuration);
        if (o.Stage_change_date__c <= dt && issue != null) issue = issue + ',Stalled Stage';
        if (o.Stage_change_date__c <= dt && issue == null) issue = 'Stalled Stage';
        return issue;
    }
                
    public void saveChanges(){   
        if (exceptionList.size()>0) exceptionList.clear();
        try{
            List<Opportunity> updateAllOpps = new List<Opportunity>();
            if (opIssue.size() > 0 ){
                for (OppIssue oi: opIssue){
                    updateAllOpps.add(oi.opp);
                }
            }
            if (!updateAllOpps.isEmpty()) {
                Database.SaveResult[] srList = Database.Update(updateAllOpps, false);
                Map<Id,Id> successOpps = new Map<ID,Id>(); 
                for (Database.SaveResult sr : srList) {
                    if (sr.isSuccess()) ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,dupeCheck.get(sr.getId()).Name + ' is updated. If you don\'t see it below you are good to go. If you still see it, please check the issue column.'));
                    successOpps.put(sr.getId(),sr.getId());
                }
                List<Opportunity> remainingOpps = new List<Opportunity>();
                for (Opportunity o : updateAllOpps){
                    if(!successOpps.containsKey(o.id)) remainingOpps.add(o);
                }
                if (remainingOpps.size()>0) update remainingOpps;
            }
        } catch (System.DmlException e) {          
            for (Integer i = 0; i < e.getNumDml(); i++) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,dupeCheck.get(e.getDmlId(i)).Name + ' - ' + e.getDmlMessage(i)));
                exceptionList.put(e.getDmlId(i),e.getDmlId(i));
            }
        }      
        Date dt = Date.today().addDays(timeDuration);
        for(Opportunity o : [Select id From Opportunity where (Stage_change_date__c <= :dt or CloseDate <: Date.today()) and 
                              stagename != 'Identification' and 
                              stagename != 'Closed Won' and 
                              stagename != 'Closed Lost' and 
                              ownerid =: UserInfo.getUserId()]) exceptionList.put(o.id, o.id);
        prePopulateOppIssueList();                   
    }   
}