public without sharing class ConsoleUserTriggerHandler{

    private static Map<String,Account> memberToAccount = new Map<String,Account>();
    private static Map<String,String> userToContact = new Map<String,String>();
    private static List<contactAndMemberRelationship> contactAndMemberToProcess = new List< contactAndMemberRelationship>();    
    private static List<leadAndMemberRelationship > leadAndMemberToProcess = new List< leadAndMemberRelationship>();
    public static List<acrProcess> acrProcessList = new List< acrProcess>();
    public static Boolean firstRun = true;
    public static String CANNOT_FIND_MATCHING_MEMBER = 'Cannot find a matching member for this user';
    public static String INACTIVE_USER = 'This user account is inactive.';
    public static String DUPLICATE_RECORD = 'This contact already has a relationship with this member.';

    private static Contact createNewContact(String first,String last,String emailaddy, String acn){
        return new Contact(FirstName = first, LastName = last, Email = emailaddy, Accountid = acn);
    }
    
    private static AccountContactRelation createNewACR (String acn,String con,String role, String username){
        return new AccountContactRelation(contactid = con,Accountid = acn,isactive = true,roles = role, Console_Username__c = username);
    }
    
    //wrapper class to contain contact record, memberid, console user record, userrole.
    private class contactAndMemberRelationship{
        private Contact con;
        private String memberid;
        private Console_User__c consoleUserRecId;
        private String userRole;
        contactAndMemberRelationship(){
            if (con == null) con = new Contact();
            if (memberid == null) memberid = '';
            if (consoleUserRecId == null) consoleUserRecId = new Console_User__c();
            if (userRole == null) userRole = '';
        }
    }
    
    //wrapper class to contain record, memberid, console user record, userrole.
    private class leadAndMemberRelationship{
        private Lead ld;
        private String memberid;
        private Console_User__c consoleUserRecId;
        private String userRole;
        leadAndMemberRelationship(){
            if (ld == null) ld = new Lead();
            if (memberid == null) memberid = '';
            if (consoleUserRecId == null) consoleUserRecId = new Console_User__c();
            if (userRole == null) userRole = '';
        }
    }
    
    //wrapper class to contain accountcontactrelation record, console user record, contact record. 
    private class acrProcess{
        private AccountContactRelation acr;
        private Console_User__c consoleUserRecId;
        private Contact con;
        acrProcess(){
            if (acr == null) acr = new AccountContactRelation();
            if (consoleUserRecId == null) consoleUserRecId = new Console_User__c();
            if (con == null) con = new Contact();
        }
    }
    
    public static void processRecords(List<Console_User__c> cuNew, Map<Id,Console_User__c> cuOld){
            
        if(firstRun){        
            
            Set<String> emails = new Set<String>();
            Set<String> memberIds = new Set<String>();
            for (Console_User__c cu: cuNew){
                if (cu.Active__c && cu.SyncStatus__c == 'Pending'){
                    emails.add(cu.email__c);
                    memberIds.add(cu.EntityId__c); 
                } else if (!cu.Active__c && cu.SyncStatus__c =='Pending' && (cuOld == null || (cuOld != null && cuOld.get(cu.id).active__c == cu.Active__c))){
                    cu.SyncStatus__c = 'Success';
                    cu.Errormessage__c = INACTIVE_USER;
                }
                
            }

            memberToAccount = memberIdAccountIdConversion(memberIds);
            userToContact = isUserALeadOrContact(emails, memberIds);
            
            List<Contact> conToUpdate = new List<Contact>();
            List<AccountContactRelation> acrToDelete = new List< AccountContactRelation >();
            List<AccountContactRelation> acrToInsert = new List< AccountContactRelation >();
            List<AccountContactRelation> acrToUpdate = new List< AccountContactRelation >();
                       
            for (Console_User__c cu: cuNew){
                if (cu.SyncStatus__c == 'Pending' && cu.Active__c && !memberToAccount.containsKey(cu.entityid__c)){
                    cu.syncstatus__c = 'Fail';
                    cu.ErrorMessage__c = CANNOT_FIND_MATCHING_MEMBER;
                    cu.active__c = false;
                } else if (cu.SyncStatus__c == 'Pending' && cu.active__c && cu.contact__c == null && cu.AccountContactRelationshipId__c == null){
                        if (!userToContact.containsKey(cu.email__c)){ 
                            contactAndMemberRelationship cmr = new contactAndMemberRelationship();
                            cmr.con = createNewContact(cu.firstname__c,cu.lastname__c,cu.email__c, memberToAccount.get(cu.entityId__c).parentid);
                            cmr.memberid = cu.entityid__c;
                            cmr.userRole = cu.userRole__c;
                            cmr.consoleUserRecId = cu;
                            contactAndMemberToProcess.add(cmr);       
                        } else if (userToContact.get(cu.email__c).startsWith('00Q')){ //email is found in a lead
                            leadAndMemberRelationship cmr = new leadAndMemberRelationship();
                            cmr.ld = new Lead(id = Id.valueOf(userToContact.get(cu.email__c)),firstname=cu.firstname__c,lastname=cu.lastname__c,email=cu.email__c);
                            cmr.memberid = cu.entityid__c;
                            cmr.userRole = cu.userRole__c;
                            cmr.consoleUserRecId = cu;
                            leadAndMemberToProcess.add(cmr);
                        } else if (userToContact.get(cu.email__c).startsWith('003')) { // email is found in a contact and contact has an account
                            List<String> idValues = userToContact.get(cu.email__c).split(',');
                            cu.contact__c = Id.valueOf(idValues[0]);
                            acrProcess acrRec = new acrProcess();
                            acrRec.acr = createNewACR (memberToAccount.get(cu.entityId__c).id,idValues[0],cu.userrole__c, cu.Username__c);
                            acrRec.consoleUserRecId = cu;
                            acrRec.con = new Contact(id = Id.valueOf(idValues[0]), firstname = cu.firstname__c, lastname = cu.lastname__c);
                            if (acrRec.con.accountid == null ) acrRec.con.accountid = memberToAccount.get(cu.entityid__c).parentid;
                            acrProcessList.add(acrRec);                   
                        }
                } else if (cu.syncstatus__c == 'Pending' && cu.active__c && cu.Contact__c != null  && cuOld != null){
                    if (cu.firstname__c != cuOld.get(cu.id).firstName__c || cu.lastname__c != cuOld.get(cu.id).lastname__c || cu.email__c != cuOld.get(cu.id).email__c){
                        conToUpdate.add(new Contact(id = cu.contact__c, firstname = cu.firstname__c, lastname = cu.lastname__c));
                        cu.SyncStatus__c = 'Success';
                        cu.Errormessage__c = null;
                    }
                    if (cu.UserRole__c != cuOld.get(cu.id).userrole__c && cu.AccountContactRelationshipId__c != null){
                        acrToUpdate.add(new AccountContactRelation (id = cu.AccountContactRelationshipId__c, roles = cu.UserRole__c));
                        cu.SyncStatus__c = 'Success';
                        cu.Errormessage__c = null;
                    }
                    if (cu.active__c != cuOld.get(cu.id).active__c && cu.AccountContactRelationshipId__c == null){
                        acrProcess acrRec = new acrProcess();
                        acrRec.acr = createNewACR (memberToAccount.get(cu.entityId__c).id,cu.contact__c,cu.userrole__c, cu.Username__c);
                        acrRec.consoleUserRecId = cu;
                        acrRec.con = new Contact(id = cu.contact__c);
                        if (acrRec.con.accountid == null ) acrRec.con.accountid = memberToAccount.get(cu.entityid__c).parentid;
                        acrProcessList.add(acrRec);  
                    } 
                    if (cu.active__c == cuOld.get(cu.id).active__c && 
                        cu.firstname__c == cuOld.get(cu.id).firstName__c && 
                        cu.lastname__c == cuOld.get(cu.id).lastname__c && 
                        cu.email__c == cuOld.get(cu.id).email__c &&
                        cu.userrole__c == cuOld.get(cu.id).userrole__c && cu.AccountContactRelationshipId__c != null) {
                        cu.SyncStatus__c = 'Success';
                        cu.Errormessage__c = null;
                    }

                } else if (cu.syncstatus__c == 'Pending' && !cu.active__c && cu.active__c != cuOld.get(cu.id).active__c && cu.AccountContactRelationshipId__c != null ){
                        acrToDelete.add(new AccountContactRelation (id = cu.AccountContactRelationshipId__c));
                        cu.AccountContactRelationshipId__c = null;
                        cu.SyncStatus__c = 'Success';
                        cu.Errormessage__c = null;
                }                               
            }
            
            if (conToUpdate.size()>0) Database.update(conToUpdate,false);
            if (acrToDelete.size()>0) Database.delete(acrToDelete,false);
            if (acrToUpdate.size()>0) Database.update(acrToUpdate,false);
        }
    }
    
    public static Map<String,String> isUserALeadOrContact(Set<String> emails, Set<String> memberIds){        

        Map<String,String> emailConversion = new Map<String,String>();
        Set<String> secondPass = new Set<String>();
        Set<String> thirdPass = new Set<String>();
        secondPass.addAll(emails);  
        Contact[] c = [select id, email, accountid from contact where email in: emails and accountid in (select parentid from account where appnexus_member_id__c in :memberIds)];
        for (Contact con: c){
            if (!emailConversion.containsKey(con.email)) {
                emailConversion.put(con.email,String.valueOf(con.id) + ',' + String.valueOf(con.accountid));
                secondPass.remove('con.email');
            }
        }
        thirdPass.addAll(secondPass);
        c.clear();
        c = [select id, email, accountid from contact where email in: secondPass]; 
        for (Contact con: c){
            if (!emailConversion.containsKey(con.email)) {
                emailConversion.put(con.email,String.valueOf(con.id) + ',' + String.valueOf(con.accountid));
                thirdPass.remove('con.email');
            }
        }
        Lead[] l = [select id, firstname, lastname, email from lead where email in: thirdPass and isconverted = false];       
        for (Lead le: l){
            if (!emailConversion.containsKey(le.email)) emailConversion.put(le.email,le.id);
        }
        return emailConversion;            
    }
    
    private static Map<String,Account> memberIdAccountIdConversion(Set<String> memberIds){   

        Map<String,Account> memberToAcn = new Map<String,Account>();
        for(Account a: [select appnexus_member_id__c, id, parentId, parent.ownerid from account where appnexus_member_id__c in :memberIds]){
            memberToAcn.put(a.appnexus_member_id__c,a);
        }
        return memberToAcn;            
    }
    
    //function to create records.
    //this function is called by the after trigger, once records are created. 
    public static void createRecords(){
    
        if (firstRun){
            firstRun = false;
            
            //creates contact records.
            if (leadAndMemberToProcess != null && leadAndMemberToProcess.size()>0){
                List<acrProcess> acrFromConvertedLeads = new List <acrProcess>();
                acrFromConvertedLeads = convertAllLeads(leadAndMemberToProcess);
                acrProcessList.addAll(acrFromConvertedLeads);
            }
            
            if (contactAndMemberToProcess.size()>0 || acrProcessList.size()>0){
                List<Contact> conList = new List<Contact>();
                Set<Contact> conSetToUpdate = new Set<Contact>();
                List<Contact> conListToUpdate = new List<Contact>();
                List<AccountContactRelation> acrList = new List<AccountContactRelation>();
                Map<String,String> acrListMap = new Map<String,String>();
                List<Console_User__c> cons = new List<Console_User__c>();
                Map<String,String> contactDupeCheck = new Map<String,String>();
                
                for (contactAndMemberRelationship cm: contactAndMemberToProcess){
                    if (!contactDupeCheck.containsKey(cm.con.email)){
                        contactDupeCheck.put(cm.con.email,'');
                        conList.add(cm.con);
                    }
                }
                
                //DML 1
                if (conList.size()> 0) insert conList;
                
                for (Contact c: conList){
                    contactDupeCheck.put(c.email,c.id);
                }
                
                for (contactAndMemberRelationship cm: contactAndMemberToProcess){
                    if (cm.con.id != null) {
                        acrList.add(createNewACR (memberToAccount.get(cm.memberid).id,cm.con.id,cm.userRole,cm.consoleUserRecId.username__c));
                    } else {
                        acrList.add(createNewACR (memberToAccount.get(cm.memberid).id,contactDupeCheck.get(cm.con.email),cm.userRole,cm.consoleUserRecId.username__c));
                    }
                }
                
                for (acrProcess acr: acrProcessList){
                    acrList.add(acr.acr);
                    conSetToUpdate.add(acr.con);
                }
                
                //DML 2
                if (conSetToUpdate.size()>0) {
                    Set<String> dupeCheck = new Set<String>();
                    for (Contact c: conSetToUpdate){
                        if (!dupecheck.contains(c.id)){
                            dupeCheck.add(c.id);
                            conListToUpdate.add(c);
                        } 
                    }
                    Database.update(conListToUpdate,false);
                }   
                                
                //DML 3
                Set<String> dupeCheckAcr = new Set<String>();
                List<AccountContactRelation> hygenelist = new List<AccountContactRelation>();

                for (AccountContactRelation acr: acrList){
                    if (!dupeCheckAcr.contains(acr.contactid+'-'+acr.Accountid)){
                        dupeCheckAcr.add(acr.contactid+'-'+acr.Accountid);
                        hygenelist.add(acr);
                    }
                }
                    
                if (hygenelist.size() > 0) Database.Insert(hygenelist, false);         
                

                for (acrProcess acr: acrProcessList){
                    Console_User__c cu = new Console_User__c();
                    if (acr.acr.id != null) {
                        cu.id = acr.consoleUserRecId.id; 
                        cu.syncstatus__c = 'Success'; 
                        cu.contact__c = acr.acr.contactid; 
                        cu.AccountContactRelationshipId__c = acr.acr.id;
                    } else {
                        cu.id= acr.consoleUserRecId.id; 
                        cu.syncstatus__c = 'Fail'; 
                        cu.contact__c = acr.acr.contactid; 
                        cu.active__c = false;
                        cu.ErrorMessage__c = DUPLICATE_RECORD;
                    }
                    cons.add(cu);   
                }
                
                for (AccountContactRelation acr: hygenelist){
                    acrListMap.put(acr.contactid+'-'+acr.accountid,acr.id);
                }
                
                for (contactAndMemberRelationship cm: contactAndMemberToProcess){
                    Console_User__c cu = new Console_User__c();

                    String relationshipid = (cm.con.id != null) ? acrListMap.get(cm.con.id+'-'+memberToAccount.get(cm.memberid).id) : acrListMap.get(contactDupeCheck.get(cm.con.email)+'-'+memberToAccount.get(cm.memberid).id)  ;

                    //if (relationshipid != null) {
                    if (cm.con.id != null) {
                        cu.id = cm.consoleUserRecId.id; 
                        cu.syncstatus__c = 'Success'; 
                        cu.contact__c = cm.con.id != null ? cm.con.id : contactDupeCheck.get(cm.con.email); 
                        cu.ErrorMessage__c = null;
                        cu.AccountContactRelationshipId__c = relationshipid;
                    } else {
                        cu.id = cm.consoleUserRecId.id; 
                        cu.syncstatus__c = 'Fail'; 
                        cu.contact__c = cm.con.id != null ? cm.con.id : contactDupeCheck.get(cm.con.email); 
                        cu.active__c = false;
                        cu.ErrorMessage__c = DUPLICATE_RECORD;
                    }
                    cons.add(cu);
                }                
                //DML 4
                if (cons.size()>0 ) update cons;                           
            }          
        }
    }
           
    private static List<acrProcess> convertAllLeads (List<leadAndMemberRelationship> lmr){
        
        List<Database.LeadConvert> leadsToConvert = new List<Database.LeadConvert>();
        Map<String, leadAndMemberRelationship> leadIdToLMR = new Map<String, leadAndMemberRelationship>();
        List<acrProcess> acrProcessToReturn = new List<acrProcess>();     
        
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];

        for (leadAndMemberRelationship lmTemp: lmr){
            leadIdToLMR.put(lmTemp.ld.id, lmTemp);
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(lmTemp.ld.id);
            lc.setAccountId(memberToAccount.get(lmtemp.memberid).parentid);
            lc.setConvertedStatus(convertStatus.MasterLabel);
            lc.setDoNotCreateOpportunity(true);
            lc.setOwnerId(memberToAccount.get(lmtemp.memberid).parent.ownerId);
            lc.setSendNotificationEmail(false);
            leadsToConvert.add(lc);
        }
        
        List<Console_User__c> cuListError = new List<Console_User__c>();
        Set<String> dupeCheck = new Set<String>();
        AccountUtils.leadIsBeingConverted = true;
        List<Database.LeadConvertResult> lcr = Database.convertLead(leadsToConvert, false); 
        for (Database.LeadConvertResult lc: lcr){
            if (lc.isSuccess()){
                acrProcess acrP = new acrProcess();
                acrP.acr = createNewACR(memberToAccount.get(leadIdToLMR.get(lc.getLeadId()).memberId).id,lc.getContactId(),leadIdToLMR.get(lc.getLeadId()).userRole,leadIdToLMR.get(lc.getLeadId()).consoleUserRecId.username__c);
                acrP.consoleUserRecId = leadIdToLMR.get(lc.getLeadId()).consoleUserRecId;
                acrP.con = new Contact(id = lc.getContactId(), email= leadIdToLMR.get(lc.getLeadId()).consoleUserRecId.email__c, firstname = leadIdToLMR.get(lc.getLeadId()).consoleUserRecId.firstname__c, lastname = leadIdToLMR.get(lc.getLeadId()).consoleUserRecId.lastname__c);
                acrProcessToReturn.add(acrP);
            } else {
                if (!dupeCheck.contains(leadIdToLMR.get(lc.getLeadId()).consoleUserRecId.id)){
                    dupeCheck.add(leadIdToLMR.get(lc.getLeadId()).consoleUserRecId.id);
                    cuListError.add(new Console_User__c(id = leadIdToLMR.get(lc.getLeadId()).consoleUserRecId.id, Active__c = false, SyncStatus__c = 'Fail', ErrorMessage__c = lc.getErrors().get(0).message.left(255)));               
                }
            }
        }
        if ( cuListError.size() > 0 ) Database.update(cuListError,false);
        return acrProcessToReturn;
    }
}