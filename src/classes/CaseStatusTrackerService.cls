public  class CaseStatusTrackerService {
	

	public static Boolean isRunning = false;
	public static void updateAgeByBusinessHours(list<Case_Status_Tracker__c> newList){

		Id defaultHrs;
    	Id nyHours;

    	defaultHrs = Utility.getHardCodeIDCSValueByName(CONSTANTS.BUS_HRS_ID);

    	if(defaultHrs==null){
    		defaultHrs = ServiceFacade.getDefaultBusinessHours().ID;
    	}

    	nyHours = Utility.getHardCodeIDCSValueByName(CONSTANTS.NY_BUS_HRS_ID);

    	if(nyHours==null){
    		nyHours = ServiceFacade.getBusinessHoursByName(CONSTANTS.NY_BUS_HRS_NAME).ID;
    	}


		for (Case_Status_Tracker__c  tmpCST :newList) {
		    if( tmpCST.End_Time__c != null ){
		          tmpCST.Age_Business_Hours__c =ServiceFacade.getDifferenceInBusinessHours(defaultHrs, tmpCST.Start_Time__c, tmpCST.End_Time__c);
		          tmpCST.Age_NY_Business_Hours__c = ServiceFacade.getDifferenceInBusinessHours(nyHours, tmpCST.Start_Time__c, tmpCST.End_Time__c);
		    }
	    }
	}
}