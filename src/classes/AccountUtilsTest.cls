@isTest
public class AccountUtilsTest{

    public static testMethod void isInternalTest(){
            
        TestingUtils.createEssentialCustomSettings();
        RecordType rt = [select id,name from recordtype where name = 'Customer Account' limit 1];
        System.assertEquals(AccountUtils.isInternal().get(rt.id),false);
        RecordType rt2 = [select id from recordtype where name = 'Internal Entity' limit 1];
        System.assertEquals(AccountUtils.isInternal().get(rt2.id),true);

    }
    
    public static testMethod void testAccountTeamModifyPermissions(){
    
        TestingUtils.createCustomSetting('accountTeamModify');
        Map<String,Boolean> testMap = AccountUtils.getPermissionToEditRole();
        for (String s : GeneralUtils.getPicklistValues('AccountTeamMember','TeamMemberRole')){
            System.assertEquals(true,testMap.get(s));
        }
    
    }
}