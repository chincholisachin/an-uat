public with sharing class RainmakerController {

    public SelectOption[] selectedMembers { get; set; }
    public SelectOption[] allMembers { get; set; }
    public SelectOption[] ownerList { get; set; }
    public String memberId {get; set; }
    public String message { get; set; }
    public Boolean cancel { get; set; }
    public RainMakerRequest__c rainMaker { get; set; }
   
   
    public RainmakerController() {
        cancel = true;
        memberId = System.currentPageReference().getParameters().get('Id');
        //previouspage = System.currentPageReference().getParameters().get('retURL​');
        rainMaker = new RainMakerRequest__c();
        selectedMembers = new List<SelectOption>();
        
        //List<AppNexus_Member__c> ANmembers = [SELECT Name, Id, account__r.Ownerid FROM AppNexus_Member__c where EDP_Flag__c = true];    
        List<Account> ANmembers = [SELECT Name, recordtypeid, Id, parent.Ownerid FROM Account where EDP_Flag__c = true];
        allMembers = new List<SelectOption>();
        ownerList = new List<SelectOption>();
        if (ANmembers.size() > 0) {
            for ( Account AN : ANmembers ) {
                if (AccountUtils.isMember().get(AN.RecordTypeId)) allMembers.add(new SelectOption(AN.Id, AN.Name));
            }
        }
    }
    
    public List<SelectOption> SellerTypeValues{
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = RainMakerRequest__c.Seller_Type__c.getDescribe();
            List<Schema.PicklistEntry> newbus = fieldResult.getPicklistValues();
            for (Schema.PicklistEntry pe : newbus) {
            options.add(new SelectOption (pe.getLabel(), pe.getValue()));
            }
            
            return options;
    
        } 
    }
    
    
   /*public List<SelectOption> NetBusinessValues{
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = RainMakerRequest__c.Network_Business__c.getDescribe();
            List<Schema.PicklistEntry> newbus = fieldResult.getPicklistValues();
            for (Schema.PicklistEntry pe : newbus) {
            options.add(new SelectOption (pe.getLabel(), pe.getValue()));
            }
            
            return options;
    
        } 
    }*/

 
    /*public List<SelectOption> NetworkBusinessExcValues{
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = RainMakerRequest__c.Network_Business_Exclusivity__c.getDescribe();
            List<Schema.PicklistEntry> newbusExc = fieldResult.getPicklistValues();
            for (Schema.PicklistEntry pe : newbusExc) {
            options.add(new SelectOption (pe.getLabel(), pe.getValue()));
            }
            
            return options;
    
        } 
    }*/
    
    public List<SelectOption> WillRunDealsValues{
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = RainMakerRequest__c.Will_Run_Deals__c.getDescribe();
            List<Schema.PicklistEntry> willRunDeals = fieldResult.getPicklistValues();
            for (Schema.PicklistEntry pe : willRunDeals) {
            options.add(new SelectOption (pe.getLabel(), pe.getValue()));
            }
            
            return options;
    
        } 
    }
    
    public List<SelectOption> firstPartyDataAvailValues{
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            Schema.DescribeFieldResult fieldResult = RainMakerRequest__c.X1st_Party_Data_Available__c.getDescribe();
            List<Schema.PicklistEntry> firstPartyDataAvail = fieldResult.getPicklistValues();
            for (Schema.PicklistEntry pe : firstPartyDataAvail) {
            options.add(new SelectOption (pe.getLabel(), pe.getValue()));
            }
            
            return options;
    
        } 
    }
    
    
    
   public PageReference save() {
        
        List<RainMakerRequest__c> rmr = new List<RainMakerRequest__c> ();
        
        Boolean first = true;
        
        if (selectedMembers.size() < 1) {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,'Please select at least one EDP Member'));
        } 
        
        try {
            for ( SelectOption so : selectedMembers ) {
                RainMakerRequest__c rm = new RainMakerRequest__c();
                rm.EDP_Member2__c = so.getValue();
                rm.Seller_Member2__c = memberId;
                rm.Request_Status__c = 'Pending';
                rm.CPM_Range__c = rainMaker.CPM_Range__c;
                rm.Uses_LLD__c = rainMaker.Uses_LLD__c;
                rm.Desired_Marketing_Name__c = rainmaker.Desired_Marketing_Name__c;
                rm.Seller_Website__c = rainmaker.Seller_Website__c;
                rm.Seller_Description__c = rainmaker.Seller_Description__c;
                rm.Primary_Markets__c = rainmaker.Primary_Markets__c;
                rm.Estimated_Monthly_Imps__c = rainmaker.Estimated_Monthly_Imps__c;
                rm.Top_Categories__c = rainmaker.Top_Categories__c;
                rm.Client_Contact_to_set_up_Deals__c = rainmaker.Client_Contact_to_set_up_Deals__c;
                rm.Non_O_O_Inventory__c = rainmaker.Non_O_O_Inventory__c;
                rm.Ad_Sizes__c = rainmaker.Ad_Sizes__c;
                rm.Email_of_Client_Contact__c = rainmaker.Email_of_Client_Contact__c;
                rm.Formats_Ad_Types__c = rainmaker.Formats_Ad_Types__c;
                rm.Will_Run_Deals__c = rainmaker.Will_Run_Deals__c;
                rm.X1st_Party_Data_Available__c = rainmaker.X1st_Party_Data_Available__c;
                rm.X1st_Party_Data_Description__c = rainmaker.X1st_Party_Data_Description__c;
                rmr.add(rm);     
            }
        
            if (rmr.size()>0){
            insert rmr;
            cancel = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Rainmaker Request has been saved Successfully!'));
            }
        
        } catch (Exception e) {
            System.debug('***********'+e);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'There is a problem creating a Rainmaker Request. Please contact your System Administrator with this message: '+e));
        
        }
        
        
        return null;     
    }
    
    public PageReference cancel() {
    PageReference pgcancel = null;
    Map<String, String> URLparameters = System.currentPageReference().getParameters();
      if(URLparameters.containsKey('retURL')) {
        pgcancel = new PageReference(URLparameters.get('retURL'));
         
        }
     return pgcancel;
    }
    
    
}