public  class EntitlementService {
	public static Entitlement fetchEntitlementByName(String EntitleMentName){
		try{
			return [SELECT id,Name from Entitlement where Name=: EntitleMentName];
		}catch(QueryException qe){
			system.debug('*** Exception ***'+qe.getMessage());
		}
		return null;
	}
}