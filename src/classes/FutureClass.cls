/************************************************************
Program Name        : FutureClass
Author              : Dheeban Jeganathan
Created Date        : 11/11/2014
Business Logic      : To update boolean flag at contact level for community users.
              
*************************************************************/

global class FutureClass {
    @future 
  public static void updContCP(Set<id> contId, Map<id, boolean> statMap) {      /* To Update bolean flag */
       List<Contact> updtContact = new List<Contact>();
       Set<id> disCont = new Set<id>();
       Set<id> disAcc = new Set<id>();
       Set<id> activeAcc = new Set<id>();
       
        List<Account> updtAcc = new List<Account>();
        List<Contact> conList = ServiceFacade.getContactByIds(contId);
        for(Contact tmpCont : conList ){
            tmpCont.Customer_Portal_User__c = statMap.get(tmpCont.id);
            if( statMap.get(tmpCont.id) == true){
              tmpCont.ReportsToid = ServiceFacade.getDummyContact();//'0031600002QtYzW'; // Dummy Contact
            }
            updtContact.add(tmpCont);
            if( statMap.get(tmpCont.id) == false ){
                disCont.add(tmpCont.id);
            }
        }
        update updtContact;
        if( disCont.size() > 0 ){
            for( Contact tmpCont1 : ServiceFacade.getContactByIds(disCont))
                    disAcc.add(tmpCont1.accountid);
        }
        for( Contact tmpCont2 :ServiceFacade.getCustomerPortalContacts(disAcc,disCont))
            activeAcc.add(tmpCont2.accountid);
        
        disAcc.removeAll(activeAcc);
        for( Account tmpAcc : ServiceFacade.getAccountsByIds(disAcc)){
            tmpAcc.IsCustomerPortal = false;
            updtAcc.add(tmpAcc);
        }
        if(updtAcc.size() > 0)
           update updtAcc;
    
  }
  
  @future 
  public static void updContUsrCP(Set<id> contId, Map<id, id> statUserMap) {  /* To Update address details */
        List<Contact> updtUsrContact = new List<Contact>(); 
        User tmpUser;
        String trimTitle = '';
        Map<id,User> usrContUpdtMap = new Map<id,User>();
        usrContUpdtMap  = ServiceFacade.getUserByContactId(contId);
                                 
        for(Contact tmpCont : ServiceFacade.getContactByIds(contId)){
            tmpUser = usrContUpdtMap.get(statUserMap.get(tmpCont.id));
            tmpCont.Customer_Portal_User__c = true;
            tmpCont.FirstName= tmpUser.FirstName;
            tmpCont.LastName= tmpUser.LastName;
            if(tmpUser.Title != null && tmpUser.Title != '')
                trimTitle = tmpUser.Title;
            if(trimTitle.length() > 80)
                    trimTitle = trimTitle.substring(0,79);                
            tmpCont.Title= trimTitle;
            tmpCont.Email= tmpUser.Email;
            tmpCont.Phone= tmpUser.Phone;
            tmpCont.MobilePhone = tmpUser.MobilePhone;
            tmpCont.Fax= tmpUser.Fax;
            tmpCont.MailingStreet= tmpUser.Street;
            tmpCont.MailingCity= tmpUser.City;
            tmpCont.MailingState= tmpUser.State;
            tmpCont.MailingCountry= tmpUser.Country;
            tmpCont.MailingPostalCode = tmpUser.PostalCode ;
            updtUsrContact.add(tmpCont);
        }
        update updtUsrContact;
    
  }

}