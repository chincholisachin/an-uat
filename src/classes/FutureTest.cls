@isTest
public class FutureTest {
      private static case cas;
      private  static  Contact con;
      private  static  Contact con1;
      private static user usr;
      static Account acc;
    
      public static void loadData(){
      
            /*Account[] accounts = AccountTriggerTest.createAccounts('Customer Account',1);
            insert accounts;
            
            Account[] accounts1 = AccountTriggerTest.createAccounts('Customer Account',1);
            insert accounts1;*/
            
            InitializeTestData.createSystemConfigSettings();
          
            acc = InitializeTestData.createAccount();
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
            insert acc;
            
            con= InitializeTestData.CreateContact(acc.Id);
            con.LastName='dummy';
            con.firstName ='CSP';
            insert con;
          
            con1= InitializeTestData.CreateContact(acc.Id);
            con1.LastName='dummy contact';
            insert con1;
            
            cas = InitializeTestData.CreateCase(con1.id,acc.id);
            insert cas;
            
            usr = InitializeTestData.createUser();
            usr.contactId = con1.id;
            usr.profileId = [select id from profile where name='Customer Portal User'].id;
            insert usr;
      }
    
      public static testMethod void initialLoadFuture(){
          loadData();
          Set<Id> ids = new Set<Id>();
          ids.add(con1.id);
          Map<Id,boolean> statMap = new Map<Id,Boolean>();
          Map<Id,Id> statuMap = new Map<Id,Id>();
          statuMap.put(con1.Id,usr.id);
          statMap.put(con1.id,true);
      //     FutureClass.updContCP(ids,statMap);
          FutureClass.updContUsrCP(ids,statuMap);
      }
    
      public static testMethod void initialLoadFuture1(){
          loadData();
          Set<Id> ids = new Set<Id>();
          ids.add(con.id);
          Map<Id,boolean> statMap = new Map<Id,Boolean>();
          Map<Id,Id> statuMap = new Map<Id,Id>();
          statuMap.put(con.Id,usr.id);
          statMap.put(con.id,true);
           FutureClass.updContCP(ids,statMap);
          //FutureClass.updContUsrCP(ids,statuMap);
      }
    
    public static testMethod void initialLoadFuture2(){
          loadData();
          Set<Id> ids = new Set<Id>();
          ids.add(con.id);
          Map<Id,boolean> statMap = new Map<Id,Boolean>();
          Map<Id,Id> statuMap = new Map<Id,Id>();
          statuMap.put(con.Id,usr.id);
          statMap.put(con.id,false);
           FutureClass.updContCP(ids,statMap);
          //FutureClass.updContUsrCP(ids,statuMap);
      }
}