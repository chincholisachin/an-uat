@isTest
private class CaseServiceTest
{

    static Account acc;
    static list<Outage_Notifications__c> outNoList;
    static Case_Weight_Settings__c cwSetting;
    static Case eachCase;
    static Contact con;
    static list<HardCodeIds__c> hdList;
    static Apttus__APTS_Agreement__c eachAgr;
    static AccountTeamMember am;
    public static Bypass__c bypassSetting;
    public static string AgreID;
    
    private static void setup()
    {
        acc = InitializeTestData.createAccount();
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        insert acc;

        outNoList = InitializeTestData.createOutageNotifications(acc.ID);
        insert outNoList;

        cwSetting = InitializeTestData.createCaseWeightSettings();

        insert cwSetting;

        con = InitializeTestData.CreateContact(acc.ID);
        insert con;

        InitializeTestData.createSystemConfigSettings();
         hdList = InitializeTestData.createHardCodeCustomSettings();
         insert hdList;
        Entitlement ent = InitializeTestData.createEntitlement(acc.ID);
        insert ent;

        InitializeTestData.createEmAsSettings();
        bypassSetting = InitializeTestData.createBypassSetting();
        insert bypassSetting;


        eachAgr = InitializeTestData.createParentAgreement(acc.ID);
        insert eachAgr;
        AgreID = eachAgr.ID;
        system.debug('==eachAgr--'+eachAgr);

        Apttus__APTS_Agreement__c eachChildAgr = InitializeTestData.createChildAgreement(acc.ID,eachAgr.ID);
        insert eachChildAgr;

        am = InitializeTestData.createAccountTeamMember(acc.ID,UserInfo.getUserId());
        insert am;
    }

    private static testmethod void calculateCaseWeightTest(){
        setup();
        eachCase = InitializeTestData.CreateCase(con.ID,acc.Id);
        insert eachCase;
        Decimal caseWe = [SELECT Case_Weight__c from Case where ID =:eachCase.ID].Case_Weight__c;
        system.assertEquals(0.0375,caseWe);
        system.debug('==eachAgr3--'+eachAgr);

        
        eachCase.Subject = 'Test Subject';
        eachCase.Related_Agreement__c = AgreID;
        eachCase.Ready_for_Signatures__c = true;
        eachCase.Status = 'Resolved';

        update eachCase;

        eachCase.Status = 'Closed';
        //system.assertEquals(1,2);
        update eachCase;
        System.debug('***eachCase***'+eachCase);

        update eachCase;

    }

    private static testmethod void setCaseBusinessAge(){
        setup();
        eachCase = InitializeTestData.CreateCase(con.ID,acc.Id);
        eachCase.Responded__c = Datetime.Now()-1;
        insert eachCase;
        Case caseWe = [SELECT Case_Weight__c,Case_Business_Age__c,Resolved_to_Closed_Date__c from Case where ID =:eachCase.ID];
        system.assertEquals(-24,caseWe.Case_Business_Age__c);
    }

    private static testmethod void setCaseBusinessAge1(){
        setup();

        eachCase = InitializeTestData.CreateCase(con.ID,acc.Id);
        eachCase.Status=CONSTANTS.RESOLVED_STATUS;
        eachCase.Sub_Status__c=CONSTANTS.FINISHED_STATUS;
        insert eachCase;
        Case caseWe1 = new Case();
        caseWe1 = [SELECT Case_Weight__c,Case_Business_Age__c,Resolved_to_Closed_Date__c from Case where ID =:eachCase.ID];
        system.assertNotEquals(null,caseWe1.Resolved_to_Closed_Date__c);
    }


    private static testmethod void communityUpdatesTest(){
        setup();

        eachCase = InitializeTestData.CreateCase(con.ID,acc.Id);
        eachCase.Category__c = 'Client Finance';
        eachCase.recordTypeID = Utility.getRecordTypeInfoByObjectName('Case').get(CONSTANTS.PRODUCT_SUPPORT_CASE_RT);

        insert eachCase;
        Case caseWe1 = new Case();
        caseWe1 = [SELECT ID,SuppliedName,SuppliedEmail,Customer_Support_Request_Form__c,Category__c,Non_Auth_Request__c from Case where ID =:eachCase.ID];
        system.assertNotEquals(null,caseWe1.SuppliedName);
    }

    private static testmethod void communityUpdatesTest1(){
        setup();

        eachCase = InitializeTestData.CreateCase(con.ID,acc.Id);
        eachCase.Category__c = 'Product Support';
        eachCase.recordTypeID = Utility.getRecordTypeInfoByObjectName('Case').get(CONSTANTS.PRODUCT_SUPPORT_CASE_RT);

        insert eachCase;
        Case caseWe1 = new Case();
        caseWe1 = [SELECT ID,SuppliedName,SuppliedEmail,Customer_Support_Request_Form__c,Category__c,Non_Auth_Request__c from Case where ID =:eachCase.ID];
        system.assertNotEquals(null,caseWe1.SuppliedName);
    }

    private static testmethod void updateRelevantAgreementsNewTestMethod(){
        system.debug('==eachAgr1--'+eachAgr+'--'+AgreID);
        setup();
        system.debug('==eachAgr1--'+eachAgr);
        Test.startTest();
        system.debug('==eachAgr1--'+eachAgr);
        //system.assertEquals(1,2);
        Case newCase = new Case();
            Case c = new Case(Subject='Test Subject', Related_Agreement__c = eachAgr.ID,Status ='New',Ready_for_Signatures__c = true);
            c.RecordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Product Support').getRecordTypeId();
            insert c;
            //system.assertEquals(null,newCase);
            system.debug('==Case Inserted in Test Class=='+c);

            
            newCase = [SELECT id,subject,status,Related_Agreement__c,isClosed,Ready_for_Signatures__c from Case where id =:c.id];
            //system.assertEquals(null,newCase);
            newCase.Status ='Closed';
            //newCase.IsClosed = true;
            //
            update newCase;
            //system.assertEquals(null,newCase);
            system.debug('==Case Updated in Test Class=='+newCase);

        Test.stopTest();
        
    }

    public static final Integer NUM_SECONDS_IN_HOUR = 3600;
    public static DateTime findCorrectBusinessHourTime(DateTime timeToCompareAgainst, Integer numHours)
    {
        return BusinessHours.add( CaseService.normalBusinessHours.Id, timeToCompareAgainst, NUM_SECONDS_IN_HOUR * numHours * 1000L  );
    }

    private static testMethod void testTimeStampBusinessDay()
    {
        final Integer NUM_HOURS_IN_ADVANCED = 24;
        DateTime compareAgainstDate = DateTime.newInstance( 2013, 3, 15 );
        DateTime expectedDate = findCorrectBusinessHourTime( compareAgainstDate, NUM_HOURS_IN_ADVANCED );

        Case caseToStamp = new Case();

        Test.startTest();
            CaseService.timeStampBusinessDay( caseToStamp, 'Next_Business_Day_Qualtrics__c', NUM_HOURS_IN_ADVANCED, compareAgainstDate );
        Test.stopTest();

        //System.assertEquals( expectedDate,caseToStamp.Next_Business_Day_Qualtrics__c , 'The field should have been stamped with the time dateToCompareAgainst (+ businessHours time) ' + NUM_HOURS_IN_ADVANCED );
    }


    private static testmethod void updateCaseOwnerToAccountOwnerTest(){
        setup();
        Test.startTest();
        eachCase = InitializeTestData.CreateCase(con.ID,acc.Id);
        eachCase.Type ='IQ Review';
        eachCase.AccountID = acc.id;
        insert eachCase;

        Case caseWe1 = new Case();
        caseWe1 = [SELECT ID,SuppliedName,SuppliedEmail,Customer_Support_Request_Form__c,Category__c,Non_Auth_Request__c from Case where ID =:eachCase.ID];
        

        CaseService.NotClosedCasesByOwner(String.valueOf(UserInfo.getUserId()));
        Test.stopTest();
        //system.assertNotEquals(null,caseWe1.SuppliedName);
    }

    private static testMethod void assignUserRegionToCaseTest(){
        setup();
        CountryISOCodes__c iso = InitializeTestData.createisoCountrySetting();
        insert iso;

        Test.startTest();
        eachCase = InitializeTestData.CreateCase(con.ID,acc.Id);
        eachCase.Type ='IQ Review';
        eachCase.AccountID = acc.id;
        insert eachCase;

        Case caseWe1 = new Case();
        caseWe1 = [SELECT ID,Submission_Country__c,Submission_Region__c from Case where ID =:eachCase.ID];

        system.assertNotEquals(null,caseWe1.Submission_Region__c);
        system.assertNotEquals(null,caseWe1.Submission_Country__c);

    }




}