public class OpportunityTeamMemberController {

    public List<OpportunityTeamMember> OTMList {get; set;}
    public OpportunityTeamMember del;
    public List< OpportunityTeamMember > addOTMList {get;set;}
    public List< OpportunityTeamMember > delOTMList {get;set;}
    public Opportunity opp;
    public Integer rowIndex {get;set;}
    public Boolean isWon {get;set;}
 
    public OpportunityTeamMemberController(ApexPages.StandardController controller) {
        opp = (Opportunity) controller.getRecord();
        addOTMList = new List<OpportunityTeamMember>();
        delOTMList = new List<OpportunityTeamMember>();
        OTMList = [SELECT Id, OpportunityAccessLevel, OpportunityId, Opportunity.Name, Opportunity.stagename, TeamMemberRole,UserId, User.Name FROM OpportunityTeamMember where OpportunityId =: opp.Id];
        isWon = (OTMList.size() > 0 && OTMList[0].opportunity.stagename == 'Closed Won') ? true : false;
    }
    
    public List<SelectOption> opportunityTeamRoles{
        get{
            List<SelectOption> options = new List<SelectOption>();
            for (String tmp: Label.OpportunityTeamMember.split('\r\n')){
                options.add(new SelectOption(tmp,tmp));
            }
            return options;
        }
    }
    
    public void deleteRow(){
 
     rowIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('rowIndex'));
     del = OTMList.remove(rowIndex);
     if (del.id != null) delOTMList.add(del);
 
   } 
   
    public void addRow(){
        addOTMList = new List< OpportunityTeamMember >();
        OTMList.add(new OpportunityTeamMember(opportunityid = opp.Id, opportunityaccesslevel = 'Edit'));
    }
    
     public PageReference ave(){
 
         List< OpportunityTeamMember > otmFinal = new List< OpportunityTeamMember >();
         if ( delOTMList.size() > 0) Database.delete(delOTMList,false);
         if ( OTMList.size() > 0 ) {
             try{
                 upsert OTMList;
             }catch(System.DmlException e){
                 for (Integer i = 0; i < e.getNumDml(); i++) {
                    if (e.getDmlStatusCode(i) == 'DUPLICATE_VALUE') ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'You are not allowed to add two opportunity owners or senior opportunity owners to same opportunity.')); 
                 }
                 return null;
             }
         }
         for (OpportunityTeamMember o: OTMList){
             if (o.TeamMemberRole == 'Opportunity Owner') {
                 Opportunity op = new opportunity(id = opp.id, ownerid = o.userid);
                 try {
                     update op;
                 } catch (exception e){
                     System.debug(String.valueOf(e));
                 }
             } else {
                 otmFinal.add(o);
             }
         }
         if (OtmList.size() == otmFinal.size()) {
             String uId = [select id, ownerid from opportunity where id =: opp.id limit 1].ownerId;
             OpportunityTeamMember otm = new OpportunityTeamMember(opportunityid = opp.id, userid = uId, opportunityaccesslevel = 'Edit', TeamMemberRole = 'Opportunity Owner');
             insert otm;
         }
         return (new ApexPages.StandardController(opp)).view();
     } 

}