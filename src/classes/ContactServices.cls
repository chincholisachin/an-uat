public class ContactServices
{

    public static contact getContactById(String conId)
    {
    
        return [SELECT FirstName,LastName,Email,AccountId, Account.Name,Account.Region_rdp__c,Account.Support_Level__c,Customer_Portal_User__c, ReportsToid
                       FROM Contact Where Id =: conId LIMIT 1];
    }
    
    public static List<contact> getContactByEmailId(String emailId) {
    
        List<contact> contactsList = [Select Id From contact Where  Email =:emailId];
        return contactsList ;
    }
    
    
    public static Id getDummyContact() {
    
        return [SELECT Id FROM Contact WHERE LastName = 'Dummy Contact'].Id;
    
    }
    
    public static List<contact> getContactByIds(Set<Id> contIds)
    {
    
        return [SELECT FirstName,LastName,Email,AccountId,Title,MobilePhone,Fax, 
                       MailingStreet, MailingCity, MailingState, MailingCountry, 
                       MailingPostalCode,Account.Name,Account.Region_rdp__c,Account.Support_Level__c,
                       Customer_Portal_User__c, ReportsToid
                       FROM Contact Where Id IN: contIds];
    }
    
    public static List<contact> getCustomerPortalContacts(Set<Id>accIds, Set<Id> conIds) {
        
        return [select accountid from contact where accountid in : accIds and Customer_Portal_User__c = true and id not in : conIds];
    }
 }