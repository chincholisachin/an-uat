public class CSPAlertController{
  @AuraEnabled
  public static List<CaseSupportAlert__c> getCSPAlert(){
       return [select id, Alert_Message__c, Alert_Type__c, Image_url__c 
                 from CaseSupportAlert__c 
                where Active__c = true 
                  and Start_Time__c <= :DateTime.now() 
                  and Expired_On__c > :DateTime.now() limit 1];
  }
}