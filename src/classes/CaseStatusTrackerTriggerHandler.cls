public  class CaseStatusTrackerTriggerHandler {
	private static CaseStatusTrackerTriggerHandler instance;

	//*** Singleton Pattern to obtain instance of the Handler***
    public static CaseStatusTrackerTriggerHandler getInstance(){
        if(instance==null){
            instance = new CaseStatusTrackerTriggerHandler();
        }
        return instance;
    }

    public void beforeUpdateMethod(list<Case_Status_Tracker__c> newList, list<Case_Status_Tracker__c> oldList, map<ID,Case_Status_Tracker__c> newMap, map<ID,Case_Status_Tracker__c> oldMap){
        
            CaseStatusTrackerService.updateAgeByBusinessHours(newList);
    }

}