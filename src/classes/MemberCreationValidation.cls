global class MemberCreationValidation{
    
    public static String[] errorList = new List<String>();

    
    webservice static List<String> validate(String AgreementId){ 
        
        Apttus__APTS_Agreement__c agr = [select id, Apttus__Account__r.id, Apttus__Account__r.VAT_Country_of_Registration__c, Apttus__Account__r.BillingStreet from Apttus__APTS_Agreement__c where id =: AgreementId];
        Account acn = new Account(id = agr.Apttus__Account__r.id, VAT_Country_of_Registration__c = agr.Apttus__Account__r.VAT_Country_of_Registration__c, BillingStreet = agr.Apttus__Account__r.BillingStreet);
        validateBillingStreet(acn);
        validateVAT(acn);
        return errorList;
   
    }
    
    private static void validateBillingStreet(Account acn){
    
        if(acn.BillingStreet.length() > 50){
            errorList.add('Street address for this account has more than 50 characters.') ;
        }
        
        
    }
    
    private static void validateVAT(Account acn){
        
        Schema.DescribeFieldResult fieldResult = Account.VAT_Country_of_Registration__c.getDescribe();
        Map<String, String> picklistValues = new Map<String, String>();
        for (Schema.PicklistEntry ple : fieldResult.getPicklistValues()){
            picklistValues.put(ple.getValue(),ple.getValue());
        }

        system.debug('picklistValues is '+ picklistValues);
        if(acn.VAT_Country_of_Registration__c != null && !picklistValues.containsKey(acn.VAT_Country_of_Registration__c)){
            errorList.add('VAT Country of Registration listed in this account is not a supported value: '+acn.VAT_Country_of_Registration__c) ;
        }
        
    
    }

}