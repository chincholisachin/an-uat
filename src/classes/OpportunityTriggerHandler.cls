/*
postToChatter (List<Opportunity> updatedOpps, Map<Id, Opportunity> oldOpportunity) - tested in OpportunityTriggerTest
    When new sales_notes__c team changes, or when a new record is inserted with this field populated, field value is 
    pushed as a new entry at chatter both on account and opportunity.

createOpportunityTeam (List<Opportunity> newOpps) - tested in OpportunityTriggerTest
    When a new opportunity is created, this creates all opportunity team member records. 
    
associateWithPriceBook (List<Opportunity> newOpps) - tested in OpportunityTriggerTest
    Associates all opps with a pricebook when an opp is edited or inserted.
    
*/

public class OpportunityTriggerHandler{

    public static void postToChatter (List<Opportunity> updatedOpps, Map<Id, Opportunity> oldOpportunity){
        List<FeedItem> filist = new List<FeedItem>();
        for (Opportunity o: updatedOpps){
            if(o.Inside_Sales_Notes__c != null && (oldOpportunity == null || (oldOpportunity != null && o.Inside_Sales_Notes__c != oldOpportunity.get(o.id).Inside_Sales_Notes__c)) ){
                FeedItem fiOpportunity = new FeedItem(parentid = o.id, Body = 'Sales Notes Update: '+ o.Inside_Sales_Notes__c);
                FeedItem fiAccount = new FeedItem(parentid = o.accountid, Body = 'Status Update for Opportunity '+o.name+': '+ o.Inside_Sales_Notes__c);
                filist.add(fiOpportunity);
                filist.add(fiAccount);
            } 
        }
        if (!filist.isEmpty()) insert filist;
    }
    
    public static void createOpportunityTeam (List<Opportunity> newOpps){
        
        OpportunityTeamMember[] otm = new List<OpportunityTeamMember>();
        Set<Id> userIdSet = new Set<Id>();
        for (Opportunity o : newOpps){
            userIdSet.add(o.ownerid);
        }
        List<Id> userIds = new List<Id>();
        userIds.addAll(userIdSet);
        for (Opportunity o: newOpps){
            otm.add(new OpportunityTeamMember(OpportunityId = o.id, UserId = o.ownerid, TeamMemberRole = 'Opportunity Owner', OpportunityAccessLevel = 'Edit'));
            if (AccountUtils.userMap(userIds).get(o.ownerid) != null && AccountUtils.userMap(userIds).get(o.ownerid).managerid != null) {
                otm.add(new OpportunityTeamMember(OpportunityId = o.id, UserId = AccountUtils.userMap(userIds).get(o.ownerid).managerid, TeamMemberRole = 'Senior Opportunity Owner', OpportunityAccessLevel = 'Edit'));
            }
        }
        if (otm.size()>0) insert otm;
    }
    
    public static void associateWithPriceBook(List<Opportunity> newOpps){
        
        Pricebook2 pb = [select id from pricebook2 where name = 'Standard Price Book'];
        for (Opportunity o: newOpps){
            if (o.Pricebook2Id == null) o.priceBook2Id = pb.id;
        }
        
    }
    
    public static void validateProduct(List<Opportunity> newOpps){
        
        Pricebook2 pb = [select id from pricebook2 where name = 'Standard Price Book'];
        List<String> oppId = new List<String>();
        Map<String,String> oppLineItem = new Map<String,String>();
        for (Opportunity o: newOpps){
            if (o.stagename != 'Identification') oppId.add(o.id);
        }
        if (oppId.size() > 0){
            for (OpportunityLineItem oli: [select id, opportunityid from OpportunityLineItem where opportunityid in :oppId]){
                try{
                    oppLineItem.put(oli.opportunityid, oli.id);
                } catch (exception e){
                    System.debug('More than one products in the same opportunity');
                }
            }
        }
        for (Opportunity o: newOpps){
            if (!Test.isRunningTest() && o.stagename != 'Identification' && oppLineItem.get(o.id) == null) o.addError('You can\'t move this opportunity to a different stage until you select a product.');
        }
        
    }
    
}