@isTest
public class CSP_GetCaseDetailsTest {
    private static AcntOwner controller;
    private  static  Contact con;
    Private static Contact con1;
    private static case cas;
    private static CaseComment CreateCaseComment;
    private static attachment createAttachment;
    private static EmailMessage CreateEmailMessage;
    static list<HardCodeIds__c> hdList;
    static Case_Weight_Settings__c cwSetting;
    private static emailSettings__c emailsettings;
  
    public static void loadData(){
        InitializeTestData.createSystemConfigSettings();
        
         hdList = InitializeTestData.createHardCodeCustomSettings();
         insert hdList;
        

        InitializeTestData.createEmAsSettings();
        InitializeTestData.createBypassSetting();
        cwSetting = InitializeTestData.createCaseWeightSettings();
         insert cwSetting;
         
        emailsettings = InitializeTestData.creatEmailSettings();
        emailsettings.Org_Wide_Email__c  = 'sales@appnexus.com';
        emailsettings.Record_Type_Exclusion__c = 'Billing, Concur';
        emailsettings.Case_Email_Priority__c = 'Emergency,Low';
        emailsettings.Automate_Email_Handling__c = true;
        insert emailsettings ;
         
        User u1 = TestingUtils.getUser('* Sales Operations');
        User[] users = [SELECT Id, ProfileId FROM User WHERE Profile.Name ='* Commercial' AND isActive = true limit 7];
        
        System.runAs(TestingUtils.getAdminUser()){
            PermissionSet ps = [SELECT id from permissionset where name = 'Account_Ownership_Change_Request_Approval' limit 1];
            PermissionSetAssignment[] psa = new List<PermissionSetAssignment>();
            psa.add(new PermissionSetAssignment(permissionsetid = ps.id, assigneeid = users[3].id));
            psa.add(new PermissionSetAssignment(permissionsetid = ps.id, assigneeid = users[4].id));
            psa.add(new PermissionSetAssignment(permissionsetid = ps.id, assigneeid = users[5].id));
            insert psa;
        }
        
        System.runAs(u1){
            
            Account[] accounts = AccountTriggerTest.createAccounts('Customer Account',1);
            insert accounts;
            
            Account[] accounts1 = AccountTriggerTest.createAccounts('Customer Account',1);
            insert accounts1;
            
            con= InitializeTestData.CreateContact(accounts[0].Id);
            insert con;
            
            cas = InitializeTestData.CreateCase(con.id,accounts[0].id);
            insert cas;
            
            createAttachment = InitializeTestData.createAttachment(cas.id);
            insert createAttachment;
            
            CreateCaseComment = InitializeTestData.CreateCaseComment(cas.Id);
            insert CreateCaseComment;
            
            CreateEmailMessage = InitializeTestData.CreateEmailMessage(cas.Id);
            insert CreateEmailMessage;
            
            AccountContactRelation ac = InitializeTestData.CreateAccountContactRelation(accounts1[0].Id,con.Id);
            insert ac;
            
            //Account acn = [select id, recordtype.name from account where recordtype.name = 'Customer Account' limit 1];
            Account[] members = AccountTriggerTest.createAccounts('Console Member',10);
            for (Integer i = 0; i<10; i++){
                members[i].AppNexus_Member_Id__c = String.valueOf(i+10000);
                //members[i].parentid = acn.id;  
                members[i].parentid = accounts[0].id;
                if(Math.mod(i,2) == 0) members[i].ownerid = users[1].id;
            }
            insert members;
            
            
        }
        
       /* Account[] memberAccounts = [select id, (select id from accountteammembers) from account where recordtype.name = 'Console Member' limit 5];
        AccountTeamMember[] toDelete = new List<AccountTeamMember>();
        AccountTeamMember[] toInsert = new List<AccountTeamMember>();
        for (Account a: memberAccounts){
            toDelete.addAll(a.accountTeamMembers);
            toInsert.add(AccountUtils.generateAccountTeamMember(a.id, users[0].id, AccountUtils.SR_ACCOUNT_OWNER));
            toInsert.add(AccountUtils.generateAccountTeamMember(a.id, users[1].id, AccountUtils.ACCOUNT_OWNER));
            toInsert.add(AccountUtils.generateAccountTeamMember(a.id, users[2].id, AccountUtils.COMMERCIAL_LEADER));
        }
        delete toDelete;
        insert toInsert;*/  
        
    }
    
    public static testMethod void initialLoadChangeOwner(){
        
        LoadData();
        Test.StartTest(); 
        CSP_GetCaseDetails.isActiveUser();
        CSP_GetCaseDetails.getCaseCategory();
        CSP_GetCaseDetails.getMemberAccounts(con.Id);
        CSP_GetCaseDetails.getPickValues('Category__c','');
        CSP_GetCaseDetails.getDependentOptions('case','Category__c','Severity__c');
        List<String> memebersList = new List<String>();
        memebersList.add('Ma-009');
        MyObject obj = new Myobject();
        obj.category = 'Product Support';
        obj.severity='This is Minor';
        obj.help='Console';
        obj.description='test';
        obj.subject='TEst';
        obj.invoiceNumber='123456';
        obj.ccEmail='test@gmail.com';
        obj.memeber='MA-009';
        obj.email='demo@gmail.com';
        obj.name='TetNAme';
        obj.company='ET';
        String caseObj = JSON.serialize(obj);
        CSP_GetCaseDetails.createCaseRecord(caseObj,memebersList);
        CSP_GetCaseDetails.uploadFile('Unit Test Attachment','Unit Test Attachment Body','',cas.Id);
        CSP_GetCaseDetails.creatComment(cas.Id,'Testing');
        CSP_GetCaseDetails.getComments(cas.Id);
        Test.StopTest();
        
    }
    
    
    public static testMethod void independentChange(){
        
        LoadData();
        Test.StartTest(); 
        CSP_GetCaseDetails.getDependentOptions('case','Categorys__c','Severitys__c');
        CSP_GetCaseDetails.getDependentOptions('cases','Categorys__c','Severitys__c');
        List<String> memebersList = new List<String>();
        //  memebersList.add('Ma-009');
        MyObject obj = new Myobject();
        obj.category = 'Product Support';
        obj.severity='This is Minor';
        obj.help='Console';
        obj.description='test';
        obj.subject='TEst';
        obj.invoiceNumber='123456';
        obj.ccEmail='test@gmail.com';
        obj.memeber='MA-009';
        obj.email='demo@gmail.com';
        obj.name='TetNAme';
        obj.company='ET';
        String caseObj = JSON.serialize(obj);
        CSP_GetCaseDetails.createCaseRecord(caseObj,memebersList);
        Test.StopTest();
        
    }
    
    public  class MyObject
    {
        String category;
        String severity;
        String help;
        String description;
        String subject;
        String invoiceNumber;
        String ccEmail;
        String memeber;
        String email;
        String name;
        String company;
        // DateRange dateRange;
    }
}