public class CaseService {

    public static Boolean isRunning = false;

    public static Map<String,Id> recordTypeNameandIDMap =  Utility.getRecordTypeInfoByObjectName('Case');

    public static Map<String,Id> queueNameAndIDMap = Utility.getQueueInfoByObjectName('Case');

    public static void calculateCaseWeight(List<Case> newCaseList){

        system.debug('--BEGIN : calculateCaseWeight--');
        // -- Attributes --//
        list<Outage_Notifications__c> outageNotificationList = new list<Outage_Notifications__c>();
        list<Case_Weight_Settings__c> caseWeightSettingsList = new list<Case_Weight_Settings__c>();
        Set<String> listOfKeyAttributes = new Set<String>();
        Map<String,Decimal> weightFieldMap = new Map<String, Decimal >();
        Map<String,String> keyAccountsMap = new Map<String,String>();

        //-- Populating values from respective Services --
        outageNotificationList = ServiceFacade.getActiveOutageNotifications();
        caseWeightSettingsList = Utility.getCaseWeightSettings();

        if(!outageNotificationList.isEmpty()){
            for(Outage_Notifications__c eachOutageNotification:outageNotificationList){
            keyAccountsMap.put(eachOutageNotification.account__c,eachOutageNotification.account__c);
            }
        }

        if(!caseWeightSettingsList.isEmpty()){
            for(Case_Weight_Settings__c cws:caseWeightSettingsList){
                weightFieldMap.put(cws.Type__c + '-' + cws.Value__c, cws.weight__c);
                listOfKeyAttributes.add(cws.Type__c);
            }
        }

        for(Case eachCase : newCaseList){
            eachCase.case_weight__c = 1;
            if(!listOfKeyAttributes.isEmpty()){
                for(String eachTypeFieldNameListed : listOfKeyAttributes){
                    eachCase.case_weight__c = eachCase.case_weight__c * (weightFieldMap.get(eachTypeFieldNameListed + '-' + eachCase.get(eachTypeFieldNameListed)) == null ? 1 : weightFieldMap.get(eachTypeFieldNameListed + '-' + eachCase.get(eachTypeFieldNameListed)));
                }
            }
            eachCase.case_weight__c = (keyAccountsMap.containsKey(eachCase.accountid) || keyAccountsMap.containsKey(eachCase.Parent_Account__C)) ? eachCase.case_weight__c * 1.5 / 40 : eachCase.case_weight__c / 40;
            system.debug('--Updating Case weight--'+eachCase.case_weight__c);
            system.debug('--END : calculateCaseWeight--');
        }
    }

    public static void setCaseBusinessAge(list<Case> newList){

        system.debug('--BEGIN : setCaseBusinessAge--');
        // -- First Things First -- Fetch Business hours to use //
        String newYorkBusHrsID = Utility.getHardCodeIDCSValueByName(CONSTANTS.BUS_HRS_ID);

        if(newYorkBusHrsID==null){
            newYorkBusHrsID = ServiceFacade.getDefaultBusinessHours().id;
        }

        for(Case eachCase : newList){
            if(eachCase.Responded__c != null && eachCase.Case_Business_Age__c == null ){ 
                Decimal diffInHours = ServiceFacade.getDifferenceInBusinessHours(newYorkBusHrsID,eachCase.CreatedDate, eachCase.Responded__c);
                if(diffInHours!=null){
                    eachCase.Case_Business_Age__c = diffInHours;
                }
            }
            if(eachCase.Status == CONSTANTS.RESOLVED_STATUS && eachCase.Sub_Status__c == CONSTANTS.FINISHED_STATUS && eachCase.Resolved_to_Closed_Date__c == null){
                Datetime busHoursAddedDatetime = ServiceFacade.addBusinessHours(newYorkBusHrsID,System.now(), 2*24*1000*60*60);
                    if(busHoursAddedDatetime!=null){
                        eachCase.Resolved_to_Closed_Date__c = busHoursAddedDatetime;
                    }
            }
        }
        system.debug('--END : setCaseBusinessAge--');
    }

    public static void setEmergencyRegion(list<Case> caseList){
        system.debug('--BEGIN : setEmergencyRegion--');
        String emergencyRegion = findEmerReg();
        for(Case eachCase : caseList){
            eachCase.Emergency_Region__c = emergencyRegion;
        }
        system.debug('--END : setEmergencyRegion--');
    }

    //-- Helper method to set emergency region on case -- //
    public static String findEmerReg(){
        system.debug('--BEGIN : findEmerReg--');
        Datetime GMTDate = system.now(); //Datetime.newInstanceGmt(2016,4,20,1,1,5);
        List<Emergency_Assignment__c> emerAssignLst = Utility.getEmergencyAssignmentSettings();
        List<Emergency_Assignment__c> emerAssignSortedLst = new Emergency_Assignment__c[emerAssignLst.size()];

        for(Emergency_Assignment__c tmpAssign:emerAssignLst){
          emerAssignSortedLst[Integer.valueOf(tmpAssign.Priority__c-1)] = tmpAssign;
        }
        String dtFrmt = '';
        Integer hr = 0;
        String dt = '';
        String assign = '-';
        String holiday = '';
        String weekday = '';

        for(Emergency_Assignment__c tmpAssign:emerAssignSortedLst){
           if(assign == '-'){
               dtFrmt = GMTDate.format('MM/dd/yyyy HH:mm:ss', tmpAssign.Time_Zone__c);
               hr = integer.ValueOf(dtFrmt.substring(11,13));
               if( hr >= integer.ValueOf(tmpAssign.Start_Time__c) &&
                   hr < integer.ValueOf(tmpAssign.End_Time__c)){
                   holiday = (tmpAssign.Holidays__c== null?'-':tmpAssign.Holidays__c);
                   dt = dtFrmt.substring(0,10);
                   if( holiday == '-' || (!holiday.contains(dt)) ){
                      Weekday = GMTDate.format('E', tmpAssign.Time_Zone__c);
                       if( tmpAssign.Weekend__c || (Weekday != 'Sat' && Weekday != 'Sun') ){
                            assign = tmpAssign.name;
                      }
                    }
               }
           }
        }
        system.debug('--END : findEmerReg--');
        return (assign == '-'?'USA':assign);

    }

    //-- This is called before insert
    public static void communityUpdates(list<Case> newList){
        system.debug('--BEGIN : communityUpdates--');
        system.debug('--recordTypeNameandIDMap--'+recordTypeNameandIDMap);

        try{
        //--local variables--//
        set<String> emailsSet = new set<String>();
        list<Contact> conList = new list<Contact>();
        map<String,Contact> suppliedEmailToContactMap = new map<String,Contact>();

        for(Case eachCase:newList){
            if(eachCase.suppliedEmail!=null){
                emailsSet.add(eachCase.suppliedEmail);
            }
        }

        if(!emailsSet.isEmpty()){
            conList = [SELECT id,Email from Contact where email IN:emailsSet];
            if(!conList.isEmpty()){
                for(Contact eachCon:conList){
                    suppliedEmailToContactMap.put(eachCon.Email,eachCon);
                }
            }
        }

        for(Case eachCase:newList){
            //-- Add the logic to call Utility Record Type Info.
            if(recordTypeNameandIDMap.containsKey(CONSTANTS.PRODUCT_SUPPORT_CASE_RT)){
                if(eachCase.Category__c!=null && eachCase.recordTypeID==recordTypeNameandIDMap.get(CONSTANTS.PRODUCT_SUPPORT_CASE_RT)){
                eachCase.Customer_Support_Request_Form__c = true;
                eachCase.Origin = CONSTANTS.CASE_ORIGIN_WEB;
                if(eachCase.SuppliedName == null){
                   eachCase.SuppliedName = UserInfo.getName();
                   eachCase.SuppliedEmail = UserInfo.getUserEmail();
                   if(suppliedEmailToContactMap.containsKey(eachCase.SuppliedEmail)){
                        //eachCase.ContactID = suppliedEmailToContactMap.get(eachCase.SuppliedEmail).ID;
                    }
                }else if(eachCase.SuppliedEmail!=null && eachCase.contactId!=null){
                    //-- Moved the OAS Support Logic here--
                     eachCase.suppliedEmail = eachCase.Contact.Email;
                }/*else if(eachCase.SuppliedEmail!=null && eachCase.contactId==null && suppliedEmailToContactMap.containsKey(eachCase.SuppliedEmail)){
                    eachCase.ContactID = suppliedEmailToContactMap.get(eachCase.SuppliedEmail).ID;
                }*/
                else{
                    eachCase.Non_Auth_Request__c = true;
                    if(suppliedEmailToContactMap.containsKey(eachCase.SuppliedEmail)){
                        eachCase.ContactID = suppliedEmailToContactMap.get(eachCase.SuppliedEmail).ID;
                    }
                }

                // -- Verify this logic between before and after insert -- // 
                // -- Global Region Value in any workflow --//
                 
                }
            }
        }
        system.debug('--END : communityUpdates--');
        }catch(Exception ex){
            Exception_Logs__c el = new Exception_Logs__c();
            el.Exception_Message__c = ex.getMessage()+'--'+ex.getCause()+'--'+ex.getLineNumber()+'--'+ex.getStackTraceString();
            insert el;

        }
    }

    public static void communityCaseHandler(list<Case> newList){
        system.debug('--BEGIN : communityCaseHandler--');
        List<Case> updateCaseList = new List<Case>();
        String entitlementId;
        String gblRegion;
        String entitlementString;
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.emailHeader.triggerUserEmail = true;
        dmlOpts.assignmentRuleHeader.useDefaultRule = true;

        try{
            String defaultCommunityEntID = ServiceFacade.fetchEntitlementByName(CONSTANTS.CSP_ENT_NAME).ID;
             Set<ID> cids = new Set<ID>();
            for(Case eachCase : newList){
                if(eachCase.Category__c!=null && eachCase.recordTypeID==recordTypeNameandIDMap.get(CONSTANTS.PRODUCT_SUPPORT_CASE_RT)){
                    cids.add(eachCase.Id);
                }
            }
            List<Case> casesList = new list<Case>();
            if(!cids.isEmpty()){
               casesList =  getCasesbyIDList(cids).values();
            }

            for(Case eachCase : casesList){    
                if(eachCase.Category__c != CONSTANTS.CAT_CLIENT_FINANCE){
                            gblRegion = (eachCase.Global_Region__c == null? CONSTANTS.AMERICAS: eachCase.Global_Region__c);

                            entitlementString = Utility.getHardCodeIDCSValueByName(CONSTANTS.CSP_ENT_HARDCODE_CS_PARAM);

                            if(entitlementString!=null){
                                for(String entitlement : entitlementString.split(',')){
                                        if(entitlement.contains(gblRegion)){
                                            entitlementId = entitlement.substring(entitlement.indexOf('|')+1);
                                        }
                                    }
                            }else{
                                entitlementId = defaultCommunityEntID;
                            }
                            //eachCase.EntitlementId = entitlementId; 
                }else{
                    eachCase.Priority = 'N/A';
                } 
                updateCaseList.add(eachCase);
            }
            // -- DML Options only works in After Insert Trigger -- //
            if(!updateCaseList.isEmpty()){
                Database.update(updateCaseList,dmlOpts);
            }
        }
        catch(Exception e){
            system.debug('--Exception--'+e.getMessage());
        }
        system.debug('--END : communityCaseHandler--');
        }   
    /*public static void assignEntitlementToCase(String EntitleMentName,list<Case> newCases){
        system.debug('--BEGIN : assignEntitlementToCase--');
        try{
            Entitlement ent = ServiceFacade.fetchEntitlementByName(EntitleMentName);
           
            for(Case eachCase : newCases){
                eachCase.EntitlementID = ent.ID;    
            }
        }
        catch(Exception e){
            system.debug('***Exception e***'+e.getMessage());
        }
        system.debug('--END : assignEntitlementToCase--');   
    }*/



    private static Datetime completionDate = Datetime.Now();

    public static void completeCaseMilestone(map<ID,Case> newMap,map<ID,Case> oldMap){
        system.debug('--BEGIN : completeCaseMilestone--'); 
        List<Id> updateCases_ClosedResolution = new List<Id>();
        for(Case eachCase:newMap.values()){
            if((eachCase.status == CONSTANTS.CLOSED_STATUS || eachCase.status == CONSTANTS.RESOLVED_STATUS)&& !oldMap.get(eachCase.id).isClosed){
                system.debug('---Matched Criteria--+completeCaseMilestone');
                updateCases_ClosedResolution.add(eachCase.id);
            }
        }

        system.debug('---updateCases_ClosedResolution---'+updateCases_ClosedResolution);

        if(!updateCases_ClosedResolution.isEmpty()){
            ServiceFacade.completeMilestone(updateCases_ClosedResolution,CONSTANTS.MS_CLOSED,completionDate);
        }
        system.debug('--END : completeCaseMilestone--');
    }

    //-- Call this method on before update, even though it is stated as After Update in earlier implementation -csp2
    public static void updateRelevantAgreements(map<ID,Case> newMap,map<ID,Case> oldMap){
        system.debug('--BEGIN : updateRelevantAgreements--');
        set<ID> casesForAgreementUpdateSet = new set<ID>();
        list<Case> filteredCasesForAgreementUpdateList = new list<Case>();
        for(Case eachCase :newMap.values()){
            if(eachCase.subject != CONSTANTS.REQUEST_FOR_E_SIGNATURES && 
                eachCase.Related_Agreement__c != null && 
                eachCase.Status=='Closed' && 
                (oldMap.get(eachCase.id).status != eachCase.status) && 
                eachCase.Ready_for_Signatures__c==true){
                
                system.debug('---Matched Criteria---');
                casesForAgreementUpdateSet.add(eachCase.Related_Agreement__c);
                filteredCasesForAgreementUpdateList.add(eachCase);
            }
        }
        system.debug('---casesForAgreementUpdateSet---'+casesForAgreementUpdateSet);
        system.debug('---filteredCasesForAgreementUpdateList---'+filteredCasesForAgreementUpdateList);
        //-- Get Agreements by Case Map here..
        Apttus__APTS_Agreement__c updateAgreement = new Apttus__APTS_Agreement__c();
        Apttus__APTS_Agreement__c updateParentAgreement = new Apttus__APTS_Agreement__c(); 
        map<ID,Apttus__APTS_Agreement__c> agreementByCaseMap = new map<ID,Apttus__APTS_Agreement__c>();
        list<Apttus__APTS_Agreement__c> agreementsToUpdateList = new list<Apttus__APTS_Agreement__c>();

        agreementByCaseMap = ServiceFacade.getAgreementsByFilteredIDs(casesForAgreementUpdateSet);

        system.debug('---agreementByCaseMap---'+agreementByCaseMap);

        for(Case eachFilteredCase :filteredCasesForAgreementUpdateList){
            if(agreementByCaseMap.containsKey(eachFilteredCase.Related_Agreement__c)){
                updateAgreement = agreementByCaseMap.get(eachFilteredCase.Related_Agreement__c);
                updateAgreement.Apttus__Status__c = CONSTANTS.AGREEMENT_CLIENT_OPS_REVIEW_COMPLETE;
                agreementsToUpdateList.add(updateAgreement);
                if(updateAgreement.Apttus__Parent_Agreement__c!=null){
                    if(!CONSTANTS.AGREEMENT_CATEGORIES.contains(updateAgreement.Apttus__Parent_Agreement__r.Apttus__Status_Category__c))
                        {
                            updateParentAgreement = new Apttus__APTS_Agreement__c(id=updateAgreement.Apttus__Parent_Agreement__c,Apttus__Status__c=CONSTANTS.AGREEMENT_CLIENT_OPS_REVIEW_COMPLETE);
                            agreementsToUpdateList.add(updateParentAgreement);
                        }
                }
            }
        }

        system.debug('---agreementsToUpdateList---'+agreementsToUpdateList);

        if(!agreementsToUpdateList.isEmpty()){
            ServiceFacade.updateAgreements(agreementsToUpdateList);
        }
        system.debug('--END : updateRelevantAgreements--');
    }


    public static void weekendRecognitionTimeStamping( Map<Id, Case> newMap, Map<Id, Case> oldMap )
    {   
        system.debug('--BEGIN : weekendRecognitionTimeStamping--');
        List<Case> allCases = getTimeStampCaseFields( newMap.values() );
        Map<Id, Case> casesToBeUpdated = new Map<Id, Case>();
        casesToBeUpdated.putAll( closeAfterFinishStamp( allCases, oldMap ) );
        casesToBeUpdated.putAll( qualtricsSurveyTimeStamp( allCases, oldMap ) );

        if(!casesToBeUpdated.isEmpty()){
            try{
                update casesToBeUpdated.values();
            }catch(DmlException e){
                for(Integer i = 0; i < e.getNumDml(); i++){
                    newMap.get(casesToBeUpdated.values()[e.getDmlIndex( i )].Id ).addError( e.getDmlMessage( i ) );
                }
            }
        }
        system.debug('--END : weekendRecognitionTimeStamping--');
    }

    public static List<Case> getTimeStampCaseFields( List<Case> cases )
    {
        system.debug('--BEGIN : getTimeStampCaseFields--');
        system.debug('--END : getTimeStampCaseFields--');
        return [Select Id,Next_Business_Day_Resolved__c, Reopened__c, Origin, Status, Account.Name,ContactId, RecordTypeId from Case where Id in :cases];
    }
   
    public static final String FINISHED_STATUS_FIELD_TO_STAMP = 'Next_Business_Day_Resolved__c';
    public static final Integer FINSHED_STAMP_TIME_AHEAD = 24;
    public static String FINISHED_STATUS = 'Finished';

     //-- Helper Methods for weekendRecognitionTimeStamping
    //-- 1 - closeAfterFinishStamp
    //-- 2 - qualtricsSurveyTimeStamp
   public static List<Case> closeAfterFinishStamp( List<Case> allCases, Map<Id, Case> oldMap )
    {
        system.debug('--BEGIN : closeAfterFinishStamp--');
        
        TriggerServices service = new TriggerServices( allCases, oldMap );
        List<Case> filteredCases = (List<Case>)service.findObjectsThatChanged( Case.Status );

        // -- Calling the clearField method to clear the values in 
        clearField( filteredCases, FINISHED_STATUS_FIELD_TO_STAMP );
        filteredCases = filter( filteredCases, 'Status', FINISHED_STATUS );
        stampAllCasesWithXBusinessHoursFromNow( filteredCases, FINISHED_STATUS_FIELD_TO_STAMP, FINSHED_STAMP_TIME_AHEAD );
        system.debug('--END : closeAfterFinishStamp--');
        return filteredCases;
    }

    public static final String QUALTRICS_SURVEY_FIELD_FIELD = 'Next_Business_Day_Qualtrics__c';
    public static final Integer QUALTRICS_SURVEY_STAMP_TIME_AHEAD = 48;

    public static final String RESOLVED_STATUS = 'Resolved';
    public static final String ORIGIN_AUTO_CLOSE = 'Auto-Close';


    public static List<Case> qualtricsSurveyTimeStamp( List<Case> allCases, Map<Id, Case> oldMap )
    {
        system.debug('--BEGIN : qualtricsSurveyTimeStamp--');
        TriggerServices service = new TriggerServices( allCases, oldMap );
        List<Case> filteredCases = (List<Case>)service.findObjectsThatChanged( new List<Schema.SObjectField>{Case.Status, Case.RecordTypeId, Case.Reopened__c, Case.Origin, Case.ContactId} );

        clearField( filteredCases, QUALTRICS_SURVEY_FIELD_FIELD );
        filteredCases = filter( filteredCases, 'Status', RESOLVED_STATUS );
        Map<Id, RecordType> recordTypes = new Map<Id, RecordType>( recordTypesNotValidForQualtricsSurveyTimeStamp );
        List<Id> recordTypesList = new List<Id>( recordTypes.keySet() );
        filteredCases = filterNotWithin( filteredCases, 'RecordTypeId', recordTypesList );
        filteredCases = filter( filteredCases, 'Reopened__c', false );
        filteredCases = filterExclude( filteredCases, 'Origin', ORIGIN_AUTO_CLOSE );
        //filteredCases = filterContactQualtricsOnDate( filteredCases );
        stampAllCasesWithXBusinessHoursFromNow( filteredCases, QUALTRICS_SURVEY_FIELD_FIELD, QUALTRICS_SURVEY_STAMP_TIME_AHEAD );
        system.debug('--END : qualtricsSurveyTimeStamp--');
        return filteredCases;
    } 

    public static void clearField( List<Case> casesToClearField, String fieldPath )
    {
        system.debug('--BEGIN : clearField--');
        for(Case c : casesToClearField )
        {
            c.put( fieldPath, null );
        }
        system.debug('--END : clearField--');
    }

    public static List<Case> filter( List<Case> cases, String fieldPath, Object type )
    {
        system.debug('--BEGIN : filter--');
        List<Case> filteredCases = new List<Case>();
        for( Case c : cases )
        {
            if( c.get( fieldPath ) == type )
            {
                filteredCases.add( c );
            }
        }
        system.debug('--END : filter--');
        return filteredCases;
    }

    public static List<Case> filterExclude( List<Case> cases, String fieldPath, Object type )
    {
        system.debug('--BEGIN : filterExclude--');
        system.debug('--AFTER : filterExclude--');
        return filterNotWithin( cases, fieldPath, new List<Object>{ type } );
    }

    public static List<Case> filterNotWithin( List<Case> cases, String fieldPath, List<Object> withinType )
    {
        system.debug('--BEGIN : filterNotWithin--');
        Set<Object> withinTypesSet = new Set<Object>( withinType );
        List<Case> filteredCases = new List<Case>();
        for( Case c : cases )
        {
            Object fieldsValue = c.get( fieldPath );
            if( !withinTypesSet.contains( fieldsValue ) )
            {
                filteredCases.add( c );
            }
        }
        system.debug('--END : filterNotWithin--');
        return filteredCases;
    }

    private static void stampAllCasesWithXBusinessHoursFromNow( List<Case> casesToStamp, String fieldToStamp, Integer hoursFromNow )
    {   
        system.debug('--BEGIN : stampAllCasesWithXBusinessHoursFromNow--');
        for( Case caseFinished : casesToStamp )
        {
            timeStampBusinessDay( caseFinished, fieldToStamp,  hoursFromNow, DateTime.now() );
        }
        system.debug('--END : stampAllCasesWithXBusinessHoursFromNow--');
    }

    public static void timeStampBusinessDay( Case caseToStamp, String fieldToStamp, Integer numHoursFromNow, DateTime startingTime )
    {
        system.debug('--BEGIN : timeStampBusinessDay--');
        final Integer NUM_SECONDS_IN_HOUR = 3600;
        DateTime nextBusinessTime = BusinessHours.add( normalBusinessHours.Id, startingTime, NUM_SECONDS_IN_HOUR * numHoursFromNow * 1000L );
        caseToStamp.put( fieldToStamp, nextBusinessTime );
        system.debug('--END : timeStampBusinessDay--');
    }

    public static BusinessHours normalBusinessHours
    {
        get
        {
            if( normalBusinessHours == null )
            {
                normalBusinessHours = ServiceFacade.getDefaultBusinessHours();
            }
            return normalBusinessHours;
        }
        private set;
    }


    public static List<RecordType> recordTypesNotValidForQualtricsSurveyTimeStamp
    {
        get
        {
            if( recordTypesNotValidForQualtricsSurveyTimeStamp == null )
            {
                // -- Change this to calling the relevant service
                recordTypesNotValidForQualtricsSurveyTimeStamp  = [Select Id
                                                                 from RecordType
                                                              where Name in ('Technical Issue','Sales Ops') 
                                                                and sObjectType='Case'
                                                              ];
            }
            return recordTypesNotValidForQualtricsSurveyTimeStamp;
        }
        private set;
    }

    public static map<ID,Case> getCasesbyIDList(set<ID> parentCaseIDList){
        map<ID,Case> caseMap = new map<ID,Case>();

        system.debug('--BEGIN : getCasesbyIDList--');

        for(Case eachCase:[Select Id,Global_Region__c,Category__c,EntitlementID, Owner.Email, CC_Emails__c, SuppliedEmail, Priority, RecordTypeId, OwnerId, Customer_Support_Request_Form__c 
                                            From Case 
                                            Where id IN :parentCaseIDList]){
            
                caseMap.put(eachCase.ID,eachCase);
            
        }
        if(!caseMap.isEmpty()){
            return caseMap;
        }

        system.debug('--END : getCasesbyIDList--');
        return caseMap;
    }

    public static void updateCaseOwnerToAccountOwner(list<Case> newList){
        system.debug('--BEGIN : updateCaseOwnerToAccountOwner--');
        Set<ID> caseAccountIDSet = new Set<ID>();

        Map<ID,list<AccountTeamMember>> accountIDToAccountTeamMap = new map<ID,list<AccountTeamMember>>();
        list<Case> filteredCaseList = new list<Case>();
        for(Case eachCase:newList){
            if(eachCase.Type=='IQ Review' || eachCase.Type=='AQ Review'){ 
                caseAccountIDSet.add(eachCase.accountID);
                filteredCaseList.add(eachCase);
            }
        }

        accountIDToAccountTeamMap = getAccountTeamRoleDetails(caseAccountIDSet);
        
        system.debug('===accountIDToAccountTeamMap==='+accountIDToAccountTeamMap);
        for(Case eachCase:filteredCaseList){
            if(accountIDToAccountTeamMap.containsKey(eachCase.accountID)){
                for(AccountTeamMember eachAM :accountIDToAccountTeamMap.get( eachCase.accountID)){
                    if(eachAM.TeamMemberRole =='Senior Account Owner'){
                        system.debug('===Owner ID ==='+eachAM.UserID);
                        eachCase.OwnerID = eachAM.UserID;
                    }
                }
            }else{
                 system.debug('===ELSE  Owner ID ===');
                 eachCase.OwnerID = eachCase.Account.Owner.Manager.ID;
            }
        }

        system.debug('--END : updateCaseOwnerToAccountOwner--');

    }

    public static map<ID,list<AccountTeamMember>> getAccountTeamRoleDetails(Set<ID> caseAccountIDSet){

        system.debug('--BEGIN : getAccountTeamRoleDetails--');
        map<ID,list<AccountTeamMember>> accountIDToAccountTeamMap = new  map<ID,list<AccountTeamMember>>();
        for(AccountTeamMember eachAccTeamMem : [SELECT Id,UserID,accountID,TeamMemberRole FROM AccountTeamMember where accountID IN :caseAccountIDSet ORDER BY accountID]){

            if(!accountIDToAccountTeamMap.containsKey(eachAccTeamMem.accountID)){
                accountIDToAccountTeamMap.put(eachAccTeamMem.accountID,new list<AccountTeamMember>{eachAccTeamMem});
            }else{
                accountIDToAccountTeamMap.get(eachAccTeamMem.accountID).add(eachAccTeamMem);
            }
        }

        if(!accountIDToAccountTeamMap.isEmpty()){
            return accountIDToAccountTeamMap;
        }  
        system.debug('--END : getAccountTeamRoleDetails--');
        return null; 
    }


    public static void businessSupportCaseMemberReview(list<Case> newList){
        system.debug('--BEGIN : businessSupportCaseMemberReview--');
        list<Case> createCaseList = new list<Case>();
        String queueID = queueNameAndIDMap.get('Business_Support');

        for(Case eachCase:newList){
            if((eachCase.RecordTypeID==recordTypeNameandIDMap.get('Member Review') && eachCase.Account.Owner.Username =='businesssupport@appnexus.com')){
                Case newCase = new Case(AccountID = eachCase.AccountID,
                    Category__c='Business Support',
                    OwnerID = queueID,
                    Related_Case__c = eachCase.ID,
                    Subject=eachCase.Type,
                    recordtypeid = recordTypeNameandIDMap.get('Business Support')
                    );

                createCaseList.add(newCase);
            }
        }
        system.debug('--END : businessSupportCaseMemberReview--');
    }
   
   public static List<case> NotClosedCasesByOwner(String OwnerId) {
   
       return [SELECT id, lastmodifieddate FROM Case where ownerid =: OwnerId and isClosed = false ORDER BY LastModifiedDate DESC];
   } 

   public static void assignUserRegionToCase(list<Case> caseList){
    
    system.debug('==assignUserRegionToCase==');
    Id currentuserID = Userinfo.getUserID();
    LoginGeo curUserLoginGeoInfo  = new LoginGeo();
    map<String,CountryISOCodes__c> isoCodeCountryMap  = new map<String,CountryISOCodes__c>();
    String country = '';
    String submissionRegion = '';

    try{
        isoCodeCountryMap = Utility.getCountryISOCodeSettings();
        system.debug('===isoCodeCountryMap==='+isoCodeCountryMap);
        if(Userinfo.getUserType() != 'Guest'){
            curUserLoginGeoInfo = [SELECT City,Country,CountryIso,CreatedById,CreatedDate,Id,Latitude,LoginTime,Longitude,PostalCode,Subdivision,SystemModstamp FROM LoginGeo where createdbyid = : currentuserID ORDER BY LoginTime LIMIT 1];
        }
        system.debug('==curUserLoginGeoInfo=='+curUserLoginGeoInfo);
        
        if(curUserLoginGeoInfo!=null){
            if(isoCodeCountryMap.containsKey(curUserLoginGeoInfo.CountryIso)){
            system.debug('===isoCodeCountryMap===INSIDE');
            country = isoCodeCountryMap.get(curUserLoginGeoInfo.CountryIso).Name;
            submissionRegion = isoCodeCountryMap.get(curUserLoginGeoInfo.CountryIso).Submission_Region__c; 
            }
        }

            system.debug('==country=='+country);
            system.debug('==submissionRegion=='+submissionRegion);

            for(Case eachCase : caseList){
                eachCase.Submission_Country__c = country;
                eachCase.Submission_Region__c = submissionRegion;
                system.debug('==eachCase=='+eachCase);
            }
        }catch(Exception ex){
            system.debug('==Exception=='+ex.getMessage());
            Exception_Logs__c el = new Exception_Logs__c();
            el.Exception_Message__c = ex.getMessage()+'--'+ex.getCause()+'--'+ex.getLineNumber()+'--'+ex.getStackTraceString();
            el.Exception_Message__c +='\n'+Userinfo.getuserType();
            insert el;
        }
    }
}