global class CreateCommunityUser {

  WebService static string createUserFromContact(String contactId) {
    List<Contact> contLst = new List<Contact>();
    List<User> usrLst = new List<User>();
    String retStr = '-';

    Contact objectContact = ContactServices.getContactById(contactId);
    if(objectContact.Email == null || objectContact.Email == '' )
         retStr = CONSTANTS.Email_Required_Message;
    else{
       contLst = ContactServices.getContactByEmailId(objectContact.email);
       if(contLst.size() > 1)
          retStr = CONSTANTS.Duplicate_Contact_Message;
       else{
          usrLst = [select id from user where IsActive = true and username =:objectContact.Email];
          if(usrLst.size() > 0) {
          retStr = CONSTANTS.Duplicate_User_Message;
          }
        }
    }    
   if(retStr == '-'){
        
    //Account acc = [SELECT OAS_Status__c FROM Account where id = :objectContact.AccountId];
    try {
      //Select community profile that you want to assign
      List<Profile> pf = [SELECT Id FROM profile 
                    WHERE name= : CONSTANTS.Customer_Portal_User LIMIT 1]; //Communities + Knowledge
                    
      //PermissionSet ps = [SELECT Id FROM PermissionSet WHERE name = 'CSP_Access_all_Data_Categories']; //Communities + Knowledge
      //Create user 
     // System.debug('jd111-->'+objectContact.Email + '--'+objectContact.Id+'--'+objectContact.FirstName+'--'+objectContact.LastName+'--'+pf[0].Id);
      User newCommunitiesUser;
      if(pf.size()>0) {
          newCommunitiesUser = new User(contactId=objectContact.Id, 
                                  username=objectContact.Email, 
                                  firstname=objectContact.FirstName,
                                  lastname=objectContact.LastName, 
                                  email=objectContact.Email,
                                  communityNickname = objectContact.LastName + '_'+Math.random(),
                                  alias = string.valueof(objectContact.FirstName.substring(0,1) + 
                                          objectContact.LastName.substring(0,1) + Math.random() ).substring(0,5), 
                                  profileid = pf[0].Id,
                                  emailencodingkey='UTF-8',
                                  languagelocalekey='en_US', 
                                  localesidkey='en_US', 
                                  timezonesidkey='America/Los_Angeles');                  
              insert newCommunitiesUser;
      }
     // system.debug('New user created starts *****-->'+ newCommunitiesUser.id);
      //Commenting Start for Master Contract Logic
      /*Map<String, String> recTypPSetMap = new Map<String, String>(); 
      recTypPSetMap.put('Yieldex Analytics Services Agreement', 'CSP_Access_Yieldex_Analytics_Data_Category');
      recTypPSetMap.put('Yieldex Direct Services Agreement', 'CSP_Access_Yieldex_Direct_Data_Category');
      recTypPSetMap.put('ANC Exhibit', 'CSP_Access_all_Data_Categories');
      recTypPSetMap.put('Bidder Exhibit', 'CSP_Access_Bidders_Data_Category');
      recTypPSetMap.put('ASI Exhibit', 'CSP_Access_ASI_Data_Category');

      Id mcId;
      String mcRecType;*/
      //Commenting End for Master Contract Logic
      
      // For SFA-5921 Jira request
       system.debug('New changes starts *****-->');
     List<String> psNames = new List<String>();
     List<Apttus__APTS_Agreement__c > aptusAgreementsList = ServiceFacade.getAgreementsByAccount(objectContact.AccountId, CONSTANTS.In_Effect_Status );
     for(Apttus__APTS_Agreement__c tmpAgree : aptusAgreementsList){
         if(tmpAgree != null){
         if( Community_Permission_Set__c.getValues(tmpAgree.recordtype.name) != null) 
                       psNames.add(Community_Permission_Set__c.getValues(tmpAgree.recordtype.name).PermissionSetName__c); 
         //exceptional categories
         if(objectContact.Account.Region_rdp__c == CONSTANTS.Region_Brazil  && tmpAgree.recordtype.name == CONSTANTS.OAS_MSA_RecordType)
           psNames.add(CONSTANTS.CSP_OAS_Category);
         //if((objectContact.Account.Account_Owner_Name_Text__c == 'Business Support' || objectContact.Account.Support_Level__c =='Foundation' ) && tmpAgree.recordtype.name == 'Bidder Service Order' )
           psNames.add(CONSTANTS.CSP_Bidders_Category);
       } 
       }
       
       //For all Appnexus Employee Contacts - assign this permission set
       if((objectContact.Account.Name.contains(CONSTANTS.Account_Name_AppNexus) || objectContact.Email.contains(CONSTANTS.Contact_Email_Check)))
           psNames.add(CONSTANTS.CSP_All_Category);
           
       
      
     //Commenting Start for Master Contract Logic
     /* for(Master_Contract__c tmpMC : [select id, recordtype.name from Master_Contract__c where Account__c = :objectContact.AccountId ORDER BY LastModifiedDate DESC limit 1]){
         mcId = tmpMC.id;
         mcRecType = tmpMC.recordtype.name;
      }
      List<String> psNames = new List<String>();
      if(mcId != null){
          if( mcRecType == 'MSA'){
              for(Addendum__c tmpAddendum : [select recordtype.name from Addendum__c where NEW_Master_Contract__c =:mcId and status__c = 'Active']){
                    if( recTypPSetMap.get(tmpAddendum.recordtype.name) != null)
                       psNames.add(recTypPSetMap.get(tmpAddendum.recordtype.name));
              }
          }else if(mcRecType == 'Data Partner'){
              for(Addendum__c tmpAddendum : [select recordtype.name from Addendum__c where NEW_Master_Contract__c =:mcId and status__c = 'Active' and recordtype.name = 'ANC Exhibit' ]){
                    if( recTypPSetMap.get(tmpAddendum.recordtype.name) != null)
                       psNames.add(recTypPSetMap.get(tmpAddendum.recordtype.name));
              }
            
          }else if(mcRecType == 'OAS'){
             if(objectContact.Account.OAS_Status__c != null && objectContact.Account.OAS_Status__c == 'Active' )
                  psNames.add('CSP_Access_OAS_Data_Category');
          }
      }
      if(objectContact.Account.OAS_Status__c != null && objectContact.Account.OAS_Status__c == 'Active' )
           psNames.add('CSP_Access_OAS_Data_Category');
     */
     //Commenting end for Master Contract Logic
      
      if( psNames.size() > 0 ){      
          //PermissionSet ps = [SELECT Id FROM PermissionSet WHERE name = 'CSP_Access_all_Data_Categories']; //Communities + Knowledge
          List<PermissionSetAssignment> psaLst = new List<PermissionSetAssignment>();
          for( PermissionSet ps : [SELECT Id FROM PermissionSet WHERE name in  :psNames]){
               system.debug('jd3-->'+ps.id);
                PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = ps.id, AssigneeId = newCommunitiesUser.id);
                psaLst.add(psa);
          }
          insert psaLst;
      }
      return CONSTANTS.User_Success_Message;
    } catch(DMLException ex) {
        if(ex.getDMLMessage(0).contains(CONSTANTS.Contains_Duplicate)) {
          return '[ERROR]';
        }
        return ex.getDMLMessage(0);
    }
   }else{
      return retStr;
   }
  }
  
  WebService static string resetPasswordUsr(String contactId) {
      Set<id> profileidSet = new Set<id>();
      List<Profile> pf1 = [SELECT Id FROM profile WHERE name=: CONSTANTS.Customer_Portal_User  LIMIT 1];
    //  List<Profile> pf2 = [SELECT Id FROM profile WHERE name=: CONSTANTS.Communities_knowledge  LIMIT 1];
    
      profileidSet.add(pf1[0].id);
     // profileidSet.add(pf2[0].id);
      Id commUsrId = [SELECT id FROM user WHERE contactid = :contactId AND profileid in :profileidSet LIMIT 1].id;
      if(commUsrId != null){
            System.resetPassword(commUsrId, true);
            return 'true';
      }else{
            return CONSTANTS.User_Error_Message;
      }
  }
}