@isTest
 private class SalesforceToSlackTest{ 
 
 static void createTestData(){
     TestingUtils.createEssentialCustomSettings();
    RecordType rt = [select id, name from recordtype where sobjecttype = 'Account' and name = 'Customer Account' limit 1];  
    Account[] accounts = TestingUtils.createAccounts(1, 'testAccount', false);
    for (Account acn : accounts){
        acn.recordtypeid = rt.id;
        acn.vertical_new__c = 'ATBs and ATDs';
        acn.Region_rdp__c = 'US/Canada';
        acn.Global_rdp__c = 'US/Canada';
    }

    insert accounts;

    RecordType rt2 = [select id, name from recordtype where sobjecttype = 'Account' and name = 'Console Member' limit 1];  

    Account[] members = TestingUtils.createAccounts(1, 'testAccountmem', false);
    members[0].AppNexus_Member_Id__c = '10000';
    members[0].parentid = accounts[0].id;   
    insert members;
 }
 
 static testmethod void testDoGetSalesforceToSlackCorrect(){
     
     SlackSettings__c[] ssUpdate = [select id, name, token__c, team_domain__c from SlackSettings__c where name = '/memberinfo' limit 1];
     if (ssUpdate.isEmpty()){ 
         SlackSettings__c ss = new SlackSettings__c(name = '/memberinfo',token__c = 'abcde', team_domain__c = 'testdomain');
         insert ss;
     }else{
         ssUpdate[0].token__c = 'abcde';
         ssUpdate[0].team_domain__c = 'testdomain';
         update ssUpdate;
     }
     
     createTestData();
     
     RestRequest req = new RestRequest(); 
     RestResponse res = new RestResponse();

     req.requestURI = '/services/apexRest/SalesforceToSlack/';
     req.httpMethod = 'GET';
     req.addParameter('token','abcde');
     req.addParameter('team_domain','testdomain');
     req.addParameter('command','/memberinfo');
     req.addParameter('text','10000');

     RestContext.request = req;
     RestContext.response = res;
     SalesforceToSlack.doPost();      
 }
 
  static testmethod void testDoGetSalesforceToSlackWrongDomain(){
     
     SlackSettings__c[] ssUpdate = [select id, name, token__c, team_domain__c from SlackSettings__c where name = '/memberinfo' limit 1];
     if (ssUpdate.isEmpty()){ 
         SlackSettings__c ss = new SlackSettings__c(name = '/memberinfo',token__c = 'abcde', team_domain__c = 'testdomain');
         insert ss;
     }else{
         ssUpdate[0].token__c = 'abcde';
         ssUpdate[0].team_domain__c = 'testdomain';
         update ssUpdate;
     }
     
     createTestData();
     
     RestRequest req = new RestRequest(); 
     RestResponse res = new RestResponse();

     req.requestURI = '/services/apexRest/SalesforceToSlack/';
     req.httpMethod = 'GET';
     req.addParameter('token','abcde');
     req.addParameter('team_domain','none');
     req.addParameter('command','/memberinfo');
     req.addParameter('text','10000');

     RestContext.request = req;
     RestContext.response = res;
     SalesforceToSlack.doPost();      
 }
 
  static testmethod void testDoGetSalesforceToSlackWrongToken(){
     
     SlackSettings__c[] ssUpdate = [select id, name, token__c, team_domain__c from SlackSettings__c where name = '/memberinfo' limit 1];
     if (ssUpdate.isEmpty()){ 
         SlackSettings__c ss = new SlackSettings__c(name = '/memberinfo',token__c = 'abcde', team_domain__c = 'testdomain');
         insert ss;
     }else{
         ssUpdate[0].token__c = 'abcde';
         ssUpdate[0].team_domain__c = 'testdomain';
         update ssUpdate;
     }
     
     createTestData();
     
     RestRequest req = new RestRequest(); 
     RestResponse res = new RestResponse();

     req.requestURI = '/services/apexRest/SalesforceToSlack/';
     req.httpMethod = 'GET';
     req.addParameter('token','12345');
     req.addParameter('team_domain','testdomain');
     req.addParameter('command','/memberinfo');
     req.addParameter('text','10000');

     RestContext.request = req;
     RestContext.response = res;
     SalesforceToSlack.doPost();      
 }
 
   static testmethod void testDoGetSalesforceToSlackInvalidId(){
     
     SlackSettings__c[] ssUpdate = [select id, name, token__c, team_domain__c from SlackSettings__c where name = '/memberinfo' limit 1];
     if (ssUpdate.isEmpty()){ 
         SlackSettings__c ss = new SlackSettings__c(name = '/memberinfo',token__c = 'abcde', team_domain__c = 'testdomain');
         insert ss;
     }else{
         ssUpdate[0].token__c = 'abcde';
         ssUpdate[0].team_domain__c = 'testdomain';
         update ssUpdate;
     }
     
     createTestData();
     
     RestRequest req = new RestRequest(); 
     RestResponse res = new RestResponse();

     req.requestURI = '/services/apexRest/SalesforceToSlack/';
     req.httpMethod = 'GET';
     req.addParameter('token','abcde');
     req.addParameter('team_domain','testdomain');
     req.addParameter('command','/memberinfo');
     req.addParameter('text','gcjgh');

     RestContext.request = req;
     RestContext.response = res;
     SalesforceToSlack.doPost();      
 }
 
    static testmethod void testDoGetSalesforceToSlackWrongCommand(){
     
     
     SlackSettings__c ss = new SlackSettings__c(name = '/testinfo',token__c = 'abcde', team_domain__c = 'testdomain');
     insert ss;

     createTestData();
     
     RestRequest req = new RestRequest(); 
     RestResponse res = new RestResponse();

     req.requestURI = '/services/apexRest/SalesforceToSlack/';
     req.httpMethod = 'GET';
     req.addParameter('token','abcde');
     req.addParameter('team_domain','testdomain');
     req.addParameter('command','/testinfo');
     req.addParameter('text','10000');

     RestContext.request = req;
     RestContext.response = res;
     SalesforceToSlack.doPost();      
 }
 
 }