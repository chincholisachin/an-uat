@isTest
private class RainMakerTest
{
    private static Account testAccount= null;
    private static RainmakerController controller;
    
    private static void loadData(){
        
        User u1 = TestingUtils.getUser('* Sales Operations');
        
        System.runAs(u1){
        
            Account[] accounts = AccountTriggerTest.createAccounts('Customer Account',200);
            insert accounts;
            
        }
        
        List<Account> acn = new List<Account>([select id, recordtype.name from account]);

        Account[] members = AccountTriggerTest.createAccounts('Console Member',200);
        for (Integer i = 0; i<200; i++){
            members[i].AppNexus_Member_Id__c = String.valueOf(i+10000);
            members[i].parentid = acn[i].id;
            if (Math.mod(i,25) == 0) members[i].EDP_Flag__c = true; 
        }
       
        insert members;
    
    }
     
    private static testMethod void controllerTest(){    
        loadData();
        Account acn = [select id,name from account where recordtype.name  = 'Console Member' and edp_flag__c = false limit 1];
        Account[] acn2 = [select id,name from account where recordtype.name  = 'Console Member' and edp_flag__c = true limit 2];
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/RainMaker');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',acn.id);
        controller = new RainmakerController();
        List<SelectOption> sellerTypeVelues = controller.SellerTypeValues;
        List<SelectOption> WillRunDealsValues = controller.WillRunDealsValues;
        List<SelectOption> firstPartyDataAvailValues = controller.firstPartyDataAvailValues;
        controller.selectedMembers.add(new SelectOption(acn2[0].id, acn2[0].name));
        controller.selectedMembers.add(new SelectOption(acn2[1].id, acn2[1].name));
        if (controller.selectedMembers.size()> 0) controller.save();      
        controller.cancel();
        Test.stopTest();
        
    }
        
    
}