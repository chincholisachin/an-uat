@RestResource(urlMapping='/SalesforceToSlack')
global class SalesforceToSlack {
    @HttpPost
    global static void doPost() {
        String strResponse;
        String memberId = RestContext.request.params.get('text');
        String authToken = RestContext.request.params.get('token');
        String domain = RestContext.request.params.get('team_domain');
        String command = RestContext.request.params.get('command');
        RestContext.response.addHeader('Content-Type', 'application/json');

        SlackSettings__c[] slack = [select name, team_domain__c, token__c from SlackSettings__c where name =:command and team_domain__c =:domain limit 1];

        if (slack.isEmpty()){
            strResponse = 'Unauthorized Access. Please contact your Salesforce Administrator.';
        }else if(!authToken.equals(slack[0].token__c)){
            strResponse = 'Unauthorized Access. Please contact your Salesforce Administrator.';
        }else if(command.equalsIgnoreCase('/memberinfo')){        
                Account[] acn = [select id, AppNexus_Member_ID__c, parent.name, parentid, Name, (select user.name,teammemberrole from accountteammembers ) from Account where AppNexus_Member_ID__c=:memberId and appnexus_member_id__c != null limit 1];
                if(acn.isEmpty()){
                    strResponse = 'Member not found or you entered an invalid Member Id';
                }else{
                    JSONGenerator gen = JSON.createGenerator(true);
                    gen.writeStartObject();
                    gen.writeFieldName('attachments');
                    gen.writeStartArray();
                    gen.writeStartObject();
                    gen.writeStringField('pretext', 'Here is the account team for AppNexus Member');
                    gen.writeStringField('color', '#fb862b' );
                    gen.writeFieldName('fields');
                    gen.writeStartArray();
                    gen.writeStartObject();
                        gen.writeStringField('title', 'Customer Account:');
                        gen.writeStringField('value', acn[0].parent.name);
                    gen.writeEndObject();
                    for (AccountTeamMember atm: acn[0].accountteammembers){
                        gen.writeStartObject();
                        gen.writeStringField('title', atm.teammemberrole+':');
                        gen.writeStringField('value', atm.user.name);
                        gen.writeEndObject();
                    }
                    gen.writeEndArray();
                    gen.writeStringField('title', acn[0].name + ' (' + acn[0].AppNexus_Member_ID__c +')');
                    gen.writeStringField('title_link', Label.BaseDomain +'/'+acn[0].id );
                    gen.writeStringField('footer', 'Brought to you by Salesforce Team');
                    gen.writeEndObject();
                    gen.writeEndArray();
                    gen.writeEndObject();
                    strResponse = gen.getAsString();
                }
        } else {
            strResponse = 'Invalid Slash Command. Please contact your Salesforce Administrator.';
        }
        RestContext.response.responseBody = Blob.valueOf(strResponse);
    }
}