global with sharing class z_BatchableMetadataDump2 implements Database.Batchable<SObject>, Database.Stateful {
    /**
    * Batch job that creates SObjectDescription, ChildRelation and FieldDescription records.
    */
    global Boolean executeBatch3;

    global z_BatchableMetadataDump2() {
        // flag if z_BatchableMetadataDump2 is executed after this class is completed.
        this(false);
    }

    global z_BatchableMetadataDump2(Boolean executeBatch3) {
        this.executeBatch3 = executeBatch3;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('select Id, name__c From z_MetaDumpSetting__c where capability__c != null and inscope__c = true');
    }

    global void execute(Database.BatchableContext BC, z_MetaDumpSetting__c[] scope){
        // caching.
        Map<String, z_SObjectDescription__c> mapSObjectDesc = new Map<String, z_SObjectDescription__c>();
        Map<String, List<z_ChildRelationship__c>> mapSObjChildRelationship = new Map<String, List<z_ChildRelationship__c>>();

        // Get z_SObjectDescription__c & z_ChildRelationship__c records.
        for (z_MetaDumpSetting__c setting : scope) {
            Schema.DescribeSObjectResult objDescribe = z_DescribeHelper.getObjectDescribe(setting.name__c);

            // SObject description.
            z_SObjectDescription__c oSObjectDescription = z_DescribeHelper.createSObjectDescription(objDescribe);
            mapSObjectDesc.put(setting.name__c, oSObjectDescription);

            // Child Relationships.
            List<z_ChildRelationship__c> lChildRelations = z_DescribeHelper.createChildRelationship(objDescribe);

            if (lChildRelations.size() > 0) {
                mapSObjChildRelationship.put(setting.name__c, lChildRelations);
            }
        }

        // Create z_SObjectDescription__c records
        //Database.insert(mapSObjectDesc.values(), false);
        Database.upsert(mapSObjectDesc.values(), z_SObjectDescription__c.Fields.UniqueKey__c, false);

        // Iterate over child records and relate them to newly saved SObjectDescription
        List<z_ChildRelationship__c> lSObjectChilds = new List<z_ChildRelationship__c>();
        for (String objName : mapSObjectDesc.keySet()) {
            if (mapSObjChildRelationship.containsKey(objName)) {
                for (z_ChildRelationship__c childRel : mapSObjChildRelationship.get(objName)) {
                    childRel.z_ParentSObjectDescription__c = mapSObjectDesc.get(objName).Id;

                    lSObjectChilds.add(childRel);
                }
            }
        }

        //Database.insert(lSObjectChilds, false);
        Database.upsert(lSObjectChilds, z_ChildRelationship__c.fields.uniquekey__c, false);

        // create z_FieldDescription__c records for every object.
        List<z_FieldDescription__c> lFieldDescriptionPerSObject = new List<z_FieldDescription__c>();
        Set<Id> inScopeObjects = new Set<ID>();
        for (String objName : mapSObjectDesc.keySet()) {
            lFieldDescriptionPerSObject.addAll(z_DescribeHelper.createFieldDescriptionRecords(mapSObjectDesc.get(objName).Id, objName));
            inScopeObjects.add(mapSObjectDesc.get(objName).Id);
        }
        
        //query existing z_FieldDescription__c records and create a map of them based on uniquekey
        Set<String> uniqueKeySet = new Set<String>();
        for (z_FieldDescription__c z: lFieldDescriptionPerSObject){
            uniqueKeySet.add(z.uniquekey__c);
        }
        
        Set<z_FieldDescription__c> fieldsToDelete = new Set<z_FieldDescription__c>();
        for (z_FieldDescription__c fields : [select id, isDeleted__c, uniquekey__c from z_FieldDescription__c where z_SObjectDescription__c in: inScopeObjects]){
            if (!uniqueKeySet.contains(fields.uniquekey__c)) {
                fields.isDeleted__c = true;
                fieldsToDelete.add(fields);
            }
        }
        
        
        
        //if any of the unique key doesn't exist in the map, mark those fields as deleted. 
        
        if (lFieldDescriptionPerSObject.size() > 0) {
            //Database.insert(lFieldDescriptionPerSObject, false);
            Database.upsert(lFieldDescriptionPerSObject, z_FieldDescription__c.fields.uniquekey__c, false);
        }
        if (fieldsToDelete.size() > 0) {
            List<z_FieldDescription__c> deletedFields = new List<z_FieldDescription__c>();
            deletedFields.addAll(fieldsToDelete);
            update deletedFields;
        }
    }

    global void finish(Database.BatchableContext BC){
        if(!executeBatch3)
            return;

        Datetime dt = System.now();
        String coreExpression = z_Util.convertToStringForScheduler(dt.addMinutes(2));
        System.schedule('z_BatchableMetadataDump3', coreExpression, new z_SchedulableBatchableMetadataDump3());
    }
}