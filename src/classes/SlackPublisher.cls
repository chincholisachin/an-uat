/*
 * Publishes a message to Slack. Invocable from Process Builder.
 */
public with sharing class SlackPublisher {
     
    public class Payload {
        @InvocableVariable(label='Slack Webhook URL')
        public String slackURL;
        @InvocableVariable(label='Message Text')
        public String messageText;
        @InvocableVariable(label='Slack Icon')
        public String slackIcon;
        @InvocableVariable(label='Slack Username')
        public String slackUsername;
        @InvocableVariable(label='Field 1 Name')
        public String field1Name;
        @InvocableVariable(label='Field 1 Value')
        public String field1Value;
        @InvocableVariable(label='Field 2 Name')
        public String field2Name;
        @InvocableVariable(label='Field 2 Value')
        public String field2Value;
        @InvocableVariable(label='Field 3 Name')
        public String field3Name;
        @InvocableVariable(label='Field 3 Value')
        public String field3Value;
        @InvocableVariable(label='Field 4 Name')
        public String field4Name;
        @InvocableVariable(label='Field 4 Value')
        public String field4Value;      
        @InvocableVariable(label='Field 5 Name')
        public String field5Name;
        @InvocableVariable(label='Field 5 Value')
        public String field5Value;
        @InvocableVariable(label='Field 6 Name')
        public String field6Name;
        @InvocableVariable(label='Field 6 Value')
        public String field6Value;
        @InvocableVariable(label='Field 7 Name')
        public String field7Name;
        @InvocableVariable(label='Field 7 Value')
        public String field7Value;
        @InvocableVariable(label='Field 8 Name')
        public String field8Name;
        @InvocableVariable(label='Field 8 Value')
        public String field8Value;
        @InvocableVariable(label='Field 9 Name')
        public String field9Name;
        @InvocableVariable(label='Field 9 Value')
        public String field9Value;
        @InvocableVariable(label='Field 10 Name')
        public String field10Name;
        @InvocableVariable(label='Field 10 Value')
        public String field10Value;

        @InvocableVariable(label='Linked Record ID')
        public String recordId;
    }
     
    @InvocableMethod(label='Post to Slack')
    public static void postToSlack(List<Payload> payloads) {
        Payload p = payloads[0]; // If bulk, only post first to avoid overloading Slack channel
        Map<String,Object> msg = new Map<String,Object>();
        String messageText = p.messageText + '\n>>>';
        if(!String.isBlank(p.field1Name) && !String.isBlank(p.field1Value)) {
            messageText += '\n*' + p.field1Name + ':* ' + p.field1Value;
        }
        if(!String.isBlank(p.field2Name) && !String.isBlank(p.field2Value)) {
            messageText += '\n*' + p.field2Name + ':* ' + p.field2Value;
        }
        if(!String.isBlank(p.field3Name) && !String.isBlank(p.field3Value)) {
            messageText += '\n*' + p.field3Name + ':* ' + p.field3Value;
        }
        if(!String.isBlank(p.field4Name) && !String.isBlank(p.field4Value)) {
            messageText += '\n*' + p.field4Name + ':* ' + p.field4Value;
        }
        
        if(!String.isBlank(p.field5Name) && !String.isBlank(p.field5Value)) {
            messageText += '\n*' + p.field5Name + ':* ' + p.field5Value;
        }
        if(!String.isBlank(p.field6Name) && !String.isBlank(p.field6Value)) {
            messageText += '\n*' + p.field6Name + ':* ' + p.field6Value;
        }
        if(!String.isBlank(p.field7Name) && !String.isBlank(p.field7Value)) {
            messageText += '\n*' + p.field7Name + ':* ' + p.field7Value;
        }
        if(!String.isBlank(p.field8Name) && !String.isBlank(p.field8Value)) {
            messageText += '\n*' + p.field8Name + ':* ' + p.field8Value;
        }
        if(!String.isBlank(p.field9Name) && !String.isBlank(p.field9Value)) {
            messageText += '\n*' + p.field9Name + ':* ' + p.field9Value;
        }
        if(!String.isBlank(p.field10Name) && !String.isBlank(p.field10Value)) {
            messageText += '\n*' + p.field10Name + ':* ' + p.field10Value;
        }
        
        if(!String.isBlank(p.recordId)) {
            messageText += '\n*Link:* <' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + p.recordId + '>';
        }
        msg.put('text', messageText);
        msg.put('mrkdwn', true);
        if (!String.isBlank(p.slackIcon)){
        msg.put('icon_url', p.slackIcon);
        }
        if (!String.isBlank(p.slackUsername)){
        msg.put('username', p.slackUsername);
        }
        String body = JSON.serialize(msg);
        System.debug('JSON Message is '+body);    
        System.enqueueJob(new QueueableSlackCall(p.slackURL, 'POST', body));
    }
     
    public class QueueableSlackCall implements System.Queueable, Database.AllowsCallouts {
        private final String url;
        private final String method;
        private final String body;
         
        public QueueableSlackCall(String url, String method, String body) {
            this.url = url;
            this.method = method;
            this.body = body;
        }
         
        public void execute(System.QueueableContext ctx) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(url);
            req.setMethod(method);
            req.setBody(body);
            Http http = new Http();
            if(!Test.isRunningTest()) {
                HttpResponse res = http.send(req);
            }
        }
    }    
}