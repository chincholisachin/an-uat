@isTest

private class FixOpportunitiesTest {

    private static FixOpportunities controller;
    private static Opportunity oppy;
    
    private static testMethod void testFixOpportunitiesPage(){
        
        User commercial = TestingUtils.getUser('* Commercial');
 
        insert new Bypass__c(SetupOwnerId=UserInfo.getOrganizationId(), WFR_Bypass__c=true);

        Opportunity[] opps = OpportunityTriggerTest.prepareOppData();
        for (Opportunity o: opps){
            o.Stage_Change_Date__c = Date.today().addDays(-125);
            o.stagename = 'Proposal/Scoping';
            o.closeDate = Date.today();
            o.ownerid = commercial.id;
        }
        insert opps;            

        System.runAs ( commercial ){
            
            Test.startTest();
            PageReference pageRef = new PageReference('/apex/fixOpportunities');
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(new Opportunity());
            controller = new FixOpportunities(sc);
            for (FixOpportunities.OppIssue o : controller.OpIssue){
                o.opp.closedate = date.today();
            }

            controller.saveChanges();
            Test.stopTest();
            
            System.assertEquals(200,[SELECT id FROM opportunity WHERE stagename = 'Proposal/Scoping' AND ownerid =: commercial.id AND closedate =: date.today()].size());
        }   
    }
}