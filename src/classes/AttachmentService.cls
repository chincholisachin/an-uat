public class AttachmentService {
    
    public static void updateAttachmentList(List<Attachment> signedAttachments) {
        Set<Id> parentIdSet= new Set<Id>();
        Attachment aptsAttachment = null;
        Map<String, Id> attachmentNameAndIdMap = new Map<String, Id>();
        List<Attachment> attachmentList = new List<Attachment>();
        for(Attachment att : signedAttachments) {
            if(String.valueOf(att.parentId.getSObjectType()).equalsIgnoreCase(CONSTANTS.ES_SIGN_AGMT_OBJECT_NAME)) {
                parentIdSet.add(att.parentId);
            }
        }
        Map<Id, echosign_dev1__SIGN_Agreement__c> esignMap = new Map<Id, echosign_dev1__SIGN_Agreement__c>([SELECT Id, Name
                                                                                                            , Apttus_EchoSign__Apttus_Agreement__c
                                                                                                            , Apttus_EchoSign__Apttus_RSPCAttachments__c
                                                                                                            , echosign_dev1__Status__c
                                                                                                            FROM echosign_dev1__SIGN_Agreement__c
                                                                                                            WHERE Id IN :parentIdSet AND Apttus_EchoSign__Apttus_Agreement__c != null LIMIT 1000]);
        
        for(Attachment att : signedAttachments) {
            if(esignMap.containsKey(att.parentId)) {
                if (att.Name.containsIgnoreCase(CONSTANTS.ES_SIGNED_ATTACHMENT_SUFFIX) ||
                    att.Name.containsIgnoreCase(CONSTANTS.ES_AUDIT_DOCUMENT_SUFFIX)) {
                        if(esignMap.get(att.ParentId).Apttus_EchoSign__Apttus_RSPCAttachments__c != null) {  
                            attachmentNameAndIdMap = (Map<String, Id>) JSON.deserialize(esignMap.get(att.ParentId).Apttus_EchoSign__Apttus_RSPCAttachments__c, Map<String, Id>.class);
                        }
                        aptsAttachment = new Attachment(
                            ParentId = attachmentNameAndIdMap.containsKey(att.Name)?
                            attachmentNameAndIdMap.get(att.Name):esignMap.get(att.ParentId).Apttus_EchoSign__Apttus_Agreement__c
                            , Name = att.Name.substringBeforeLast('_') +
                            ' -'+att.Name.substringAfterLast('-')
                            , Body = att.Body
                            , OwnerId = att.OwnerId
                            , ContentType = att.ContentType);
                        
                        attachmentList.add(aptsAttachment);
                    }
            }
        } 
        
        if(attachmentList.size() > 0) {
            insert attachmentList;      
            
        }
    }
}