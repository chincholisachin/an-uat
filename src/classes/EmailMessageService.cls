public  class EmailMessageService {
    
    public static emailSettings__c emailSettingRecord  = Utility.getEmailSettingsCS();
    public static list<Id> theIdList = new list<Id>();
    
    
    static OrgWideEmailAddress owa = [Select id, Address, DisplayName From OrgWideEmailAddress Where Address = :emailSettingRecord.Org_Wide_Email__c Limit 1];
    
    
    
    public static void handleIncomingEmailMessage(list<EmailMessage> newList){

        set<Id> parentCaseIDList = new set<Id>();
        for(EmailMessage eachEmailMessage : newList){
            parentCaseIDList.add(eachEmailMessage.ParentID);
        }

        map<ID,Case> mapCase = new map<ID,Case>(ServiceFacade.getCasesbyIDList(parentCaseIDList));

        list<String> excludedRecordTypes = emailSettingRecord.Record_Type_Exclusion__c.split(',');

        Set<String> passingPriority_set = new Set<String>();
            if(emailSettingRecord.Case_Email_Priority__c!=null) {
                for(String tempPriority : emailSettingRecord.Case_Email_Priority__c.split(',')) {
                    passingPriority_set.add(tempPriority.trim());
                }
            }

        Map<String,Boolean> fromAddressToBooleanFlagMap = new Map<String,Boolean>();
        fromAddressToBooleanFlagMap = checkEmailCaseLoop(newList);

        for(EmailMessage eachEm :newList){
            if(eachEm.FromAddress!=null && eachEm.FromAddress !=''){
                if(fromAddressToBooleanFlagMap.containsKey(eachEm.FromAddress)){
                    if(fromAddressToBooleanFlagMap.get(eachEm.FromAddress)==true){
                        eachEm.addError('Auto case loop identified. case Id:' + eachEm.parentid);
                        //--Somewhere should add recordtype filter and mapcase.containskey(parentid)
                    }else if(fromAddressToBooleanFlagMap.get(eachEm.FromAddress)==false &&
                        emailSettingRecord.Automate_Email_Handling__c==true &&
                        passingPriority_set.contains(mapCase.get(eachEm.ParentId).priority)){
                            //-- If the incoming email is not from owner--//
                        if(eachEm.Incoming == true && eachEm.FromAddress <> mapCase.get(eachEm.ParentId).Owner.Email){
                            
                                incomingEmailMessage(eachEm,mapCase.get(eachEm.ParentId),true);
                            theIdList.add(eachEm.Id);
                        }else if(eachEm.FromAddress == mapCase.get(eachEm.ParentId).Owner.Email){
                            eachEm.Incoming = false;    //Since this is actually an outgoing email from Support to Customer
                            incomingEmailMessage(eachEm,mapCase.get(eachEm.ParentId), false);
                            theIdList.add(eachEm.Id);
                        }else if (eachEm.FromAddress == mapCase.get(eachEm.ParentId).Owner.Email ) {
                            theIdList.add(eachEm.Id);
                        }   
                    }
                }
            }
        }
    }

    //--Calling this method in After Insert method for as is implementation

    public static void sendEmailMessage(){
        if (((Limits.getLimitFutureCalls() - Limits.getFutureCalls()) >= 1) && (!theIdList.isEmpty())) {
            sendEmailMessage(theIdList);
        }
    }

    static set<string> theEmailDomainSet = new set<string>();
    // send email in the future in order to get attachments
    @future
    static public void sendEmailMessage(list<Id> theEMIdList) {
        // get email messages
        list<EmailMessage> theEmailMessageList = new list<EmailMessage>([
            select e.ToAddress, e.TextBody, e.ParentId, e.Incoming, e.HtmlBody, e.Headers, e.HasAttachment, 
                    e.FromAddress, e.CcAddress, e.BccAddress, e.Subject, e.FromName 
                from EmailMessage e where Id in: theEMIdList]);

        
        // get case ids
        set<Id> theCaseIdSet = new set<Id>();
        for (EmailMessage em : theEmailMessageList) {
            theCaseIdSet.add(em.ParentId);
        }
                
        // get cases
        map<Id, Case> theCaseMap = new map<Id, Case>([select Id, OwnerId, Owner.Email, SuppliedEmail, CC_Emails__c, Thread_Id__c from Case where Id in: theCaseIdSet]); 
        
        // get email domain to exclude. 3.2
        //mc = emailSettings__c.getOrgDefaults();
        if (emailSettingRecord.Email_Domain_Exclusion__c != null) {
            for (String s : emailSettingRecord.Email_Domain_Exclusion__c.split(',')) theEmailDomainSet.add(s);
        }        
        Messaging.SingleEmailMessage eachEmail;
        list<Messaging.SingleEmailMessage> emailMessageList = new list<Messaging.SingleEmailMessage>();
        for (EmailMessage em : theEmailMessageList) {
            if (!theCaseMap.containsKey(em.ParentId)) continue;
            
            // TO email accordingly. see ResendIncomingEmailMessage method.
            boolean incoming = false;
            if (em.Incoming == true && em.FromAddress <> theCaseMap.get(em.ParentId).Owner.Email) {
                incoming = true;
            }
           
            // send the email - Wrong
            //-- Collection Mail variables and send at once.
            
            eachEmail = ResendIncomingEmailMessage(em, theCaseMap.get(em.ParentId), incoming);
            if(eachEmail!=null){
                emailMessageList.add(eachEmail);
            }
        }

        if(!emailMessageList.isEmpty()){
            Messaging.sendEmail(emailMessageList);
        }
    }


    // send Email to Case Owner or Customer when incoming email is sent from outside of SF
    static public Messaging.SingleEmailMessage ResendIncomingEmailMessage(EmailMessage em, Case c, boolean incoming) {
        OrgWideEmailAddress owa;
        try{
            owa = [Select id, Address, DisplayName From OrgWideEmailAddress Where Address = :emailSettingRecord.Org_Wide_Email__c Limit 1];
        }catch(QueryException qe){
            system.debug('==Exception=='+qe.getMessage());
        }
        
        if (Limits.getEmailInvocations() < Limits.getLimitEmailInvocations()) {
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[]{};
            String[] BccAddresses = new String[]{}; // 3.1
            
            // set To email list 
            string theOwnerId = (string) c.OwnerId;
            if (c.Owner.Email <> null) BccAddresses.add(c.Owner.Email);  // BCC owner no matter what it is, user or queue. 3.2
            if (c.SuppliedEmail <> null) toAddresses.add(c.SuppliedEmail);
 
                      
            mail.setToAddresses(toAddresses);
            mail.setBccAddresses(BccAddresses); // 3.1
            
            // set CC email list 
            if (c.CC_Emails__c <> null) {
                mail.setCcAddresses(em.CcAddress.split(','));
            }
            
            // check for attachments    
            if (em.HasAttachment) {
                List<Attachment> a = [Select ParentId, Name, Body From Attachment Where Parentid=:em.Id];
                List<Messaging.EmailFileAttachment> allAttachments = new List<Messaging.EmailFileAttachment>();
                
                for (Attachment att: a) {
                    Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                    attach.setFileName(att.Name);
                    attach.Body = att.Body;
                    attach.setInline(false);
                    allAttachments.add(attach);
                }
                system.debug(allAttachments);
                
                mail.setFileAttachments(allAttachments);
            }
            
            // before sendind the email, make sure subject contain Case Thread Id. also get rid of RE:. 3.2
            string theSubject = em.Subject.replaceAll('RE:', '').trim();
            if (!theSubject.contains(c.Thread_Id__c))
            
            theSubject = theSubject + ' ' + c.Thread_Id__c;
            mail.setSubject(theSubject);
                                       
            //set additional email parameters       
            mail.setHtmlBody(em.HtmlBody);
            mail.setPlainTextBody(em.TextBody);
            mail.setSaveAsActivity(true);
                
               
            system.debug(incoming);                   
            if (incoming == false) {
                //set From Name & Address when email is outbound to Customer
                mail.setOrgWideEmailAddressId(owa.id);
            } else {                
                // add condition here for appnexus.com or other domain. 3.2
                system.debug(theEmailDomainSet);
                system.debug(em.FromAddress);
                string theFromEmailDomain = em.FromAddress.split('@').get(1); // email domain based on @
                if (theEmailDomainSet.contains(theFromEmailDomain)) {
                    mail.setOrgWideEmailAddressId(owa.id);
                } else {
                    mail.setSenderDisplayName(em.FromName); 
                }    
            }
                
            //enter the email service address here from Email2Case (this will be used in email when the customer responds)
            mail.setReplyTo(emailSettingRecord.Routing_Email__c);       
            
            // send email
            //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
            if(mail!=null){
                 return mail;
            }
           
        }
        return null;
    }


    //** Stop Case Email Looping**/
    public static Map<String,Boolean> checkEmailCaseLoop(List<EmailMessage> tmpMsgs){   

        system.debug('---checkEmailCaseLoop---');

        set<String> emailCaseFromAddressSet = new set<String>();

        for(EmailMessage eachEmailMessage: tmpMsgs){
            emailCaseFromAddressSet.add(eachEmailMessage.FromAddress);
        }

        integer addMnt = -5;
        integer mailCnt = 5;
        if(emailSettingRecord.Case_Loop_Minutes__c != null){
            addMnt = integer.ValueOf(emailSettingRecord.Case_Loop_Minutes__c);
        }
        if(addMnt > 0){
           addMnt = addMnt - (addMnt*2);
        }
        if(emailSettingRecord.Case_Loop_Count__c != null){
            mailCnt = integer.ValueOf(emailSettingRecord.Case_Loop_Count__c);
        }
        
        map<String,list<Emailmessage>> fromAddressToListEmailMessageMap = new map<String,list<Emailmessage>>();

        for(EmailMessage eachEmailMessage : [ Select Subject, ParentId,FromAddress 
                    from EmailMessage 
                   where Incoming = true 
                     and createddate >= :Datetime.now().addMinutes(addMnt)
                     and FromAddress IN : emailCaseFromAddressSet
                order by createddate desc]){
            if(!fromAddressToListEmailMessageMap.containsKey(eachEmailMessage.FromAddress)){
                fromAddressToListEmailMessageMap.put(eachEmailMessage.FromAddress,new list<Emailmessage>{eachEmailMessage});
            }else{
                fromAddressToListEmailMessageMap.get(eachEmailMessage.FromAddress).add(eachEmailMessage);
            }
        }

        map<String,list<Email_Loop_Alert__c>> fromAddressEmailLoopAlertMap = new map<String,list<Email_Loop_Alert__c>>();

        for(Email_Loop_Alert__c eachEmailLoopAlert:[select id,From_Address__c from Email_Loop_Alert__c where Alert_Date__c = today and From_Address__c IN:emailCaseFromAddressSet]){
            if(!fromAddressEmailLoopAlertMap.containsKey(eachEmailLoopAlert.From_Address__c)){
                fromAddressEmailLoopAlertMap.put(eachEmailLoopAlert.From_Address__c, new list<Email_Loop_Alert__c>{eachEmailLoopAlert});
            }else{
                fromAddressEmailLoopAlertMap.get(eachEmailLoopAlert.From_Address__c).add(eachEmailLoopAlert);
            }
        }

        list<EmailMessage> tempEmailMessageList = new list<EmailMessage>();
        list<Email_Loop_Alert__c> newEmailAlertsToCreateList = new list<Email_Loop_Alert__c>();
        map<String,Boolean> fromAddressToBooleanFlagMap = new map<String,Boolean>();


        for(String eachFromEmailAddress:emailCaseFromAddressSet){

            if(fromAddressToListEmailMessageMap.containsKey(eachFromEmailAddress)){
                tempEmailMessageList = fromAddressToListEmailMessageMap.get(eachFromEmailAddress);
            }

            if(!tempEmailMessageList.isEMpty()){
                if(tempEmailMessageList.size()>=mailCnt){
                    if(!fromAddressEmailLoopAlertMap.containskey(eachFromEmailAddress)){
                        newEmailAlertsToCreateList.add(new Email_Loop_Alert__c(Parent_Id__c= tempEmailMessageList[0].ParentId, From_Address__c= tempEmailMessageList[0].FromAddress, Loop_Details__c = 'Subject :' + tempEmailMessageList[0].Subject));
                        // false - Since No alert was earlier created - Mail count doesn't matter?
                        fromAddressToBooleanFlagMap.put(eachFromEmailAddress,false);
                    }else{
                        //--true - Since Email Alert was already there..
                        fromAddressToBooleanFlagMap.put(eachFromEmailAddress,true);
                    } 
                }
            }else{
                //--Since No of Emails in 5 min is less than the count set..
                fromAddressToBooleanFlagMap.put(eachFromEmailAddress,false);
            }
        }

       if(!newEmailAlertsToCreateList.isEmpty()){
            insert newEmailAlertsToCreateList;
       }
       if(!fromAddressToBooleanFlagMap.isEmpty()){
        return fromAddressToBooleanFlagMap;
       }
       return null;
    }

    // update email messages based on some fields
    //-- This should only be called on before insert--
    public static void incomingEmailMessage(EmailMessage em, Case c, boolean incoming) {
        // update email messages on before insert

            // verify field values
            if (em.CcAddress == null) em.CcAddress = '';      
            if (em.BccAddress == null) em.BccAddress = ''; 
            
            // trim subject
            em.Subject.replaceAll('RE:', '').trim();    
            
            // set To email list 
            if (c.Owner.Email <> null && incoming == true) {     //set To as case owner for incoming email
                string theOwnerId = (string) c.OwnerId;
                if ((!em.BccAddress.contains(c.Owner.Email)) && (!em.CcAddress.contains(c.Owner.Email))) {
                    em.BccAddress  = em.BccAddress + ';' + c.Owner.Email; // BCC owner no matter what it is, user or queue. 3.2
                }   
            }    
            if (c.SuppliedEmail <> null && incoming == false) {   //set To as webmail for outgoing email
                em.ToAddress = c.SuppliedEmail;
            }    
            
            // to cater for when the Cc recipient replies to support - add the webmail to the new email
            if (!em.FromAddress.contains(c.SuppliedEmail) && incoming == true) {
                if ((!em.ToAddress.contains(c.SuppliedEmail)) && (!em.CcAddress.contains(c.SuppliedEmail))) {
                    em.ToAddress = em.ToAddress + ';' + c.SuppliedEmail;
                }   
            }

            // set CC email list      
            if (c.CC_Emails__c <> null) {
                em.CcAddress = c.CC_Emails__c;
            }
    }

    private static Datetime completionDate = Datetime.Now();
    public static void completeCaseMilestone(list<EmailMessage> newList){

        list<ID> caseIds = new list<ID>();

        for(EmailMessage eachEM : newList){
            if(eachEM.Incoming == false){
                caseIds.add(eachEM.parentId);
            }
        }

        if(!caseIds.isEmpty()){
            ServiceFacade.completeMilestone(caseIds,CONSTANTS.FIRST_RESPONSE,completionDate);
        }
    }
}