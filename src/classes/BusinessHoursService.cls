public  class BusinessHoursService {
	
	public static Businesshours getDefaultBusinessHours(){
		try{
			return [SELECT id from Businesshours where isDefault=true];
		}catch(Exception e){
			system.debug('--Exception--'+e.getMessage());
		}
		return null;
	}

	public static Businesshours getBusinessHoursByName(String businessHourName){
		try{
			return [SELECT id from Businesshours where name=:businessHourName];
		}catch(Exception e){
			system.debug('--Exception--'+e.getMessage());
		}
		return null;
	}

	public static Decimal getDifferenceInBusinessHours(String BusinessHourID, Datetime startDate, Datetime endDate){
		try{
			//--returns in hours --//
			return BusinessHours.diff(BusinessHourID,startDate,endDate)/1000/3600;
		}catch(Exception e){
			system.debug('--Exception e--'+e.getMessage());
		}
		return null;
	}

	public static Datetime addBusinessHours(String BusinessHourID, Datetime startDate, Long intervalMilliseconds){
		try{
			return BusinessHours.add(BusinessHourID,startDate,intervalMilliseconds);
		}catch(Exception e){
			system.debug('--Exception e--'+e.getMessage());
		}
		return null;
	}
}