public class CSPAddCampaignMem{

  @AuraEnabled
  public static String getAddCampMem(){
    Id campaignIdVal;
    String returnString = '';
    String statusStr = 'Requested';
    if( HardCodeIds__c.getValues('CampaignId').value__c != null)
       campaignIdVal = HardCodeIds__c.getValues('CampaignId').value__c;
    else
       campaignIdVal = [select id from campaign where name = 'AppNexus Platform Certificate: Buy-Side Essentials'].id;
     User cspuser = [select id, email from user where id = :userInfo.getUserId()];
     CampaignMember cp;
     for(Contact tmpCont: [select id from contact where email = :cspuser.email]){
        List<CampaignMember> camMemLst = [select id, status from CampaignMember 
                                                           where CampaignId= :campaignIdVal 
                                                             and ContactId= :tmpCont.id];
        if(camMemLst.size() > 0){
           for(CampaignMember campMem : camMemLst){
              if(campMem.Status != 'Failed'){
                //returnString = 'User already enrolled. - Current Status : '+campMem.Status;
                
                returnString = System.Label.TrainingRequestedAgainWhileWaiting;
              }else{
                statusStr = 'Requested A Second Time';
                cp = campMem;
                cp.Status = statusStr;
                
                Database.UpsertResult sr = Database.upsert(cp,false);
                if (sr.isSuccess()){
                  returnString = System.Label.TrainingRequestForSecondTime;
                  }else{
                  returnString = System.Label.TrainingRequestFailure + ' - ' + sr.getErrors();
                  }
                
                
              }
           }
        }
        if(returnString == ''){
            if( statusStr == 'Requested')
                cp = new CampaignMember(CampaignId= campaignIdVal, ContactId= tmpCont.id, Status= statusStr);
            
            Database.UpsertResult sr = Database.upsert(cp,false);
            if (sr.isSuccess()){
              returnString = System.Label.TrainingEnrollSuccess;
              }else{
              returnString = System.Label.TrainingRequestFailure + ' - ' + sr.getErrors();
              }
        }
        
     }
    return returnString; 
    //return 'success1';
  }
  
}