public class AttachmentTriggerHandler {
    
    private static AttachmentTriggerHandler instance;
     //*** Singleton Pattern to obtain instance of the Handler***
    public static AttachmentTriggerHandler getInstance(){
        if(instance==null){
            instance = new AttachmentTriggerHandler();
        }
        return instance;
    }
    
    public void beforeInsertMethod(list<Attachment> newList,map<ID,Attachment> newMap){
       
    }
    
    public void beforeUpdateMethod(list<Attachment> newList, list<Attachment> oldList, map<ID,Attachment> newMap, map<ID,Attachment> oldMap){
         
    }
    
    public void afterInsertMethod(list<Attachment> newList,map<ID,Attachment> newMap){
    	AttachmentService.updateAttachmentList(newList);
    }
 
    public void afterUpdateMethod(list<Attachment> newList,list<Attachment> oldList,map<ID,Attachment> newMap,map<ID,Attachment> oldMap){
      
    }
}