@isTest
public class AcntOwnerTest{

    private static AcntOwner controller;
    
    public static void loadData(){
    
        User u1 = TestingUtils.getUser('* Sales Operations');
        User[] users = [SELECT Id, ProfileId FROM User WHERE Profile.Name ='* Commercial' AND isActive = true limit 8];
        
        System.runAs(TestingUtils.getAdminUser()){
            PermissionSet ps = [SELECT id from permissionset where name = 'Account_Ownership_Change_Request_Approval' limit 1];
            PermissionSetAssignment[] psa = new List<PermissionSetAssignment>();
            psa.add(new PermissionSetAssignment(permissionsetid = ps.id, assigneeid = users[3].id));
            psa.add(new PermissionSetAssignment(permissionsetid = ps.id, assigneeid = users[4].id));
            psa.add(new PermissionSetAssignment(permissionsetid = ps.id, assigneeid = users[5].id));
            insert psa;
        }
        
        System.runAs(u1){
        
            Account[] accounts = AccountTriggerTest.createAccounts('Customer Account',1);
            insert accounts;
            AccountTeamMember[] atmLoad = new List<AccountTeamMember>();
            for (Account a: accounts){
                atmLoad.add(AccountUtils.generateAccountTeamMember(a.id, users[7].id, 'Collector'));
            }
            insert atmLoad;
            //Account acn = [select id, recordtype.name from account where recordtype.name = 'Customer Account' limit 1];
            Account[] members = AccountTriggerTest.createAccounts('Console Member',10);
            for (Integer i = 0; i<10; i++){
                members[i].AppNexus_Member_Id__c = String.valueOf(i+10000);
                //members[i].parentid = acn.id;  
                members[i].parentid = accounts[0].id;
                if(Math.mod(i,2) == 0) members[i].ownerid = users[1].id;
            }
            insert members;
        }
        
        Account[] memberAccounts = [select id, (select id from accountteammembers) from account where recordtype.name = 'Console Member' limit 5];
        AccountTeamMember[] toDelete = new List<AccountTeamMember>();
        AccountTeamMember[] toInsert = new List<AccountTeamMember>();
        for (Account a: memberAccounts){
            toDelete.addAll(a.accountTeamMembers);
            toInsert.add(AccountUtils.generateAccountTeamMember(a.id, users[0].id, AccountUtils.SR_ACCOUNT_OWNER));
            toInsert.add(AccountUtils.generateAccountTeamMember(a.id, users[1].id, AccountUtils.ACCOUNT_OWNER));
            toInsert.add(AccountUtils.generateAccountTeamMember(a.id, users[2].id, AccountUtils.COMMERCIAL_LEADER));
            toInsert.add(AccountUtils.generateAccountTeamMember(a.id, users[7].id, 'Collector'));
        }
        delete toDelete;
        insert toInsert;
    
    }
    
    public static testMethod void initialLoadChangeOwner(){
    
        LoadData();
        Account acn = [select id from account where recordtype.name = 'Customer Account' limit 1];
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/AcntOwner?id='+acn.id);
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(new Account());
        controller = new AcntOwner(sc);
        List<SelectOption> commercialLeaders = controller.commercialLeaders;
        User[] users = [SELECT Id, ProfileId FROM User WHERE Profile.Name ='* Commercial' AND isActive = true limit 2];
        for (AccountTeamMember atm: controller.atmList){
            if (atm.teamMemberRole == AccountUtils.ACCOUNT_OWNER) atm.userid = users[0].id;
            if (atm.teamMemberRole == AccountUtils.SR_ACCOUNT_OWNER) atm.userid = users[1].id;
        }
        controller.saveRequest();
        Test.stopTest();
        

    }
    
    public static testMethod void independentChange(){
    
        LoadData();
        Account acn = [select id from account where recordtype.name = 'Customer Account' limit 1];
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/AcntOwner?id='+acn.id);
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(new Account());
        controller = new AcntOwner(sc);
        
        User[] users = [SELECT Id, ProfileId FROM User WHERE Profile.Name ='* Commercial' AND isActive = true limit 2];
        for (AccountTeamMember atm: controller.atmList){
            if (atm.teamMemberRole == AccountUtils.ACCOUNT_OWNER) atm.userid = users[0].id;
            if (atm.teamMemberRole == AccountUtils.SR_ACCOUNT_OWNER) atm.userid = users[1].id;
        }
        controller.renderIndependent();
        
        
        for (AcntOwner.AccountAndOwners ao: controller.lam){
        
            if(ao.owner.TeamMemberRole == AccountUtils.ACCOUNT_OWNER) ao.owner.userid = users[0].id;
        
        }
        controller.confirmScreen();
        controller.backFromThird();
        controller.backFromSecond();
        controller.saveIndependentRequest();
        Test.stopTest();

    }
    
    public static testMethod void teamChange(){
    
        User implementationC = TestingUtils.getUser('* Implementation Consultant');
        LoadData();
        Account acn = [select id from account where recordtype.name = 'Customer Account' limit 1];
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/AcntOwner?id='+acn.id);
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(new Account());
        controller = new AcntOwner(sc);
        controller.renderOtherMembers();
        List<SelectOption> teamRole = controller.AccountTeamRoles;
        System.currentPageReference().getParameters().put('rowIndex', '0');
        controller.deleteRow();
        controller.addRow();
        for (AccountTeamMember atm: controller.otherMembers){
            if (atm.userid == null) {
                atm.userid = implementationC.id;
                atm.teammemberrole = 'Collector';
            }
        }
        controller.ave();
        Test.stopTest();
        System.assertEquals(1,[select id from accountTeamMember where accountid =:acn.id and userid =:implementationC.id and teammemberrole = 'Collector'].size());
        System.assertEquals(1,[select id from accountTeamMember where accountid =:acn.id and teammemberrole = 'Collector'].size());

    }


}