/*
 * Sends an outage notification based on Outage notification object. SFA-6414.
 */
public  class OutageNotification {
     

    public class KeyVariables {
        @InvocableVariable(label='Email Template')
        public String emailTemplate;
        @InvocableVariable(label='Email From')
        public String emailFrom;
        @InvocableVariable(label='Related Record Id')
        public String recordId;
        @InvocableVariable(label='Account Id')
        public String accountId;
        @InvocableVariable(label='Parent Account Id')
        public String parentAccountId;
    }
     
    @InvocableMethod(label='Send Email Notification')
    public static void sendEmailNotification(List<KeyVariables> records) {
        EmailTemplate emTemp;

        //-- This retrieves all-should we apply any filters?
        Outage_Notifications__c[] outageNotifications = [select id, Account__c, Email_Addresses__c from Outage_Notifications__c where isactive__c = true];
        
        if (outageNotifications.size()>0){
            
            String emailTemplateId;
            if (records[0].emailTemplate != null){
                try{
                    emailTemplateId = [select id from EmailTemplate where developername = :records[0].emailTemplate limit 1 ].id;
                } catch (exception e){
                    system.debug(e);
                        emTemp = Utility.getEmailTemplateByName(CONSTANTS.EMAIL_TEMPLATE_OUTAGE_NOTIFICATION);
                        if(emTemp!=null){
                            emailTemplateId = emTemp.id;
                        }
                        
                }
            } else {
                emTemp = Utility.getEmailTemplateByName(CONSTANTS.EMAIL_TEMPLATE_OUTAGE_NOTIFICATION);
                        if(emTemp!=null){
                            emailTemplateId = emTemp.id;
                        }
            }
        
        
            Map<String, String> accountEmail = new Map<String,String>();
            for (Outage_Notifications__c notification: outageNotifications){
                if (accountEmail.isEmpty() || accountEmail.get(notification.account__c) == null) accountEmail.put(notification.account__c, notification.Email_Addresses__c);
            }
            
            for (KeyVariables r: records){
                if (accountEmail.containsKey(r.accountId) || accountEmail.containsKey(r.parentAccountId)){
                    Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
                    mail.setToAddresses(accountEmail.get(r.accountId) != null ? accountEmail.get(r.accountId).split(',') : accountEmail.get(r.parentAccountId).split(','));
                    mail.setSenderDisplayName(r.emailFrom);
                    mail.setTargetObjectId(Label.OutageNotification_Contact_Id);
                    mail.setSaveAsActivity(false);
                    mail.setWhatId(r.recordId);
                    mail.setTemplateId(emailTemplateId);
                    try{
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    } catch (exception e){
                        System.debug(e);
                    }
                }
            }
        }
    }
}