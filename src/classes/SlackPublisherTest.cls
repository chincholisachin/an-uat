/*
 * Test class for SlackPublisher
 */
@isTest
public class SlackPublisherTest {
    
    @isTest
    static void postToSlackTest () {
        SlackPublisher.Payload payload = new SlackPublisher.Payload();
        payload.slackURL = 'https://www.example.test/12345';
        payload.messageText = 'Test message.';
        payload.field1Name = 'Field 1';
        payload.field1Value = 'Value 1';
        payload.field2Name = 'Field 2';
        payload.field2Value = 'Value 2';
        payload.field3Name = 'Field 3';
        payload.field3Value = 'Value 3';
        payload.field4Name = 'Field 4';
        payload.field4Value = 'Value 4';
        payload.field5Name = 'Field 5';
        payload.field5Value = 'Value 5';
        payload.field6Name = 'Field 6';
        payload.field6Value = 'Value 6';
        payload.field7Name = 'Field 7';
        payload.field7Value = 'Value 7';
        payload.field8Name = 'Field 8';
        payload.field8Value = 'Value 8';
        payload.field9Name = 'Field 9';
        payload.field9Value = 'Value 9';
        payload.field10Name = 'Field 10';
        payload.field10Value = 'Value 10';
        payload.slackIcon = 'slackIcon';
        payload.slackUsername = 'slackUsername';
        payload.recordId = '1234567890';
        
        Test.startTest();

        SlackPublisher.postToSlack(new List<SlackPublisher.Payload>{payload});
        
        Test.stopTest();
    }

}