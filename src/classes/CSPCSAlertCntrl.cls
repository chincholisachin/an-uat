public class CSPCSAlertCntrl{

  @AuraEnabled
  public static String getCSPCSAlert(){
     String visibility = 'visibility: hidden';
     if(userInfo.getUserType() == 'Guest')
          visibility = 'visibility: visible';
       return visibility;
  }
  
  @AuraEnabled
  public static String getCSPUserType(){
     String cspUsrType = 'CspLite';
     cspUsrType = userInfo.getUserType();
       return cspUsrType;
  }
  
}