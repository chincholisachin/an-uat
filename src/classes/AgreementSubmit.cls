public with sharing class AgreementSubmit {
     public Case caseObj{get;set;}
     public id agrId;
     public id legalReqId;
     public id salOpsOwnrid;
     public id salOpsRecid;
     public boolean soCaseFlg {get;set;}
     public Apttus__APTS_Agreement__c agreement {get;set;} 
     public Apttus__APTS_Agreement__c uptAgreement {get;set;} 
     public Legal_Request__c updateLR {get;set;}
     
     public AgreementSubmit (ApexPages.StandardController controller) {
       agrId = (ID)ApexPages.CurrentPage().getParameters().get('agrId');
       legalReqId = (ID) ApexPages.CurrentPage().getParameters().get('lrId');
       try{
           salOpsOwnrid = [select Id from Group where Type = 'Queue' and name = 'Sales Ops' limit 1].id;
           salOpsRecid = [select id from recordtype where name = 'Sales Ops'].id;
           this.caseObj= (Case)controller.getRecord();
           caseObj.recordtypeid = salOpsRecid;
           soCaseFlg = true;
           agreement = [Select Name, Apttus__Account__c, Apttus__Account__r.Related_Party__c,
                               Apttus__Related_Opportunity__c, 
                               Apttus__Related_Opportunity__r.Data_Provider_Type__c, createdby.email
                          from Apttus__APTS_Agreement__c
                         where id = :agrId];
           for(Case tmpCase :[select id from case where Related_Agreement__c = :agrId and isclosed = false] ){
                soCaseFlg = false;
           }
        }catch (DmlException e) { 
           System.debug(e.getMessage());
           ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The Requested Agreement could not be found. Please try again.'));
        }
     }
      public PageReference saveRec(){
        caseObj.subject = 'Sales Ops case for Agreement: '+agreement.Name;
        caseObj.Status = 'New';
        caseObj.Sub_Status__c = 'New';
        caseObj.Sales_Ops_Case_Type__c = 'Contract Submission';
        caseObj.Related_Agreement__c = agrId;
        caseObj.ownerid = salOpsOwnrid;
        caseObj.recordtypeid = salOpsRecid;
        caseObj.Account_Related_To_Sales_Ops_Request__c = agreement.Apttus__Account__c;
        caseObj.Accountid = agreement.Apttus__Account__c;
        caseObj.Related_Party__c = agreement.Apttus__Account__r.Related_Party__c;
        caseObj.Opportunity__c = agreement.Apttus__Related_Opportunity__c;
        //caseObj.Data_Provider_Type__c= agreement.Apttus__Related_Opportunity__r.Data_Provider_Type__c;
        List <Contact> cList = [select firstname, lastname, id from contact where email =:agreement.createdby.email limit 1];
        if (cList.size() >0){
          caseObj.contactid = cList.get(0).id;
        }
        //caseObj.ContactEmail = agreement.createdby.email;
        insert caseObj;
        uptAgreement = [select id,Apttus__Status_Category__c, Apttus__Status__c, Apttus__Parent_Agreement__c, Parent_Status_Category__c
                          from Apttus__APTS_Agreement__c
                         where id=:agrId];//new Apttus__APTS_Agreement__c(id=agrId);
        //uptAgreement.Apttus__Status_Category__c = 'In Authoring';
        uptAgreement.Apttus__Status__c = 'In Client Ops Review';
        update uptAgreement;
        if(uptAgreement.Apttus__Parent_Agreement__c !=  null && !(('|In Effect|Amended|Expired|Terminated|Cancelled|').contains(uptAgreement.Parent_Status_Category__c)) ){
            Apttus__APTS_Agreement__c uptPrAgreement = new Apttus__APTS_Agreement__c(id=uptAgreement.Apttus__Parent_Agreement__c);
            uptPrAgreement.Apttus__Status__c = 'In Client Ops Review';
            update uptPrAgreement;
        }
        //added for SFA-12194
        try{
        updateLR = [select sendEmail__c from Legal_Request__c where id =: legalReqId];
        updateLR.SendEmail__c = true;
        update updateLR;
        } catch (Exception e) {
          System.debug(e.getMessage());
           ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Error while Sending Email.'));
        
        }
        //end for SFA-12194
        PageReference pr = new PageReference('/'+agrId);
        pr.setRedirect(true);
        return pr;
        
      }
     public PageReference cancelRec(){
        PageReference pr = new PageReference('/'+agrId);
        pr.setRedirect(true);
        return pr;
     }
}