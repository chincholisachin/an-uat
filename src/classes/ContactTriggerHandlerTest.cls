@isTest
public class ContactTriggerHandlerTest{

    public static void setupCSPContact(){
        
        TestingUtils.createEssentialCustomSettings();
        Account acn = new Account(Name = 'AppNexus Employees', recordTypeId = [Select id from RecordType where sobjecttype = 'Account' and Name = 'Internal Entity' limit 1].id);
        insert acn;
        System.assertNotEquals(acn.id, null);
        
        Contact con = new Contact(accountid = acn.id, FirstName = 'CSP', LastName = 'Dummy', recordtypeid = [Select id from RecordType where sobjecttype = 'Contact' and Name = 'AN Employee' limit 1].id);
        insert con;
        System.assertNotEquals(con.id, null);
    }
    
    public static testMethod void createAppNexusContacts(){
    
        setupCSPContact();
        User[] users = TestingUtils.createUsers(5, 'testUser', 'testUser', '* Commercial', false);
        for (Integer i = 10000; i<10005; i++){
            users[i-10000].drt_person_id__c = String.valueOf(i);
        }
        insert users;
        
        Test.startTest();
        
        Account an = [select id from account where name = 'AppNexus Employees' limit 1];
        
        Contact[] conRecs = TestingUtils.createContacts(200, 'LastName', false);
        for (Integer t = 0;t<200;t++ ){
            conRecs[t].drt_person_id__c = String.valueOf(t+10000);
            conRecs[t].accountid = an.id;
        }
        insert conRecs;
        
        Test.stopTest();
        
        System.assertEquals(5,[select id from contact where drt_person_id__c != null and user__c != null].size());
        System.assertEquals(195,[select id from contact where drt_person_id__c != null and user__c = null].size());

        
    }
        

}