public class TriggerServices
{
	public List<sObject> sObjects;
	public Map<Id, sObject> oldMap;

	public TriggerServices( List<sObject> sObjects )
	{
		this.sObjects = sObjects;
		this.oldMap = new Map<Id, sObject>();
	}

	public TriggerServices( List<sObject> sObjects, Map<Id, sObject> oldMap )
	{
		this.sObjects = sObjects;
		this.oldMap = oldMap;
	}

	public List<AdjustedObject> findFieldsWithOldValuesThatChanged( Schema.SObjectField fieldPath )
	{
		return findFieldsWithOldValuesThatChanged( new List<Schema.SObjectField>{ fieldPath } );
	}
	public List<sObject> findObjectsThatChanged( Schema.SObjectField fieldPath )
	{
		return findObjectsThatChanged( new List<Schema.SObjectField>{ fieldPath } );
	}

	public List<sObject> findObjectsThatChanged( List<Schema.SObjectField> fieldPaths )
	{
		List<sObject> objectsThatChanged = new List<sObject>();
		List<AdjustedObject> adjustedObjects = findFieldsWithOldValuesThatChanged( fieldPaths );
		for( AdjustedObject adjustedObj : adjustedObjects )
		{
			objectsThatChanged.add( adjustedObj.obj );
		}
		return objectsThatChanged;
	}

	public List<AdjustedObject> findFieldsWithOldValuesThatChanged( List<Schema.SObjectField> fieldPaths )
	{
		List<AdjustedObject> adjustedObjects = new List<AdjustedObject>();
		for(sObject obj : sObjects)
		{
			List<FieldValue> valuesThatChanged = new List<FieldValue>();
			sObject oldObj = oldMap.get(obj.Id);
			if(oldObj != null)
			{
				for(Schema.SObjectField fieldPath : fieldPaths)
				{
					Object oldValue = oldObj.get( fieldPath );
					Object newValue = obj.get( fieldPath );
					if(oldValue != newValue)
						valuesThatChanged.add( new FieldValue( fieldPath, oldValue, newValue ) );
				}
			}
			else
			{
				for(Schema.SObjectField fieldPath : fieldPaths )
				{
					Object oldValue = null;
					Object newValue = obj.get( fieldPath );
					valuesThatChanged.add( new FieldValue( fieldPath, oldValue, newValue ) );
				}
			}

			if( !valuesThatChanged.isEmpty() )
			{
				adjustedObjects.add( new AdjustedObject( obj, valuesThatChanged ) );
			}
		}
		return adjustedObjects;
	}

	public class AdjustedObject
	{
		public sObject obj;
		public List<FieldValue> fieldsThatChanged;

		public AdjustedObject(sObject objThatChanged, List<FieldValue> fieldsThatChanged)
		{
			this.obj = objThatChanged;
			this.fieldsThatChanged = fieldsThatChanged;
		}
	}

	public class FieldValue
	{
		public Object oldValue;
		public Object newValue;
		public Schema.SObjectField fieldPath;

		public FieldValue(Schema.SObjectField fieldPath, Object oldValue, Object newValue)
		{
			this.fieldPath = fieldPath;
			this.oldValue = oldValue;
			this.newValue = newValue;
		}
	}
}