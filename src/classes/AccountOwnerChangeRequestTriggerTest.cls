@isTest
public class AccountOwnerChangeRequestTriggerTest{

    private static void loadData(){
        AcntOwnerTest.loadData();
        User[] u = [select id from user where profile.name = '* Commercial' and isactive = true limit 6];
        Account_Owner_Change_Request__c[] aocr = new List<Account_Owner_Change_Request__c>();
        Account customer = [select id from account where recordtype.name = 'Customer Account' limit 1];
        Account[] members = [select id from account where recordtype.name = 'Console Member'];
        for (Account a: members){
            aocr.add(new Account_Owner_Change_Request__c(market_owner__c = u[0].id, 
                                                         New_Account_Owner__c = u[1].id, 
                                                         New_Sr_Account_Owner__c = u[2].id, 
                                                         Previous_Commercial_Leader__c = u[3].id, 
                                                         Previous_Account_Owner__c = u[4].id, 
                                                         Previous_Sr_Account_Owner__c = u[5].id, 
                                                         related_account__c = customer.id,
                                                         Change_Account_Only__c = true,
                                                         appnexus_member2__c = a.id,
                                                         status__c = 'Pending')); 
        }
        aocr.add(new Account_Owner_Change_Request__c(market_owner__c = u[0].id, 
                                                         New_Account_Owner__c = u[1].id, 
                                                         New_Sr_Account_Owner__c = u[2].id, 
                                                         Previous_Commercial_Leader__c = u[3].id, 
                                                         Previous_Account_Owner__c = u[4].id, 
                                                         Previous_Sr_Account_Owner__c = u[5].id, 
                                                         related_account__c = customer.id,
                                                         Change_Account_Only__c = true,
                                                         status__c = 'Pending'));
        aocr.add(new Account_Owner_Change_Request__c(market_owner__c = u[0].id, 
                                                         New_Account_Owner__c = u[1].id, 
                                                         New_Sr_Account_Owner__c = u[2].id, 
                                                         Previous_Commercial_Leader__c = u[3].id, 
                                                         Previous_Account_Owner__c = u[4].id, 
                                                         Previous_Sr_Account_Owner__c = u[5].id, 
                                                         related_account__c = customer.id,
                                                         Change_Account_Only__c = false,
                                                         status__c = 'Pending'));
                                                         
        insert aocr;  
    }

    public static testMethod void testUpdateAccountOwnerShip(){
    
        loadData();
        Account_Owner_Change_Request__c aocr = [select id, status__c, Market_Owner__c,
                                                New_Account_Owner__c,
                                                New_Sr_Account_Owner__c,
                                                Previous_Commercial_Leader__c,
                                                Previous_Account_Owner__c,
                                                Previous_Sr_Account_Owner__c
                                                from Account_Owner_Change_Request__c where status__c = 'Pending' and Change_Account_Only__c = false limit 1];
        aocr.status__c = 'Approved';
        update aocr;
    
    }
    
    public static testMethod void testUpdateAccountOwnerShipIndependent(){
    
        loadData();
        Account_Owner_Change_Request__c[] aocr = [select id, status__c, Market_Owner__c,
                                                New_Account_Owner__c,
                                                Appnexus_member2__c,
                                                New_Sr_Account_Owner__c,
                                                Previous_Commercial_Leader__c,
                                                Previous_Account_Owner__c,
                                                Previous_Sr_Account_Owner__c
                                                from Account_Owner_Change_Request__c where status__c = 'Pending' and Change_Account_Only__c = true];
        for (Account_Owner_Change_Request__c a: aocr) a.status__c = 'Approved';
        update aocr;
    
    }

}