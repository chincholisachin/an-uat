@istest
public class UpdateCSPAlertsTest {

    static testMethod void  testPostRestService(){
    
    insert new CustomWebService__c(Name = 'UpdateCSPAlerts', Username__c = 'Test',Password__c = 'Test');
    
    insert new CaseSupportAlert__c(Alert_Message__c = 'Test', Expired_On__c = Datetime.newInstance(2017, 2, 1), start_time__c = Datetime.newInstance(2017, 1, 1), Alert_Type__c = 'Information' , Active__c = true);

    String JsonMsg=JSON.serialize(  new Map<String,Object> { 'uname' => 'test', 'pwd' => 'test', 'expirationdate' => system.now(), 'alertmessage' => 'test' });
    
    Test.startTest();
    
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    req.requestURI = '/services/apexrest/UpdateCSPAlerts';  //Request URL
    req.httpMethod = 'POST';//HTTP Request Type
    req.requestBody = Blob.valueof(JsonMsg);
    
    RestContext.request = req;
    RestContext.response = res;
    
    //UpdateCSPAlerts csp = new UpdateCSPAlerts();
    
    Datetime dat = system.now();
    String result = UpdateCSPAlerts.doPost('test','pwd',dat,'alertmessage');
    System.assertEquals(0,[select id from CaseSupportAlert__c where Expired_On__c = :dat].size());
    String result2 = UpdateCSPAlerts.doPost('test','test',dat,'alertmessage');
    System.assertEquals(1,[select id from CaseSupportAlert__c where Expired_On__c = :dat].size());

    Test.stopTest();
    
    
    }

}