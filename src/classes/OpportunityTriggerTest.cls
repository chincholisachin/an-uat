@isTest
public class OpportunityTriggerTest{

    public static List<Opportunity> prepareOppData(){
        
        Id pricebookId = Test.getStandardPricebookId();
        
        Pricebook2 pb = new PriceBook2(name = 'Standard Price Book');
        insert pb;
        
        List<Product2> pr = new List<Product2>();
        pr.add(new Product2(isActive = true,
                                   Family = 'Buy Side',
                                   Name = 'Header Bidding'));
        pr.add(new Product2(isActive = true,
                                   Family = 'Sell Side',
                                   Name = 'Console'));

        insert pr;
        List<PriceBookEntry> pbeSt = new List<PriceBookEntry>();
        List<PriceBookEntry> pbe = new List<PriceBookEntry>();
        pbeSt.add(new PriceBookEntry(pricebook2id = pricebookId, product2id = pr[0].id, UnitPrice = 1, UseStandardPrice = false));
        pbeSt.add(new PriceBookEntry(pricebook2id = pricebookId, product2id = pr[1].id, UnitPrice = 1, UseStandardPrice = false));
        pbe.add(new PriceBookEntry(pricebook2id = pb.id, product2id = pr[0].id, UnitPrice = 1, UseStandardPrice = false, isactive = true));
        pbe.add(new PriceBookEntry(pricebook2id = pb.id, product2id = pr[1].id, UnitPrice = 1, UseStandardPrice = false, isactive = true));
        insert pbeSt;
        insert pbe;                
        
        
        Account[] accounts = AccountTriggerTest.createAccounts('Customer Account', 200);
        insert accounts;
        
        
        Integer i = 0;
        Opportunity[] opps = TestingUtils.createOpportunities(200, 'Test Opportunity', 'Identification', false);
        for (Opportunity o: opps){
            o.accountid = accounts[i].id;
            i++;
        }
        return opps;        
        
        
    }
    
    public static testMethod void postToChatterTestAtInsert(){
    
        Opportunity[] opps = prepareOppData();
        Test.startTest();
        Integer i = 0;
        for (Opportunity o : opps){
            o.inside_sales_notes__c = String.valueOf(i);
            i++;
        }
        insert opps;
        Test.stopTest();
        
        Account acn = [select id, (select id, name, inside_sales_notes__c from opportunities) from account where recordtype.name = 'Customer Account' limit 1];
        System.assertEquals( [select body from feeditem where parentid = :acn.id limit 1].body, 'Status Update for Opportunity '+acn.opportunities[0].name+': '+ acn.opportunities[0].Inside_Sales_Notes__c);
        System.assertEquals( [select body from feeditem where parentid = :acn.opportunities[0].id limit 1].body, 'Sales Notes Update: '+acn.opportunities[0].Inside_Sales_Notes__c);

    }
    
    public static testMethod void postToChatterTestAtUpdate(){
    
        Opportunity[] opps = prepareOppData();
        Insert opps;
        Integer i = 0;
        for (Opportunity o : opps){
            o.inside_sales_notes__c = String.valueOf(i);
            i++;
        }
        Test.startTest();
        update opps;
        Test.stopTest();
        
        Account acn = [select id, (select id, name, inside_sales_notes__c from opportunities) from account where recordtype.name = 'Customer Account' limit 1];
        System.assertEquals( [select body from feeditem where parentid = :acn.id limit 1].body, 'Status Update for Opportunity '+acn.opportunities[0].name+': '+ acn.opportunities[0].Inside_Sales_Notes__c);
        System.assertEquals( [select body from feeditem where parentid = :acn.opportunities[0].id limit 1].body, 'Sales Notes Update: '+acn.opportunities[0].Inside_Sales_Notes__c);

    }
    
    public static testMethod void testOpportunityTeamCreation(){
    
        Opportunity[] opps = prepareOppData();
        User u = TestingUtils.getUser('* Commercial');
        for (Opportunity o : opps){
            o.ownerid = u.id;
        }
        Test.startTest();
        Insert opps;
        Test.stopTest();
        
        System.assertEquals(200, [select id from opportunityteammember where teammemberrole = 'Opportunity Owner' and userid =:u.id].size());

    }
        
        

}