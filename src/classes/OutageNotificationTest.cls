/*
 * Test class for Outage Notification Test
 */
@isTest (seealldata=false)
public class OutageNotificationTest {

    private static AcntOwner controller;
    private  static  Contact con;
    Private static Contact con1;
    private static case cas;
    private static Account[] accounts; 
    
    public static void loadData(){
        
         InitializeTestData.createSystemConfigSettings();
        accounts = AccountTriggerTest.createAccounts('Customer Account',1);
        insert accounts;
    
        
        con= InitializeTestData.CreateContact(accounts[0].Id);
        insert con;
        
        cas = InitializeTestData.CreateCase(con.id,accounts[0].id);
        insert cas;
       
        
    }
    
    @isTest
    static void sendOutageNotification () {
        
        loadData();
         InitializeTestData.createSystemConfigSettings();
        User thisUser = [ select Id from User where Id =: UserInfo.getUserId()];
        System.runAs ( thisUser ) {
            EmailTemplate emailTemp = new EmailTemplate (body = 'test',name='testTemplate',developername='testTemplate',isactive=true, subject='test',folderid='00lf4000000cliO', templatetype='text');
            insert emailTemp;
        }
   
        Outage_Notifications__c onot = new Outage_Notifications__c(account__c =accounts[0].id, IsActive__c = true, Email_Addresses__c = 'test@email.com');
        insert onot;
      
        OutageNotification.KeyVariables kv = new OutageNotification.KeyVariables();
        kv.emailTemplate = 'testTemplate';
        kv.emailFrom = 'Test';
        kv.recordId = cas.id;
        kv.accountId = accounts[0].id;
        kv.parentAccountId = null;
     
        Test.startTest();
       		 OutageNotification.sendEmailNotification(new List<OutageNotification.keyVariables>{kv});
        Test.stopTest();
    }
    
     @isTest
    static void sendOutageNotification1 () {
        
        loadData();
         InitializeTestData.createSystemConfigSettings();
        User thisUser = [ select Id from User where Id =: UserInfo.getUserId()];
        System.runAs ( thisUser ) {
            EmailTemplate emailTemp = new EmailTemplate (body = 'test',name='Client Specific Outage Alert',developername='testTemplate',isactive=true, subject='test',folderid='00lf4000000cliO', templatetype='text');
            insert emailTemp;
        }
   
        Outage_Notifications__c onot = new Outage_Notifications__c(account__c =accounts[0].id, IsActive__c = true, Email_Addresses__c = 'test@email.com');
        insert onot;
      
        OutageNotification.KeyVariables kv = new OutageNotification.KeyVariables();
      
        kv.emailFrom = 'Test';
        kv.recordId = cas.id;
        kv.accountId = accounts[0].id;
        kv.parentAccountId = null;
     
        Test.startTest();
       		 OutageNotification.sendEmailNotification(new List<OutageNotification.keyVariables>{kv});
        Test.stopTest();
    }
    
    
    @isTest
    static void sendOutageNotification2 () {
        
        loadData();
         InitializeTestData.createSystemConfigSettings();
        User thisUser = [ select Id from User where Id =: UserInfo.getUserId()];
        System.runAs ( thisUser ) {
            EmailTemplate emailTemp = new EmailTemplate (body = 'test',name='Client Specific Outage Alert',developername='testTemplate',isactive=true, subject='test',folderid='00lf4000000cliO', templatetype='text');
            insert emailTemp;
        }
   
        Outage_Notifications__c onot = new Outage_Notifications__c(account__c =accounts[0].id, IsActive__c = true, Email_Addresses__c = 'test@email.com');
        insert onot;
      
        OutageNotification.KeyVariables kv = new OutageNotification.KeyVariables();
         kv.emailTemplate = 'testTemplate1';
        kv.emailFrom = 'Test';
        kv.recordId = cas.id;
        kv.accountId = accounts[0].id;
        kv.parentAccountId = null;
     
        Test.startTest();
       		 OutageNotification.sendEmailNotification(new List<OutageNotification.keyVariables>{kv});
        Test.stopTest();
    }

}