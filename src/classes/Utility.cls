public class Utility {

    public static list<Case_Weight_Settings__c> getCaseWeightSettings(){
        try{
            return Case_Weight_Settings__c.getAll().values();
        }catch(Exception e){
            system.debug('--Exception--'+e.getMessage());
        }
        return null;
    }

    public static String getHardCodeIDCSValueByName(String csParamName){
        try{
            return HardCodeIds__c.getValues(csParamName).value__c;
        }catch(Exception e){
            system.debug('--Exception e--'+e.getMessage());
        }
        return null;
    }

    public static List<Emergency_Assignment__c> getEmergencyAssignmentSettings(){
        try{
            return Emergency_Assignment__c.getall().values();
        }catch(Exception e){
            system.debug('--Exception e--'+e.getMessage());
        }
        return null;
    }


    public static emailSettings__c getEmailSettingsCS(){
        try{
            return emailSettings__c.getOrgDefaults();
        }catch(Exception e){
            system.debug('--Exception e--'+e.getMessage());
        }
        return null;
    }

    public static Map<String,Id> getRecordTypeInfoByObjectName(String objectName){
        Map<String,Id> mapofCaseRecordTypeNameandId = new Map<String,Id>();

        Schema.DescribeSObjectResult sobjectResult = Schema.getGlobalDescribe().get(objectName).getDescribe();
        List<Schema.RecordTypeInfo> recordTypeInfo = sobjectResult.getRecordTypeInfos();
            for(Schema.RecordTypeInfo info : recordTypeInfo){
                mapofCaseRecordTypeNameandId.put(info.getName(),info.getRecordTypeId());
            }
        system.debug('***mapofCaseRecordTypeNameandId*'+mapofCaseRecordTypeNameandId);

        if(!mapofCaseRecordTypeNameandId.isEmpty()){
            return mapofCaseRecordTypeNameandId;
        }
        return null;
    }
    
    public static Map<Id,String> getRecordTypeInfoByObject(String objectName){
        Map<Id,String> mapofCaseRecordTypeNameandId = new Map<Id,String>();

        Schema.DescribeSObjectResult sobjectResult = Schema.getGlobalDescribe().get(objectName).getDescribe();
        List<Schema.RecordTypeInfo> recordTypeInfo = sobjectResult.getRecordTypeInfos();
            for(Schema.RecordTypeInfo info : recordTypeInfo){
                mapofCaseRecordTypeNameandId.put(info.getRecordTypeId(),info.getName());
            }
        system.debug('***mapofCaseRecordTypeNameandId*'+mapofCaseRecordTypeNameandId);

        if(!mapofCaseRecordTypeNameandId.isEmpty()){
            return mapofCaseRecordTypeNameandId;
        }
        return null;
    }

    public static Map<String,Id> getQueueInfoByObjectName(String objectName){
        Map<String,Id> mapofQueueNameandId = new Map<String,Id>();

        for(QueueSobject eachQueueSObject :[Select Id, SobjectType, QueueId,Queue.Name from QueueSobject where SobjectType =: objectName]){
            mapofQueueNameandId.put(eachQueueSObject.Queue.Name,eachQueueSObject.QueueId);
        }
        
        if(!mapofQueueNameandId.isEmpty()){
            return mapofQueueNameandId;
        }
        return null;
    }


    public static EmailTemplate getEmailTemplateByName(String emailTemName){

        EmailTemplate emailTempInstance = new EmailTemplate();
        
        emailTempInstance = [SELECT id,Name from EmailTemplate where Name=:emailTemName];

        if(emailTempInstance!=null){
            return emailTempInstance;
        }
        return null;
    }


    //this method returns a map of an object and its fields api names. If isCustomOnly is true, it returns custom fields only.
    
    public static Map<String, String> getAllFields(String sobjectname, Boolean isCustomOnly){
           
          if(!Schema.getGlobalDescribe().containsKey(sobjectname)) return new Map<String, String>{'Exception' => 'Invalid object name'};
            Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get(sobjectname).getDescribe().SObjectType.getDescribe().fields.getMap();
            List<String> accessiblefields = new List<String>();
      
            for(Schema.SObjectField field : fields.values()){
                if(field.getDescribe().isAccessible() && ( isCustomOnly ? field.getDescribe().isCustom() : field.getDescribe().isCustom() || !field.getDescribe().isCustom())) accessiblefields.add(field.getDescribe().getName());
            }
      
            String allfields='';
      
            for(String fieldname : accessiblefields)
                allfields += fieldname+',';
      
            allfields = allfields.subString(0,allfields.length()-1);
            system.debug('---sobjectname'+sobjectname+'---allfields'+allfields);
            return new Map<String, String>{sobjectname => allfields};
    }

    public static map<String,CountryISOCodes__c> getCountryISOCodeSettings(){

        map<String,CountryISOCodes__c> isoCodeCountryRecordMap = new map<String,CountryISOCodes__c>();

        for(CountryISOCodes__c eachISO:CountryISOCodes__c.getall().values()){
            isoCodeCountryRecordMap.put(eachISO.ISO2__c,eachISO);
        }

        if(!isoCodeCountryRecordMap.isempty()){
            return isoCodeCountryRecordMap;
        }
        return null;
    }
}