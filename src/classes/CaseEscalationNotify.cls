global class CaseEscalationNotify implements Database.Batchable<sObject> {
	 //custom settings
    emailSettings__c emailSettingCSRecord = Utility.getEmailSettingsCS();
    
    //constants and variable declaration    
    Integer TIME_INTERVAL = (Integer)emailSettingCSRecord.Time_Interval__c;   //value needs to be between 1-59
    String CASE_PRIORITY = emailSettingCSRecord.Case_Priority__c;             //Priority value that needs to be included in criteria
    String CASE_STATUS = CONSTANTS.NEW_STATUS;                             //Status value that needs to be excluded in criteria
    List<Case> tempList = new List<Case>();    

   global Database.QueryLocator start(Database.BatchableContext BC){
        
      //query all case records for the desired criteria                
      return Database.getQueryLocator([Select Id, CaseNumber, Owner.Name, OwnerId, emailFlag__c FROM Case WHERE (Priority = :CASE_PRIORITY AND Status = :CASE_STATUS) AND Owner.Type = 'Queue' ]);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
    
        List<case> listCase = (List<case>)scope;
        for(case a : listCase ){
    
            if (a.emailFlag__c == True)
                a.EmailFlag__c = False;
            else 
                a.emailFlag__c = True;          
        tempList.add(a);
    
        }
        //updates the flag value for each case record  
        Update tempList;
    }        

   global void finish(Database.BatchableContext BC){

        //aborts the current job
        if (emailSettingCSRecord.Job_Id__c  <> null)   
            System.abortJob(emailSettingCSRecord.Job_Id__c);
   
        //schedules the next job after the desired time interval (in this case 10 mins)
        Integer minute = DateTime.now().minute();
        minute = (minute < (60-TIME_INTERVAL)) ? minute + TIME_INTERVAL : minute - (60-TIME_INTERVAL);
       // emailSettingCSRecord.Job_Id__c = System.schedule('Emergency Case Notification ' + DateTime.now().getTime(), '00 ' + String.valueOf(minute) + ' 1-23 * * ?', new Shift_CaseEscalationNotify_Scheduler() );
    
        //updates the custom settings with current Job ID
        update emailSettingCSRecord;       
   }
}