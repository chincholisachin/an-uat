@isTest
private class ConsoleUserTriggerHandlerTest{

    private static List<Console_User__c> createConsoleUsers(){

        List<Console_User__c> cuList = new List<Console_User__c>();
        for (Integer i = 0; i<200; i++){
            cuList.add(new Console_User__c(Active__c = true, 
                                           FirstName__c = 'Firstname'+String.valueOf(i), 
                                           LastName__c = 'LastName'+String.valueOf(i), 
                                           SyncStatus__c = 'Pending', 
                                           Username__c = 'username' + String.valueOf(i),
                                           Email__c = 'username' +  + String.valueOf(i) + '@example.com',
                                           userid__c = i, 
                                           UserRole__c = 'Finance', 
                                           UserType__c = 'member'));
        }
        return cuList; 
    }
    
    private static void setupTestData(){
    
        TestingUtils.createEssentialCustomSettings();
        Map<String,Id> recordTypes = new Map<String,Id>();
        for (RecordType rt: [select id, name from recordtype where sobjecttype = 'Account' and name in ('Customer Account','Console Member')]){
            recordTypes.put(rt.name,rt.id);
        }  
        Account[] accounts = TestingUtils.createAccounts(200, 'testAccount', false);
        for (Account acn : accounts){
            acn.recordtypeid = recordTypes.get('Customer Account');
            acn.vertical_new__c = 'ATBs and ATDs';
            acn.Region_rdp__c = 'US/Canada';
            acn.Global_rdp__c = 'US/Canada';
        }
        insert accounts;
    
        Account[] members = TestingUtils.createAccounts(200, 'testMember', false);
        for (Integer i = 0; i<200; i++){
            members[i].recordtypeid = recordTypes.get('Console Member');
            members[i].AppNexus_Member_Id__c = String.valueOf(i+100);
            members[i].parentid = accounts[i].id;   
        }
        insert members;
    }
    
    private static void setupTestUsers(){
        
        setupTestData();
        Account[] acnList = [select id from account where recordtype.name = 'Customer Account'];
        Contact[] conList = new List<Contact>();
        for (Integer i = 0; i<200; i++){
            conList.add(new Contact(lastname = 'last', firstname = 'first', email = 'username' + String.valueOf(i) + '@example.com', accountid = acnList[0].id));
        }
        insert conList;
        
        List<Console_User__c> cuList = createConsoleUsers();
        Integer i = 100;
        for (Console_User__c cu : cuList){
            cu.entityid__c = String.valueOf(i);
            i++; 
        }
        insert cuList;
    }
    
    // Scenario 1: When a new active user is inserted, related member is not found, his status becomes failed with explanation.
    // Scenario 2: When a new active user is inserted, he is not a lead or a contact, a new contact is created by under the parent account of the member, 
    // and he is associated with the correct member, including the username, active status. Console user record is populated with Relationship id, 
    // and newly formed contact record.

    public static testMethod void scenario1and2(){
    
        setupTestData();
        List<Console_User__c> cuList = createConsoleUsers();
        Integer i = 250;
        for (Console_User__c cu : cuList){
            cu.entityid__c = String.valueOf(i);
            i++; 
        }
        Test.startTest();
        insert cuList;
        Test.stopTest();
        
        System.assertEquals(150,[select id from console_user__c where SyncStatus__c = 'Fail' and ErrorMessage__c = :ConsoleUserTriggerHandler.CANNOT_FIND_MATCHING_MEMBER].size());
        System.assertEquals(50,[select id from contact where account.parentid = null].size());
        System.assertEquals(50,[select id from accountcontactrelation where account.recordtype.name = 'Console Member' and Console_Username__c like 'username%'].size());
        System.assertEquals(50,[select id from console_user__c where SyncStatus__c = 'Success' and contact__c != null and AccountContactRelationshipId__c != null].size());
    }
    
    //Scenario 3:
    //When a new active user is inserted, and he is a lead, the lead is converted. Lead conversion fails and console user record is updated with fail message.
    
    // Scenario 4:
    // When a new active user is inserted, and he is a lead, the lead is converted. The new contact is under the parent account. 
    // New contact's name, first name, last name, email address is updated with the information on the console user record. 
    // Relationship is created under the member. Console user record is populated with Relationship id, and contact record, with success message.   
    
    
    public static testMethod void scenario3and4(){
    
        setupTestData();
        Lead[] ld = TestingUtils.createLeads(200, 'LastName', 'Company', 'Open',false);
        for (Integer i = 100; i<300; i++){
            ld[i-100].Email = 'username' + String.valueOf(i) + '@example.com';
        }
        insert ld;
        List<Console_User__c> cuList = createConsoleUsers();
        Integer i = 100;
        for (Console_User__c cu : cuList){
            cu.entityid__c = String.valueOf(i);
            i++; 
        }
        Test.startTest();
        insert cuList;
        Test.stopTest();
        List<Lead> leads = [select id, convertedcontactid, convertedaccountid, convertedaccount.parentid from lead where isconverted = true];
        System.assertEquals(100,leads.size()); //100 leads are now converted.
        System.assertEquals(null, leads[0].convertedaccount.parentid); // lead is converted to a top level account
        Account acc  = [select id, recordtype.name from account where id =:leads[0].convertedaccountid limit 1];
        System.assertEquals(acc.recordtype.name, 'Customer Account'); // lead is converted to a customer account. 
        System.assertEquals(200,[select id from contact].size()); //100 new contacts are created. with 100 leads we have now 200 new contacts.
        List< accountcontactrelation> acr = [select id, contact.firstname, contact.accountid from accountcontactrelation where account.recordtype.name = 'Console Member' and Console_Username__c like 'username%'];
        System.assertEquals(200,acr.size()); // 200 new relationship created on members.
        System.assert(acr[0].contact.firstname != acr[1].contact.firstname); //first names are not the same, it should be like firstname1 and firstname2.
        System.assert(acr[0].contact.firstname.contains('Firstname')); //firstname should contain Firstname
        
        
        Set< String > customerAccountIds = new Set<String>();
        for (Account a: [select id from account where recordtype.name = 'Customer Account']){
            customerAccountIds.add(a.id);
        }
        System.assert(customerAccountIds.contains(acr[0].contact.accountid));
        Console_User__c cu = [select id, SyncStatus__c, contact__r.firstname, contact__r.lastname, contact__r.email, firstname__c, lastname__c, email__c, AccountContactRelationshipId__c from console_user__c where contact__c = :leads[0].convertedcontactid limit 1];
        System.assertEquals(cu.contact__r.firstname, cu.firstname__c);
        System.assertEquals(cu.contact__r.lastname, cu.lastname__c);
        System.assertEquals(cu.contact__r.email, cu.email__c);
        System.assert(cu.AccountContactRelationshipId__c != null);
        System.assertEquals('Success',cu.SyncStatus__c);
    
    }
   
    // Scenario 5:
    // When a new active user is inserted, and he is a contact with account field NOT populated, 
    // contact is attached to the parent account, relationship to member is created. 
    // Console user record is populated with Relationship id, and contact record, with success message. 


    public static testMethod void scenario5(){
    
        setupTestData();
        
        List<Contact> cList = new List<Contact>();
        for (integer i = 0;i<200;i++){
            cList.add(new Contact(lastname = 'Lastname'+i, firstname = 'Firstname'+i, email = 'username' + String.valueOf(i) + '@example.com'));
        }
        insert cList;
        List<Console_User__c> cuList = createConsoleUsers();
        Integer i = 100;
        for (Console_User__c cu : cuList){
            cu.entityid__c = String.valueOf(i);
            i++; 
        }
        Test.startTest();
        insert cuList;
        Test.stopTest();
        
        System.assertEquals(0,[select id from contact where accountid = null].size());
        Map<String,string> memberToParent = new Map<String,String>();
        Map<String,string> cuToAccount = new Map<String,String>();
        for (Account a : [select parentid, appnexus_member_id__c from account where appnexus_member_id__c != null]){
            memberToParent.put(a.appnexus_member_id__c, a.parentid);
        }
        
        for (Console_user__c cu : [select entityid__c, contact__r.accountid from console_user__c where entityid__c != null]){
            cuToAccount.put(String.valueOf(cu.entityid__c), cu.contact__r.accountid);
        }
        
        for (String s: memberToParent.keySet()){
            System.assertEquals(memberToParent.get(s), cuToAccount.get(s));
        }
    
    }

    // Scenario 6: 
    // When a new active user is inserted, and he is a contact with account field populated, contact is associated to the member. 
    // Console user record is populated with Relationship id, and contact record, with success message. Relationship records are also created.

    public static testMethod void scenario6(){
    
        setupTestData();
        Account[] acnList = [select id from account where recordtype.name = 'Customer Account'];
        Contact[] conList = new List<Contact>();
        for (Integer i = 0; i<200; i++){
            conList.add(new Contact(lastname = 'last', firstname = 'first', email = 'username' + String.valueOf(i) + '@example.com', accountid = acnList[0].id));
        }
        insert conList;
        
        List<Console_User__c> cuList = createConsoleUsers();
        Integer i = 100;
        for (Console_User__c cu : cuList){
            cu.entityid__c = String.valueOf(i);
            i++; 
        }
        Test.startTest();
        insert cuList;
        Test.stopTest();
        
        System.assertEquals(200,[select id from console_user__c where AccountContactRelationshipId__c != null and contact__c in: conList and SyncStatus__c = 'Success'].size());
        
        Set< String > consoleAccountIds = new Set<String>();
        for (Account a: [select id from account where recordtype.name = 'Console Member']){
            consoleAccountIds.add(a.id);
        }
        
        System.assertEquals(200,[select id from accountcontactrelation where contactid in :conList and accountid in :consoleAccountIds ].size());
        
    
    }
   
    // Scenario 7:
    // When an active user become deactive, and contact, relationship id fields are populated, relationship record is deleted.
    public static testMethod void scenario7(){
    
        setupTestUsers();
   
        Test.startTest();
        
        List<Console_user__c> userlist = [select id, active__c, userrole__c, syncstatus__c, contact__c, entityid__c,AccountContactRelationshipId__c from console_user__c where active__c = true];
            
        ConsoleUserTriggerHandler.firstRun = true;
        ConsoleUserTriggerHandler.acrProcessList.clear();

        for (Console_User__c cu: userlist){
            cu.active__c = false;
            cu.syncstatus__c = 'Pending';
        }
        
        
        update userlist;
                
        Test.stopTest();

        List<String> entityIds = new List<String>();
        List<Console_user__c> cuNew = [select id, active__c, syncstatus__c, contact__c, entityid__c,AccountContactRelationshipId__c from console_user__c where active__c = false ];
                
        System.assertEquals(200,cuNew.size());
    
        for (Console_User__c c: cuNew){
            System.assertEquals(null, c.AccountContactRelationshipId__c);
            System.assert(c.Contact__c != null);
            entityIds.add(String.valueOf(c.entityid__c));
        }
        
        System.assertEquals(0,[select id from accountcontactrelation where account.appnexus_member_id__c in: entityIds].size());
    }

    // Scenario 8: 
    // When a deactive user becomes active, and contact field is populated, relationship record is created.
    // Scenario 9: 
    // When a deactive user becomes active, and contact field is NOT populated, contact field is populated, relationship record is created. 
    
    public static testMethod void scenario8and9(){
        
        setupTestData();
        Account[] acnList = [select id from account where recordtype.name = 'Customer Account'];
        Contact[] conList = new List<Contact>();
        for (Integer i = 0; i<100; i++){
            conList.add(new Contact(lastname = 'last', firstname = 'first', email = 'username' + String.valueOf(i) + '@example.com', accountid = acnList[0].id));
        }
        insert conList;
        Map<String,String> emailToContact = new Map<String, String>();
        for (Contact c: conList){
            emailToContact.put(c.email,c.id);
        }
        
        List<Console_User__c> cuList = createConsoleUsers();
        Integer i = 100;
        for (Console_User__c cu : cuList){
            cu.entityid__c = String.valueOf(i);
            cu.contact__c = emailToContact.get(cu.email__c);
            cu.active__c = false;
            cu.syncstatus__c = 'Pending';
            i++; 
        }
        insert cuList;
        
        System.assertEquals(0,[select id from accountcontactrelation where account.recordtype.name = 'Console Member'].size());
        
        ConsoleUserTriggerHandler.firstRun = true;
        ConsoleUserTriggerHandler.acrProcessList.clear();
        Test.startTest();
        for (Console_User__c cu: cuList){
            cu.active__c = true;
        }
        
        update cuList;
        System.assertEquals(200,[select id from accountcontactrelation where account.recordtype.name = 'Console Member'].size());
        System.assertEquals(200,[select id from contact].size());
        
    } 

    // Scenario 10: 
    // Role or name changes

    public static testMethod void scenario10(){
    
        setupTestData();
        Account[] acnList = [select id from account where recordtype.name = 'Customer Account'];
        Contact[] conList = new List<Contact>();
        for (Integer i = 0; i<200; i++){
            conList.add(new Contact(lastname = 'last', firstname = 'first', email = 'username' + String.valueOf(i) + '@example.com', accountid = acnList[0].id));
        }
        insert conList;
        
        List<Console_User__c> cuList = createConsoleUsers();
        Integer i = 100;
        for (Console_User__c cu : cuList){
            cu.entityid__c = String.valueOf(i);
            i++; 
        }
        insert cuList;
        
        system.debug('$$$ 1 cuList[0] ' +  cuList[0]);      
        
        ConsoleUserTriggerHandler.firstRun = true;
        ConsoleUserTriggerHandler.acrProcessList.clear();
        Test.startTest();
        
        for (Console_User__c cu : cuList){
            cu.UserRole__c = 'Other';
            cu.FirstName__c = 'Name'+String.valueOf(i);
            cu.LastName__c = 'Last'+String.valueOf(i);
            cu.SyncStatus__c = 'Pending';
            i++;
        }
        update cuList;
        
        system.debug('$$$ 2 cuList[0] ' + cuList[0]);
        
        system.debug('$$$ contact '+ [select firstname from contact where id = :cuList[0].contact__c limit 1]);
        
        Test.stopTest();
        
        System.assertEquals(200,[select id from contact where firstname like 'Name%'].size());
        System.assertEquals(200,[select id from accountcontactrelation where Roles = 'Other'].size());
        
    }

}