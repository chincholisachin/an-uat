public  class SalesOpsCaseExtV2
{
    

    public Case newCase{get;set;}
    public List<Schema.FieldSetMember> caseFieldSet{get;set;}
    public Boolean typeReasonSelected{get;set;}
    public Boolean displayingCaseTypeAndReasonPage{get;private set;}
    public List<SalesOps_Descrip__c> typeDescriptions{get;set;}
    public List<SalesOps_Descrip__c> reasonDescriptions{get;set;}
    public Blob theFile{get;set;}
    public String theFileName{get;set;}
    public static Map<String,Id> recordTypeNameandIDMap =  Utility.getRecordTypeInfoByObjectName('Case');

    public String reminder
    {
        get
        {
            List<SalesOps_Descrip__c> mydescript = [SELECT Reminder__c
                                                    FROM SalesOps_Descrip__c
                                                    WHERE CaseReason__c = :newCase.Sales_Ops_Case_Reason__c
                                                    AND CaseType__c = :newCase.Sales_Ops_Case_Type__c
                                                    AND Case_Type_Description__c = false];
            if(mydescript.size() >= 1)
            {
                return mydescript[0].Reminder__c;
            }
            else
            {
                return '';
            }
        }
    }

    public SalesOpsCaseExtV2() //Don't delete, necessary for attachment
    {
        newCase = new Case();
        Map<String, String> urlParams = Apexpages.currentPage().getParameters();

        if(urlparams.keyset().contains(CONSTANTS.TYPE_PARAM))
            newCase.Sales_Ops_Case_Type__c = urlParams.get(CONSTANTS.TYPE_PARAM);
        if(urlparams.keyset().contains(CONSTANTS.CASE_ID_PARAM))
            newCase.Id = urlparams.get(CONSTANTS.CASE_ID_PARAM);
        if(urlparams.keyset().contains(CONSTANTS.REASON_PARAM))
            newCase.Sales_Ops_Case_Reason__c = urlparams.get(CONSTANTS.REASON_PARAM);
      /*  if( String.isNotEmpty( urlParams.get(RELATED_ACCOUNT_ID) ) )
        {
            newCase.Account_Related_To_Sales_Ops_Request__c = urlparams.get(RELATED_ACCOUNT_ID);
        } */
        displayingCaseTypeAndReasonPage = true;
    }

    public void nextPage()
    {
        if( (String.isBlank(newCase.Sales_Ops_Case_Reason__c)  && newCase.Sales_Ops_Case_Type__c != CONSTANTS.Exception_Review) || String.isBlank( newCase.Sales_Ops_Case_Type__c )  )
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, CONSTANTS.NO_SALESOPS_REASON) );
        }
        else
        {
            populateFieldSet();
            if( typeReasonSelected )
            {
                displayingCaseTypeAndReasonPage = false;
            }
        }
    }

    public ApexPages.PageReference goHome()
    {
        return new ApexPages.PageReference('/');
    }

    public Boolean displaySaveAndGoBtn
    {
        get
        {
            return (!(newCase.Sales_Ops_Case_Type__c == CONSTANTS.Contract_Submission || (newCase.Sales_Ops_Case_Type__c == CONSTANTS.Consult_With_Sales_Ops  && newCase.Sales_Ops_Case_Reason__c == CONSTANTS.Contract_Review) ) ) & typeReasonSelected;
        }
        private set;
    }

    public SalesOpsCaseExtV2(Apexpages.StandardController std)
    {

        typeDescriptions = [SELECT CaseType__c, Description__c FROM SalesOps_Descrip__c WHERE Case_Type_Description__c = true ORDER BY Order__c ASC];
        try
        {
            //RecordType salesOpRT = recordTypeNameandIDMap.get(CONSTANTS.SALES_OP);
            newCase = new Case(recordTypeId = recordTypeNameandIDMap.get(CONSTANTS.SALES_OP));
        }
        catch (Exception e)
        {
            newCase = new Case();
        }

        Map<String, String> getParams = Apexpages.currentPage().getParameters();
        if(getParams.keySet().contains(CONSTANTS.TYPE_PARAM))
        {
            newCase.Sales_Ops_Case_Type__c = getParams.get(CONSTANTS.TYPE_PARAM);
            this.changeType();
            if(getParams.keySet().contains(CONSTANTS.REASON_PARAM))
            {
                newCase.Sales_Ops_Case_Reason__c = getParams.get(CONSTANTS.REASON_PARAM);
            }
        }
        if( String.isNotEmpty( getParams.get(CONSTANTS.CASE_ID_PARAM ) ) )
        {
            newCase.Id = getParams.get( CONSTANTS.CASE_ID_PARAM );
        }
       /* if( String.isNotEmpty( getParams.get( RELATED_ACCOUNT_ID ) ) )
        {
            newCase.Account_Related_To_Sales_Ops_Request__c = getParams.get( RELATED_ACCOUNT_ID );
        } */
        if( String.isNotEmpty( getParams.get( CONSTANTS.OPPORTUNITY_ID_PARAM ) ) )
        {
            newCase.Opportunity__c = getParams.get(CONSTANTS.OPPORTUNITY_ID_PARAM );
        }

        typeReasonSelected = false;
        displayingCaseTypeAndReasonPage = true;
    }

    public void populateFieldSet()
    {
        if( newCase.Sales_Ops_Case_Type__c != null && newCase.Sales_Ops_Case_Type__c == CONSTANTS.Exception_Review)
            newCase.Sales_Ops_Case_Reason__c = '';
        if(String.isBlank(newCase.Sales_Ops_Case_Type__c) || (String.isBlank(newCase.Sales_Ops_Case_Reason__c) && newCase.Sales_Ops_Case_Type__c != CONSTANTS.Exception_Review))
        {
            ApexPages.addMessage(new Apexpages.Message(Apexpages.SEVERITY.Error, CONSTANTS.NO_SALESOPS_REASON));
            return;
        }
        else
        {
            newCase = new Case(Sales_Ops_Case_Reason__c = newCase.Sales_Ops_Case_Reason__c,
                               Sales_Ops_Case_Type__c = newCase.Sales_Ops_Case_Type__c,
                               Opportunity__c = newCase.Opportunity__c,
                               RecordTypeId = newCase.RecordTypeId);

            Map<String, Schema.FieldSet> fsetMap = Case.sObjectType.getDescribe().Fieldsets.getMap();
            String fsetKey = newCase.Sales_Ops_Case_Type__c + '0' + (newCase.Sales_Ops_Case_Reason__c == null?'':newCase.Sales_Ops_Case_Reason__c);
            fsetKey = fsetKey.replaceAll('[^a-zA-Z0-9]', '');
            if(fsetKey.length() > 40)
            {
                fsetKey = fsetKey.substring(0,40);
            }
            if(fsetMap.keySet().contains(fsetKey.toLowerCase()))
            {
                Schema.FieldSet targetFieldset = fsetMap.get(fsetKey.toLowerCase());
                if(targetFieldset.getName() == fsetKey)
                {
                    caseFieldSet = fsetMap.get(fsetKey.toLowerCase()).getFields();
                    typeReasonSelected = true;
                    return;
                }
            }
            ApexPages.addMessage(new Apexpages.Message(Apexpages.SEVERITY.Error, CONSTANTS.UNABLE_TO_FIND_FS + fsetKey));
            return;
        }
    }

    public void changeType()
    {
        reasonDescriptions = [SELECT CaseReason__c, Description__c FROM SalesOps_Descrip__c WHERE Case_Type_Description__c = false AND CaseType__c = :newCase.Sales_Ops_Case_Type__c ORDER BY Order__c ASC];
    }

    public PageReference addAttachment()
    {
        Attachment newAttachment = new Attachment();
        if(!(String.isBlank(theFileName) || theFile == null))
        {
            newAttachment.Body = theFile;
            newAttachment.Name = theFileName;
            newAttachment.OwnerId = UserInfo.getUserId();
            newAttachment.ParentId = newCase.id;

            try
            {
                insert newAttachment;
                PageReference ref = new PageReference('/' + newCase.Id);
                ref.setRedirect(true);
                return ref;
            }
            catch(system.dmlException dEX)
            {
                Apexpages.addMessages(dEX);
                return null;
            }
        }
        else
        {
            if(newCase.Sales_Ops_Case_Type__c == CONSTANTS.Contract_Submission)
            {
                Apexpages.addMessage(new Apexpages.Message(Apexpages.SEVERITY.ERROR , CONSTANTS.CONTRACT_ATTACH_REQUIRED));
            }
        }
        return null;
    }

    public PageReference saveCaseAndAttachment()
    {
       if( !save() )
            return null;
            
        PageReference ref = Page.SalesOpsFileAttacher;
        ref.getParameters().put(CONSTANTS.CASE_ID_PARAM, newCase.Id);
        ref.getParameters().put(CONSTANTS.TYPE_PARAM, newCase.Sales_Ops_Case_Type__c);
        ref.setRedirect(true);
        return ref;
       
    }

    public Boolean save()
    {
        User currentUser = [SELECT Id, email FROM User WHERE Id = :UserInfo.getUserId()];
        List<Contact> con = [select id from contact where email = :currentUser.email and account.Name = :CONSTANTS.ACCOUNT_NAME];
        if(con.size() > 0)
        {
            newCase.contactId = con[0].Id;
        }
        if(!newCase.Expedite__c )
        {
            newCase.Hard_Deadline__c = null;
           // newCase.Expedite_Justification__c = null;
        }
        /*if(newCase.Account_Related_To_Sales_Ops_Request__c != null){
            Account acn = [select id, related_party__c from account where id =:newCase.Account_Related_To_Sales_Ops_Request__c];
            newCase.Description = acn.related_party__c == 'Yes' ? 'NOTE: This account has been flagged as a Related Person. Please notify legal if this request is non-standard.\n' + newCase.Description : newCase.Description;
        }*/
        Database.DmlOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.useDefaultRule = true;
        dmlOpts.EmailHeader.triggerAutoResponseEmail = true;
        dmlOpts.EmailHeader.triggerUserEmail = true;
        Database.SaveResult result = Database.insert(newCase, dmlOpts);
        if(!result.isSuccess())
        {
            Apexpages.addMessage(new Apexpages.Message(Apexpages.SEVERITY.ERROR, CONSTANTS.CASE_CREATION_FAILED + result.getErrors()[0].getMessage()));
            return false;
        }
        return true;
    }

    public PageReference saveAndGo()
    {
        return save() ? new PageReference('/' + newCase.Id) : null;
    }

    public pageReference goBack()
    {
        PageReference ref = Apexpages.currentPage();
        displayingCaseTypeAndReasonPage = true;
        ref.getParameters().put(CONSTANTS.REASON_PARAM, newCase.Sales_Ops_Case_Reason__c);
        ref.getParameters().put(CONSTANTS.TYPE_PARAM, newCase.Sales_Ops_Case_Type__c);
        ref.setRedirect(true);
        return ref;
    }
}