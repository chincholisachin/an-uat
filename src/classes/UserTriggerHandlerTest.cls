@isTest
public class UserTriggerHandlerTest{

    public static void setupCSPContact(){
        
        TestingUtils.createEssentialCustomSettings();
        Account acn = new Account(Name = 'AppNexus Employees', recordTypeId = [Select id from RecordType where sobjecttype = 'Account' and Name = 'Internal Entity' limit 1].id);
        insert acn;
        System.assertNotEquals(acn.id, null);
        
        Contact con = new Contact(accountid = acn.id, FirstName = 'CSP', LastName = 'Dummy', recordtypeid = [Select id from RecordType where sobjecttype = 'Contact' and Name = 'AN Employee' limit 1].id);
        insert con;
        System.assertNotEquals(con.id, null);
    }
    
    
    public static testMethod void insertUsers(){
    
        setupCSPContact();
        Test.startTest();
        User[] users = TestingUtils.createUsers(5, 'testUser', 'testUser', '* Commercial', true);
        Test.stopTest();
        Set<Id> userIds = new Set<Id>();
        for (User u : users){
            userIds.add(u.id);
        }
        //System.assertEquals(5,userIds.size());
        
        
    }
    
    /*   
    public static testMethod void testFutureMethods(){
    
        setupCSPContact();

        User[] users = TestingUtils.createUsers(5, 'testUser', 'testUser', '* Commercial', true);

        Set<Id> userIds = new Set<Id>();
        for (User u : users){
            userIds.add(u.id);
        }
        System.assertEquals(5,userIds.size());
        
        Contact c = [select id, accountid, recordtypeid from contact where FirstName = 'CSP' and LastName = 'Dummy' limit 1];
        
        Test.startTest();
        UserTriggerHandler.createUserContactFuture(userIds, c.recordtypeid, c.id, c.accountId);
        
        for (User u: users){
            u.department = 'New Department';
        }
        update users;
        UserTriggerHandler.updateUserContactFuture(userIds);
        
        Test.stopTest();
        //System.assertEquals(5,[select id from contact where firstName != 'CSP' and LastName !='Dummy' and department = 'New Department'].size());
        
        
    }
    */
    
    public static testMethod void updateUsers(){
    
        setupCSPContact();
        User[] users = TestingUtils.createUsers(5, 'testUser', 'testUser', '* Commercial', true);
                
        System.runAs(TestingUtils.getAdminUser()){
            PermissionSet ps = new PermissionSet(Name = 'TestSet', Label = 'TestSet');
            insert ps;
            PermissionSetAssignment[] psa = new List<PermissionSetAssignment>();
            List<String> userIds = new List<String>();
            for (User u : [select id from user where lastname = 'testUser' and profile.name = '* Commercial']){
                psa.add(new PermissionSetAssignment(permissionsetid = ps.id, assigneeid = u.id));
                userIds.add(u.id);
            }
            insert psa;
            //System.assertEquals(5,[select id from permissionsetAssignment where assigneeid in:userIds and permissionSet.label = 'TestSet'].size());
            TestingUtils.profileMapLoaded = false;
            for (User u: users){
                u.profileid = TestingUtils.getProfile('Salesforce').get('* Services');
                u.department = 'New Department'; 
            }
            Test.startTest();
            //update users;
            Test.stopTest();
        }
        System.assertEquals(0,[select id from permissionsetAssignment where assigneeid in:users and permissionSet.label = 'TestSet'].size());
        
    }

}