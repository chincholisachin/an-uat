@isTest
public class overrideConTest{

    private static overrideCon controller;

    public static testMethod void loadAccount(){
    
        TestingUtils.createEssentialCustomSettings();
        Account[] acn = TestingUtils.createAccounts(1, 'Test', true);
        PageReference pageRef = new PageReference('/apex/acctOverride?id='+acn[0].id);
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(new Account());
        controller = new overrideCon(sc);
        controller.redirect();
    }

}