/**
* APTTUS
* @author           Luis Arismendy=<larismendy@apttus.com> (LA)
* Project:          APPNEXUS
* Description:      Handles selection of Agreement Recordtype and redirects to Apttus Agreement creation
*
* Changes (Version)
* -------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    ---------------
* @version  1.0     05-19-2016      Luis Arismendy(LA)      Class created
* @version  1.1     06-01-2016      Luis Arismendy(LA)      Implemented Custom agreement recordtype for master list
*************************************************************************************************************/
public class APTS_AgreementRecordtype_Controller 
{  
    public list<SelectOption> rTypeOpt {get;set;}
    public list<RecordType> rTypeDesc {get;set;}
    public String rTypeId {get;set;}
    
    private String agreementPage;
    private String agreementType;

    //public APTS_AgreementRecordtype_Controller(){}
    public APTS_AgreementRecordtype_Controller(ApexPages.StandardController objSC)
    {
        rTypeOpt = new list<SelectOption>();
        rTypeDesc = new list<RecordType>();
        agreementType = 'master';
        String agreementId = '';
        
        if(ApexPages.currentPage().getParameters().get('type') != null)
        {
            agreementType = ApexPages.currentPage().getParameters().get('type');
        }
        
        Map<String, Agreement_Master_Records__c> mapRecordTypes = Agreement_Master_Records__c.getall();
        
        List<RecordType> lstRType = new List<RecordType>();
        
        if(agreementType == 'master')
        {
            Set<String> setRecordTypes = new Set<String>();
            setRecordTypes.addAll(mapRecordTypes.keyset());
            setRecordTypes.add('Custom');
            lstRType = [SELECT id, Name, Description FROM RecordType WHERE sObjectType = 'Apttus__APTS_Agreement__c' and isActive = true  AND Name IN :setRecordTypes ORDER BY Name];
            agreementPage = '/apex/Apttus__AgreementNew?';
        }
        else
        {       
            if(ApexPages.currentPage().getParameters().get('id') != null)
            {
                agreementId = 'id=' + ApexPages.currentPage().getParameters().get('id') + '&';
            } 
            
            lstRType = [SELECT id, Name, Description FROM RecordType WHERE sObjectType = 'Apttus__APTS_Agreement__c' and isActive = true  AND Name NOT IN :mapRecordTypes.keyset() ORDER BY Name];          
            agreementPage = '/apex/Apttus__ChildAgreementNew?' + agreementId;
        }
        
        for(RecordType rtype : lstRType)
        {
            if(rTypeId == null)
            {
                rTypeId = rtype.id;
            }
            rTypeOpt.add(new SelectOption(rtype.id,rtype.name));
            rTypeDesc.add(rtype);
        }              
    }
    
    public PageReference selectRecordtype()
    {              
        String retURL = '';
        String saveURL = '';
        String rType = '&RecordType=' + rTypeId;
        
        if(agreementType != 'master')
        {
            rType = '&recordTypeId=' + rTypeId;
        }           
        
        if(ApexPages.currentPage().getParameters().get('retURL') != null)
        {
            retURL = '&retURL=' + ApexPages.currentPage().getParameters().get('retURL');
        }
        
        PageReference pr = new PageReference( agreementPage + 'save_new=1&sfdc.override=1' + rType + retURL);           
        
        if(ApexPages.currentPage().getParameters().get('save_new_url') != null)
        {
            String replace = '/a55/e?';
            System.debug('replace --> ' + replace);
            saveURL = ApexPages.currentPage().getParameters().get('save_new_url');
            System.debug('saveURL before --> ' + saveURL);
            saveURL = saveURL.replace(replace,'');
            System.debug('saveURL after --> ' + saveURL);
            pr = new PageReference( agreementPage + saveURL + rType + '&save_new=1&sfdc.override=1');           
        }

        pr.setRedirect( true );                
        
        return pr;
    }
}