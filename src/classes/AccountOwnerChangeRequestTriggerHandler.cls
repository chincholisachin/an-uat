public class AccountOwnerChangeRequestTriggerHandler{
    
    public static void updateAccountOwnerShip(List<Account_Owner_Change_Request__c> aocr, Map<Id,Account_Owner_Change_Request__c> aocrOldMap){
        
        Set<Id> entireAccount = new Set<Id>(); // when accounts and their relevant accounts need to change.
        Set<Id> justAccount = new Set<Id>();
        Set<Id> justMember = new Set<Id>();
        Set<Account> accountsToUpdate = new Set<Account>();
        List<Account> updateAccounts = new List<Account>();
        List<AccountTeamMember> atmList = new List<AccountTeamMember>();
        List<AccountTeamMember> atmListDelete = new List<AccountTeamMember>();
        Map<Id,List<AccountTeamMember>> accountAndAccountTeamMember = new  Map<Id,List<AccountTeamMember>>();
        
        for (Account_Owner_Change_Request__c ao : aocr){
            if (ao.Status__c == 'Approved' && aocrOldMap.get(ao.id).Status__c != ao.Status__c){
                
                if (ao.Change_Account_Only__c){
                
                    if (ao.AppNexus_Member2__c == null) {
                        accountsToUpdate.add(new Account(id = ao.related_account__c, ownerid = ao.New_Account_Owner__c));
                        justAccount.add(ao.related_account__c);
                        atmlist.add(AccountUtils.generateAccountTeamMember(ao.related_account__c,ao.New_Sr_Account_Owner__c,AccountUtils.SR_ACCOUNT_OWNER));
                        atmlist.add(AccountUtils.generateAccountTeamMember(ao.related_account__c,ao.Market_Owner__c,AccountUtils.COMMERCIAL_LEADER));
                    } else {
                        
                        accountsToUpdate.add(new Account(id = ao.appnexus_member2__c, ownerid = ao.New_Account_Owner__c));
                        justMember.add(ao.appnexus_member2__c);
                        atmlist.add(AccountUtils.generateAccountTeamMember(ao.appnexus_member2__c,ao.New_Account_Owner__c,AccountUtils.ACCOUNT_OWNER));
                        atmlist.add(AccountUtils.generateAccountTeamMember(ao.appnexus_member2__c,ao.New_Sr_Account_Owner__c,AccountUtils.SR_ACCOUNT_OWNER));
                        atmlist.add(AccountUtils.generateAccountTeamMember(ao.appnexus_member2__c,ao.Market_Owner__c,AccountUtils.COMMERCIAL_LEADER));
                    
                    }
                
                } else {
                
                    if (ao.Previous_Account_Owner__c != ao.New_Account_Owner__c) accountsToUpdate.add(new Account(id = ao.related_account__c, ownerid = ao.New_Account_Owner__c)); 
                    justAccount.add(ao.related_account__c); // this is to remove all account team members for this account only. It doesn't touch its children.
                    atmlist.add(AccountUtils.generateAccountTeamMember(ao.related_account__c,ao.New_Sr_Account_Owner__c,AccountUtils.SR_ACCOUNT_OWNER)); // parent accounts new account team member
                    atmlist.add(AccountUtils.generateAccountTeamMember(ao.related_account__c,ao.Market_Owner__c,AccountUtils.COMMERCIAL_LEADER)); // parent accounts new account team member
                    List<AccountTeamMember> temp = new List<AccountTeamMember>(); // temp list to hold all account team members for the children.
                    temp.add(AccountUtils.generateAccountTeamMember(null,ao.New_Account_Owner__c,AccountUtils.ACCOUNT_OWNER));
                    temp.add(AccountUtils.generateAccountTeamMember(null,ao.New_Sr_Account_Owner__c,AccountUtils.SR_ACCOUNT_OWNER));
                    temp.add(AccountUtils.generateAccountTeamMember(null,ao.Market_Owner__c,AccountUtils.COMMERCIAL_LEADER));
                    accountAndAccountTeamMember.put(ao.related_account__c,temp); // map for parent account and children's account team members.

                }
                
            }
        }
     
        List<String> ownerList = new List<String>{AccountUtils.SR_ACCOUNT_OWNER, AccountUtils.COMMERCIAL_LEADER, AccountUtils.ACCOUNT_OWNER}; // to be user in queries.
        
        if (accountAndAccountTeamMember.keySet().size() > 0){ //there are parent accounts
            List<Account> childAccounts = new List<Account>([select id, 
                                                                    recordtypeid, 
                                                                    parentid,
                                                                    parent.ownerid, 
                                                                    ownerid, 
                                                                    (select id from accountteammembers where teammemberrole in :ownerList) 
                                                                    from account where parentid in :accountAndAccountTeamMember.keySet()]); // queries all children accounts and their team members.
            if (childAccounts.size()>0){ //if there are children
                for (Account a: childAccounts){ //iterate over children
                    if (AccountUtils.isMember().get(a.recordtypeId) && a.ownerid == a.parent.ownerid){ //if child account is a member and it shares the same owner with its owner, then account teams to refresh
                    //if (AccountUtils.isMember().get(a.recordtypeId)){ 
                        atmListDelete.addAll(a.accountteammembers); // add all team members to a list to delete -- this should be if owner doesn't equal to its owner
                        for (AccountTeamMember at : AccountAndAccountTeamMember.get(a.parentid)){
                            if (at.TeamMemberRole == AccountUtils.ACCOUNT_OWNER) accountsToUpdate.add(new Account(id=a.id,ownerid=at.userid));
                            atmlist.add(AccountUtils.generateAccountTeamMember(a.id,at.userid,at.TeamMemberRole));
                        }
                    }
                }
            }
        }
        
        if (accountsToUpdate.size()>0) {
            updateAccounts.addAll(accountsToUpdate);
            update updateAccounts;
        }
        
        if (justAccount.size() > 0) atmListDelete.addAll(deleteTeamMembers(justAccount, new List<String>{AccountUtils.SR_ACCOUNT_OWNER, AccountUtils.COMMERCIAL_LEADER}));
        if (justMember.size() > 0) atmListDelete.addAll(deleteTeamMembers(justMember, new List<String>{AccountUtils.SR_ACCOUNT_OWNER, AccountUtils.COMMERCIAL_LEADER, AccountUtils.ACCOUNT_OWNER}));
        if (atmListDelete.size() > 0) delete atmListDelete;
        if (atmlist.size() > 0) insert atmlist;  
        
        
    }
    
    private static List<AccountTeamMember> deleteTeamMembers(Set<Id> accountIds, List<String> rolesToDelete){

        List<AccountTeamMember> at = new List<AccountTeamMember>([select id from accountteammember where accountid in :accountIds and teammemberrole in :rolesToDelete]);
        return at;
    
    }
    
}