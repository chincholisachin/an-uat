public class UserTriggerHandler{
    
    /*
    public static void createUserContact(List<User> newUsers){
    
        Set<Id> userList = new Set<Id>();
        for (User u : newUsers){
            userList.add(u.id);
        }
        createUserContactFuture(userList, getCspContact().recordtypeid, getCspContact().Id, getCspContact().accountId);
    }
    
    
    @future
    public static void createUserContactFuture(Set<Id> userIds, ID recordTypeId, ID cspId, ID accountId){
    
        List<User> newUsers = [select id, firstname, lastname, email, department, title from user where id in: userIDs and profile.usertype = 'Standard']; 
        Contact[] cList = new List<Contact>();
        for (User u: newUsers){
            Contact c = new Contact();
            c.firstname = u.firstname;
            c.lastname = u.lastname;
            c.email = u.email;
            c.department = u.department;
            c.title = u.title;
            c.accountid = accountId;
            c.reportstoid = cspId;
            c.User__c = u.id;
            c.recordtypeid = recordTypeId;
            cList.add(c);
        }
        if (cList.size()> 0){
            Database.insert(cList, false);
        }

    }
    
    public static void updateContactWhenUserChanges(List<User> newUsers, Map<Id, User> oldUser){
        Set<Id> userIds = new Set<Id>();
        for (User u: newUsers){
            if (u.firstname != oldUser.get(u.id).firstName || u.lastName != oldUser.get(u.id).lastName || u.email != oldUser.get(u.id).email || u.department != oldUser.get(u.id).department || u.title != oldUser.get(u.id).title ) userIds.add(u.id);
        }
        
        if (userIds.size() > 0 ) updateUserContactFuture(userIds);
    }
    
    @future
    public static void updateUserContactFuture(Set<Id> userIds){
    
        Map<Id,User> updatedUsers = new Map<Id,User>([select id, firstname, lastname, email, department, title from user where id in: userIDs]); 
        Contact[] cList = new List<Contact>([select id, firstname, lastname, email, department, user__c, title from contact where user__c in :updatedUsers.keySet()]);
        for (Contact c: clist){
            c.firstname = updatedUsers.get(c.user__c).firstname;
            c.lastname = updatedUsers.get(c.user__c).lastname;
            c.email = updatedUsers.get(c.user__c).email;
            c.department = updatedUsers.get(c.user__c).department;
            c.title = updatedUsers.get(c.user__c).title;
        }
        if (cList.size()> 0){
            Database.update(cList, false);
        }

    }
    */
    
    public static void resetPermissionSets(List<User> newUsers, Map<Id,User> oldUsers){
        
        Set<String> userIdList = new Set<String>();
        for (User u: newUsers){
            if (oldUsers.get(u.id).profileid != u.profileid){
                userIdList.add(u.id);
            }
        }
        if (userIdList.size() > 0){
            PermissionSetAssignment [] permSets = [select Id from PermissionSetAssignment where AssigneeId in:userIdList and permissionset.isownedbyprofile = false];
            if (permSets.size()> 0) Database.delete(permsets,false);
        }
    
    }

}