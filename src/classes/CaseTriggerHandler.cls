public class CaseTriggerHandler {

  private static CaseTriggerHandler instance;

  //*** Singleton Pattern to obtain instance of the Handler***
    public static CaseTriggerHandler getInstance(){
        if(instance==null){
            instance = new CaseTriggerHandler();
        }
        return instance;
    }

    // --All before insert operations will be called from here --//
    public void beforeInsertMethod(list<Case> newList,map<ID,Case> newMap){
            CaseService.calculateCaseWeight(newList);
            CaseService.setCaseBusinessAge(newList);
            CaseService.setEmergencyRegion(newList);
            CaseService.communityUpdates(newList);
            GenericServices.updateDependentField(newList);
            CaseService.updateCaseOwnerToAccountOwner(newList);
            CaseService.businessSupportCaseMemberReview(newList);
            CaseService.assignUserRegionToCase(newList);
    }

    // --All after insert operations will be called from here --//
    public void afterInsertMethod(list<Case> newList,map<ID,Case> newMap){
            
            //-- This is called here for DML Ops functionality
            CaseService.communityCaseHandler(newList);
    }

    // --All before update operations will be called from here --//
    public void beforeUpdateMethod(list<case> newList, list<case> oldList, map<ID,Case> newMap, map<ID,Case> oldMap){
            CaseService.setCaseBusinessAge(newList);
            CaseService.updateRelevantAgreements(newMap,oldMap);
            CaseService.completeCaseMilestone(newMap,oldMap);
            CaseService.updateCaseOwnerToAccountOwner(newList);
            CaseService.businessSupportCaseMemberReview(newList);

    }

    // --All after update operations will be called from here --//
    public void afterUpdateMethod(list<case> newList,list<case> oldList,map<ID,Case> newMap,map<ID,Case> oldMap){
            CaseService.isRunning = true;
            CaseService.weekendRecognitionTimeStamping(newMap,oldMap);
            CaseService.communityCaseHandler(newList);
    }
}