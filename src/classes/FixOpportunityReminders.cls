global class FixOpportunityReminders implements Schedulable {

    global void execute(SchedulableContext SC) {     
        
        Date dt = Date.today().addDays(-120);
        
        //get the list of offensive opportinities.
        
        Opportunity[] opps = [select id, ownerid from Opportunity where 
                              (Stage_change_date__c <= :dt OR CloseDate <: Date.today())
                              AND stagename != 'Identification' and 
                              stagename != 'Closed Won' and 
                              stagename != 'Closed Lost' and owner.isactive = true];
        
        // get a set of owners
        
        Set<String> UserList = new Set<String>();
        
        if (!opps.isEmpty()){
            for (Opportunity o: opps){           
                UserList.add(o.ownerid);    
            }
        }
        
        if (!UserList.isEmpty()){
        
            List<String> UserListEmail = new List<String>();
            UserListEmail.addAll(UserList);
            
            //Get Email Template
    
            EmailTemplate e = [select id from emailTemplate where developername = 'Fix_Opportunities' limit 1];
            Messaging.MassEmailMessage emailMessage = new Messaging.MassEmailMessage();
            emailMessage.setTargetObjectIds(UserListEmail);
            emailMessage.setTemplateID(e.id);
            emailMessage.setSenderDisplayName('Your Friendly Salesforce Admin');
            emailMessage.setSaveAsActivity(false);
            Messaging.sendEmail(new Messaging.MassEmailMessage[] { emailMessage });

        }
    }

}