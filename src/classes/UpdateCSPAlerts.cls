@RestResource(urlMapping='/UpdateCSPAlerts/*')
global without sharing class UpdateCSPAlerts {
    
    
    @HttpPost
    global static String doPost(String uname, String pwd, Datetime expirationDate, String alertMessage) {
    
        List<CustomWebService__c> cws = new List<CustomWebService__c>([select id, username__c, password__c from CustomWebService__c where name='UpdateCSPAlerts' and username__c =:uname and password__c =:pwd ]);

        if(cws.size()>0){
        
        List<CaseSupportAlert__c> csa = new List<CaseSupportAlert__c>([select id, name, Alert_Message__c, Expired_On__c from CaseSupportAlert__c order by name desc limit 1]);
        
        if(csa.size()>0){

        CaseSupportAlert__c csaupdate = new CaseSupportAlert__c();
        csaupdate.id = csa[0].id;
        csaupdate.Alert_Message__c = alertMessage;
        csaupdate.Expired_On__c = expirationDate; 
        
        database.update(csaupdate);
        system.debug('**** CSA update completed');
        String suc = 'Success';
        return suc;
        }else{
        String error = 'Failure';
        return error;
        }
        
        } else {
        
        String error = 'Incorrect username, password';
        return error;     
    } 
}
}