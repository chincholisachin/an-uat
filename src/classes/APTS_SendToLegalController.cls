//Controller to create a Legal request record when clicked on Send to Legal on Agreement.
public with sharing class APTS_SendToLegalController {

    public ID LegalQueueId ;
    public List<Id> LegalQueueMembers ;

    private Apttus__APTS_Agreement__c AgrRecord;
    public String message{get;set;}
    public List<String> userEmails = new List<String>();
   
    public Boolean sendDisable{get;set;}
    public String LegalComment {get;set;}
    public String RequestorName {get;set;}
    public String DefaultContactName {get;set;}
    public Id DefaultContactId;
        
    List<Id> AgrId = new List<Id>();
    public APTS_SendToLegalController(ApexPages.StandardController stdController){
        List<Apttus__APTS_Agreement__c > AgrRecordList = [select Id, Name,Apttus__Related_Opportunity__c,Expedite__c,APTS_Expedite_Details__c,Comments_for_from_Legal__c from Apttus__APTS_Agreement__c where Id = :stdController.getRecord().Id];
         if(AgrRecordList.size()>0)
        {
            AgrRecord = AgrRecordList.get(0);
            LegalComment = AgrRecordList.get(0).Comments_for_from_Legal__c;
            AgrId.add(AgrRecordList.get(0).Id);
        }
          
        
        
    }
    
    public void initSend(){
        sendDisable = false;
    }
    //Method to create Legal request record and Task record  
    public PageReference send(){
            
        AgrRecord.Comments_for_from_Legal__c = LegalComment;
        //AgrRecord.Apttus__Status_Category__c = 'In Authoring';
        //AgrRecord.Apttus__Status__c = 'In Legal Review';
        
        Legal_Request__c legalrecord= new Legal_Request__c();
        legalrecord.Related_Agreement__c= AgrRecord.Id;
        legalrecord.Opportunity__c = AgrRecord.Apttus__Related_Opportunity__c;
        //legalrecord.Requested_Attorney__c=AgrRecord.Requested_Attorney__c;
        legalrecord.Additional_Details_for_Legal_Review__c=AgrRecord.Comments_for_from_Legal__c;
        legalrecord.APTS_Expedite_Details__c=AgrRecord.APTS_Expedite_Details__c;
        
        Insert legalrecord;
        
       
        
        
        
      // Insert Activity History
        Task task = new Task();
        task.ActivityDate = Date.today();
        task.Description = 'Sent to Legal';
        task.OwnerId = UserInfo.getUserId();
        task.Priority = 'Normal';
        task.Status = 'Completed';
        task.Subject = 'Sent to Legal';
        task.WhatId = AgrId.get(0);
        //    task.RecordTypeId = taskELARecordTypeID;
        insert task; 
      

/*      mail.saveAsActivity = false;
        
        Messaging.SendEmailResult[] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        System.debug('myMsg --'+result );
 */       
       //   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'A record for Legal request has been successfully created.');
        
        
    //    ApexPages.addMessage(myMsg);

   //     sendDisable = true;
        
        PageReference detailView = new ApexPages.StandardController(AgrRecord).view();
        
        return detailView;
     
    }
 

    public PageReference goBack(){
        
        PageReference detailView = new ApexPages.StandardController(AgrRecord).view();
        
        return detailView;
        
    }
    
}