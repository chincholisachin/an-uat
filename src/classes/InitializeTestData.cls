@isTest
public class InitializeTestData {
    
    public static User createUser(){
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'Grandhi',
            Email = 'Grandhi@gmail.com',
            Username = 'Grandhi@gmail.com' + System.currentTimeMillis(),
            CompanyName = 'ET Marlabs',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            
        );
        return u;
    }
    
    public static Account createAccount(){
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-Console Member').getRecordTypeId();
        Account acc= new Account(
            Name='acc1',
            RecordTypeId= accRecordTypeId,
            BillingCity ='Chennai' ,
            BillingCountry='india',
            BillingPostalCode='600075',
            BillingState='tamil nadu',
            BillingStreet='water well street', 
            ShippingCity=null,
            ShippingCountry=null,
            ShippingPostalCode=null,
            ShippingState=null,
            ShippingStreet=null,
            AppNexus_Member_ID__c ='123'

        ); 
        return acc;
    }
    
    public static Account_Record_Type__c recordTypeSettings () {
        Account_Record_Type__c art = new Account_Record_Type__c();
        art.isMember__c = true;
        art.Name = 'Non-Console Member';
        return art;
    }
    
    Public static Contact CreateContact(String AccId){
        Contact con = new Contact(
            FirstName='Test',
            LastName='Aditya',
            Accountid= AccId
        );
        return con;
    }
     Public static Contact CreateContactwithEmail(String AccId){
        Contact con = new Contact(
            FirstName='Test',
            LastName='Aditya',
            Accountid= AccId,
            Email = 'aditya@gmail.com'
        );
        return con;
    }
    Public static case CreateCase(string conID,String accId){
        Case c= new Case(           
            ContactId = conID,
            AccountId = accId,
            Status = 'New',
            Origin = 'Web',
            Assigned_to_CS__c = false,
            Cloned_To_CS__c = false,
            RecordTypeID='012f4000000slHgAAI' //Hardcoded
        );
        return c; 
    }
    
    public static Attachment createAttachment(String parentId) {
        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=parentId;
        return attach;
    }
    public static CaseComment CreateCaseComment(String CaseParentId){
        CaseComment cc = new CaseComment(
            CommentBody = 'Case Comment Body Test Text',
            ParentId = CaseParentId
        );
        return cc;  
    }
    public static EmailMessage CreateEmailMessage(String CaseId){   
        EmailMessage emailMsg =new EmailMessage(
            FromAddress = 'test@abc.org', 
            Incoming = True, 
            ToAddress= 'testtoaddress@gmail.com',
            Subject = 'Subject Text Test email', TextBody = 'Email Test Text Body', 
            ParentId = CaseId);
        return emailMsg;
        
    }
    public static AccountContactRelation CreateAccountContactRelation(String accId,String conId){
        AccountContactRelation accRel= new AccountContactRelation(
        AccountId = accId,
        ContactId = conId
                                );
        return accRel;
    }
    
    public static Account_Record_Type__c  CreateAccountCustomSetting(String recName,boolean isInternal, boolean isCustomer, boolean isMember){
        Account_Record_Type__c accCustSetting= new Account_Record_Type__c(
            Name= recName,
            isInternal__c= isInternal,
            isCustomer__c= isInternal,
            isMember__c= isMember
        );
       // insert accCustSetting;
        return accCustSetting;
    }
    
    public static emailSettings__c  creatEmailSettings() {
        emailSettings__c es = new emailSettings__c();
        es.Name='Tets Email Seetings';
        es.Time_Interval__c = 4;
        es.Case_Priority__c = 'Low';
        es.Org_Wide_Email__c ='customer-support@appnexus.com';
        return es;
    }

    public static Case_Weight_Settings__c  createCaseWeightSettings() {
        Case_Weight_Settings__c cw = new Case_Weight_Settings__c();
        cw.Name='1-Test';
        cw.Type__c = 'Priority';
        cw.Value__c = 'P3: This is Minor';
        cw.Weight__c = 1.000;
        return cw;
    }

    private static integer noOfRecords = 10;

    public static list<Outage_Notifications__c> createOutageNotifications(String AccId){
        Outage_Notifications__c outNo;
        list<Outage_Notifications__c> outNoList = new list<Outage_Notifications__c>();
        for(Integer i=0;i<noOfRecords;i++){
            outNo = new Outage_Notifications__c();
            outNo.Account__c = AccId;
            outNo.IsActive__c = true;
            outNo.Email_Addresses__c = 'spchincholi'+i+'@gmail.com';
            outNoList.add(outNo);
        }
        return outNoList;
    }

    public static void createSystemConfigSettings(){
        list<System_Configs__c> scList;
        System_Configs__c sc1 =  new System_Configs__c();
        System_Configs__c sc2 =  new System_Configs__c();
        System_Configs__c sc3 =  new System_Configs__c();
        System_Configs__c sc4 =  new System_Configs__c();
        sc1.Name='Case';
        sc1.Is_Run__c = true;
        sc2.Name='CaseComment';
        sc2.Is_Run__c = true;
        sc3.Name='CaseStatusTracker';
        sc3.Is_Run__c = true;
        sc4.Name='EmailMessage';
        sc4.Is_Run__c = true;
        scList = new list<System_Configs__c>{sc1,sc2,sc3,sc4};
        insert scList;
    }
    
    public static List<HardCodeIds__c> createHardCodeCustomSettings(){
         List<HardCodeIds__c>  hdList = new List<HardCodeIds__c>();
         HardCodeIds__c hd = new HardCodeIds__c();
         hd.Name = 'BusHrsID';
         hd.value__c='01mf40000002CDZ';

         HardCodeIds__c hd1 = new HardCodeIds__c();
         hd1.Name = 'CommEntitlement';
         
         hd1.value__c='Americas|5505B000000Ha8C';

         return new list<HardCodeIds__c>{hd,hd1};
    }

    public static Entitlement createEntitlement(String accID){
        Entitlement ent = new Entitlement();
        ent.name = 'CSP AMS Entitlement';
        ent.AccountID = accID;

        return ent;
    }

    public static void createEmAsSettings(){
        Emergency_Assignment__c ea1 = new Emergency_Assignment__c();
        ea1.End_Time__c = 16.00;
        ea1.Holidays__c = '01/30/2017,04/14/2017';
        ea1.Priority__c = 2;
        ea1.Start_Time__c =9.00; 
        ea1.Time_Zone__c = 'Asia/Singapore';
        ea1.Weekend__c = false;
        ea1.Name='APAC';

        Emergency_Assignment__c ea2 = new Emergency_Assignment__c();
        ea2.End_Time__c = 14.00;
        ea2.Holidays__c = '01/30/2017,04/14/2017';
        ea2.Priority__c = 1;
        ea2.Start_Time__c =9.00; 
        ea2.Time_Zone__c = 'Europe/London';
        ea2.Weekend__c = false;
        ea2.Name='EMEA';

        insert new list<Emergency_Assignment__c>{ea1,ea2};

    }

    public static Apttus__APTS_Agreement__c createParentAgreement(String accID){
        Apttus__APTS_Agreement__c eachAgreement = new Apttus__APTS_Agreement__c();
        eachAgreement.Name='Test Agreement Parent';
        eachAgreement.Apttus__Account__c = accID;
        eachAgreement.Apttus__Status__c = 'Request';
        eachAgreement.Apttus__Status_Category__c = 'Request';
        return eachAgreement;
    }

    public static Apttus__APTS_Agreement__c createChildAgreement(String accID,String agrID){
        Apttus__APTS_Agreement__c eachAgreement = new Apttus__APTS_Agreement__c();
        eachAgreement.Name='Test Agreement Child';
        eachAgreement.Apttus__Account__c = accID;
        eachAgreement.Apttus__Status_Category__c = 'Request';
        eachAgreement.Apttus__Parent_Agreement__c = agrID;
        return eachAgreement;
    }

    public static AccountTeamMember createAccountTeamMember(String accID,String userID){
        AccountTeamMember am = new AccountTeamMember();
        am.userID = userID;
        am.TeamMemberRole = 'Senior Account Owner';
        am.accountID = accID;
        return am;
    }
    public static boolean isBypassCreated = false;
    public static Bypass__c bp = new Bypass__c();
    
    public static Bypass__c createBypassSetting(){
          bp.Trigger_Bypass__c = false;
          return bp;
    }

    public static CountryISOCodes__c createisoCountrySetting(){
        CountryISOCodes__c iso = new CountryISOCodes__c();
        iso.Name = 'UNITED STATES OF AMERICA';
        iso.iso2__c = 'US';
        iso.iso3__C = 'USA';
        iso.Submission_Region__c = 'AMS';
        return iso;
    }
}