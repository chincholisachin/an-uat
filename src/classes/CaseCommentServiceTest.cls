@isTest
private class CaseCommentServiceTest
{
    static Account acc;
    static list<Outage_Notifications__c> outNoList;
    static Case_Weight_Settings__c cwSetting;
    static Case eachCase;
    static Contact con;
    static list<HardCodeIds__c> hdList;
    static Apttus__APTS_Agreement__c eachAgr;
    static AccountTeamMember am;
    public static string AgreID;

    private static void setup()
    {
        acc = InitializeTestData.createAccount();
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        insert acc;
        
        Account_Record_Type__c Ar = InitializeTestData.CreateAccountCustomSetting('Customer Account',false,false,false);
        insert ar;

        outNoList = InitializeTestData.createOutageNotifications(acc.ID);
        insert outNoList;

        cwSetting = InitializeTestData.createCaseWeightSettings();

        insert cwSetting;

        con = InitializeTestData.CreateContact(acc.ID);
        insert con;

        InitializeTestData.createSystemConfigSettings();
        hdList = InitializeTestData.createHardCodeCustomSettings();
        insert hdList;
        Entitlement ent = InitializeTestData.createEntitlement(acc.ID);
        insert ent;

        InitializeTestData.createEmAsSettings();
        InitializeTestData.createBypassSetting();



    }

    private static testmethod void updateCaseMilestoneTest(){
        setup();
        eachCase = new Case();
        eachCase = InitializeTestData.CreateCase(con.ID,acc.Id);
        insert eachCase;
        insert new CaseComment(CommentBody='Test',ParentID=eachCase.ID,isPublished=true);
    }
}