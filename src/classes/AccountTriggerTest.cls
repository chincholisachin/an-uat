@isTest
public class AccountTriggerTest{

    // tests to make sure when customer accounts are created account owners are created as team members
     
    public static List<Account> createAccounts(String recordTypeName, Integer accountCount){
        
        TestingUtils.createEssentialCustomSettings();
        RecordType rt = [select id, name from recordtype where sobjecttype = 'Account' and name = :recordTypeName limit 1];  
        Account[] accounts = TestingUtils.createAccounts(accountCount, 'testAccount', false);
        for (Account acn : accounts){
            acn.recordtypeid = rt.id;
            acn.vertical_new__c = 'ATBs and ATDs';
            acn.Region_rdp__c = 'US/Canada';
            acn.Global_rdp__c = 'US/Canada';
        }
        return accounts;
    
    }

    public static testMethod void createCustomerAccounts(){
    
        User u1 = TestingUtils.getUser('* Commercial');
        User u2 = TestingUtils.getUser('* Sales Operations');
        Team_Owner__c to = new Team_Owner__c(Market__c = 'US/Canada', Vertical__c = 'ATBs and ATDs', Team_Owner__c = u2.id);
        insert to;
        
        Test.startTest();
        System.runAs(u1){
        
            Account[] accounts = createAccounts('Customer Account', 200);
            insert accounts;
        
        }
        Test.stopTest();
        System.assertEquals(200,[select id from account].size());
        System.assertEquals(200,[select id from accountteammember where teammemberrole =:AccountUtils.ACCOUNT_OWNER].size());
    
    }
    
    public static testMethod void updateCustomerAccounts(){
    
        Account[] accounts = createAccounts('Customer Account', 200);
        insert accounts;
        
        User u = TestingUtils.getUser('* Commercial');
        
        Test.startTest();
        for (Account a: accounts){
            a.ownerid = u.id;
            a.Account_Tier__c = '3';
        }
        update accounts;
        Test.stopTest();
        
        System.assertEquals(200,[select id from accountteammember where teammemberrole =:AccountUtils.ACCOUNT_OWNER].size());
        System.assertEquals(200,[select id from accountteammember where teammemberrole =:AccountUtils.ACCOUNT_OWNER and userid = :u.id].size());
        System.assertEquals(0,[select id from integration_request__c].size()); 
        
    }   

    public static testMethod void createMemberAccounts(){
    
        //TestingUtils.createCustomSetting('accountMemberSetup');
        User u1 = TestingUtils.getUser('* Sales Operations');
        
        System.runAs(u1){
        
            Account[] accounts = createAccounts('Customer Account',200);
            insert accounts;
            
        }
        
        System.assertEquals(200,[select id from accountteammember where userid =:u1.id and teammemberrole =:AccountUtils.ACCOUNT_OWNER].size());
        System.assertEquals(200,[select id, recordtypeid from account where ownerid =:u1.id and recordtype.name = 'Customer Account'].size());
        
        List<Account> acn = new List<Account>([select id, recordtype.name from account where recordtype.name = 'Customer Account']);

        Test.startTest();
        Account[] members = createAccounts('Console Member',200);
        for (Integer i = 0; i<200; i++){
            members[i].AppNexus_Member_Id__c = String.valueOf(i+10000);
            members[i].parentid = acn[i].id;   
        }
       
        insert members;
        Test.stopTest();
          
        System.assertEquals(400,[select id from accountteammember where teammemberrole =:AccountUtils.ACCOUNT_OWNER and userid = :u1.id].size());
        Account acnn = [select id from account where recordtype.name = 'Customer Account' limit 1];
        System.assertEquals([select id,userid from accountteammember where accountid= :acnn.id and teammemberrole =:AccountUtils.ACCOUNT_OWNER limit 1].userid,[select id,userid from accountteammember where account.parentid= :acnn.id and teammemberrole =:AccountUtils.ACCOUNT_OWNER limit 1].userid );  

    }
    
    public static testMethod void createMemberAccountsAndPopulateParentFieldsSetupBillingInfo(){
    
        TestingUtils.createCustomSetting('countryStateCodes');
        User u1 = TestingUtils.getUser('* Sales Operations');
        
        System.assert(Label.AccountFieldsToCopy.split('\r\n').size() > 1);
        
        System.runAs(u1){
        
            Account[] accounts = createAccounts('Customer Account',200);
            for(Account acn : accounts){
                acn.billingstreet = 'FirstLineqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnm\r\n'+'SecondLineqwertyuiopasdfghjklzxcvbnmdfgh\r\n' + 'ThirdLine';
                acn.billingcity = 'Test2';
                acn.billingstate = 'California';
                acn.billingCountry = 'USA';
                acn.phone = '5555555555';
            }          
            insert accounts;
            
        }
        
        
        List<Account> acn = new List<Account>([select id, recordtype.name from account]);

        Test.startTest();
        Account[] members = createAccounts('Console Member',200);
        
        for (Integer i = 0; i<200; i++){
            members[i].AppNexus_Member_Id__c = String.valueOf(i+10000);
            members[i].parentid = acn[i].id;   
        }
       
        insert members;
        Test.stopTest();
        
        String q = 'select id,';
        for (String s: Label.AccountFieldsToCopy.split('\r\n')){
            q = q + s + ',';
        }
        q = q + 'parentid, billingStreet, billingCity, billingState, billingCountry, ';
        q = q + 'seller_address_line__c, Infor_Seller_Address_1__c, Infor_Seller_Address_2__c, Infor_Seller_Address_3__c, ';
        q = q + 'Info_Address_1__c, Info_Address_2__c, Info_Address_3__c, Invoice_Member_Code__c, seller_address_code__c, Address_Code__c,  Country_Code__c, State_Code__c, ';
        q = q + 'Seller_Country_Code__c, Seller_State_Code__c from account where recordtype.name = \'Console Member\' limit 1';
          
        Account[] memberAcn = Database.query(q);
        
        String q2 = 'select id,';
        for (String s: Label.AccountFieldsToCopy.split('\r\n')){
            q2 = q2 + s + ',';
        }
        q2 = q2 + 'recordtypeid, billingStreet, billingCity, billingState, billingCountry from account where id =\'' + memberAcn[0].parentid + '\'';
        
        Account[] customerAcn = Database.query(q2);
        
        for (String s: Label.AccountFieldsToCopy.split('\r\n')){
            System.assertEquals(memberAcn[0].get(s), customerAcn[0].get(s));
        }
        System.assertEquals(memberAcn[0].billingStreet,customerAcn[0].billingStreet);
        System.assertEquals(memberAcn[0].billingCity,customerAcn[0].billingCity);
        System.assertEquals(memberAcn[0].billingState,customerAcn[0].billingState);
        System.assertEquals(memberAcn[0].State_Code__c,'CA');
        System.assertEquals(memberAcn[0].Country_Code__c,'US');
        System.assertEquals(memberAcn[0].Seller_State_Code__c,'CA');
        System.assertEquals(memberAcn[0].Seller_Country_Code__c,'US');
        System.assertEquals(memberAcn[0].Info_Address_1__c,'FirstLineqwertyuiopasdfghjklzxcvbnmqwertyuiopasdf-');
        System.assertEquals(memberAcn[0].Info_Address_2__c,'ghjklzxcvbnm SecondLineqwertyuiopasdfghjklzxcvbnm-');
        System.assertEquals(memberAcn[0].Info_Address_3__c,'dfgh ThirdLine');
        System.assertEquals(memberAcn[0].Infor_Seller_Address_1__c,'FirstLineqwertyuiopasdfghjklzxcvbnmqwertyuiopasdf-');
        System.assertEquals(memberAcn[0].Infor_Seller_Address_2__c,'ghjklzxcvbnm SecondLineqwertyuiopasdfghjklzxcvbnm-');
        System.assertEquals(memberAcn[0].Infor_Seller_Address_3__c,'dfgh ThirdLine');
        System.assertEquals(memberAcn[0].Address_Code__c, 'A' + memberAcn[0].Invoice_Member_Code__c);
        System.assertEquals(memberAcn[0].Seller_Address_Code__c, 'AS' + memberAcn[0].Invoice_Member_Code__c.substring(1));        
        

    }
    
    public static testMethod void testDisableCSP(){
        
        User u1 = TestingUtils.getUser('* Sales Operations');
        
        System.runAs(u1){
        
            Account[] accounts = createAccounts('Customer Account',1);
            insert accounts;
            
            Contact[] con = TestingUtils.createContacts(200, 'Test', false);
            for (Contact c: con){
                c.accountid = accounts[0].id;
            }
            insert con;
            
            for (Integer i = 0; i<con.size(); i++){
                if (Math.mod(i,2) == 0) con[i].Customer_Portal_User__c = true;
            }
            update con;
            
            System.assertEquals(100, [select id from contact where Customer_Portal_User__c = true and accountid =:accounts[0].id].size());
            
        }
        
        Set<Id> accountIdSet = new Set<Id>();
        Account acn = [select id from account where recordType.name = 'Customer Account' limit 1];
        accountIdSet.add(acn.id);
        
        Test.startTest();
        AccountTriggerHandler.updateCSPContacts(accountIdSet); //testingFutureMethod
        Test.stopTest();
        
        System.assertEquals(0, [select id from contact where Customer_Portal_User__c = true and accountid =:acn.id].size());
        System.assertEquals(200, [select id from contact where Customer_Portal_User__c = false and accountid =:acn.id].size());
        
    }
   
    public static testMethod void integrationRequest(){
    
        //TestingUtils.createCustomSetting('accountMemberSetup');
        User u1 = TestingUtils.getUser('* Sales Operations');
        User u2 = TestingUtils.getUser('* Commercial');
        
        System.runAs(u1){
        
            Account[] accounts = createAccounts('Customer Account',200);
            insert accounts;
            
        }
        
        List<Account> acn = new List<Account>([select id, recordtype.name from account where recordtype.name = 'Customer Account']);

        Account[] members = createAccounts('Console Member',200);
        for (Integer i = 0; i<200; i++){
            members[i].AppNexus_Member_Id__c = String.valueOf(i+10000);
            members[i].parentid = acn[i].id;   
        }
       
        insert members;
        
        Test.startTest();
        
        for(Account a: acn){
            
            a.ownerid = u2.id;
            a.Account_Tier__c = '3';
        
        }
        
        update acn;
        
        Test.stopTest();
          
        System.assertEquals(200,[select id from integration_request__c where integration_type__c = 'Audit Tier'].size());
        System.assertEquals(200,[select id from integration_request__c where integration_type__c = 'Account Manager'].size());

    }
    
    public static testMethod void createAssetRecords(){
    
        Id pricebookId = Test.getStandardPricebookId();
        
        Pricebook2 pb = new PriceBook2(name = 'Standard Price Book');
        insert pb;
        
        List<Product2> pr = new List<Product2>();
        pr.add(new Product2(isActive = true,
                                   Family = 'Buy Side',
                                   Name = 'Header Bidding'));

        insert pr;
        List<PriceBookEntry> pbeSt = new List<PriceBookEntry>();
        List<PriceBookEntry> pbe = new List<PriceBookEntry>();
        pbeSt.add(new PriceBookEntry(pricebook2id = pricebookId, product2id = pr[0].id, UnitPrice = 1, UseStandardPrice = false));
        pbe.add(new PriceBookEntry(pricebook2id = pb.id, product2id = pr[0].id, UnitPrice = 1, UseStandardPrice = false, isactive = true));
        insert pbeSt;
        insert pbe;                
        
        User u1 = TestingUtils.getUser('* Sales Operations');
        
        System.runAs(u1){
        
            Account[] accounts = createAccounts('Customer Account',200);
            insert accounts;
            
        }
               
        List<Account> acn = new List<Account>([select id, recordtype.name from account where recordtype.name = 'Customer Account']);
                
        
        Integer i = 0;
        Opportunity[] opps = TestingUtils.createOpportunities(200, 'Test Opportunity', 'Identification', false);
        for (Opportunity o: opps){
            o.accountid = acn[i].id;
            i++;
        }
        insert opps;
        
        OpportunityLineItem[] oli = new List< OpportunityLineItem >();
        for (Opportunity o: opps){
            System.assert(o.id != null);
            oli.add(new OpportunityLineItem(opportunityid = o.id, pricebookentryid = pbe[0].id, product2id = pr[0].id, quantity =12, unitprice = 10));
        }       
        insert oli;

        System.assertEquals(200,[select id from opportunitylineitem].size());
        
        Test.startTest();
        
        Account[] members = createAccounts('Console Member',200);
        for (Integer t = 0; t<200; t++){
            members[t].AppNexus_Member_Id__c = String.valueOf(t+10000);
            members[t].parentid = acn[t].id;
            members[t].opportunity__c = opps[t].id;   
        }
              
        insert members;
        
        Test.stopTest();

        System.assertEquals(200,[select id from asset].size());
      

    }

}