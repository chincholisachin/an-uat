public class UserServices {

    public static Map<id,User> getUserByContactId(Set<Id> conIds)
    {

        Map<id,User> usrContUpdtMap = new Map<id,User>([Select Id, FirstName, LastName, Title, Email, Phone, MobilePhone, Fax,
                                                           Street, City, State, Country, PostalCode 
                                                           From User where contactId in :conIds]);
                                                           
         return usrContUpdtMap ;
    }
}