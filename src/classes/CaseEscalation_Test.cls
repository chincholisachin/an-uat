@isTest(seeAllData=false)
private class CaseEscalation_Test {
   
    //Test cases which are Emergency and are Assigned to a Queue
    private static testMethod void testUnassignedQueue(){

        List<Case> testCases = new List<Case>();
         InitializeTestData.createSystemConfigSettings();
         emailSettings__c mc = InitializeTestData.creatEmailSettings();//emailSettings__c.getOrgDefaults();
        insert mc;
        //Create a new queue and map to Case object    
        Group grp = new Group(Name='TestQueue',Type='Queue');
        insert grp;
        QueueSobject mappingObject = new QueueSobject(QueueId = grp.Id, SobjectType = 'Case');
        System.runAs(new User(Id = UserInfo.getUserId()))
        {insert mappingObject;}
    
        for(Integer i = 0; i < 1; i++)
            testCases.add(new Case(Status='New',Priority='Emergency',Subject='Test1',emailFlag__c=true,OwnerId=grp.Id));
        
        Test.startTest();    
        insert testCases;
        System.debug(mc+'**&&*&^^*^*mc');
        System.debug(mc.Time_Interval__c+'**&&*&^^*^*');
        CaseEscalationNotify_Scheduler testNotify = new CaseEscalationNotify_Scheduler();
        String schedule = '0 20 0 * * ?';
        // system.schedule('Notification', schedule, testNotify);
      //  update mc;
        
        CaseEscalationNotify job = new CaseEscalationNotify();
        Database.BatchableContext BC;
        job.execute(BC,testCases);
        ID batchprocessid = Database.executeBatch(job);
    
        Test.stopTest();

        testCases = [SELECT Id, emailFlag__c FROM Case WHERE Priority = 'Emergency' and Subject='Test1'];
        system.assertEquals(1, testCases.size());
        //system.assertEquals(True, testCases[0].emailFlag__c);
        //system.assertEquals(True, testCases[1].emailFlag__c);
            
    }
    
     private static testMethod void testUnassignedQueueCase(){

        List<Case> testCases = new List<Case>();
         InitializeTestData.createSystemConfigSettings();
        //Create a new queue and map to Case object    
        Group grp = new Group(Name='TestQueue',Type='Queue');
        insert grp;
        QueueSobject mappingObject = new QueueSobject(QueueId = grp.Id, SobjectType = 'Case');
        System.runAs(new User(Id = UserInfo.getUserId()))
        {insert mappingObject;}
    
        for(Integer i = 0; i < 1; i++)
            testCases.add(new Case(Status='New',Priority='Emergency',Subject='Test1',emailFlag__c=false,OwnerId=grp.Id));
        
        Test.startTest();    
        insert testCases;
        emailSettings__c mc = InitializeTestData.creatEmailSettings();//emailSettings__c.getOrgDefaults();
        insert mc;
        System.debug(mc+'**&&*&^^*^*mc');
        System.debug(mc.Time_Interval__c+'**&&*&^^*^*');
         CaseEscalationNotify_Scheduler testNotify = new CaseEscalationNotify_Scheduler();
        String schedule = '0 20 0 * * ?';
        // system.schedule('Notification', schedule, testNotify);
      //  update mc;
        
        CaseEscalationNotify job = new CaseEscalationNotify();
        Database.BatchableContext BC;
        job.execute(BC,testCases);
        ID batchprocessid = Database.executeBatch(job);
    
        Test.stopTest();

        testCases = [SELECT Id, emailFlag__c FROM Case WHERE Priority = 'Emergency' and Subject='Test1'];
        system.assertEquals(1, testCases.size());
        //system.assertEquals(True, testCases[0].emailFlag__c);
        //system.assertEquals(True, testCases[1].emailFlag__c);
            
    }
   /*  private static testMethod void testUnassignedQueueCase1(){

        List<Case> testCases = new List<Case>();
         InitializeTestData.createSystemConfigSettings();
        //Create a new queue and map to Case object    
        Group grp = new Group(Name='TestQueue',Type='Queue');
        insert grp;
        QueueSobject mappingObject = new QueueSobject(QueueId = grp.Id, SobjectType = 'Case');
        System.runAs(new User(Id = UserInfo.getUserId()))
        {insert mappingObject;}
    
        for(Integer i = 0; i < 1; i++)
            testCases.add(new Case(Status='New',Priority='Emergency',Subject='Test1',emailFlag__c=false,OwnerId=grp.Id));
        
        Test.startTest();    
        insert testCases;
        emailSettings__c mc = InitializeTestData.creatEmailSettings();//emailSettings__c.getOrgDefaults();
         mc.Job_Id__c = '0001X46574773GB';
        insert mc;
        System.debug(mc+'**&&*&^^*^*mc');
        System.debug(mc.Time_Interval__c+'**&&*&^^*^*');
        CaseEscalationNotify_Scheduler testNotify = new CaseEscalationNotify_Scheduler();
        String schedule = '0 20 0 * * ?';
        // system.schedule('Notification', schedule, testNotify);
      //  update mc;
        
        CaseEscalationNotify job = new CaseEscalationNotify();
        Database.BatchableContext BC;
        job.execute(BC,testCases);
        ID batchprocessid = Database.executeBatch(job);
    
        Test.stopTest();

        testCases = [SELECT Id, emailFlag__c FROM Case WHERE Priority = 'Emergency' and Subject='Test1'];
        system.assertEquals(1, testCases.size());
        //system.assertEquals(True, testCases[0].emailFlag__c);
        //system.assertEquals(True, testCases[1].emailFlag__c);
            
    }*/
}