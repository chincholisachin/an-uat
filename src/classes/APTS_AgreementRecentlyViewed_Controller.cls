/**
* APTTUS
* @author           Luis Arismendy=<larismendy@apttus.com> (LA)
* Project:          APPNEXUS
* Description:      Controller class for APTS_AgreementRecentlyViewed VF
*
* Changes (Version)
* -------------------------------------
*           No.     Date            Author                  Description
*           -----   ----------      --------------------    ---------------
* @version  1.0     05-24-2016      Luis Arismendy(LA)      Class created
*************************************************************************************************************/
public class APTS_AgreementRecentlyViewed_Controller
{       
    public List<Apttus__APTS_Agreement__c> agreementList {get; set;}
    public ApexPages.StandardSetController ssc  {get; set;}
    public String AccFilterId {get; set;}
    
    public APTS_AgreementRecentlyViewed_Controller()
    {
        List<RecentlyViewed> listRv;
        if(!Test.isRunningTest()){
          listRv = [SELECT Id FROM RecentlyViewed WHERE Type = 'Apttus__APTS_Agreement__c' AND LastViewedDate != NULL ORDER BY LastViewedDate DESC LIMIT 10];
        }
        
        Set<Id> setRv = new Set<Id>();
        if(!Test.isRunningTest()){
            for(RecentlyViewed rv :listRv)
            {
                setRv.add(rv.Id);
            }
        } else {
            for (Apttus__APTS_Agreement__c agr : [select id from Apttus__APTS_Agreement__c limit 10]){
                setRv.add(agr.id);
            }
        }
        agreementList = [SELECT Id, Name, Apttus__FF_Agreement_Number__c, RecordType.Name, Apttus__Account__c, Apttus__Account__r.Name, Apttus__Status_Category__c, Apttus__Status__c, Apttus__Contract_End_Date__c, CreatedDate FROM Apttus__APTS_Agreement__c WHERE Id in:setRv ORDER BY LastViewedDate DESC];
    }
    
    //Get all available list view for Quote
    public SelectOption[] getAgreementExistingViews()
    {
        ssc = new ApexPages.StandardSetController(agreementList);
        return ssc.getListViewOptions();
    }
    
    //Navigate to Selected ListView
    public PageReference redirectToList() 
    { 
        Schema.DescribeSObjectResult result = Apttus__APTS_Agreement__c.SObjectType.getDescribe();
        PageReference pageRef = new PageReference('/' + result.getKeyPrefix() + '?fcf=' + String.valueOf(AccFilterId).substring(0, 15));
        pageRef.setRedirect(true);
        return pageRef; 
    }
}