public  class AgreementService {
    
    public static map<ID,Apttus__APTS_Agreement__c> getAgreementsByFilteredIDs(set<ID> agreementIDSet){
        
        //--Parent_Status_Category__c this field is missing from the migration.
        //-- Adding this manually for now.
        map<ID,Apttus__APTS_Agreement__c> agreementMap = new map<ID,Apttus__APTS_Agreement__c>();
        try{
            for(Apttus__APTS_Agreement__c eachAgreement:[select id, Apttus__Status__c, Apttus__Parent_Agreement__c,Apttus__Parent_Agreement__r.Apttus__Status_Category__c from Apttus__APTS_Agreement__c where id IN: agreementIDSet]){
                agreementMap.put(eachAgreement.ID,eachAgreement);
            }
            return agreementMap;
        }
        catch(Exception e){
            system.debug('--Exception e--'+e.getMessage());
        }
        return null;
    }
    public static void updateAgreements(list<Apttus__APTS_Agreement__c> agreementsToUpdateList){
        try{
            //-- All or None Update
            update agreementsToUpdateList;
        }catch(Exception e){
            system.debug('--Exception--'+e.getMessage());
        }
    }
    public static List<Apttus__APTS_Agreement__c> getAgreementsByAccount(String aptusAccountId ,String status )  {
        
        List<Apttus__APTS_Agreement__c>  agreementsList = [select id, recordtype.name from Apttus__APTS_Agreement__c where Apttus__Account__c =: aptusAccountId  and Apttus__Status_Category__c=:status ];
        return agreementsList ;
    }  
    // Modified this Service class from here for Agreement Trigger
    public static void populateDefaultOutputFormat(list<Apttus__APTS_Agreement__c> agreementstoPopulateFormats) {
        for (Apttus__APTS_Agreement__c ag : agreementstoPopulateFormats){
            if (ag.APTS_Output_Format__c != null && ag.Apttus__AllowableOutputFormats__c != ag.APTS_Output_Format__c) { 
                ag.Apttus__AllowableOutputFormats__c = ag.APTS_Output_Format__c;
            }
        }
    }
    public static void fillAgreementFields(List<Apttus__APTS_Agreement__c> updatedAgreements){
        try{
            Set<Id> accountIds = new Set<Id>();
            Map<Id, Account> aMap = new Map<Id,Account>();
            Date Lastday;
            Map<String,Id> agreementRecordTypesMap = new Map<String,Id>();
            agreementRecordTypesMap = utility.getRecordTypeInfoByObjectName('Apttus__APTS_Agreement__c');
            
            Map<Id,String> agreementRecordTypesMapByName = new Map<Id,String>();
            agreementRecordTypesMapByName = utility.getRecordTypeInfoByObject('Apttus__APTS_Agreement__c');
            
            for(Apttus__APTS_Agreement__c agr : updatedAgreements){
                System.debug(agr.RecordTypeId+'Record Type Id::::::');
                accountIds.add(agr.Apttus__Account__c);
                
                String recTypName = agreementRecordTypesMapByName.get(agr.recordtypeid);
                agr.Contract_Name__c = Agreement_RT_Mapping__c.getValues(recTypName) == null ? recTypName : 
                Agreement_RT_Mapping__c.getValues(recTypName).Contract_Name__c;
                
                if(agr.RecordTypeId == agreementRecordTypesMap.get(CONSTANTS.Bidder_Service_Order_Record_Type)){
                    agr.Apttus__Perpetual__c = true;
                }
                else if(agr.RecordTypeId == agreementRecordTypesMap.get(CONSTANTS.Console_Service_Order_Record_Type)){
                    agr.Apttus__Termination_Notice_Days__c = CONSTANTS.Apttus_Termination_Notice_Days_60;
                }
                
                else if(agr.RecordTypeId == agreementRecordTypesMap.get(CONSTANTS.Notice_of_Non_Renewal_Record_Type) 
                        || agr.RecordTypeId == agreementRecordTypesMap.get(CONSTANTS.Notice_of_Breach_Termination_Record_Type)) {
                            System.debug('::::::::::'+ agreementRecordTypesMap.get(CONSTANTS.Notice_of_Non_Renewal_Record_Type));
                            agr.Apttus__Contract_Start_Date__c = Date.today();
                            System.debug(Date.today()+':::::::::$$$'+agr.Apttus__Contract_Start_Date__c);
                            if(agr.RecordTypeId == agreementRecordTypesMap.get(CONSTANTS.Notice_of_Non_Renewal_Record_Type))
                            {
                                agr.APTS_Parent_Termination_Date__c = Calculatelstday(agr.Apttus__Contract_Start_Date__c).addDays(CONSTANTS.Apttus_Termination_Notice_Days_60);
                            }
                            else
                            {
                                agr.APTS_Parent_Termination_Date__c = Calculatelstday(agr.Apttus__Contract_Start_Date__c).addDays(CONSTANTS.Apttus_Termination_Notice_Days_30);
                            }
                            
                        }
                System.debug(agr+'agr Info ');
            } 
        }catch(Exception e) {
            System.debug('Error Occured::::'+e.getMessage());
            
        }
        
        /*  for(Account a: [Select Id, Name, Senior_Account_Owner__c, Market_Owner__c from Account where Id IN: accountIds]){
aMap.put(a.Id, a);
}

for(Apttus__APTS_Agreement__c agr : updatedAgreements){     
string recordtypename = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosById().get(agr.recordtypeid).getname();
String agrRecordType = agr.Name;
if(amap.containsKey(agr.Apttus__Account__c)){
Account acc = aMap.get(agr.Apttus__Account__c);
agr.Senior_Account_Owner__c = acc.Senior_Account_Owner__c;
agr.Commercial_Leader_Account__c = acc.Market_Owner__c;

}
}*/
        
        
    }
    public static void fillAgreementFields1(List<Apttus__APTS_Agreement__c> updatedAgreements){
        
        Map<String,Id> agreementRecordTypesMap = new Map<String,Id>();
        agreementRecordTypesMap = utility.getRecordTypeInfoByObjectName('Apttus__APTS_Agreement__c');
        
        for(Apttus__APTS_Agreement__c agr : updatedAgreements){
            if(agr.Sell_Side_Only__c == true && agr.RecordTypeId == agreementRecordTypesMap.get(CONSTANTS.MSA_Record_Type) ){
                agr.Authorized_Third_Party_Exchanges__c = '';
            }
            else if(agr.RecordTypeId == agreementRecordTypesMap.get(CONSTANTS.MSA_Record_Type) && agr.Sell_Side_Only__c != true){
                agr.Authorized_Third_Party_Exchanges__c = 'Google DoubleClick Ad Exchange';
            }
            if (agr.SYSTEM_SrAcnOwner__c != null) {
                agr.Senior_Account_Owner__c = agr.SYSTEM_SrAcnOwner__c;
            }
            if (agr.SYSTEM_CommercialLeader__c != null) {
                agr.Commercial_Leader_Account__c = agr.SYSTEM_CommercialLeader__c;
            }
        }
    }
    public static void fillAgreementfieldsafterinsert(Map<Id,Apttus__APTS_Agreement__c> newagreements) {
        Map<String,Id> agreementRecordTypesMap = new Map<String,Id>();
        Map<Id,Apttus__APTS_Agreement__c > agrMap = new Map<Id,Apttus__APTS_Agreement__c >();
        List<Apttus__APTS_Agreement__c> agrmntsList = new List<Apttus__APTS_Agreement__c>();
        
        agreementRecordTypesMap = utility.getRecordTypeInfoByObjectName('Apttus__APTS_Agreement__c');
        for(Apttus__APTS_Agreement__c agr : [Select Id,Internal_Entity__c,Apttus__Requestor__c,
                                             RecordTypeId,Apttus__Parent_Agreement__c,
                                             Apttus__Parent_Agreement__r.Internal_Entity__c,
                                             Apttus__Parent_Agreement__r.Apttus__Requestor__c,
                                             Apttus__Parent_Agreement__r.RecordTypeId
                                             from Apttus__APTS_Agreement__c WHERE Id IN: newagreements.values()]) {
                                                 
                                                 agrMap.put(agr.Id,agr);
                                             }
        
        for(Apttus__APTS_Agreement__c agreement : newagreements.values()) {
            Apttus__APTS_Agreement__c agrmnt = new Apttus__APTS_Agreement__c();
            if(agreement.Apttus__Parent_Agreement__c != null && 
               agreement.recordTypeId != agreementRecordTypesMap.get(CONSTANTS.MSA_Record_Type)                 &&
               agreement.recordTypeId != agreementRecordTypesMap.get(CONSTANTS.MHA_Record_Type)                 &&
               agreement.recordTypeId != agreementRecordTypesMap.get(CONSTANTS.MSA_YieldEx_Record_Type)         &&
               agreement.recordTypeId != agreementRecordTypesMap.get(CONSTANTS.OAS_MSA_RecordType)              &&
               agreement.recordTypeId != agreementRecordTypesMap.get(CONSTANTS.YieldEx_Direct_Service_Order)    &&
               agreement.recordTypeId != agreementRecordTypesMap.get(CONSTANTS.YieldEx_Analytics_Service_Order) 
              ) {
                  agrmnt.Internal_Entity__c = agrMap.get(agreement.Id).Apttus__Parent_Agreement__r.Internal_Entity__c;
                  if(agreement.RecordTypeId == agreementRecordTypesMap.get(CONSTANTS.Notice_of_Non_Renewal_Record_Type)) {
                      agrmnt.Sales_Contact__c = agrMap.get(agreement.Id).Apttus__Parent_Agreement__r.Apttus__Requestor__c;
                  }
                  agrmnt.Id = agreement.Id;
                  agrmntsList.add(agrmnt);
              }
            if( agreement.Apttus__Parent_Agreement__r.RecordTypeId != agreementRecordTypesMap.get(CONSTANTS.MSA_Record_Type) &&
               agreement.Apttus__Parent_Agreement__r.RecordTypeId != agreementRecordTypesMap.get(CONSTANTS.MHA_Record_Type) &&
               agreement.Apttus__Parent_Agreement__r.RecordTypeId != agreementRecordTypesMap.get(CONSTANTS.MSA_YieldEx_Record_Type) && 
               agreement.Apttus__Parent_Agreement__r.RecordTypeId != agreementRecordTypesMap.get(CONSTANTS.OAS_MSA_RecordType)) {
                   // Notices_Contact_Lookup__c filed is not there Need to check With Sachin
                   //  agrmnt.Id = agreement.Id;
                   //  agrmntsList.add(agrmnt);
                   //  
               }
            
            if(agrmntsList.size()>0) {
                try {
                    update agrmntsList;
                } catch(Exception e) {
                    System.debug('&&&&&&*****'+e.getMessage());
                }
            }
        }
    }
    public static void filLegalRequestRecords(Map<Id, Apttus__APTS_Agreement__c> newAgreements, Map<Id,Apttus__APTS_Agreement__c> oldAgreements){
        
        Map<Id, Id> mapLegalRequest = new Map<Id, Id>();
        List<Legal_Request__c> updateLegalRequestList = new List<Legal_Request__c>();
        
        for(Legal_Request__c legalRequest : [SELECT Id,Related_Agreement__c FROM Legal_Request__c WHERE Related_Agreement__c IN :newAgreements.keySet()]){
            mapLegalRequest.put(legalRequest.Related_Agreement__c, legalRequest.Id);
        }
        
        
        for(Apttus__APTS_Agreement__c newval : newAgreements.values()){
            
            if(newval.Apttus__Status__c == CONSTANTS.Apttus_Status && oldAgreements.get(newval.Id).Apttus__Status__c != newval.Apttus__Status__c){
                
                Legal_Request__c legalobj = new Legal_Request__c();
                legalobj.Id = mapLegalRequest.get(newval.Id);
                legalobj.Legal_Review_Status__c = CONSTANTS.CLOSED_STATUS;
                updateLegalRequestList.add(legalobj);
            }
            
        }
        
        if(!updateLegalRequestList.isEmpty()){
            try{
                update updateLegalRequestList;
            }catch(Exception exp){
                system.debug('ERROR: '+exp.getMessage());
            }
        }
        
    }
    public static void updateRelatedAgreementWhenLegalRequestChanges(List<Apttus__APTS_Agreement__c> updatedAgreements, Map<Id, Apttus__APTS_Agreement__c > oldValues){
        
        if(!CheckRecursion.checkAgreementRecursion(CONSTANTS.AFTER_UPDATE)){
            //CheckRecursion.Run_LegalAgreementAfterUpdate = true;
            List<Legal_Request__c> requestsToUpdate = new List<Legal_Request__c>();
            for (Apttus__APTS_Agreement__c agr : updatedAgreements){
                if (agr.legal_request__c != oldValues.get(agr.id).legal_request__c){
                   
                   if(agr.legal_request__c != null){
                        Legal_Request__c lr = new Legal_Request__c(id = agr.legal_request__c, related_agreement__c = agr.id, Related_Parent_Agreement__c = agr.Apttus__Parent_Agreement__c);
                        requestsToUpdate.add(lr);
                    }
                  /* if (oldValues.get(agr.id).Legal_Request__c != null){
                        Legal_Request__c lr = new Legal_Request__c(id = oldValues.get(agr.id).legal_request__c, related_agreement__c = null, Related_Parent_Agreement__c = null);
                        requestsToUpdate.add(lr);           
                    }*/
                }   
            }
            
            if (requestsToUpdate.size()>0) { 
                try { 
                    update requestsToUpdate;   
                } catch(Exception e) {
                    System.debug('Error Occured'+e.getMessage());
                }
            }
        }
    } 
    public static void teminateAccountsforTerminatedAgreements(List<Apttus__APTS_Agreement__c> agreementsList) {
        Set<Id> accountIdsSet = new Set<Id>();
        Set<Id> AccountsIdsSettoSkip = new Set<Id>();
        List<Account> AccountsListtoUpdate = new List<Account>();
        for(Apttus__APTS_Agreement__c tmpAgrmt :agreementsList){
            if(tmpAgrmt.Apttus__Status_Category__c == CONSTANTS.Apttus_Status_Category){
                accountIdsSet.add(tmpAgrmt.Apttus__Account__c);
            }
        }
        for(Apttus__APTS_Agreement__c tmpAgr: [select Apttus__Account__c from Apttus__APTS_Agreement__c 
                                               where Apttus__Status_Category__c !=: CONSTANTS.Apttus_Status_Category
                                               and Apttus__Account__c in :accountIdsSet]){
                                                   
                                                   AccountsIdsSettoSkip.add(tmpAgr.Apttus__Account__c);
                                                   
                                               }
        if(accountIdsSet.size() > 0){
            accountIdsSet.removeAll(AccountsIdsSettoSkip);
            for(Account acc : [select id, Status__c from account where id in :accountIdsSet]){
                acc.Status__c = CONSTANTS.Apttus_Status_Category;
                AccountsListtoUpdate.add(acc);
            }
            if(AccountsListtoUpdate.size() > 0) {
                try { 
                    update AccountsListtoUpdate;
                } catch(Exception e) {
                    System.debug('error Occured:::::'+e.getMessage());
                }
            }
        }
    }
    public  static Date Calculatelstday(Date chkdate){
        Integer numberOfDays = Date.daysInMonth(chkdate.year(), chkdate.month());
        Date lastDayOfMonth = Date.newInstance(chkdate.year(), chkdate.month(), numberOfDays);
        return lastDayOfMonth;
        
    }
}