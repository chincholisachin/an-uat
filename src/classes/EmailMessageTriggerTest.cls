@isTest
public class EmailMessageTriggerTest{
    
    private static EmailMessage  newEmailMessages;
    private static Account newAccount;
    private static contact newContact;
    private static case newCase;
    private static emailSettings__c emailsettings;
    Private static Attachment createAttachment;
    private static user usr;
    
    public static void Init() {
    
        InitializeTestData.createSystemConfigSettings();
        
        /*newAccount =InitializeTestData.createAccount();
        insert newAccount ;*/
        
        newAccount = InitializeTestData.createAccount();
        newAccount .RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        insert newAccount ;
        
        newContact =  InitializeTestData.CreateContact(newAccount.Id);
        newContact .LastName='dummy';
        newContact .firstName ='CSP';
        insert newContact ;
        
        newCase = InitializeTestData.CreateCase(newContact .Id,newAccount.Id);
        newCase.SuppliedEmail = 'demo@gmail.com';
        newCase.CC_Emails__c = 'demoTest@demo.com,test@demo.com';
        insert newCase;
        System.debug(newCase+'&&*^*&^&^%&');
        newCase = [SELECT Id,ContactId,AccountId,Status,Origin,Assigned_to_CS__c,CC_Emails__c,SuppliedEmail,Cloned_To_CS__c, Case.OwnerId,Case.Owner.Email,Thread_Id__c FROM CASE WHERE Id =:newCase.id];
        emailsettings = InitializeTestData.creatEmailSettings();
        emailsettings.Org_Wide_Email__c  = 'sales@appnexus.com';
        emailsettings.Record_Type_Exclusion__c = 'Billing, Concur';
        emailsettings.Case_Email_Priority__c = 'Emergency,Low';
        emailsettings.Automate_Email_Handling__c = true;
        insert emailsettings ;
        
        newEmailMessages= InitializeTestData.CreateEmailMessage(newCase.Id);
        newEmailMessages.ccAddress='test@demo.com';
        newEmailMessages.Incoming = true;
        insert newEmailMessages;
        System.debug(newEmailMessages+'&&*^*&^&^%&NewEmailMessage');
        
        createAttachment = InitializeTestData.createAttachment(newEmailMessages.Id);
        insert createAttachment ;
        
        newEmailMessages = [SELECT FromAddress,Incoming,BccAddress,TextBody,HtmlBody,ToAddress,FromName,Subject,ParentId,ccAddress,HasAttachment FROM EmailMessage WHERE Id=:newEmailMessages.Id ];
        System.debug(newEmailMessages+'&^&^*^&newEmailMessages ');    
        
        usr = InitializeTestData.createUser();
        usr.contactId = newContact .id;
        usr.profileId = [select id from profile where name='Customer Portal User'].id;
        insert usr;
        
        Email_Loop_Alert__c El = new Email_Loop_Alert__c();
        el.From_Address__c = 'test@abc.org';
        el.Alert_Date__c = System.today();
        insert el;
        System.debug(el+'^&**&*^%$Alert_Date__c');
        
    }
    
     public static testMethod void emialMessageMethod() {
         Init();
       System.runAs(usr)  {
       
      
        list<Id> emailsIdsList = new List<Id>{emailsettings.Id};
       
        EmailMessageService.ResendIncomingEmailMessage(newEmailMessages,newCase,true);
          EmailMessageService.sendEmailMessage(emailsIdsList );
        try {
         EmailMessageService.incomingEmailMessage(newEmailMessages,newCase,false);
         
         } catch(exception e){
             system.debug('Exception' +e);
          system.debug('Exception message'+e.getMessage());
          system.debug('Exception line'+e.getLineNumber());
         }
        
    }
  }
  
  public static testMethod void emialMessageMethod1() {
         Init();
   
        list<Id> emailsIdsList = new List<Id>{emailsettings.Id};
       
        EmailMessageService.ResendIncomingEmailMessage(newEmailMessages,newCase,true);
          EmailMessageService.sendEmailMessage(emailsIdsList );
        try {
         EmailMessageService.incomingEmailMessage(newEmailMessages,newCase,false);
         
         } catch(exception e){
             system.debug('Exception' +e);
          system.debug('Exception message'+e.getMessage());
          system.debug('Exception line'+e.getLineNumber());
         }
        
    
  }
  
  public static testMethod void emialMessageMethod2() {
         Init();
   
        list<Id> emailsIdsList = new List<Id>{emailsettings.Id};
       
        EmailMessageService.ResendIncomingEmailMessage(newEmailMessages,newCase,false);
          EmailMessageService.sendEmailMessage(emailsIdsList );
        try {
         EmailMessageService.incomingEmailMessage(newEmailMessages,newCase,true);
         
         } catch(exception e){
             system.debug('Exception' +e);
          system.debug('Exception message'+e.getMessage());
          system.debug('Exception line'+e.getLineNumber());
         }
        
    
  }
  
   public static testMethod void emialMessageMethod4() {
         Init();
       System.runAs(usr)  {
       
        list<Id> emailsIdsList = new List<Id>{emailsettings.Id};
       
        EmailMessageService.ResendIncomingEmailMessage(newEmailMessages,newCase,false);
          EmailMessageService.sendEmailMessage(emailsIdsList );
        try {
         EmailMessageService.incomingEmailMessage(newEmailMessages,newCase,true);
         
         } catch(exception e){
             system.debug('Exception' +e);
          system.debug('Exception message'+e.getMessage());
          system.debug('Exception line'+e.getLineNumber());
         }
        
    }
  }
}