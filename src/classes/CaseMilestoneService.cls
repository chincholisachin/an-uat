public  class CaseMilestoneService {
	public static void completeMilestone(List<Id> caseIds, String milestoneName, DateTime complDate) {

    system.debug('*** completeMilestone Method Called ***');
    
    List<CaseMilestone> cmsToUpdate = [select Id, completionDate from CaseMilestone cm where caseId in :caseIds and cm.MilestoneType.Name LIKE: milestoneName+'%' and completionDate = null and IsViolated = false and TargetDate > : System.now() limit 1];
    
    if (cmsToUpdate.isEmpty() == false){
      for (CaseMilestone cm : cmsToUpdate){
        cm.completionDate = complDate;
      }
      try{
        update cmsToUpdate;
        system.debug('*** updated cmsToUpdate ***'+cmsToUpdate);
      }
      catch(Exception ex){
        system.debug('*** Exception while updating case Milestone  Exception Line number -'+ex.getLineNumber()+' Exception -- '+ex);
      }
    } // end if
  }
}