@isTest
public class FixOpportunityRemindersTest{

    
    private static testMethod void sendNotificationPastDate(){
        
        Opportunity[] opps = OpportunityTriggerTest.prepareOppData();
        insert new Bypass__c(SetupOwnerId=UserInfo.getOrganizationId(), WFR_Bypass__c=true);
        for (Opportunity o: opps){
            o.Stage_Change_Date__c = Date.today().addDays(-125);
            o.stagename = 'Proposal/Scoping';
            o.closeDate = Date.today();
        }
        insert opps;
        Test.startTest();    
        FixOpportunityReminders cin = new FixOpportunityReminders();
        cin.execute(null);
        System.assertEquals(Limits.getEmailInvocations(),1);
        Test.stopTest();
        
    }
    
    
    private static testMethod void closeDateNotification(){
        
        Opportunity[] opps = OpportunityTriggerTest.prepareOppData();
        insert new Bypass__c(SetupOwnerId=UserInfo.getOrganizationId(), WFR_Bypass__c=true);
        for (Opportunity o: opps){
            o.stagename = 'Proposal/Scoping';
            o.closeDate = Date.today().addDays(-1);
        }
        insert opps;
        Test.startTest();    
        FixOpportunityReminders cin = new FixOpportunityReminders();
        cin.execute(null);
        System.assertEquals(Limits.getEmailInvocations(),1);
        Test.stopTest();
        
    }
    
    private static testMethod void noNotification(){
        
        Opportunity[] opps = OpportunityTriggerTest.prepareOppData();
        for (Opportunity o: opps){
            o.stagename = 'Identification';
            o.closeDate = Date.today().addDays(-1);
        }
        insert opps;
        Test.startTest();    
        FixOpportunityReminders cin = new FixOpportunityReminders();
        cin.execute(null);
        System.assertEquals(Limits.getEmailInvocations(),0);
        Test.stopTest();
        
    }

}