public class AgreementTriggerHandler {
    
    private static AgreementTriggerHandler instance;
     //*** Singleton Pattern to obtain instance of the Handler***
    public static AgreementTriggerHandler getInstance(){
        if(instance==null){
            instance = new AgreementTriggerHandler();
        }
        return instance;
    }
    
    public void beforeInsertMethod(list<Apttus__APTS_Agreement__c> newList,map<ID,Apttus__APTS_Agreement__c> newMap){
        AgreementService.populateDefaultOutputFormat(newList);
        AgreementService.fillAgreementFields(newList);
    }
    
    public void beforeUpdateMethod(list<Apttus__APTS_Agreement__c> newList, list<Apttus__APTS_Agreement__c> oldList, map<ID,Apttus__APTS_Agreement__c> newMap, map<ID,Apttus__APTS_Agreement__c> oldMap){
         AgreementService.populateDefaultOutputFormat(newList);
         AgreementService.fillAgreementFields1(newList);
    }
    
    public void afterInsertMethod(list<Apttus__APTS_Agreement__c> newList,map<ID,Apttus__APTS_Agreement__c> newMap){
        AgreementService.fillAgreementfieldsafterinsert(newMap);
    }
 
    public void afterUpdateMethod(list<Apttus__APTS_Agreement__c> newList,list<Apttus__APTS_Agreement__c> oldList,map<ID,Apttus__APTS_Agreement__c> newMap,map<ID,Apttus__APTS_Agreement__c> oldMap){
        AgreementService.filLegalRequestRecords(newMap,oldMap);
        AgreementService.updateRelatedAgreementWhenLegalRequestChanges(newList,oldMap);
        AgreementService.teminateAccountsforTerminatedAgreements(newList);
    }
}