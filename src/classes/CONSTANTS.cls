public  class CONSTANTS {

    public static final set<String> AGREEMENT_CATEGORIES = new set<String>{'In Effect',
                                                                            'Amended',
                                                                            'Expired',
                                                                            'Terminated',
                                                                            'Cancelled'};
                                                                            
    

    public static final String AGREEMENT_CLIENT_OPS_REVIEW_COMPLETE = 'Client Ops Review Complete - Ready for Signatures';                                                                          

    public static final String FIRST_RESPONSE = '1st Response';

    public static final String BUS_HRS_ID = 'BusHrsID';

    public static final String NY_BUS_HRS_ID = 'NYBusHrs';

    public static final String NY_BUS_HRS_NAME = 'NY Office Business Hours';

    public static final String RESOLVED_STATUS = 'Resolved';
    public static final String FINISHED_STATUS = 'Finished';

    //--CSP RecordType--//
    public static final String CSP_CASE_RT = 'CP View';
    public static final String PRODUCT_SUPPORT_CASE_RT = 'Product Support';
    public static final String OA_CASE_RT = 'OAS Support';

    public static final String CASE_ORIGIN_WEB = 'Web';

    public static final String CSP_ENT_NAME = 'CSP AMS Entitlement';

    public static final String CSP_ENT_HARDCODE_CS_PARAM = 'CommEntitlement';

    public static final String CAT_CLIENT_FINANCE = 'Client Finance';

    public static final String AMERICAS = 'Americas';

    public static final String CLOSED_STATUS = 'Closed';

    public static final String MS_CLOSED = 'Case Closure';

    public static final String REQUEST_FOR_E_SIGNATURES = 'Request for eSignatures';
    
    public static final String Email_Required_Message ='Contact should have a valid email to proceed.';
    
    public static final String Duplicate_Contact_Message = 'Cannot enable communities access, duplicate contacts exists.';
    
    public static final String Duplicate_User_Message = 'Cannot enable communities access, a user with this email address already exists.';
    
    public static final String In_Effect_Status = 'In Effect';
    
    public static final String Region_Brazil = 'Brazil'; 
   
    public static final String CSP_OAS_Category = 'CSP_Access_OAS_LatAm_Data_Category';
    
    public static final String CSP_Bidders_Category = 'CSP_Bidders_Data_Category_with_Business_Support';
    
    public static final String CSP_All_Category = 'CSP_All_Data_Categories_for_Internal_CSP_Users';
    
    public static final String Account_Name_AppNexus = 'Appnexus';
    
    public static final String Contact_Email_Check = '@appnexus.com';
    
    public static final String User_Success_Message = 'User successfully enabled for Customer service portal.';
    
    public static final String User_Error_Message = 'Not a AppnNexus customer service portal user';
    
    public static final String Contains_Duplicate = 'Duplicate Username'; 
    
    public static final String CSP_Knowledge = 'CSP + Knowledge';
    
    public static final String Communities_knowledge = 'Communities + Knowledge';
    
    public static final String Customer_Portal_User = 'Customer Portal User';
    
    public static final String Email_Template = 'Email Template';
    
    public static final String Email_Form = 'Email From';
    
    public static final String Related_Record_Id= 'Related Record Id';
    
    public static final String AccountIdLabel= 'Account Id';
    
    public static final String ParentAccountLabel= 'Parent Account Id';
    
    public static final String SALES_OP = 'Sales_Ops';
    
    public static final String CASE_SOBJECTTYPE = 'Case';
    
    public static final String REASON_PARAM = 'creason';
    
    public static final String TYPE_PARAM = 'ctype';
    
    public static final String CASE_ID_PARAM = 'caseid';
    
    public static final String RELATED_ACCOUNT_ID = 'relatedAccount';
    
    public static final String OPPORTUNITY_ID_PARAM = 'oppId';
    
    public static final String ACCOUNT_NAME = 'AppNexus';
    
    public static final String CASE_CREATION_FAILED = 'Case creation failed: ';
    
    public static final String CONTRACT_ATTACH_REQUIRED = 'You must attach a contract for contract submission types';
    
    public static final String UNABLE_TO_FIND_FS = 'Unable to find fieldset with api name:';
    
    public static final String NO_SALESOPS_REASON = 'No Sales Ops Case Type or Reason Selected';
    
    public static final String Exception_Review = 'Exception Review';
    
    public static final String Contract_Submission ='Contract Submission';
    
    public static final String Consult_With_Sales_Ops = 'Consult With Sales Ops';
    
    public static final String Contract_Review = 'Contract Review';
    

    public static final String NEW_STATUS = 'New';

    public static final String EMAIL_TEMPLATE_OUTAGE_NOTIFICATION = 'Client Specific Outage Alert';
    
    //-- Triggers--//

    public static final String BEFORE_INSERT = 'BEFORE_INSERT';
    public static final String AFTER_INSERT = 'AFTER_INSERT';
    public static final String BEFORE_UPDATE = 'BEFORE_UPDATE';
    public static final String AFTER_UPDATE = 'AFTER_UPDATE';
    
    public static final String Bidder_Service_Order_Record_Type = 'Bidder Service Order';
    public static final String Console_Service_Order_Record_Type = 'Console Service Order (ATG)';
    public static final String Notice_of_Non_Renewal_Record_Type = 'Notice of Non-Renewal';
    public static final String Notice_of_Breach_Termination_Record_Type = 'Notice of Breach Termination';
    public static final String MSA_YieldEx_Record_Type = 'MSA (YieldEx)';
    public static final String MHA_Record_Type = 'MHA';
    public static final String OAS_MSA_RecordType = 'OAS MSA';
    public static final String YieldEx_Analytics_Service_Order = 'YieldEx Analytics Service Order';
    public static final String YieldEx_Direct_Service_Order = 'YieldEx Direct Service Order';
    public static final Integer Apttus_Termination_Notice_Days_60 = 60;
    public static final Integer Apttus_Termination_Notice_Days_30 = 30;
    public static final string MSA_Record_Type = 'MSA';
    public static final String Apttus_Status = 'Activated';
    public static final String Apttus_Status_Category = 'Terminated';
    public static final String Apttus_System_Properties = 'System Properties'; 
    public static final String ES_SIGN_AGMT_OBJECT_NAME  = 'echosign_dev1__SIGN_Agreement__c';
    public static final String ES_SIGNED_ATTACHMENT_SUFFIX = 'signed.pdf';
    public static final String ES_AUDIT_DOCUMENT_SUFFIX = 'audit.pdf';
    
    
    
    
    
}