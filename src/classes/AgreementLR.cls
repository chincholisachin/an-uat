public with sharing class AgreementLR {
     public Legal_Request__c lrObj{get;set;}
     public boolean createLRFlg {get;set;}
     public boolean existLRFlg {get;set;}
     public boolean parentLRFlg {get;set;}
     public id agrId;
     public Apttus__APTS_Agreement__c agreement {get;set;} 
     public Apttus__APTS_Agreement__c uptAgreement {get;set;} 
     public String parentName {get;set;}
     public String globalParentName {get;set;}
     
     public AgreementLR (ApexPages.StandardController controller) {
       agrId = (ID)ApexPages.CurrentPage().getParameters().get('agrId');
       createLRFlg = false;
       
           this.lrObj= (Legal_Request__c)controller.getRecord();
           agreement = [Select Name, Apttus__Account__c, Apttus__Account__r.Related_Party__c,
                               Apttus__Related_Opportunity__c, Legal_Request__c,
                               recordtype.name, Apttus__Parent_Agreement__c,
                               Apttus__Parent_Agreement__r.recordtype.name,
                               //Apttus__Related_Opportunity__r.Data_Provider_Type__c, createdby.email,
                               Apttus__Account__r.name,  Apttus__Account__r.parentid, Apttus__Account__r.parent.name, 
                               Apttus__Account__r.Global_Account_Parent__r.name,
                               Apttus__Non_Standard_Legal_Language__c, owner.email,
                               APTS_Expedite_Details__c
                               
                          from Apttus__APTS_Agreement__c
                         where id = :agrId];
         
        boolean attFlg;
        attFlg = false;
        existLRFlg = false;
        parentLRFlg = false;
        
        if(('MHA|MSA|MSA (YieldEx)').contains(agreement.recordtype.name)){
           List<Apttus__APTS_Related_Agreement__c> relAgrLst = new List<Apttus__APTS_Related_Agreement__c>();
           relAgrLst = [select id 
                         from Apttus__APTS_Related_Agreement__c
                        where Apttus__APTS_Contract_To__c = :agrId
                          and Apttus__Relationship_From_Type__c = 'Is Amended By'];
           if(relAgrLst.size() == 0)
               parentLRFlg = true;
            
        }
        
        for(Attachment tmpAtt :[select Id, ParentId From Attachment where ParentId = :agrId limit 1]) 
             attFlg = true;    
          
        /* WPP Check - Start 
        boolean wppFlg;
        Account prntacn;                
        if (agreement.Apttus__Account__r.parentid != null)
            prntacn = [select id, parentid, parent.name, Global_Account_Parent__r.name 
                         from account 
                        where id =:agreement.Apttus__Account__r.parentid];
        parentName = '';
        globalParentName = '';
        if (agreement.Apttus__Account__r.parent.name != null) 
            parentName = agreement.Apttus__Account__r.parent.name + ' ' + prntacn.parent.name + ' ' + prntacn.Global_Account_Parent__r.name;
        if (agreement.Apttus__Account__r.Global_Account_Parent__r.name != null) 
            globalParentName = agreement.Apttus__Account__r.Global_Account_Parent__r.name;
        wppFlg = !parentName.contains('WPP') && !agreement.Apttus__Account__r.Name.contains('WPP') && !globalParentName.contains('WPP') ? true : false;
        WPP Check - End */
        
        /*if(agreement.Legal_Request__c != null)
            existLRFlg = true;*/            
        for(Legal_Request__c tmpLR :[select id from Legal_Request__c where Related_Agreement__c = :agrId] ){
            existLRFlg = true;
        }
        if(/*wppFlg &&*/!existLRFlg && !parentLRFlg /*&& (agreement.Apttus__Account__r.Related_Party__c == 'Yes' || agreement.Apttus__Non_Standard_Legal_Language__c)*/)
            createLRFlg = true;
        /*for(Legal_Request__c tmpLR :[select id from Legal_Request__c where Related_Agreement__c = :agrId] ){
            createLRFlg = false;
        }*/
     }
     
      public PageReference saveRec(){
        lrObj.Opportunity__c = agreement.Apttus__Related_Opportunity__c;
        lrObj.Related_Contact_Email__c = agreement.owner.email;
        lrObj.Related_Agreement__c = agrId;
        //lrObj.Requested_Attorney__c = agreement.Requested_Attorney__c;
        //lrObj.APTS_Expedite_Details__c = agreement.APTS_Expedite_Details__c;
        if(agreement.Apttus__Parent_Agreement__c ==  null){
            //lrObj.Contract_Type_New__c = agreement.recordtype.name;
        }else{
            //lrObj.Contract_Type_New__c = agreement.Apttus__Parent_Agreement__r.recordtype.name;
            lrObj.Related_Parent_Agreement__c = agreement.Apttus__Parent_Agreement__c;
        }
        insert lrObj;
        uptAgreement = new Apttus__APTS_Agreement__c(id=agrId);
        uptAgreement.Apttus__Status__c = 'In Legal Review';
        uptAgreement.Comments_for_from_Legal__c = lrObj.Additional_Details_for_Legal_Review__c;
        uptAgreement.Legal_Request__c = lrObj.id;
        update uptAgreement;
        /*
        if(agreement.Apttus__Parent_Agreement__c !=  null){
            Apttus__APTS_Agreement__c uptPrAgreement = new Apttus__APTS_Agreement__c(id=agreement.Apttus__Parent_Agreement__c);
            uptPrAgreement.Legal_Request__c = lrObj.id;
            update uptPrAgreement;
        }
        */
        
        PageReference pr = new PageReference('/'+agrId);
        pr.setRedirect(true);
        return pr;
      }
     public PageReference cancelRec(){
        PageReference pr = new PageReference('/'+agrId);
        pr.setRedirect(true);
        return pr;
     }
}