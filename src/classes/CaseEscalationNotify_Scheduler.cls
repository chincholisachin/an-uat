global class CaseEscalationNotify_Scheduler implements Schedulable{
	
	global void execute(SchedulableContext scMain) {
        CaseEscalationNotify  m = new CaseEscalationNotify ();
        ID idBatch = Database.executeBatch(m, 1);
    }

}