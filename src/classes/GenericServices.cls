public with sharing class GenericServices {
	public static Boolean ranUpdateDependentField = false;
    
    // this method updates dependent fields based on controlling field values
    public static void updateDependentField(List<sObject> sObjectList){
        
        if(!ranUpdateDependentField){
            


            for(Field_Dependency__c fd : Field_Dependency__c.getAll().values()){
                
                if(fd.object__c=='Case'){

                    List<String> controllingFields = fd.Controlling_Fields__c.split(',');
                    String dependentField = fd.Dependent_Field__c;
                    String objectType = String.valueOf(sObjectList.getSObjectType());
                    Map<String,String> loadCustomSettingMetaData = Utility.getAllFields('Field_Map_Values__c', true);
                    String customSettingQuery = 'select ' +  loadCustomSettingMetaData.get('Field_Map_Values__c') + ' from Field_Map_Values__c where object__c = \''+ objectType +'\'';
                    List< Field_Map_Values__c  > fm = Database.query(customSettingQuery);
                    Map<String, Object> controllingDependentFieldValue = new Map<String, Object>();
                    for (Field_Map_Values__c f : fm){
                        String mapKey;
                        for (String s : controllingFields){
                            mapKey = mapKey == null ? String.valueOf(f.get(s)) : mapKey + String.valueOf(f.get(s));  
                        }
                        controllingDependentFieldValue.put(mapKey,f.get(dependentField));   
                    }
                    
                    for (sObject s: sObjectList){
                        String requestKey;
                        for (String st : controllingFields){
                            requestKey = requestKey == null ? String.valueOf(s.get(st)) : requestKey + String.valueOf(s.get(st));  
                        }
                        s.put(dependentField,controllingDependentFieldValue.get(requestKey));
                    }
                }
                ranUpdateDependentField = true;
        }    
    }
    
    }
    
    
}