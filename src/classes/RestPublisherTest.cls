/*
 * Test class for RestPublisher
 */
@isTest
public class RestPublisherTest {
    
    @isTest
    static void postToRESTTest () {
        RestPublisher.Payload payload = new RestPublisher.Payload();
        payload.endPoint = 'https://www.example.test/12345';
        payload.sessionId = 'SessionId';
        payload.recordId = '123456789';
        payload.enterpriseURL = 'https://www.example.test/12345';
        payload.sObjectType ='sObject';
        payload.field1Name = 'Field 1';
        payload.field1Value = 'Value 1';
        payload.field2Name = 'Field 2';
        payload.field2Value = 'Value 2';
        payload.field3Name = 'Field 3';
        payload.field3Value = 'Value 3';
        payload.field4Name = 'Field 4';
        payload.field4Value = 'Value 4';
        payload.field5Name = 'Field 5';
        payload.field5Value = 'Value 5';
        payload.field6Name = 'Field 6';
        payload.field6Value = 'Value 6';
        payload.field7Name = 'Field 7';
        payload.field7Value = 'Value 7';
        payload.field8Name = 'Field 8';
        payload.field8Value = 'Value 8';
        payload.field9Name = 'Field 9';
        payload.field9Value = 'Value 9';
        payload.field10Name = 'Field 10';
        payload.field10Value = 'Value 10';
        
        Test.startTest();

        RestPublisher.postToREST(new List<RestPublisher.Payload>{payload});
        
        Test.stopTest();
    }

}