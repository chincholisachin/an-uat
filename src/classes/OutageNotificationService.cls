public  class OutageNotificationService {
	

	public static list<Outage_Notifications__c> getActiveOutageNotifications(){
		try{
			return [SELECT Account__c from Outage_Notifications__c where IsActive__c = true];
		}catch(QueryException qe){
			system.debug('***Exception qe***'+qe.getMessage());
		}
		return null;
	}

}