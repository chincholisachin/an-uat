public class AccountUtils {

    /*
    Account Utilities:
    
    AccountUtils.fetchAccDetails(List<Account> members) - Tested in AccountTriggerTest
        Returns a map of <Id,Account> where id is the Id of the child member and Account is the parent account.
        Region, address fields are returned with this method. More fields can be added.
        Members list need to contain parentid and recordtypeid fields for this to work. 
        The list of fields which are retrieved in this map is stored in AccountFieldsToCache custom label.
    
    AccountUtils.isInternal().get(String recordTypeId) - Tested in AccountUtilsTest
        Returns a boolean to indicate if account is an internal account or not, based on its record type. 
        RT types are stored in Account Record Type (Account_Record_Type__c) custom setting. 
    
    AccountUtils.isMember().get(String recordTypeId) - Tested in AccountTriggerTest
        Returns a boolean to indicate if account is a member record or not, 
        based on its record type. RT types are stored in Account Record Type (Account_Record_Type__c) custom setting. 
    
    AccountUtils.isCustomer().get(String recordTypeId) - Tested in AccountTriggerTest
        Returns a boolean to indicate if account is a customer account or not, 
        based on its record type. RT types are stored in Account Record Type (Account_Record_Type__c) custom setting. 
    
    AccountUtils.generateAccountTeamMember(Id acn, Id usr, String role) - Tested in AccountTriggerTest
        Returns a AccountTeamMember record with Edit permission on Account, Case, Opportunity, Contact for provided role. 
    
    AccountUtils.userMap(List<Id> userIds) - Tested in AccountTriggerTest
        Returns a map type Id,User for given userIds.
    
    AccountUtils.commercialLeaderMap() - Tested in AccountTriggerTest
        Returns a map type String,Id where string is combination of region-market and id is the userid of commercial leader.
        
    AccountUtils.getPermissionToEditRole() - Tested in AccountUtilsTest
        Returns a map type String, Boolean to see the person running the application has the permission to edit the role.
        
    */
    
    private static Map<Id,Account> acn = new Map<Id,Account>();
    private static Map<Id, Account> accountMap = new Map<Id,Account>();
    private static Map<Id, RecordType> rt = new Map<Id,RecordType>();
    private static Map<String,Account_Record_Type__c> art = new Map<String,Account_Record_Type__c>();
    private static Map<String,Boolean> internalMap = new Map<String,Boolean>();
    private static Map<String,Boolean> memberMap = new Map<String,Boolean>();
    private static Map<String,Boolean> customerMap = new Map<String,Boolean>();
    private static Map<String, Id> teamOwnerMap = new Map<String, Id>();
    private static Map<Id,User> userMap = new Map<Id,User>();
    private static Boolean isAccountListCached = false;
    private static Boolean isAccountRecordTypeCached = false;
    private static Boolean isInternalMapped = false;
    private static Boolean isMemberMapped = false;
    private static Boolean isCustomerMapped = false;
    private static Boolean isUserMapReturned = false;
    private static Boolean teamOwnerMapLoaded = false;
    private static Boolean recordTypesLoaded = false;
    private static Boolean accountLoaded = false;
    public static Boolean leadIsBeingConverted = false;
    public static String SR_ACCOUNT_OWNER = 'Senior Account Owner'; //used in Accounttriggerhandler
    public static String ACCOUNT_OWNER = 'Account Owner'; //used for testing on AccountTriggerTest
    public static String COMMERCIAL_LEADER = 'Commercial Leader'; //used in Accounttriggerhandler       
    private static Map<Id,RecordType> getRecordTypes(String sObj){
        if(recordTypesLoaded) return rt;
        Map<Id,RecordType> rtList = new Map<Id,RecordType>([select id,name from recordType where sobjecttype =: sObj]);
        recordTypesLoaded = true;
        rt = rtList;
        return rt;    
    }
    
    private static Map<String,Account_Record_Type__c> getAccountRecordTypes(){
        if(isAccountRecordTypeCached) return art;
        Map<String, Account_Record_Type__c> artMap = Account_Record_Type__c.getAll();
        isAccountRecordTypeCached = true;
        art = artMap;
        return art;
    }
    
    public static Map<Id,Account> fetchAccDetails(List<Account> members){
        
        if (accountLoaded) return accountMap;
        accountMap = new Map<Id, Account>();
        List<Account> membersOnly = new List<Account>();
        for (Account a: members){
            if(AccountUtils.isMember().containsKey(a.recordTypeId)) {
                if (AccountUtils.isMember().get(a.recordTypeId)) 
                    membersOnly.add(a);
                }
        }
        
        //when new fields are needed, please add them to AccountFieldsToCache Custom Label.
        if (membersOnly.size() > 0){
        
            // construct the query to fire
            String q = 'SELECT Id';
            for (String s : Label.AccountFieldsToCache.split('\r\n')){
                q = q + ',' + s;
            }
            q = q + ' FROM Account WHERE ID IN ('; 
            for (String s: Pluck.ids('ParentId',membersOnly)){
                q = q + '\'' + s + '\',';
            }
            q = q.removeEnd(',') + ')'; 
            system.debug('query is ' + q);
            for (Account a: Database.query(q)){
                accountMap.put(a.id,a);
            }
            accountLoaded = true;
        }
        return accountMap;     
    }
    
    
    //returns a map of account id and if they are internal accounts or not. Commented out, since it is not being used. 
    public static Map<String,Boolean> isInternal(){
        if(isInternalMapped) return internalMap;
        Map<String,Boolean> internal = new Map<String,Boolean>();
        for (RecordType r: getRecordTypes('Account').values()){
            if(getAccountRecordTypes().get(r.name) != null ) internal.put((r.id),getAccountRecordTypes().get(r.name).isInternal__c);             
        }
        isInternalMapped = true;
        internalMap = internal;
        return internalMap;
    }
    
    //returns a map of accound id and if they are member accounts or not.
    public static Map<String,Boolean> isMember(){
        if(isMemberMapped) return memberMap;
        Map<String,Boolean> member = new Map<String,Boolean>();
        for (RecordType r: getRecordTypes('Account').values()){
            if(getAccountRecordTypes().get(r.name) != null ) member.put(r.id,getAccountRecordTypes().get(r.name).isMember__c);             
        }
        isMemberMapped = true;
        memberMap = member;
        return memberMap;
    }
    
    
    public static Map<String,Boolean> isCustomer(){
        if(isCustomerMapped) return customerMap;
        Map<String,Boolean> customer = new Map<String,Boolean>();
        for (RecordType r: getRecordTypes('Account').values()){
            if(getAccountRecordTypes().get(r.name) != null ) customer.put(r.id,getAccountRecordTypes().get(r.name).isCustomer__c);             
        }
        isCustomerMapped = true;
        customerMap = customer;
        return customerMap;
    }
    
    public static AccountTeamMember generateAccountTeamMember(Id acn, Id usr, String role){
        
        AccountTeamMember atm = new AccountTeamMember(accountid = acn, 
                                                      accountaccesslevel = 'Edit', 
                                                      caseaccesslevel = 'Edit', 
                                                      contactaccesslevel = 'Edit',
                                                      opportunityaccesslevel = 'Edit',
                                                      TeamMemberRole = role,
                                                      Userid = usr);
        return atm;

    }
    
    public static Map<Id,User> userMap(List<Id> userIds){
        if(isUserMapReturned) return userMap;
        Map<Id,User> userList = new Map<Id,User>([select id, managerid from user where id in: userIds]);
        isUserMapReturned = true;
        userMap = userList;
        return userMap;
    }
    
    
    public static Map<String, Id> commercialLeaderMap(){
        if(teamOwnerMapLoaded) return teamOwnerMap;
        Map<String,Id> leaderMap = new Map<String,Id>();
        for(Team_Owner__c to : [select market__c, vertical__c, team_owner__c from Team_Owner__c]){
            leaderMap.put(to.market__c + ' - ' + to.vertical__c , to.team_owner__c);
        }
        teamOwnerMapLoaded = true;
        teamOwnerMap = leaderMap;
        return teamOwnerMap;
    }
    
    public static Map<String,Boolean> getPermissionToEditRole(){
        
        Map<String,Boolean> permissionList = new Map<String,Boolean>();
        Set<String> ownerGroup = new Set<String>(Label.AccountTeamRoleOwner.split('\r\n'));
        Set<String> financeGroup = new Set<String>(Label.AccountTeamRoleFinance.split('\r\n'));
        Set<String> supportGroup = new Set<String>(Label.AccountTeamRoleSupport.split('\r\n'));
        
        Account_Team_Modify__c atm = Account_Team_Modify__c.getInstance();
        
        for (String s : Generalutils.getPicklistValues('AccountTeamMember','TeamMemberRole')){
            if (ownerGroup.contains(s)) permissionList.put(s,atm.OwnerRoles__c);
            if (financeGroup.contains(s)) permissionList.put(s,atm.FinanceRoles__c);
            if (supportGroup.contains(s)) permissionList.put(s,atm.SupportRoles__c);
            if (!ownerGroup.contains(s) && !financeGroup.contains(s) && !supportGroup.contains(s)) permissionList.put(s,atm.OtherRoles__c);
        }
        
        return permissionList;
    
    }

}