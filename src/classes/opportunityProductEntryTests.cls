@istest
private class opportunityProductEntryTests {

    private static void setupData(){
        
        TestingUtils.createEssentialCustomSettings();
        Account[] acn = TestingUtils.createAccounts(1, 'TestAccount', true);
          
        Id pricebookId = Test.getStandardPricebookId();
        
        Pricebook2 pb = new PriceBook2(name = 'Standard Price Book');
        insert pb;
        
        List<Product2> pr = new List<Product2>();
        pr.add(new Product2(isActive = true,
                                   Family = 'Buy Side',
                                   Name = 'Header Bidding'));
        pr.add(new Product2(isActive = true,
                                   Family = 'Sell Side',
                                   Name = 'Console'));

        insert pr;
        List<PriceBookEntry> pbeSt = new List<PriceBookEntry>();
        List<PriceBookEntry> pbe = new List<PriceBookEntry>();
        pbeSt.add(new PriceBookEntry(pricebook2id = pricebookId, product2id = pr[0].id, UnitPrice = 1, UseStandardPrice = false));
        pbeSt.add(new PriceBookEntry(pricebook2id = pricebookId, product2id = pr[1].id, UnitPrice = 1, UseStandardPrice = false));
        pbe.add(new PriceBookEntry(pricebook2id = pb.id, product2id = pr[0].id, UnitPrice = 1, UseStandardPrice = false, isactive = true));
        pbe.add(new PriceBookEntry(pricebook2id = pb.id, product2id = pr[1].id, UnitPrice = 1, UseStandardPrice = false, isactive = true));
        insert pbeSt;
        insert pbe;
        Opportunity[] opp = TestingUtils.createOpportunities(1, 'Test', 'Identification', false);
        opp[0].accountid = acn[0].id;
        insert opp;
        OpportunityLineItem oli = new OpportunityLineItem(opportunityid = opp[0].id, pricebookentryid = pbe[0].id, product2id = pr[0].id, quantity =12, unitprice = 10);       
        insert oli;
        system.assert(opp[0].id != null);

    }
    
    
    static testMethod void theTests(){
        
        setupData();
                    
        OpportunityLineItem oli = [select Id, PricebookEntryId, PricebookEntry.Pricebook2Id, PricebookEntry.Name, PriceBookEntry.Product2Id, OpportunityId, Opportunity.AccountId from OpportunityLineItem limit 1];
             
        PageReference pageRef = Page.opportunityProductEntry;
        pageRef.getParameters().put('Id',oli.OpportunityId);
        Test.setCurrentPageReference(pageRef);
        
        opportunityProductEntryExtension oPEE = new opportunityProductEntryExtension(new ApexPages.StandardController(oli.Opportunity));
        
        // test 'getChosenCurrency' method
        if(UserInfo.isMultiCurrencyOrganization())
            System.assert(oPEE.getChosenCurrency()!='');
        else
            System.assertEquals(oPEE.getChosenCurrency(),'');

        // we know that there is at least one line item, so we confirm
        Integer startCount = oPEE.ShoppingCart.size();
        system.assert(startCount>0);

        //test search functionality without finding anything
        oPEE.searchString = 'michaelforce is a hip cat';
        oPEE.updateAvailableList();
        system.assert(oPEE.AvailableProducts.size()==0);
        
        //test remove from shopping cart
        oPEE.toUnselect = oli.PricebookEntryId;
        oPEE.removeFromShoppingCart();
        system.assert(oPEE.shoppingCart.size()==startCount-1);
        
        //test save and reload extension
        oPEE.onSave();
        oPEE = new opportunityProductEntryExtension(new ApexPages.StandardController(oli.Opportunity));
        system.assert(oPEE.shoppingCart.size()==startCount-1);
        
        // test search again, this time we will find something
        oPEE.searchString = oli.PricebookEntry.Name;
        oPEE.updateAvailableList();
        system.assert(oPEE.AvailableProducts.size()>0);       

        // test add to Shopping Cart function
        oPEE.toSelect = oPEE.AvailableProducts[0].Id;
        oPEE.addToShoppingCart();
        system.assert(oPEE.shoppingCart.size()==startCount);
                
        // test save method - WITHOUT quanitities and amounts entered and confirm that error message is displayed
        oPEE.onSave();
        system.assert(ApexPages.getMessages().size()>0);
        
        // add required info and try save again
        for(OpportunityLineItem o : oPEE.ShoppingCart){
            o.quantity = 5;
            o.unitprice = 300;
        }
        oPEE.onSave();
        
        // query line items to confirm that the save worked
        opportunityLineItem[] oli2 = [select Id from opportunityLineItem where OpportunityId = :oli.OpportunityId];
        system.assert(oli2.size()==startCount);
        
        // test on new Opp (no pricebook selected) to make sure redirect is happening
        Opportunity newOpp = new Opportunity(Name='New Opp',stageName='Identification',Amount=10,closeDate=System.Today()+30,AccountId=oli.Opportunity.AccountId);
        insert(newOpp);
        oPEE = new opportunityProductEntryExtension(new ApexPages.StandardController(newOpp));
        System.assert(oPEE.priceBookCheck()==null);
        
        // final quick check of cancel button
        System.assert(oPEE.onCancel()!=null);
        

        pageRef = Page.opportunityProductRedirect;
        pageRef.getParameters().put('Id',oli2[0].Id);
        Test.setCurrentPageReference(pageRef);

        // load the extension and confirm that redirect function returns something
        opportunityProductRedirectExtension oPRE = new opportunityProductRedirectExtension(new ApexPages.StandardController(oli2[0]));
        System.assert(oPRE.redirect()!=null);
        
    }
}