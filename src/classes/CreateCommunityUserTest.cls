@istest(SeeAllData=false)
public without sharing class CreateCommunityUserTest 
{
    public static Account testAccount = null;
    public static Contact testContact = null;
    public static Apttus__APTS_Agreement__c testAgreement = null;

    Static testmethod void testUserCreate(){
        InitializeTestData.createSystemConfigSettings();
        User usr = TestingUtils.ADMIN_USER;   
       /* testAccount = TestingUtils.createAccounts(1, 'Appnexus Test', false)[0];
        testAccount.BillingCity = 'Wayne';
        testAccount.BillingState = 'New Jersey';
        testAccount.BillingPostalCode = '07470';
        testAccount.BillingCountry = 'USA';
        testAccount.Global_rdp__c='EMEA';
        testAccount.Region_rdp__c='DACH';
        testAccount.Support_Level__c ='Foundation';
        testAccount.Owner = usr;
        insert testAccount;*/
        
         testAccount = InitializeTestData.createAccount();
         testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        insert testAccount;
        
        //Creating Test Contact record
        testContact = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = testAccount.id, Email='he@gmail.com', Role_Level__c='Director');
        insert testContact;
        
        Set<String> ids = new Set<String>();
     
        ids.add(testContact.id);
        if (ids.size()>0){
            for (String idtemp : ids){
                CreateCommunityUser.createUserFromContact(idtemp);
            }
        }
        
    }
    Static testmethod void testContactPermissionSet(){
         InitializeTestData.createSystemConfigSettings();
        //Creating Test User
        User usr = TestingUtils.ADMIN_USER;
          
        //creating Test Account record
        testAccount = TestingUtils.createAccounts(1, 'Appnexus Test', false)[0];
        testAccount.BillingCity = 'Wayne';
        testAccount.BillingState = 'New Jersey';
        testAccount.BillingPostalCode = '07470';
        testAccount.BillingCountry = 'USA';
        testAccount.Global_rdp__c='EMEA';
        testAccount.Region_rdp__c='DACH';
        testAccount.Support_Level__c ='Foundation';
        testAccount.Owner = usr;
        insert testAccount;
        
        //Creating Test Contact record
        testContact =  InitializeTestData.CreateContactwithEmail(testAccount.id);
          testContact.LastName='dummy';
        testContact.firstName ='CSP';
        insert testContact;
        
        RecordType rt = [select id,Name from RecordType where SobjectType='Apttus__APTS_Agreement__c' and Name='AAA' Limit 1];
        
        //Creating Test Agreement record
        testAgreement = new Apttus__APTS_Agreement__c (Name='Test Agreement',Apttus__Contract_End_Date__c =System.today()+1, RecordType  = rt, Apttus__Status_Category__c='In Effect', Apttus__Account__c= testAccount.id);
        insert testAgreement;
        
        //Creating Custom setting
        Community_Permission_Set__c setting = new Community_Permission_Set__c();
        setting.Name = 'AAA';
        setting.PermissionSetName__c = 'CSP_Access_OAS_LatAm_Data_Category';
        insert setting;
        
        System.RunAs(usr) {
            String res = null;
            res = CreateCommunityUser.createUserFromContact(testContact.id);
            String s=CreateCommunityUser.resetPasswordUsr(testContact.id);
        }
        
        
    }
    Static testmethod void testContactPermissionSet1(){
        InitializeTestData.createSystemConfigSettings();
        //Creating Test User
        User usr = TestingUtils.ADMIN_USER;
         
        //creating Test Account record
        testAccount = TestingUtils.createAccounts(1, 'Appnexus Test', false)[0];
        testAccount.BillingCity = 'Wayne';
        testAccount.BillingState = 'New Jersey';
        testAccount.BillingPostalCode = '07470';
        testAccount.BillingCountry = 'USA';
        testAccount.Global_rdp__c='EMEA';
        testAccount.Region_rdp__c='DACH';
        testAccount.Support_Level__c ='Foundation';
        testAccount.Owner = usr;
        insert testAccount;
        
        //Creating Test Contact record
        testContact = InitializeTestData.CreateContact(testAccount.id); 
        testContact.LastName='dummy';
        testContact.firstName ='CSP';
        insert testContact;
        
        System.runAs(usr){
        user usr1 = InitializeTestData.createUser();
        usr1.ProfileId=  [SELECT Id FROM Profile WHERE Name = 'Customer Portal User'].Id;
        usr1.ContactId = testContact.id;
        insert usr1;
        }
        
        RecordType rt = [select id,Name from RecordType where SobjectType='Apttus__APTS_Agreement__c' and Name='AAA' Limit 1];
        
        //Creating Test Agreement record
        testAgreement = new Apttus__APTS_Agreement__c (Name='Test Agreement',Apttus__Contract_End_Date__c =System.today()+1, RecordType  = rt, Apttus__Status_Category__c='In Effect', Apttus__Account__c= testAccount.id);
        insert testAgreement;
        
        //Creating Custom setting
        Community_Permission_Set__c setting = new Community_Permission_Set__c();
        setting.Name = 'AAA';
        setting.PermissionSetName__c = 'CSP_Access_OAS_LatAm_Data_Category';
        insert setting;
        
      //  System.RunAs(usr) {
            String res = null;
            res = CreateCommunityUser.createUserFromContact(testContact.id);
            String s=CreateCommunityUser.resetPasswordUsr(testContact.id);
        //}
        
        
    }
    Static testmethod void testContactPermissionSet2(){
         InitializeTestData.createSystemConfigSettings();
        //Creating Test User
        User usr = TestingUtils.ADMIN_USER;
       
        //creating Test Account record
        testAccount = TestingUtils.createAccounts(1, 'Appnexus Test', false)[0];
        testAccount.BillingCity = 'Wayne';
        testAccount.BillingState = 'New Jersey';
        testAccount.BillingPostalCode = '07470';
        testAccount.BillingCountry = 'USA';
        testAccount.Global_rdp__c='EMEA';
        testAccount.Region_rdp__c='DACH';
        testAccount.Support_Level__c ='Foundation';
        //testAccount.Owner = usr;
        insert testAccount;
        
        //Creating Test Contact record
        testContact = InitializeTestData.CreateContactwithEmail(testAccount.id); 
        testContact.LastName='dummy';
        testContact.firstName ='CSP';
        insert testContact;
        
        contact testContact1 = InitializeTestData.CreateContactwithEmail(testAccount.id); 
        insert testContact1;
        
        user usr1 = InitializeTestData.createUser();
        usr1.ProfileId=  [SELECT Id FROM Profile WHERE Name = 'Customer Portal User'].Id;
        usr1.ContactId = testContact.id;
        insert usr1;
        
        RecordType rt = [select id,Name from RecordType where SobjectType='Apttus__APTS_Agreement__c' and Name='AAA' Limit 1];
        
        //Creating Test Agreement record
        testAgreement = new Apttus__APTS_Agreement__c (Name='Test Agreement',Apttus__Contract_End_Date__c =System.today()+1, RecordType  = rt, Apttus__Status_Category__c='In Effect', Apttus__Account__c= testAccount.id);
        insert testAgreement;
        
        //Creating Custom setting
        Community_Permission_Set__c setting = new Community_Permission_Set__c();
        setting.Name = 'AAA';
        setting.PermissionSetName__c = 'CSP_Access_OAS_LatAm_Data_Category';
        insert setting;
        
       // System.RunAs(usr) {
            String res = null;
            res = CreateCommunityUser.createUserFromContact(testContact.id);
            String s=CreateCommunityUser.resetPasswordUsr(testContact.id);
      // }
        
        
    }
     Static testmethod void testContactPermissionSet3(){
         InitializeTestData.createSystemConfigSettings();
        //Creating Test User
        User usr = TestingUtils.ADMIN_USER;
       
        //creating Test Account record
        testAccount = TestingUtils.createAccounts(1, 'Appnexus Test', false)[0];
        testAccount.BillingCity = 'Wayne';
        testAccount.BillingState = 'New Jersey';
        testAccount.BillingPostalCode = '07470';
        testAccount.BillingCountry = 'USA';
        testAccount.Global_rdp__c='EMEA';
        testAccount.Region_rdp__c='DACH';
        testAccount.Support_Level__c ='Foundation';
        //testAccount.Owner = usr;
        insert testAccount;
        
        //Creating Test Contact record
        testContact = InitializeTestData.CreateContactwithEmail(testAccount.id); 
        testContact.LastName='dummy';
        testContact.firstName ='CSP';
        testContact.email = 'testappnexus12345@demo.com'; 
        insert testContact;
        
        contact testContact1 = InitializeTestData.CreateContactwithEmail(testAccount.id); 
        insert testContact1;
        
        user usr1 = InitializeTestData.createUser();
        usr1.username  = 'testappnexus12345@demo.com' ;
        usr1.ProfileId=  [SELECT Id FROM Profile WHERE Name = 'Customer Portal User'].Id;
        usr1.ContactId = testContact.id;
        insert usr1;
        
        RecordType rt = [select id,Name from RecordType where SobjectType='Apttus__APTS_Agreement__c' and Name='AAA' Limit 1];
        
        //Creating Test Agreement record
        testAgreement = new Apttus__APTS_Agreement__c (Name='Test Agreement',Apttus__Contract_End_Date__c =System.today()+1, RecordType  = rt, Apttus__Status_Category__c='In Effect', Apttus__Account__c= testAccount.id);
        insert testAgreement;
        
        //Creating Custom setting
        Community_Permission_Set__c setting = new Community_Permission_Set__c();
        setting.Name = 'AAA';
        setting.PermissionSetName__c = 'CSP_Access_OAS_LatAm_Data_Category';
        insert setting;
        
       // System.RunAs(usr) {
            String res = null;
            res = CreateCommunityUser.createUserFromContact(testContact.id);
            String s=CreateCommunityUser.resetPasswordUsr(testContact.id);
      // }
        
        
    }
}