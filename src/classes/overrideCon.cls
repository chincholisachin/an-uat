public class overrideCon {

    String recordId;
    public Boolean isMember {get; set;}
    public Boolean isInternal {get; set;}
    public Boolean isCustomer {get; set;}
    public Integer atmNumber {get;set;}
    public List<AccountTeamMember> atm {get;set;}
   
    public overrideCon(ApexPages.StandardController controller) {
        //controller.addFields(New List<String>{'recordtypeid'});
        //Account acc = (Account) controller.getRecord();
        Account acc = [select id, recordtypeid from account where id =:controller.getId() limit 1];
        isMember = AccountUtils.isMember().get(acc.recordtypeid);
        isInternal = AccountUtils.isInternal().get(acc.recordtypeid);
        isCustomer = AccountUtils.isCustomer().get(acc.recordtypeid);
        atm = [select id, userid, teammemberrole from accountteammember where accountid =:acc.id]; 
        atmNumber = atm.size();
    }
    
    

    public PageReference redirect() {
        /*      
        Profile p = [select name from Profile where id =:UserInfo.getProfileId()];
        if ('* Account Managers - Pilot Only'.equals(p.name) || '* AEs - Pilot Only'.equals(p.name)){
           PageReference customPage =  Page.acctOverride_AM_Pilot;
           customPage.setRedirect(true);
           customPage.getParameters().put('id', recordId);
           return customPage;
        } else {
            return null;        
        }
        */   
        return null;
    }

}