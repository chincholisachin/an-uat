public class AccountTriggerHandler{

    /* AccountTriggerHandler
    
    copyAccountOwnerToTeam(Map<Id,Account> newAccounts, Map<Id,Account> oldAccounts) - Tested in AccountTriggerTest
        if Accounts are customer accounts, this method copies the account ownerid, and creates an account team member record
        where owner is the account owner. Method also kicks in when account data changes and owner id is updated to something else.
        In this case it deletes the existing "Account Owner" record from team member and creates a new one.
        
    createTeamMembersWhenMemberIsCreated(Map<Id,Account> newAccounts) - Tested in AccountTriggerTest
        If account record that is created (insert only) is a member record, this method copies account team members from
        parent account, and generates new account team member entries which are exclusive to newly generated member accounts.
        method doesn't copy all team members. Instead it only copies the member types which are listed in custom label
        AccountTeamRoleOwner.
        
    propagateAccountTeam(Map<Id,Account> newAccounts) - Tested in AccountTriggerTest
        If account record which is created is a customer account, this method populates the
        sr account owner and commercial leader account team member records. Sr Account owner is the manager of creating user,
        and commercial leader is driven off Market Owner (Team_Owner__c) field mapping. Commercial leader is based on Region and
        Market fields on the account.
        
    inheritFieldsFromParentAccount(List<Account> members) - Tested in AccountTriggerTest
        This makes sure that a member account inherits some of the key fields from parent account.
        In order to add more fields or remove fields to be copied over, modify AccountFieldsToCopy and AccountFieldsToCache custom labels.
        
    setupBillingInfo(List<Account> accounts, Map<Id,Account> oldAccounts) - Tested in AccountTriggerTest
        This field makes sure that member account is inherits seller/buyer invoice address fields, 
        Populates fields related to Infor integration (trimming fields to 50 chars), populate billing address code fields,
        state code fields etc. 
        
    createIntegrationRequest(List<Account> newAccounts, Map<Id,Account> oldAccounts) - Tested in AccountTriggerTest
        Creates integration request record when account manager or audit tier changes.
        
    deactivateCustomerPortal(List<Account> accounts, Map<Id,Account> oldAccounts) - Not tested yet
        If customer postal flag of an account is turned false from true, it gathers all account ids and send it to future method
        updateCSPContacts(Set<Id> AccIds) for later processing.
    
    updateCSPContacts(Set<Id> AccIds) - not tested yet
        Future method: takes all account ids, queries all related contacts for those accounts and cut their portal access.
        
    createAssetsWhenMemberIsCreated(List<Account> accounts) - 
        When a new member account is created, and opportunity is tied to it, a new asset record is created.
        
    */ 
    
    
    public static void dontOverRideNullFieldsDuringConversion(List<Account> newAccounts,Map<Id,Account> oldAccounts){
        
        if (AccountUtils.leadIsBeingConverted) {
            List<String> strList = new List<String>();
            Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap();
            strList = new List<String>(objectFields.keySet());          
            for (Account a: newAccounts){
                for (String s: strList){
                    if (a.get(s) != oldAccounts.get(a.id).get(s) && oldAccounts.get(a.id).get(s) == null) a.put(s,null);
                }
            }
        }
        
    }
    
    public static void createIntegrationRequest(List<Account> newAccounts, Map<Id,Account> oldAccounts){
        Set<String> integrationType = new Set<String>();
        Set<String> accountIdsToQuery = new Set<String>();
        for (Account a: newAccounts){
            if( AccountUtils.isMember().containsKey(a.recordTypeId) && !AccountUtils.isMember().get(a.recordTypeId) && ((a.Account_Tier__c != null && oldAccounts.get(a.id).Account_Tier__c != a.Account_Tier__c) || (oldAccounts.get(a.id).ownerid != a.ownerid))) {
                accountIdsToQuery.add(a.id);
                if (a.Account_Tier__c != null && oldAccounts.get(a.id).Account_Tier__c != a.Account_Tier__c) integrationType.add(a.id+'-AuditTier');
                if (oldAccounts.get(a.id).ownerid != a.ownerid) integrationType.add(a.id+'-AccountManager');
            }
        }
        if (accountIdsToQuery.size()>0){
            List<Account> parentChild = [select id, (select id from ChildAccounts where recordtype.name = 'Console Member') from account where id in :accountIdsToQuery ];
            List<Integration_Request__c> irList = new List<Integration_Request__c>();
            for (Account acn: parentChild){
                if (acn.ChildAccounts.size()>0){
                    if (integrationType.contains(acn.id+'-AuditTier')){
                        irList.add(new Integration_Request__c(integration_object__c = 'Account',
                                                              integration_record__c = acn.id,
                                                              integration_type__c = 'Audit Tier',
                                                              success__c = 'Pending'));
                    } 
                    
                    if (integrationType.contains(acn.id+'-AccountManager')){
                        irList.add(new Integration_Request__c(integration_object__c = 'Account',
                                                              integration_record__c = acn.id,
                                                              integration_type__c = 'Account Manager',
                                                              success__c = 'Pending'));
                    }
                }
            }
        if (irList.size()>0) Database.insert(irList,false);
        }
    }
    
    
    public static void copyAccountOwnerToTeam(Map<Id,Account> newAccounts, Map<Id,Account> oldAccounts){
   
        List<AccountTeamMember> newAtm = new List<AccountTeamMember>();
        List<AccountTeamMember> existingAtm = new List<AccountTeamMember>();
        List<Id> changingAccounts = new List<Id>();
              
        for (Account acn : newAccounts.values()){
            if(AccountUtils.isCustomer().containsKey(acn.recordtypeid)){
            if (AccountUtils.isCustomer().get(acn.recordtypeid)){
                if (oldAccounts == null){
                    AccountTeamMember atm = AccountUtils.generateAccountTeamMember(acn.id,acn.ownerid,AccountUtils.ACCOUNT_OWNER);
                    newAtm.add(atm);
                } else {             
                    if(newAccounts.get(acn.id).ownerid != oldAccounts.get(acn.id).ownerid){        
                        changingAccounts.add(acn.id);
                        AccountTeamMember atm = AccountUtils.generateAccountTeamMember(acn.id,acn.ownerid,AccountUtils.ACCOUNT_OWNER);
                        newAtm.add(atm);
                    }
                }
            }
            }
        }
        
        if (changingAccounts.size()>0){
            existingAtm = [select id, accountid, userid from AccountTeamMember where accountid in:changingAccounts and teammemberrole = :AccountUtils.ACCOUNT_OWNER];
            if (existingAtm.size() > 0) delete existingAtm;
        }
        
        if ( newAtm.size() > 0 ) Insert newAtm;
    
    }
    
    public static void createTeamMembersWhenMemberIsCreated(Map<Id,Account> newAccounts){
    
        List<Id> parentAccountIds = new List<Id>();
        Map <Id, List<AccountTeamMember>> parentAccountTeamMembers = new Map<Id, List<AccountTeamMember>>();
        String[] teamMembersToCopy = new List<String>(Label.AccountTeamRoleOwner.split('\r\n'));
        List<AccountTeamMember> atmNew = new List<AccountTeamMember>();
        for (Account acn : newAccounts.values()){
            if (AccountUtils.isMember().containsKey(acn.recordtypeid)){
                if (AccountUtils.isMember().get(acn.recordtypeid)){
                   if(acn.parentid != null) parentAccountIds.add(acn.parentid);
                }
            }
        }
        if (parentAccountIds.size() > 0){
            for (Account acn: [select  id, (select AccountAccessLevel, 
                                                   AccountId, 
                                                   CaseAccessLevel, 
                                                   ContactAccessLevel, 
                                                   OpportunityAccessLevel,
                                                   TeamMemberRole, UserId
                                                   from AccountTeamMembers
                                                   where TeamMemberRole in: teamMembersToCopy ) from account where id in: parentAccountIds]){
            parentAccountTeamMembers.put(acn.id, acn.AccountTeamMembers);
            }     
        }
        for (Account acn : newAccounts.values()){
            if (AccountUtils.isMember().containsKey(acn.recordtypeid)) {
            if (AccountUtils.isMember().get(acn.recordtypeid)){
               if(parentAccountTeamMembers.containsKey(acn.parentid)) {
                   for (AccountTeamMember a: parentAccountTeamMembers.get(acn.parentid)){ 
                       AccountTeamMember atm = AccountUtils.generateAccountTeamMember(acn.id,a.userid,a.TeamMemberRole);
                       atmNew.add(atm);
                   }
               }
            }
            }
        }
        if (atmNew.size() > 0) Database.insert (atmNew,false);
      
    }
    
    public static void propagateAccountTeam(Map<Id,Account> newAccounts){
       
        List<AccountTeamMember> atmNew = new List<AccountTeamMember>();
        Set<Id> ownerIds = new Set<Id>();
        List<Id> userIds = new List<Id>();
        for (Account acn : newAccounts.values()){
            if (AccountUtils.isCustomer().containsKey(acn.recordtypeid) && AccountUtils.isCustomer().get(acn.recordtypeid)){
                ownerIds.add(acn.ownerid);
            }
        }
        userIds.addAll(ownerIds);  
        
        //this section defaults all owner related team members to their default position. any logic change here should be done here.
        //current stata, based on region and vertical information, commercial leader is pulled from team_owner__c object.
        //sr account owner is the drt manager of account manager. 
        
        for (Account acn : newAccounts.values()){
            if (AccountUtils.isCustomer().containsKey(acn.recordtypeid)){
                if (AccountUtils.commercialLeaderMap().get(acn.Region_rdp__c + ' - ' + acn.Vertical_New__c) != null) atmNew.add(AccountUtils.generateAccountTeamMember(acn.id, AccountUtils.commercialLeaderMap().get(acn.Region_rdp__c + ' - ' + acn.Vertical_New__c),AccountUtils.COMMERCIAL_LEADER));
                if (AccountUtils.userMap(userIds).get(acn.ownerid) != null && AccountUtils.userMap(userIds).get(acn.ownerid).managerid != null) atmNew.add(AccountUtils.generateAccountTeamMember(acn.id, AccountUtils.userMap(userIds).get(acn.ownerid).managerid, AccountUtils.SR_ACCOUNT_OWNER));
            }
        }
        
        if (atmNew.size() > 0) Database.insert (atmNew, false);
   
    }
    
    public static void inheritFieldsFromParentAccount(List<Account> members){
        
        Map<Id,Account> accountMap = new Map<Id,Account>();
        
        accountMap = AccountUtils.fetchAccDetails(members);
        
        system.debug('accountMap value is '+accountMap);
        
        if (accountMap != null && accountMap.size() > 0){
        // In order to add new fields to be copied to a member during creation, just add field api names to AccountFieldsToCopy and AccountFieldsToCache custom labels. 
            for(Account member : members){
                if(member.parentId != null && AccountUtils.isMember().containsKey(member.recordtypeid)&& AccountUtils.isMember().get(member.recordtypeid)){          
                    for (String s: Label.AccountFieldsToCopy.split('\r\n')){
                        if (member.get(s) == null) member.put(s,accountmap.get(member.parentId).get(s));
                    }        
                }
            }
        }
    }
    
    public static void setupBillingInfo(List<Account> accounts, Map<Id,Account> oldAccounts){
    
        Map<String, CountryISOCodes__c> countryISO = CountryISOCodes__c.getAll();
        Map<String, StateCodes__c> stateCodes = StateCodes__c.getAll();       
        Map<Id,Account> accountMap = AccountUtils.fetchAccDetails(accounts);
        for (Account member: accounts){
            if(AccountUtils.isMember().containsKey(member.recordTypeId)) {
            if(AccountUtils.isMember().get(member.recordTypeId)) {
            
                // when it is a member record and isInsert OR isUpdate and Copy_from_Account_Seller__c is true.
                if(oldAccounts == null || (oldAccounts != null && member.Copy_from_Account_Seller__c)){ 
                    member.Seller_Address_Line__c = accountMap.get(member.ParentId).BillingStreet;
                    member.Seller_Town_City__c = accountMap.get(member.ParentId).BillingCity;
                    member.Seller_State__c = accountMap.get(member.ParentId).BillingState;
                    member.Seller_Postal_Code__c = accountMap.get(member.ParentId).BillingPostalCode;
                    member.Seller_Country__c = accountMap.get(member.ParentId).BillingCountry;
                    member.Seller_Phone__c = accountMap.get(member.ParentId).phone;   
                    member.copy_from_account_seller__c = false;
                }
                
                // when it is a member record and isInsert OR isUpdate and Copy_from_Account__c is true.
                if(oldAccounts == null || (oldAccounts != null && member.Copy_from_Account__c)){ 
                         
                    member.BillingStreet = accountMap.get(member.ParentId).BillingStreet;
                    member.BillingCity = accountMap.get(member.ParentId).BillingCity;
                    member.BillingState = accountMap.get(member.ParentId).BillingState;
                    member.BillingPostalCode = accountMap.get(member.ParentId).BillingPostalCode;
                    member.BillingCountry = accountMap.get(member.ParentId).BillingCountry;   
                    member.Phone = accountMap.get(member.ParentId).phone;
                    member.copy_from_account__c = false;           
                }
                
                // if this is insert or update & billing street information has changed populates infor address fields.
                if(oldAccounts == null || (oldAccounts != null && member.billingstreet != oldAccounts.get(member.id).billingstreet )){ 
                
                    if (member.billingStreet != null){
                    
                        // resets the fields in case this is a update. We don't want any left over information
                        member.Info_Address_1__c = '';
                        member.Info_Address_2__c = '';
                        member.Info_Address_3__c = '';
                        
                        // splits the street name and inserts them to infor address fields where these if a 50 char length limitation.
                        String[] linesOfAddress = member.billingStreet.split('\r\n');
                        if (linesOfAddress.size() == 1) member.billingStreet.split('\n');
                        String address = '';
                        String stringLeftOver = '';
                        integer lineCnt = 0;
                        for(String line : linesOfAddress){
                            if( lineCnt == 0 ) if (line.length() > 50) {
                                                   member.Info_Address_1__c = line.substring(0,49)+'-';
                                                   stringLeftOver = line.substring(49,line.length());
                                               } else {
                                                   member.Info_Address_1__c = line;
                                               }
                            if( lineCnt == 1 ) if (!stringLeftOver.equals('')){
                                                   //line = stringLeftOver + ' ' + line;
                                                   stringLeftOver = stringLeftOver + ' ' + line;
                                                   if (stringLeftOver.length() > 50) {
                                                       member.Info_Address_2__c = stringLeftOver.substring(0,49)+'-';
                                                       String temp = stringLeftOver.substring(49,stringLeftOver.length());
                                                       stringLeftOver = '';
                                                       stringLeftOver = temp;
                                                   } else {
                                                       member.Info_Address_2__c = stringLeftOver;
                                                       stringLeftOver = '';                                                   }                                
                                               } else {
                                                   if (line.length() > 50) {
                                                       member.Info_Address_2__c = line.substring(0,49)+'-';
                                                       stringLeftOver = line.substring(49,line.length());
                                                   } else {
                                                       member.Info_Address_2__c = line;
                                                   }
                                               }
                            if (lineCnt == 2 ) if (!stringLeftOver.equals('')){
                                                   stringLeftOver = stringLeftOver + ' ' +line;
                                               } else {
                                                   stringLeftOver = line;
                                               }
                            if( lineCnt > 2 ) stringLeftOver = stringLeftOver + ',' + line;
                            lineCnt++; 
                        }                    
                        
                        if (!stringLeftOver.equals('')) member.Info_Address_3__c = stringLeftOver.left(50);                      
                    }
                }
                
                // splits the street name and inserts them to infor seller address fields where these if a 50 char length limitation.
                if(oldAccounts == null || (oldAccounts != null && member.Seller_Address_Line__c != oldAccounts.get(member.id).Seller_Address_Line__c )){ 
                
                    if (member.Seller_Address_Line__c != null){
                    
                        // resets the fields in case this is a update. We don't want any left over information
                        member.Infor_Seller_Address_1__c = '';
                        member.Infor_Seller_Address_2__c = '';
                        member.Infor_Seller_Address_3__c = '';
                        
                        // splits the street name and inserts them to infor address fields where these if a 50 char length limitation.
                        String[] linesOfAddress = member.Seller_Address_Line__c.split('\r\n');
                        if (linesOfAddress.size() == 1) member.Seller_Address_Line__c.split('\n');
                        String address = '';
                        String stringLeftOver = '';
                        integer lineCnt = 0;
                        for(String line : linesOfAddress){
                            if( lineCnt == 0 ) if (line.length() > 50) {
                                                   member.Infor_Seller_Address_1__c = line.substring(0,49)+'-';
                                                   stringLeftOver = line.substring(49,line.length());
                                               } else {
                                                   member.Infor_Seller_Address_1__c = line;
                                               }
                            if( lineCnt == 1 ) if (!stringLeftOver.equals('')){
                                                   line = stringLeftOver + ' ' + line;
                                                   if (line.length() > 50) {
                                                       member.Infor_Seller_Address_2__c = line.substring(0,49)+'-';
                                                       stringLeftOver = '';
                                                       stringLeftOver = line.substring(49,line.length());
                                                   } else {
                                                       member.Infor_Seller_Address_2__c = line;
                                                       stringLeftOver = '';
                                                   }                                
                                               } else {
                                                   if (line.length() > 50) {
                                                       member.Infor_Seller_Address_2__c = line.substring(0,49)+'-';
                                                       stringLeftOver = line.substring(49,line.length());
                                                   } else {
                                                       member.Infor_Seller_Address_2__c = line;
                                                   }
                                               }
                            if (lineCnt == 2 ) if (!stringLeftOver.equals('')){
                                                   stringLeftOver = stringLeftOver + ' ' + line;
                                               } else {
                                                   stringLeftOver = line;
                                               }
                            if( lineCnt > 2 ) stringLeftOver = stringLeftOver + ',' + line;
                            lineCnt++; 
                        }                    
                        
                        if (!stringLeftOver.equals('')) member.Infor_Seller_Address_3__c = stringLeftOver.left(50);                      
                    }
                }
                               
                member.Country_Code__c = (member.BillingCountry != null && CountryISO.get(member.BillingCountry.toUpperCase()) != null) ? countryISO.get(member.BillingCountry.toUpperCase()).ISO3__c : null;
                member.State_Code__c = (member.BillingState != null && stateCodes.get(member.BillingState.toUpperCase()) != null) ? stateCodes.get(member.BillingState.toUpperCase()).StateCd__c : null;
                member.Seller_Country_Code__c = (member.Seller_Country__c != null && CountryISO.get(member.Seller_Country__c.toUpperCase()) != null) ? countryISO.get(member.Seller_Country__c.toUpperCase()).ISO3__c : null;
                member.Seller_State_Code__c = (member.Seller_State__c != null && stateCodes.get(member.Seller_State__c.toUpperCase()) != null) ? stateCodes.get(member.Seller_State__c.toUpperCase()).StateCd__c : null;              
                if(oldAccounts == null && member.Invoice_Member_Code__c != null) member.Address_Code__c = 'A' + member.Invoice_Member_Code__c;
                if(member.Invoice_Member_Code__c != null && member.Seller_Address_Line__c != null) member.Seller_Address_Code__c = 'AS' + member.Invoice_Member_Code__c.substring(1);             
            }
            }
        }
    }
   
    public static void deactivateCustomerPortal(List<Account> accounts, Map<Id,Account> oldAccounts){
        
        Set<Id> AccIds = new Set<Id>();
        for(Account tmpAcc : accounts){
            if(tmpAcc.IsCustomerPortal == false && oldAccounts.get(tmpAcc.id).IsCustomerPortal == true && AccountUtils.isCustomer().containsKey(tmpAcc.recordTypeId)&& AccountUtils.isCustomer().get(tmpAcc.recordTypeId)){
                AccIds.add(tmpAcc.id);
            }
        }
        
        if (AccIds.size() > 0) updateCSPContacts(AccIds);

    }
    
    
    public static void createAssetsWhenMemberIsCreated(List<Account> accounts){
        
        List<String> oppIds = new List<String>();
        Map<String,String> oppToMember = new Map<String,String>();
        
        for (Account member: accounts){
            
            if(AccountUtils.isMember().containsKey(member.recordTypeId)&&AccountUtils.isMember().get(member.recordTypeId)) {
                if (member.opportunity__c != null) {
                    oppIds.add(member.Opportunity__c);
                    try{
                        oppToMember.put(member.opportunity__c, member.id);
                    } catch (exception e){
                        System.debug('Opportunity is linked to multiple members');
                    }
                }
            }
            
        }
        
        if (oppIds.size() > 0){
            OpportunityLineItem[] OLI = [Select UnitPrice, Quantity, PricebookEntry.Product2Id, PricebookEntry.Product2.Name, Description, Converted_to_Asset__c, opportunity.closeDate  
                                          From OpportunityLineItem 
                                          where OpportunityId in :oppIds and Converted_to_Asset__c = false];
            Asset[] ast = new Asset[]{};
            
            for(OpportunityLineItem ol: OLI){
                Asset a = new Asset();
                a.AccountId = oppToMember.get(ol.opportunityid);
                a.Product2Id = ol.PricebookEntry.Product2Id;
                a.Quantity = ol.Quantity;
                a.Price =  ol.UnitPrice;
                a.PurchaseDate = ol.opportunity.closedate;
                a.Status = 'Purchased';
                a.Description = ol.Description;
                a.Name = ol.PricebookEntry.Product2.Name;
                ast.add(a);
                ol.Converted_to_Asset__c = true;
            }
            update OLI; 
            insert ast;
        }
        
    }
    
    @future
    public static void updateCSPContacts(Set<Id> AccIds){
    
        List<Contact> uptCspContacts = new List<Contact>();
        for(Contact updtCont : [select id, Customer_Portal_User__c from contact where Customer_Portal_User__c = true and Accountid in :AccIds]){
            updtCont.Customer_Portal_User__c = false;
            uptCspContacts.add(updtCont);
        }
        update uptCspContacts;
    
    }
   
   /*
    public static void insertInvoiceContact( List<AppNexus_Member__c> appNexMembers ){
        String cloudRecType = '';
        String consoleRecType = '';
        if( HardCodeIds__c.getValues('CloudMemberRecType') != null && HardCodeIds__c.getValues('CloudMemberRecType').value__c != null) {
            cloudRecType = HardCodeIds__c.getValues('CloudMemberRecType').value__c;
        }else{
            cloudRecType =[select id from recordtype where name= 'Cloud Member Contact'].id;
        }
        if( HardCodeIds__c.getValues('consoleRecType') != null && HardCodeIds__c.getValues('consoleRecType').value__c != null) {
            consoleRecType = HardCodeIds__c.getValues('consoleRecType').value__c;
        }else{
            consoleRecType = [select id from recordtype where name= 'Console Member Contact'].id;
        }

        // Loop through all member and check if Related Agreement exist
        
        List<Member_Contact__c> memContact = new List<Member_Contact__c>();
        List<AppNexus_Member__c> targetMembers = new List<AppNexus_Member__c> ();
        Set<Id> memberIds = new Set<Id>();
        for (AppNexus_Member__c am : appNexMembers) {
            if (am.Related_Agreement__c != null) {
                targetMembers.add (am);
                memberIds.add (am.Id);
            }
        }
        
        if (memberIds.size() > 0) {
            for (AppNexus_Member__c am : [select id, Name, AppNexus_Member_ID__c, Related_Agreement__c, Related_Agreement__r.Apttus__Parent_Agreement__c, Related_Agreement__r.Apttus__Parent_Agreement__r.Billing_Contact_Lookup__c from AppNexus_Member__c where Id in :memberIds]){
                String recordId = consoleRecType;
                Id billingContactId;
                if(am.AppNexus_Member_ID__c == null) 
                     recordId = cloudRecType;
                     
                billingContactId = am.Related_Agreement__r.Apttus__Parent_Agreement__r.Billing_Contact_Lookup__c;
                if (billingContactId != null) {                     
                    Member_Contact__c invCont = new Member_Contact__c(Contact__c = billingContactId, AppNexus_Member__c = am.id, Type_of_Contact__c = 'Invoice', Include_for_Invoicing__c = true, recordtypeid = recordId);
                    memContact.add(invCont);
                }
            }
        }

        if(memContact.size() >  0)
            insert memContact;
    }
    
    */
    
}