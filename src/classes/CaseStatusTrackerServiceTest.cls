@isTest
private class CaseStatusTrackerServiceTest
{
    static Account acc;
    static list<Outage_Notifications__c> outNoList;
    static Case_Weight_Settings__c cwSetting;
    static Case eachCase;
    static Contact con;
    static list<HardCodeIds__c> hdList;
    static Apttus__APTS_Agreement__c eachAgr;
    static AccountTeamMember am;
    public static string AgreID;
    public static Bypass__c bypassSetting;

    private static void setup()
    {
        acc = InitializeTestData.createAccount();
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
        insert acc;

        outNoList = InitializeTestData.createOutageNotifications(acc.ID);
        insert outNoList;

        cwSetting = InitializeTestData.createCaseWeightSettings();

        insert cwSetting;

        con = InitializeTestData.CreateContact(acc.ID);
        insert con;

        InitializeTestData.createSystemConfigSettings();
         hdList = InitializeTestData.createHardCodeCustomSettings();
         insert hdList;
        Entitlement ent = InitializeTestData.createEntitlement(acc.ID);
        insert ent;

        InitializeTestData.createEmAsSettings();
        bypassSetting = InitializeTestData.createBypassSetting();
        insert bypassSetting ;
    }
    public static Case_Status_Tracker__c cst = new Case_Status_Tracker__c();
    private static testmethod void updateAgeByBusinessHoursTest(){
        setup();
        
        eachCase = new Case();
        eachCase = InitializeTestData.CreateCase(con.ID,acc.Id);
        insert eachCase;
        cst = new Case_Status_Tracker__c(Case_Number__c =eachCase.ID, Start_Time__c=DateTime.Now()-1,Status__c='New');
        insert cst;
        //--Hack
        CaseStatusTrackerService.updateAgeByBusinessHours(new list<Case_Status_Tracker__c>{cst});
        
        CheckRecursion.Run_CaseStatusTrackerBeforeUpdate = true;
        Case_Status_Tracker__c cst1 = new Case_Status_Tracker__c();
        cst1 = [SELECT id,End_Time__c from Case_Status_Tracker__c where id=:cst.ID];
        cst1.End_Time__c = Datetime.Now();
        system.debug('--updating cst--');
        update cst1;    
        
    }
}