public with sharing class RestPublisher {
     
    public class Payload {
        @InvocableVariable(label='* REST Endpoint')
        public String endPoint;
        @InvocableVariable(label='* Session Id')
        public String sessionId;
        @InvocableVariable(label='* Linked Record ID')
        public String recordId;
        @InvocableVariable(label='* Enterprise URL')
        public String enterpriseURL;
        @InvocableVariable(label='* sObjectType')
        public String sObjectType;
        @InvocableVariable(label='Field 1 Name')
        public String field1Name;
        @InvocableVariable(label='Field 1 Value')
        public String field1Value;
        @InvocableVariable(label='Field 2 Name')
        public String field2Name;
        @InvocableVariable(label='Field 2 Value')
        public String field2Value;
        @InvocableVariable(label='Field 3 Name')
        public String field3Name;
        @InvocableVariable(label='Field 3 Value')
        public String field3Value;
        @InvocableVariable(label='Field 4 Name')
        public String field4Name;
        @InvocableVariable(label='Field 4 Value')
        public String field4Value;      
        @InvocableVariable(label='Field 5 Name')
        public String field5Name;
        @InvocableVariable(label='Field 5 Value')
        public String field5Value;
        @InvocableVariable(label='Field 6 Name')
        public String field6Name;
        @InvocableVariable(label='Field 6 Value')
        public String field6Value;
        @InvocableVariable(label='Field 7 Name')
        public String field7Name;
        @InvocableVariable(label='Field 7 Value')
        public String field7Value;
        @InvocableVariable(label='Field 8 Name')
        public String field8Name;
        @InvocableVariable(label='Field 8 Value')
        public String field8Value;
        @InvocableVariable(label='Field 9 Name')
        public String field9Name;
        @InvocableVariable(label='Field 9 Value')
        public String field9Value;
        @InvocableVariable(label='Field 10 Name')
        public String field10Name;
        @InvocableVariable(label='Field 10 Value')
        public String field10Value;
    }
     
    @InvocableMethod(label='REST Publisher')
    
    public static void postToREST(List<Payload> payloads) {

        String endPoint = payloads[0].endPoint;
        String sessionId = payloads[0].sessionId;
        String enterpriseURL = payloads[0].enterpriseURL;
        String sObjectType = payloads[0].sObjectType;
        
        List<Map<String,Object>> listOfMaps = new List<Map<String,Object>>();

        for (Payload p: payloads){

            Map<String,Object> msg = new Map<String,Object>();
        
            if(!String.isBlank(p.recordId)) {
                msg.put('Id',p.recordId);
            }

            if(!String.isBlank(p.field1Name) && !String.isBlank(p.field1Value)) {
                msg.put(p.field1Name,p.field1Value);
            }
            if(!String.isBlank(p.field2Name) && !String.isBlank(p.field2Value)) {
                msg.put(p.field2Name,p.field2Value);
            }
            if(!String.isBlank(p.field3Name) && !String.isBlank(p.field3Value)) {
                msg.put(p.field3Name,p.field3Value);
            }
            if(!String.isBlank(p.field4Name) && !String.isBlank(p.field4Value)) {
                msg.put(p.field4Name,p.field4Value);
            }
            
            if(!String.isBlank(p.field5Name) && !String.isBlank(p.field5Value)) {
                msg.put(p.field5Name,p.field5Value);
            }
            if(!String.isBlank(p.field6Name) && !String.isBlank(p.field6Value)) {
                msg.put(p.field6Name,p.field6Value);
            }
            if(!String.isBlank(p.field7Name) && !String.isBlank(p.field7Value)) {
                msg.put(p.field7Name,p.field7Value);
            }
            if(!String.isBlank(p.field8Name) && !String.isBlank(p.field8Value)) {
                msg.put(p.field8Name,p.field8Value);
            }
                
            if(!String.isBlank(p.field9Name) && !String.isBlank(p.field9Value)) {
                msg.put(p.field9Name,p.field9Value);
            }
            if(!String.isBlank(p.field10Name) && !String.isBlank(p.field10Value)) {
                msg.put(p.field10Name,p.field10Value);
            }

            listOfMaps.add(msg);

        }
        
        Map<String,List<Map<String,Object>>> finalJSON = new Map<String,List<Map<String,Object>>>();
        
        finalJSON.put(sObjectType,listOfMaps);
         
        String mainbody = JSON.serialize(finalJSON);      

        System.enqueueJob(new QueueableRestCall(endPoint, 'POST', mainbody, sessionId, enterpriseURL));
    }
     
    public class QueueableRestCall implements System.Queueable, Database.AllowsCallouts {
        private final String url;
        private final String method;
        private final String body;
        private final String sessionId;
        private final String enterpriseURL;
        
         
        public QueueableRestCall(String url, String method, String body, String sessionId, String enterpriseURL) {
            this.url = url;
            this.method = method;
            this.body = body;
            this.sessionId = sessionId;
            this.enterpriseURL = enterpriseURL;
        }
         
        public void execute(System.QueueableContext ctx) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(url);
            req.setMethod(method);
            req.setBody(body);
            req.setHeader('OrganizationId',UserInfo.getOrganizationId());
            req.setHeader('EnterpriseURL', enterpriseURL);
            req.setHeader('SessionId',sessionId);
            req.setHeader('UserEmail',UserInfo.getUserEmail());
            Http http = new Http();
            if(!Test.isRunningTest()) {
                HttpResponse res = http.send(req);
            }
        }
    }    
}