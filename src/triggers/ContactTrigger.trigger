trigger ContactTrigger on Contact (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

    Bypass__c bypass = Bypass__c.getInstance();
    if (bypass.Trigger_Bypass__c) return; 
    
    if (Trigger.isBefore){
           if(Trigger.isInsert) contactTriggerHandler.createUserRelationship(Trigger.new);
    }

}