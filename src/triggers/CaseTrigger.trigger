trigger CaseTrigger on Case (before insert,before update,after insert,after update) {

    Bypass__c bypass = Bypass__c.getInstance();
    if (bypass.Trigger_Bypass__c) return;

    CaseTriggerHandler instance = CaseTriggerHandler.getInstance();

    try{
        if(System_Configs__c.getValues('Case').Is_Run__c) { // Controls the trigger from custom setting.
                
                if(Trigger.isBefore) {  
                    if(Trigger.isInsert) {
                        if(CheckRecursion.checkCaseRecursion(CONSTANTS.BEFORE_INSERT)){
                            instance.beforeInsertMethod(Trigger.new,Trigger.NewMap);
                        }
                        
                    }
                    if(Trigger.isUpdate){
                        if(CheckRecursion.checkCaseRecursion(CONSTANTS.BEFORE_UPDATE)){
                            instance.beforeUpdateMethod(Trigger.new,Trigger.old,Trigger.NewMap,Trigger.oldMap);
                        }
                    }
                }
                
                if(Trigger.isAfter) {
                    if(Trigger.isInsert) {
                        if(CheckRecursion.checkCaseRecursion(CONSTANTS.AFTER_INSERT)){
                            instance.afterInsertMethod(Trigger.new,Trigger.NewMap);
                        }
                    }
                    if(Trigger.isUpdate) {  
                        if(CheckRecursion.checkCaseRecursion(CONSTANTS.AFTER_UPDATE)){                 
                            instance.afterUpdateMethod(Trigger.new,Trigger.old,Trigger.NewMap,Trigger.oldMap);
                        }
                    }
                }
            }
        }catch(Exception e){
            system.debug('--'+e.getMessage());
        }        
}