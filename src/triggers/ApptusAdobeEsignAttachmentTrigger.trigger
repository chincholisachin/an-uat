trigger ApptusAdobeEsignAttachmentTrigger on Attachment(before insert,after insert,before update,after update) {
    AttachmentTriggerHandler instance = AttachmentTriggerHandler.getInstance();
    Apttus_Echosign__ApttusAdobeESignatureProperties__c AEA = Apttus_EchoSign__ApttusAdobeESignatureProperties__c.getInstance(CONSTANTS.Apttus_System_Properties);
    if(AEA != null && AEA.Apttus_EchoSign__UpdateRelatedAgreements__c) {
        if(Trigger.isBefore) { 
            if(Trigger.isInsert) {
                if(CheckRecursion.checkAttachmentRecursion(CONSTANTS.BEFORE_INSERT)){
                    instance.beforeInsertMethod(Trigger.new,Trigger.NewMap);
                }
            }
            if(Trigger.isUpdate){
                if(CheckRecursion.checkAttachmentRecursion(CONSTANTS.BEFORE_UPDATE)){
                    instance.beforeUpdateMethod(Trigger.new,Trigger.old,Trigger.NewMap,Trigger.oldMap);
                }
            }
        }
        
        if(Trigger.isAfter) { 
            if(Trigger.isInsert) {
                if(CheckRecursion.checkAttachmentRecursion(CONSTANTS.AFTER_INSERT)){
                    instance.afterInsertMethod(Trigger.New,Trigger.newMap);
                }
            }
            if(Trigger.isUpdate) {
                if(CheckRecursion.checkAttachmentRecursion(CONSTANTS.AFTER_UPDATE)){
                    instance.afterUpdateMethod(Trigger.New,Trigger.old,Trigger.newMap,Trigger.oldMap);
                    
                }
            }
        } 
    }
}