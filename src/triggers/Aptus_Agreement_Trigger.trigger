trigger Aptus_Agreement_Trigger on Apttus__APTS_Agreement__c (before insert,after insert,before update,after update) {
    AgreementTriggerHandler instance = AgreementTriggerHandler.getInstance();

    if(Trigger.isBefore) { 
        if(Trigger.isInsert) {
            if(CheckRecursion.checkAgreementRecursion(CONSTANTS.BEFORE_INSERT)){
                instance.beforeInsertMethod(Trigger.new,Trigger.NewMap);
            }
        }
        if(Trigger.isUpdate) {
            if(CheckRecursion.checkAgreementRecursion(CONSTANTS.BEFORE_UPDATE)){
                instance.beforeUpdateMethod(Trigger.new,Trigger.old,Trigger.NewMap,Trigger.oldMap);
            }
        }
    }
    
    if(Trigger.isAfter) { 
        if(Trigger.isInsert) {
            if(CheckRecursion.checkAgreementRecursion(CONSTANTS.AFTER_INSERT)){
                instance.afterInsertMethod(Trigger.New,Trigger.newMap);
            }
        }
        if(Trigger.isUpdate) {
             if(CheckRecursion.checkAgreementRecursion(CONSTANTS.AFTER_UPDATE)){
                instance.afterUpdateMethod(Trigger.New,Trigger.old,Trigger.newMap,Trigger.oldMap);
                
            }
        }
    }   
    
}