trigger CaseStatusTrackerTrigger on Case_Status_Tracker__c (before insert) {
    
    Bypass__c bypass = Bypass__c.getInstance();
    if (bypass.Trigger_Bypass__c) return;
    
    CaseStatusTrackerTriggerHandler instance = CaseStatusTrackerTriggerHandler.getInstance();


    if(System_Configs__c.getValues('CaseStatusTracker').Is_Run__c){
        if(Trigger.isBefore) {  
            if(Trigger.isInsert) {
                if(CheckRecursion.checkCaseStatusTrackerRecursion(CONSTANTS.BEFORE_INSERT)){
                    //instance.beforeInsertMethod(Trigger.New,Trigger.NewMap);
                }   
            }
            if(Trigger.isUpdate){
                if(CheckRecursion.checkCaseStatusTrackerRecursion(CONSTANTS.BEFORE_UPDATE)){
                    instance.beforeUpdateMethod(Trigger.new,Trigger.old,Trigger.NewMap,Trigger.oldMap);
                }
                
            }
        }

        if(Trigger.isAfter){
            if(Trigger.isInsert){
                //instance.afterInsertMethod(Trigger.new,Trigger.NewMap);
            }
            if(Trigger.isUpdate){                 
                //instance.afterUpdateMethod(Trigger.new,Trigger.old,Trigger.NewMap,Trigger.oldMap);
            }
        }
    }
}