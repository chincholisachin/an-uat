trigger UserTrigger on User (before Insert, before update, before delete, after insert, after update) {

    if (Trigger.isBefore){

    } else if (Trigger.isAfter){
    
        if (Trigger.isUpdate) {
            UserTriggerHandler.resetPermissionSets(Trigger.new, Trigger.OldMap); // added to reset all perm sets when profiles change. 
            // UserTriggerHandler.updateContactWhenUserChanges(Trigger.new, Trigger.OldMap); //if one of the key attributes of the user changes, this updates the relevant contact.
        }
        // if (Trigger.isInsert) UserTriggerHandler.createUserContact(Trigger.new); //creates user's contact record when a user is inserted.
    
    }

}