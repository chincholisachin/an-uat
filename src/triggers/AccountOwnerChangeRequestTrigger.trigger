trigger AccountOwnerChangeRequestTrigger on Account_Owner_Change_Request__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    Bypass__c bypass = Bypass__c.getInstance();
    if (bypass.Trigger_Bypass__c) return; 
    
    if (Trigger.isAfter && Trigger.isUpdate){
    
        AccountOwnerChangeRequestTriggerHandler.updateAccountOwnerShip(Trigger.new, Trigger.oldMap);
    
    }
    

}