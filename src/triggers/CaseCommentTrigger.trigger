trigger CaseCommentTrigger on CaseComment (before insert) {
    
    Bypass__c bypass = Bypass__c.getInstance();
    if (bypass.Trigger_Bypass__c) return;
    
    CaseCommentTriggerHandler instance = CaseCommentTriggerHandler.getInstance();
    
    if(System_Configs__c.getValues('CaseComment').Is_Run__c){
        if(Trigger.isBefore) {  
            if(Trigger.isInsert) {
                if(CheckRecursion.checkCaseCommentRecursion(CONSTANTS.BEFORE_INSERT)){
                    instance.beforeInsertMethod(Trigger.New,Trigger.NewMap);
                }
            }
            if(Trigger.isUpdate){
                //instance.beforeUpdateMethod(Trigger.new,Trigger.old,Trigger.NewMap,Trigger.oldMap);
            }
        }

        if(Trigger.isAfter){
            if(Trigger.isInsert){
                //instance.afterInsertMethod(Trigger.new,Trigger.NewMap);
            }
            if(Trigger.isUpdate){                   
                //instance.afterUpdateMethod(Trigger.new,Trigger.old,Trigger.NewMap,Trigger.oldMap);
            }
        }
    }
}