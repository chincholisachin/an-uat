trigger EmailMessageTrigger on EmailMessage (before insert,before update,after insert,after update) {
    
    Bypass__c bypass = Bypass__c.getInstance();
    if (bypass.Trigger_Bypass__c) return;

    EmailMessageTriggerHandler instance = EmailMessageTriggerHandler.getInstance();
    
    if(System_Configs__c.getValues('EmailMessage').Is_Run__c){
        if(Trigger.isBefore) {  
            if(Trigger.isInsert) {
                if(CheckRecursion.checkEmailMessageRecursion(CONSTANTS.BEFORE_INSERT)){
                    instance.beforeInsertMethod(Trigger.New,Trigger.NewMap);
                }
            }
            if(Trigger.isUpdate){
                //instance.beforeUpdateMethod(Trigger.new,Trigger.old,Trigger.NewMap,Trigger.oldMap);
            }
        }

        if(Trigger.isAfter){
            if(Trigger.isInsert){
                if(CheckRecursion.checkEmailMessageRecursion(CONSTANTS.AFTER_INSERT)){
                    instance.afterInsertMethod(Trigger.new,Trigger.NewMap);
                }
            }
            if(Trigger.isUpdate){                   
                //instance.afterUpdateMethod(Trigger.new,Trigger.old,Trigger.NewMap,Trigger.oldMap);
            }
        }
    }
}