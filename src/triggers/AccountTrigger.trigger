trigger AccountTrigger on Account (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    Bypass__c bypass = Bypass__c.getInstance();
    if (bypass.Trigger_Bypass__c) return; 
    
    if (Trigger.isBefore){
        
        if (Trigger.isInsert){
            
            // when a new member account is created, if the account record type is a member type, it inherits all key fields from its account.
            AccountTriggerHandler.inheritFieldsFromParentAccount(Trigger.New);
            AccountTriggerHandler.setupBillingInfo(Trigger.new, null);

        }
        
        if (Trigger.isUpdate){
        
            AccountTriggerHandler.dontOverRideNullFieldsDuringConversion(Trigger.new, Trigger.oldMap);
            AccountTriggerHandler.setupBillingInfo(Trigger.new, Trigger.oldMap);           
        }
    
    }
    
    if (Trigger.isAfter){
    
        if (Trigger.isUpdate) {
            // when account owner changes on account record, moves the owner information to account team, while deleting the previous entry.
            AccountTriggerHandler.copyAccountOwnerToTeam(Trigger.newMap, Trigger.OldMap); 
            // when account's customer portal access is cut, it queries all members and cut their access
            AccountTriggerHandler.deactivateCustomerPortal(Trigger.new, Trigger.oldMap);
            // when account's owner or audit tier changes, inserts new integration request record.
            AccountTriggerHandler.createIntegrationRequest(Trigger.new, Trigger.oldMap);
        }
        if (Trigger.isInsert){
            // when account owner changes on account record, moves the owner information to account team.
            AccountTriggerHandler.copyAccountOwnerToTeam(Trigger.newMap, Trigger.OldMap);
            // when a new member is created it inherits all account team members listed in custom setting 'Team Members to Copy from Account'
            AccountTriggerHandler.createTeamMembersWhenMemberIsCreated(Trigger.newMap);
            // when a new customer account is created it propagates entire account team
            AccountTriggerHandler.propagateAccountTeam(Trigger.newMap);
            AccountTriggerHandler.createAssetsWhenMemberIsCreated(Trigger.new);
            //AppNexusMemberServices.insertInvoiceContact(Trigger.new);
            
        }
    
    }

}