trigger OpportunityTrigger on Opportunity (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {

    Bypass__c bypass = Bypass__c.getInstance();
    if (bypass.Trigger_Bypass__c) return; 
    
    if (Trigger.isBefore){
           if(Trigger.isInsert) OpportunityTriggerHandler.associateWithPriceBook(Trigger.new);
           if(Trigger.isInsert || Trigger.isUpdate) OpportunityTriggerHandler.validateProduct(Trigger.new);
    }
    
    if (Trigger.isAfter){
    
        if (Trigger.isInsert) {
            OpportunityTriggerHandler.postToChatter(Trigger.new,null);
            OpportunityTriggerHandler.createOpportunityTeam(Trigger.new);
        }
        if (Trigger.isUpdate) OpportunityTriggerHandler.postToChatter(Trigger.new,Trigger.oldMap);
    
    }

}