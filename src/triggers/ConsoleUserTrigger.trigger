trigger ConsoleUserTrigger on Console_User__c (before insert, before update, after insert, after update) {
    
    Bypass__c bypass = Bypass__c.getInstance();
    if (bypass.Trigger_Bypass__c) return; 
    
    If (Trigger.isBefore) ConsoleUserTriggerHandler.processRecords(Trigger.new, Trigger.oldMap);
    If (Trigger.isAfter) ConsoleUserTriggerHandler.createRecords();
}