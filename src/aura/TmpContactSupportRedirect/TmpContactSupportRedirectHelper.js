({
	getCSPUserType: function(component) {
        var action = component.get("c.getCSPUserType");
        var self = this;
        action.setCallback(this, function(a) {
           component.set("v.logDivFlg", a.getReturnValue());
           var caseId = (location+"").slice(-18);
           if(caseId != null && caseId != ''){
            if( a.getReturnValue() == 'Guest'){
            	window.location.href = '/help/s/login?startURL=/help/s/case/'+caseId;
            }
           }
        });
        $A.enqueueAction(action);
    },
})