({
	getAddCampMem: function(component) {
        var action = component.get("c.getAddCampMem");
        var self = this;
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.msg", a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
})