({
	getCSPAlert: function(component) {
        var action = component.get("c.getCSPAlert");
        var self = this;
        action.setCallback(this, function(a) {
            component.set("v.cspAlerts", a.getReturnValue());
        });
        $A.enqueueAction(action);
    },
})