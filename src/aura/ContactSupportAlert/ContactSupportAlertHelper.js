({
	getCSPCSAlert: function(component) {
        var action = component.get("c.getCSPCSAlert");
        var self = this;
        action.setCallback(this, function(a) {
            component.set("v.cspAlertmsg", a.getReturnValue());
        });
        $A.enqueueAction(action);
    },
})