({
	getCSPUserType: function(component) {
        var action = component.get("c.getCSPUserType");
        var self = this;
        action.setCallback(this, function(a) {
           var state = a.getState();
            if (component.isValid() && state === "SUCCESS"){
           component.set("v.cspUserTyp", a.getReturnValue());
           var caseId = location.search.split('caseid=')[1];
           if(caseId != null && caseId != ''){
            if( a.getReturnValue() == 'Guest'){
            	window.location.href = '/s/login?startURL=/s/case/'+caseId;
            }else{
            	var urlEvent = $A.get("e.force:navigateToURL");
            	urlEvent.setParams({
                 "url": "/case/"+caseId
                });
               urlEvent.fire();
               //window.location.href = '/help/s/case/'+caseId;            
            }
           }
        }
        });
        $A.enqueueAction(action);
    },
})