({
    fetchCommentsEmails : function(component, event, helper) {
        
        var action = component.get("c.getComments");
        action.setParams({
            "recordId":component.get("v.recordId")
        });
        
        action.setCallback(this,function(response) {
            
            var state  = response.getState();
            if(state === "SUCCESS") {
                component.set("v.CaseComments",response.getReturnValue());
                 var spinner = component.find("mySpinner");
                $A.util.removeClass(spinner, "slds-show");
                $A.util.addClass(spinner, "slds-hide");
            }
        });
        $A.enqueueAction(action);
        
    },
    
    insertComment : function(component,event,helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
        var action = component.get("c.creatComment");
        action.setParams({ 
            
            "CaseParentId" : component.get("v.recordId"),
            "caseComment" : component.get("v.caseComment")
            
        });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                helper.fetchCommentsEmails(component, event, helper);
                component.set("v.caseComment",'');
               
            }
        });
        $A.enqueueAction(action);
    },
    checkValidity : function(component,event,helper) {
        var isValid = true;
        var commentFiled = component.find('caseComment');
        if(!commentFiled.get("v.validity").valid) {
            isValid = false;
            commentFiled.showHelpMessageIfInvalid();
        }
        return isValid;
    },
     show: function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
    },
    hide:function (cmp) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
    },
})