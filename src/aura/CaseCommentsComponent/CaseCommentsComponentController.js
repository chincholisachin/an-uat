({
	getCommentsEmails : function(component, event, helper) {
	helper.fetchCommentsEmails(component, event, helper);
	},
    creatNewComment : function(component,event,helper) {
        if(helper.checkValidity(component,event,helper)) {
        	helper.insertComment(component,event,helper);
        }
    }
})