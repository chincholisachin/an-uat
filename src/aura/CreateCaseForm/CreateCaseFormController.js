({
    doInit : function(component, event, helper) {
        debugger;
        var caseInformation = {
            
            category:"",
            severity:"",
            memberId:"",
            invoiceNumber:"",
            description:"",
            subject:"",
            ccEmail:"",
            name:"",
            email:"",
            company:"",
            help:""
        };
        component.set("v.caseInfo",caseInformation);
        helper.getCategories(component, event, helper);
        helper.getDependentPicklistSeverityValues(component);
        helper.getDependentPicklistHelpValues(component);
        helper.getLoggedUserInfo(component,event,helper);
        //helper.getMemebers(component,event,helper);
    },
    clickCreate : function(component,event,helper){
        
        var currentCase = component.get("v.newCase");
        console.log('--ccEmails--'+JSON.stringify(currentCase));
        console.log(currentCase.CC_Emails__c);
        var emailList;
        if(currentCase.CC_Emails__c!=null){
            emailList = currentCase.CC_Emails__c.split(';');
        }
        console.log(emailList);
        
        var action = component.get("c.createNewCase");
        
        action.setParams({
            "newCase" : currentCase,
            "emailList" :emailList
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                component.set("v.newCase", response.getReturnValue());
            }
        });
        
        $A.enqueueAction(action); 
    },
    onChangeCategory : function(component,event,helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
        var selectedValue = component.find("Category").get("v.value");
        if(selectedValue === 'Client Finance') {
            component.set("v.InvoiceMemberdisableMode",false);
        } else {
            component.set("v.InvoiceMemberdisableMode",true);
        }
        helper.getDependentSeverityOnChange(component);
        helper.getDependentHelpOnChange(component);
    },
    onChangeSeverity : function(component,event,helper) {
        debugger;
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
        var selectedValue = component.find("Severity").get("v.value");
        if(selectedValue != '') {
             helper.getDependentHelpOnChange(component);

        }
        else {
              component.set("v.helpdisableMode",true);
               $A.util.removeClass(spinner, "slds-show");
               $A.util.addClass(spinner, "slds-hide");
        }
        
    },
    showToolTip : function(component,event,helper) {
        //alert('Hii');
    },
    CreateCase : function(component,event,helper){
        helper.insertCase(component,event,helper);
    },
    handleFilesChange: function(component, event, helper) {
        debugger;
        var fileName = 'No File Selected..';
        var choosenFileName = [];
        var finalFilestobeUploaded=[];
        if(component.get("v.finalfileToBeUploaded").length>0){
            var previousFiles = [];
            var previousFileNames = [];
            previousFiles = component.get("v.finalfileToBeUploaded");
            for(var j=0 ;j<previousFiles.length;j++) {
                finalFilestobeUploaded.push(previousFiles[j]);
                choosenFileName.push(previousFiles[j]['name']);
            }
        }
        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0][0]['name'];
            for (var i = 0; i < event.getSource().get("v.files")[0].length; i++) {
                choosenFileName.push(event.getSource().get("v.files")[0][i]['name']);
                finalFilestobeUploaded.push(event.getSource().get("v.files")[0][i]);
            }
        }
        component.set("v.selectedFiles", choosenFileName);
        component.set("v.finalfileToBeUploaded",finalFilestobeUploaded);
        console.log(component.get("v.selectedFiles"));
    },
    removeFile : function(component,event,helper) {
        debugger;
        var selectedRow = event.getSource().get("v.name");
        var selectfiles = component.get("v.selectedFiles");
        var attachments = component.get("v.fileToBeUploaded");
        var finalAttachments = component.get("v.finalfileToBeUploaded");
        var dupattachments = [];
        var selectArray =[];
        //pushing the data into arrat except selected file to remove
       /* for(var a=0; a < attachments[0].length; a++){
            if(a !== selectedRow){
                dupattachments[a]=attachments[0][a];
            }
        }*/
        for(var b = 0 ; b < finalAttachments.length;b++) {
            if(b!== selectedRow) {
            	dupattachments.push(finalAttachments[b]);
            }
        }
        //getting object format so converted to array format
        for(var x in selectfiles){
            selectArray.push(selectfiles[x]);
        }  
        selectArray.splice(selectedRow,1);
        component.set("v.selectedFiles",selectArray);
        //component.set("v.fileToBeUploaded",dupattachments);
        component.set("v.finalfileToBeUploaded",dupattachments);
        console.log(selectfiles);
        
        
    },
    getAllPills : function(component,event,helper) {
          var message = event.getParam("data");
          component.set("v.allPillsList", message);
          console.log(component.get("v.allPillsList")+'*****');
    }
    
})