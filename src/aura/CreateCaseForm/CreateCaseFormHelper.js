({ 
    getLoggedUserInfo : function(component,event,helper) {
        var myAction = component.get("c.isActiveUser"); 
        myAction.setCallback(this, function(response) { 
            if(response.getState() === "SUCCESS") { 
                component.set("v.loggedinUser", response.getReturnValue());
                helper.getMemebers(component,event,helper);
            }}); 
        
        $A.enqueueAction(myAction);
    },
    getMemebers: function(component,event,helper){
        debugger;
        var action = component.get("c.getMemberAccounts") ;
        var userInfo = component.get("v.loggedinUser");
        action.setParams({
            "recordId":userInfo.contactId
        });
        action.setCallback(this,function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                var members = [];
                members.push({value:'--None--',key:'--None--'})
                for ( var key in result ) {
                    members.push({value:result[key], key:key});
                }
                component.set("v.memberMap",members);
            }
        });
        $A.enqueueAction(action);
    },
    getCategories : function(component,event,helper) {
        
        var action = component.get("c.getPickValues");
        action.setParams({
            
            "field_name":"Category__c"
            
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state =="SUCCESS"){
                component.set("v.caseCategories",response.getReturnValue());
            }else{
                console.log("Failed with state"+JSON.stringify(state));
            }
        });
        $A.enqueueAction(action);
    },
    getDependentPicklistSeverityValues : function(component) {
        var sObject = component.get("v.object");
        var controllingfieldName = 'Category__c';
        var dependentfieldName = 'Severity__c';
        
        var action = component.get("c.getDependentOptions");
        action.setParams({
            pObjName : component.get("v.object"),
            pControllingFieldName : controllingfieldName,
            pDependentFieldName : dependentfieldName
        });
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                component.set("v.ControllingAndDependentValues", a.getReturnValue());
                console.log(JSON.stringify(component.get("v.ControllingAndDependentValues"))+'@Hello');
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });
        $A.enqueueAction(action);
    },
    getDependentPicklistHelpValues : function(component) {
        var sObject = component.get("v.object");
        var controllingfieldName = 'Category__c';
        var dependentfieldName = 'What_can_we_help_with__c';
        
        var action = component.get("c.getDependentOptions");
        action.setParams({
            pObjName : component.get("v.object"),
            pControllingFieldName : controllingfieldName,
            pDependentFieldName : dependentfieldName
        });
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                component.set("v.ControllingAndDependentHelpValues", a.getReturnValue());
                console.log(JSON.stringify(component.get("v.ControllingAndDependentHelpValues"))+'@Hello');
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });
        $A.enqueueAction(action);
    },
    getDependentSeverityOnChange : function(component,event,helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
        var caseCategoriesList = component.get("v.caseCategories"); 
        var mymap = component.get("v.ControllingAndDependentValues");
        var selectedValue = component.find("Category").get("v.value");
        var dependentPicklistValue = [];
        var abc = [];
        
        for (var i = 0; i < caseCategoriesList.length; i++) {
            if(selectedValue === caseCategoriesList[i]) {
                dependentPicklistValue.push(mymap[selectedValue]);
            }
        }
        if(dependentPicklistValue[0] != undefined && dependentPicklistValue != null) {
            for(var i=0;i<dependentPicklistValue.length;i++){
                for(var x=0;x<dependentPicklistValue[i].length;x++) {
                    abc.push(dependentPicklistValue[i][x]);
                }
            }
        }
        component.set("v.Severity",abc);
        if(abc.length>0){
            component.set("v.SeveritydisableMode",false);
        }else {
            component.set("v.SeveritydisableMode",true);
        }
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
        
    },
    getDependentHelpOnChange : function(component,event,helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
        var caseCategoriesList = component.get("v.caseCategories"); 
        var mymap = component.get("v.ControllingAndDependentHelpValues");
        var selectedValue = component.find("Category").get("v.value");
      
        var dependentPicklistValue = [];
        var abc = [];
        
        for (var i = 0; i < caseCategoriesList.length; i++) {
            if(selectedValue === caseCategoriesList[i]) {
                dependentPicklistValue.push(mymap[selectedValue]);
            }
        }
        if(dependentPicklistValue[0] != undefined && dependentPicklistValue != null) {
            for(var i=0;i<dependentPicklistValue.length;i++){
                for(var x=0;x<dependentPicklistValue[i].length;x++) {
                    abc.push(dependentPicklistValue[i][x]);
                }
            }
        }
        component.set("v.helpLine",abc);
        if(abc.length>0 ){
            component.set("v.helpdisableMode",false);
        }else {
            component.set("v.helpdisableMode",true);
        }
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
    },
    insertCase : function(component,event,helper) {    	
        debugger;

        var vlidityCheck = helper.checkValidity(component,event,helper);
        if(vlidityCheck) {
            helper.show(component,event);
            var invoice ='';
            var selectedValue = component.find("Category").get("v.value");
            if(selectedValue === 'Client Finance') {
                invoice = component.find('invoice').get('v.value');
            }
            var members = [];
            //if(selectedValue === 'Client Finance') {
                members= component.get('v.allPillsList');
            //}
            console.log(members+'==Members==');
            var caseDetails = component.get("v.caseInfo");
            //console.log(JSON.deserialize(caseDetails)+'==caseDetails==');
            var action = component.get("c.createCaseRecord"); 
            action.setParams({
                "caseInfo" :  JSON.stringify(caseDetails),
                "membersList" : members
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    //alert('case created successfully');
                    debugger;
                    var caseId = response.getReturnValue();
                    var userInfo = component.get("v.loggedinUser");
                    var files = component.get("v.finalfileToBeUploaded");
                    var attFiles = [];
     
                    if (files && files.length >0 && userInfo.userProfileName !=  $A.get("$Label.c.guestUserProfile")) {
                      /*  var i=0;
                        for (var key in files[0]) {
                            attFiles.push(files[0][i]);
                            i++;
                        }
                        attFiles = attFiles.filter(function( element ) {
                            return element !== undefined;
                        });*/
                        if (files && files.length > 0) {
                            for (var i = 0; i < files.length; i++) {
                                helper.onFileUploaded(component,event,helper,files[i],caseId);
                            }         
                            
                        }
                        else {
                             helper.hide(component);
                             helper.redirection(component,caseId);
                            /* var urlEvent = $A.get("e.force:navigateToURL");
                             urlEvent.setParams({
                                "url": window.location.origin+'/s/case/'+ caseId
                             });
                             urlEvent.fire();*/
                        }
                    } else {
                        debugger;
                        helper.hide(component);
                        
                        var urlEvent = $A.get("e.force:navigateToURL");
                        if(userInfo.userProfileName != $A.get("$Label.c.guestUserProfile")){
                            helper.redirection(component,caseId);
                           /* urlEvent.setParams({
                                "url": window.location.origin+'/s/case/'+ caseId
                            });*/ 
                        }else{
                           // helper.redirection(component,'thank-you');
                            urlEvent.setParams({
                                "url": window.location.origin+'/s/thank-you'
                            });
                        }
                        
                        urlEvent.fire();
                    }
                }  
            });
            $A.enqueueAction(action);
        }
    },
    checkValidity : function(component,event,helper) {
        var isValid = true;
        var logedUserInfo = component.get("v.loggedinUser");
        if(logedUserInfo.userProfileName === $A.get("$Label.c.guestUserProfile")) {
            var nameField = component.find('name');
            if(!nameField.get("v.validity").valid) {
                isValid = false;
                nameField.showHelpMessageIfInvalid();
            }
            var emailField = component.find('email');
            if(!emailField.get("v.validity").valid) {
                isValid = false;
                emailField.showHelpMessageIfInvalid();
            }
            var companyField = component.find('company');
            if(!companyField.get("v.validity").valid) {
                isValid = false;
                companyField.showHelpMessageIfInvalid();
            }
        }
        var CategoryField = component.find('Category');
        if(!CategoryField.get("v.validity").valid) {
            isValid = false;
            CategoryField.showHelpMessageIfInvalid();
        }
        var severityField = component.find('Severity');
        if(severityField != undefined){
            if(!severityField.get("v.validity").valid) {
                isValid = false;
                severityField.showHelpMessageIfInvalid();
            }
        }
        var helplineField = component.find('helpline');
        if(helplineField != undefined) {
            if(!helplineField.get("v.validity").valid) {
                isValid = false;
                helplineField.showHelpMessageIfInvalid();
            }
        }
        var subjectField = component.find('subject');
        if(!subjectField.get("v.validity").valid) {
            isValid = false;
            subjectField.showHelpMessageIfInvalid();
        }
        var descriptionField = component.find('description');
        if(!descriptionField.get("v.validity").valid) {
            isValid = false;
            descriptionField.showHelpMessageIfInvalid();
        }
        var selectedValue = component.find("Category").get("v.value");
        if(selectedValue === 'Client Finance') {
            var memberField = component.find('member');
            /*if(!memberField.get("v.validity").valid) {
                isValid = false;
                memberField.showHelpMessageIfInvalid();
            }
            var memberField = component.get("v.allPillsList");
            if(memberFiled == '' || memberField.length == 0) {
                isValid = false;
            }*/
            var invoiceField = component.find('invoice');
            
            if(!invoiceField.get("v.validity").valid) {
                isValid = false;
                invoiceField.showHelpMessageIfInvalid();
            }
        }
        return isValid;
    },
    onFileUploaded:function(component,event,helper,file,parentId){
        
        var reader = new FileReader();
        reader.onloadend = function() {
            var dataURL = reader.result;
            var content = dataURL.match(/,(.*)$/)[1];
            helper.upload(component, file,content, parentId,helper);
        }
        reader.readAsDataURL(file);
        
        
    },
    upload: function(component,file,base64Data,parentId,helper) {
        var action = component.get("c.uploadFile");
        console.log('type: ' + file.type);
        action.setParams({
            fileName: file.name,
            base64Data: base64Data,
            contentType: file.type,
            ParentId :parentId
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                helper.hide(component);
                helper.redirection(component,parentId);
               /* var urlEvent = $A.get("e.force:navigateToURL");
               urlEvent.setParams({
                  "url": window.location.origin+'/s/case/'+ parentId
                });
               urlEvent.fire();*/
            }
        });
        $A.enqueueAction(action);
    },
    show: function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
    },
    hide:function (cmp) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
    },
    redirection: function(component,recordId) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": window.location.origin+'/s/case/'+ recordId
        });
        urlEvent.fire();
    }
})